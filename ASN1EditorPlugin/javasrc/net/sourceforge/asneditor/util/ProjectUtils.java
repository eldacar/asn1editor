/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.sourceforge.asneditor.ASNEditorPlugin;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Utils for the ASN.1 project
 */
public class ProjectUtils {
    private static final String PATH = "path";
    private static final String KIND = "kind";
    private static final String TAG_IMPORT_ENTRY = "importentry";
    private static final String BAD_FILE_FORMAT = "Bad file format";
    private static final String TAG_IMPORT = "import";
    private static final String IMPORTS_FILE = ".asn_import";

    private static final int IPE_SOURCE  = 1000;
    private static final int IPE_PROJECT = 1001;

    /**
     * Value of the project's raw classpath if the .classpath file contains invalid entries.
     */
    public static final List<IResource> INVALID_CLASSPATH = new LinkedList<IResource>();

    /**
     * Saves the classpath in a shareable format (VCM-wise) only when necessary, that is, if  it is semantically different
     * from the existing one in file. Will never write an identical one.
     *
     * @param newClasspath IClasspathEntry[]
     * @param newOutputLocation IPath
     * @return boolean Return whether the .classpath file was modified.
     * @throws JavaModelException
     */
    public static boolean saveImportPath(IProject project, List<IResource> newImportPath) {
        if (!project.isAccessible()) return false;

        List<IResource> oldImportPath = readImportPath(project, false /*don't create markers*/, false/*don't log problems*/);
        if (oldImportPath != null && isImportPathEqualsTo(project, newImportPath, oldImportPath)) {
            // no need to save it, it is the same
            return false;
        }

        try {
            // actual file saving
            setSharedProperty(project, IMPORTS_FILE, encodeImportPathList(project, newImportPath, true));
        } catch (CoreException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
        }
        return true;
    }

    /**
     * Compare current classpath with given one to see if any different.
     * Note that the argument classpath contains its binary output.
     * @param newClasspath IClasspathEntry[]
     * @param newOutputLocation IPath
     * @param otherClasspathWithOutput IClasspathEntry[]
     * @return boolean
     */
    public static boolean isImportPathEqualsTo(IProject project, List<IResource> newImportPath, List<IResource> otherImportPathWithOutput) {

        if (otherImportPathWithOutput != null && otherImportPathWithOutput.size() > 0) {

            int length = otherImportPathWithOutput.size();
            if (length == newImportPath.size()) {
                // compare classpath entries
                for (int i = 0; i < length - 1; i++) {
                    if (!otherImportPathWithOutput.get(i).equals(newImportPath.get(i)))
                        return false;
                }
            }
        }
        return false;
    }

    /**
     * Returns the XML String encoding of the class path.
     */
    protected static String encodeImportPathList(IProject project, List<IResource> importPath, boolean indent) throws IOException {
        ByteArrayOutputStream s = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(s, "UTF8"); //$NON-NLS-1$
        XMLWriter xmlWriter = new XMLWriter(writer);

        xmlWriter.startTag(TAG_IMPORT, indent);
        for (IResource res : importPath) {
          elementEncodeImportPathEntry(res, xmlWriter, project.getFullPath(), indent, true);
        }

        xmlWriter.endTag(TAG_IMPORT, indent);
        writer.flush();
        writer.close();
        return s.toString("UTF8");//$NON-NLS-1$
    }
    /**
     * Returns the XML encoding of the class path.
     * @param resource
     */
    public static void elementEncodeImportPathEntry(IResource resource, XMLWriter writer, IPath projectPath, boolean indent, boolean newLine) {
        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put(KIND, kindToString(IPE_SOURCE));

        IPath xmlPath = resource.getFullPath();
        parameters.put(PATH, String.valueOf(xmlPath));

        writer.printTag(TAG_IMPORT_ENTRY, parameters, indent, newLine, true);
    }

    /**
     * Reads the .classpath file from disk and returns the list of entries it contains (including output location entry)
     * Returns null if .classfile is not present.
     * Returns INVALID_CLASSPATH if it has a format problem.
     */
    public static List<IResource> readImportPath(IProject project, boolean createMarker, boolean logProblems) {

        try {
            String xmlImportPath = getSharedProperty(project, IMPORTS_FILE);
            if (xmlImportPath == null) {
//                if (createMarker && project.isAccessible()) {
//                        this.createClasspathProblemMarker(new JavaModelStatus(
//                            IJavaModelStatusConstants.INVALID_CLASSPATH_FILE_FORMAT,
//                            Util.bind("classpath.cannotReadClasspathFile", this.getElementName()))); //$NON-NLS-1$
//                }
                return null;
            }
            return decodeImportPathList(xmlImportPath, createMarker, logProblems);
        } catch(Exception e) {
            // file does not exist (or not accessible)
//            if (createMarker && project.isAccessible()) {
//                    this.createClasspathProblemMarker(new JavaModelStatus(
//                        IJavaModelStatusConstants.INVALID_CLASSPATH_FILE_FORMAT,
//                        Util.bind("classpath.cannotReadClasspathFile", this.getElementName()))); //$NON-NLS-1$
//            }
            if (logProblems) {
                ASNEditorPlugin.log("Exception while retrieving "+ project.getLocation() //$NON-NLS-1$
                    +"/."+IMPORTS_FILE+", will revert to default classpath", e); //$NON-NLS-1$
            }
        }
        return null;
    }

    /**
     * Reads and decode an XML classpath string
     * @param xmlImportPath
     * @param createMarker
     * @param logProblems
     * @return
     */
    private static List<IResource> decodeImportPathList(String xmlImportPath, boolean createMarker, boolean logProblems) {
        ArrayList<IResource> paths = new ArrayList<IResource>();
        try {
            if (xmlImportPath == null) return null;
            StringReader reader = new StringReader(xmlImportPath);
            Element importElement;

            try {
                DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                importElement = parser.parse(new InputSource(reader)).getDocumentElement();
            } catch (SAXException e) {
                throw new IOException(BAD_FILE_FORMAT);
            } catch (ParserConfigurationException e) {
                throw new IOException(BAD_FILE_FORMAT);
            } finally {
                reader.close();
            }

            if (!importElement.getNodeName().equalsIgnoreCase(TAG_IMPORT)) {
                throw new IOException(BAD_FILE_FORMAT);
            }
            NodeList list = importElement.getElementsByTagName(TAG_IMPORT_ENTRY);
            int length = list.getLength();

            for (int i = 0; i < length; ++i) {
                Node node = list.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    IResource entry = elementDecodeImportEntry((Element)node);
                    paths.add(entry);
                }
            }
        } catch (IOException e) {
            // bad format
//            if (createMarker && this.project.isAccessible()) {
//                    this.createClasspathProblemMarker(new JavaModelStatus(
//                            IJavaModelStatusConstants.INVALID_CLASSPATH_FILE_FORMAT,
//                            Util.bind("classpath.xmlFormatError", this.getElementName(), e.getMessage()))); //$NON-NLS-1$
//            }
//            if (logProblems) {
//                ASNEditorPlugin.log(
//                    "Exception while retrieving "+ project.getPath() //$NON-NLS-1$
//                    +"/.classpath, will mark classpath as invalid", e); //$NON-NLS-1$
//            }
            return INVALID_CLASSPATH;
        } catch (Exception e) {
            // failed creating CP entries from file
//            if (createMarker && project.isAccessible()) {
//                this.createClasspathProblemMarker(new JavaModelStatus(
//                        IJavaModelStatusConstants.INVALID_CLASSPATH_FILE_FORMAT,
//                        Util.bind("classpath.illegalEntryInClasspathFile", this.getElementName(), e.getMessage()))); //$NON-NLS-1$
//            }
            if (logProblems) {
                ASNEditorPlugin.log(
                    "Exception while retrieving "//$NON-NLS-1$
                    +"/.classpath, will mark classpath as invalid", e); //$NON-NLS-1$
            }
            return INVALID_CLASSPATH;
        }
        return paths;
    }

    /**
     * @param element
     * @return
     */
    private static IResource elementDecodeImportEntry(Element element) {
        String kindAttr = element.getAttribute(KIND); //$NON-NLS-1$
        String pathAttr = element.getAttribute(PATH); //$NON-NLS-1$

        final int kind = kindFromString(kindAttr);

        switch (kind) {
        case IPE_PROJECT: {
            final IProject resource = ASNEditorPlugin.getWorkspace().getRoot().getProject(pathAttr);
            return resource;
        }
        case IPE_SOURCE: {
            IResource resource = ASNEditorPlugin.getWorkspace().getRoot().findMember(pathAttr);
            return resource;
        }
        default:
            return null;
        }
    }

    /**
     * Returns the kind of a <code>PackageFragmentRoot</code> from its <code>String</code> form.
     */
    static int kindFromString(String kindStr) {

        if (kindStr.equalsIgnoreCase("prj")) //$NON-NLS-1$
            return IPE_PROJECT;
//        if (kindStr.equalsIgnoreCase("var")) //$NON-NLS-1$
//            return IPE_VARIABLE;
//        if (kindStr.equalsIgnoreCase("con")) //$NON-NLS-1$
//            return IPE_CONTAINER;
        if (kindStr.equalsIgnoreCase("src")) //$NON-NLS-1$
            return IPE_SOURCE;
//        if (kindStr.equalsIgnoreCase("lib")) //$NON-NLS-1$
//            return IPE_LIBRARY;
        return -1;
    }

    /**
     * @param kind
     * @return the kind of a <code>PackageFragmentRoot</code> from its <code>int</code> form.
     */
    private static String kindToString(int kind) {
        switch (kind) {
        case IPE_PROJECT:
            return "prj";
        case IPE_SOURCE:
            return "src";
        default:
            return null;
        }
    }

    /**
     * Retrieve a shared property on a project. If the property is not defined, answers null.
     * Note that it is orthogonal to IResource persistent properties, and client code has to decide
     * which form of storage to use appropriately. Shared properties produce real resource files which
     * can be shared through a VCM onto a server. Persistent properties are not shareable.
     *
     * @param fileName String
     * @see JavaProject#setSharedProperty(String, String)
     * @return String
     * @throws CoreException
     * @throws IOException
     */
    public static String getSharedProperty(IProject project, String fileName) throws IOException, CoreException {
        String property = null;
        IFile rscFile = project.getFile(fileName);
        if (rscFile.exists()) {
            byte[] bytes = ResourceUtils.getResourceContentsAsByteArray(rscFile);
            try {
                property = new String(bytes, "UTF-8"); //$NON-NLS-1$ // .asn_import always encoded with UTF-8
            } catch (UnsupportedEncodingException e) {
                ASNEditorPlugin.log("Could not read .classpath with UTF-8 encoding", e); //$NON-NLS-1$
                // fallback to default
                property = new String(bytes);
            }
        }
        return property;
    }

    /**
     * @param container the root from which the recursive search will start
     * @return list of asn.1 file resources
     */
    public static List<IResource> getAsnFiles(IContainer container) {
        List<IResource> list = new LinkedList<IResource>();

        final Object[] children = ResourceUtils.getChildren(container);

        for (int i = 0; i < children.length; i++) {
            final IResource res = (IResource) children[i];
            if (res instanceof IFile) {
                final String fileName = res.getName().toLowerCase();
                // TODO get from properties
            if (fileName.endsWith(".asn") | fileName.endsWith(".asn1"))
                list.add(res);
            } else if (res instanceof IContainer) {
                list.addAll(getAsnFiles((IContainer) res));
            }
        }
        return list;
    }
    /**
     * Record a shared persistent property onto a project.
     * Note that it is orthogonal to IResource persistent properties, and client code has to decide
     * which form of storage to use appropriately. Shared properties produce real resource files which
     * can be shared through a VCM onto a server. Persistent properties are not shareable.
     *
     * shared properties end up in resource files, and thus cannot be modified during
     * delta notifications (a CoreException would then be thrown).
     *
     * @param key String
     * @param value String
     * @see JavaProject#getSharedProperty(String key)
     * @throws CoreException
     */
    public static void setSharedProperty(IProject project, String key, String value) throws CoreException {
        IFile rscFile = project.getFile(key);
        byte[] bytes = null;
        try {
            bytes = value.getBytes("UTF-8"); //$NON-NLS-1$ // .classpath always encoded with UTF-8
        } catch (UnsupportedEncodingException e) {
            ASNEditorPlugin.log("Could not write .classpath with UTF-8 encoding ", e); //$NON-NLS-1$
            // fallback to default
            bytes = value.getBytes();
        }
        InputStream inputStream = new ByteArrayInputStream(bytes);
        // update the resource content
        if (rscFile.exists()) {
            if (rscFile.isReadOnly()) {
                // provide opportunity to checkout read-only .classpath file (23984)
                ResourcesPlugin.getWorkspace().validateEdit(new IFile[]{rscFile}, null);
            }
            rscFile.setContents(inputStream, IResource.FORCE, null);
        } else {
            rscFile.create(inputStream, IResource.FORCE, null);
        }
    }

    public static IProject getProject(String projectName) {
      return ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
    }

}
