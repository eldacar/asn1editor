/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.model.IWorkbenchAdapter;

/**
 * Utils for Resources
 */
public class ResourceUtils {
  private static final int DEFAULT_READING_SIZE = 8192;

  /**
	 * Returns the given file's contents as a byte array.
	 * @throws CoreException
	 */
	public static byte[] getResourceContentsAsByteArray(IFile file)
	throws IOException, CoreException {
		InputStream stream = new BufferedInputStream(file.getContents(true));
		try {
			return getInputStreamAsByteArray(stream, -1);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				// ignore
			}
		}
	}

	/**
	 * Returns the given input stream's contents as a byte array.
	 * If a length is specified (ie. if length != -1), only length bytes
	 * are returned. Otherwise all bytes in the stream are returned.
	 * Note this doesn't close the stream.
	 * @throws IOException if a problem occured reading the stream.
	 */
	public static byte[] getInputStreamAsByteArray(InputStream stream, int length) throws IOException {
		byte[] contents;
		if (length == -1) {
			contents = new byte[0];
			int contentsLength = 0;
			int amountRead = -1;
			do {
				int amountRequested = Math.max(stream.available(), DEFAULT_READING_SIZE);  // read at least 8K

				// resize contents if needed
				if (contentsLength + amountRequested > contents.length) {
					System.arraycopy(
							contents,
							0,
							contents = new byte[contentsLength + amountRequested],
							0,
							contentsLength);
				}

				// read as many bytes as possible
				amountRead = stream.read(contents, contentsLength, amountRequested);

				if (amountRead > 0) {
					// remember length of contents
					contentsLength += amountRead;
				}
			} while (amountRead != -1);

			// resize contents if necessary
			if (contentsLength < contents.length) {
				System.arraycopy(
						contents,
						0,
						contents = new byte[contentsLength],
						0,
						contentsLength);
			}
		} else {
			contents = new byte[length];
			int len = 0;
			int readSize = 0;
			while ((readSize != -1) && (len != length)) {
				// See PR 1FMS89U
				// We record first the read size. In this case len is the actual read size.
				len += readSize;
				readSize = stream.read(contents, len, length - len);
			}
		}

		return contents;
	}

  /**
   * Returns the implementation of IWorkbenchAdapter for the given
   * object.  Returns null if the adapter is not defined or the
   * object is not adaptable.
   * <p>
   * </p>
   *
   * @param element the element
   * @return the corresponding workbench adapter object
   */
    private static IWorkbenchAdapter getAdapter(Object element) {
    if (!(element instanceof IAdaptable)) {
        return null;
    }
    return (IWorkbenchAdapter)((IAdaptable) element).getAdapter(IWorkbenchAdapter.class);
  }

  /* (non-Javadoc)
   * Method declared on ITreeContentProvider.
   */
  public static Object[] getChildren(Object element) {
    IWorkbenchAdapter adapter = getAdapter(element);
    if (adapter != null) {
        return adapter.getChildren(element);
    }
    return new Object[0];
  }

  /**
   * @param location
   * @return <b>true</b> if location represents a path inside the workspace
   */
  public boolean isInWorkspace(String location) {
    IPath projectPath= Path.fromOSString(location);
    return Platform.getLocation().isPrefixOf(projectPath);
  }

}
