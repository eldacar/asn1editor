/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * Presents a class which contains an ArrayList and a Hashtable
 * to handle both needs (order and hashing).
 */
public class HashableList {
	private final ArrayList list;
	private final Hashtable hashtable;

	public HashableList() {
		list = new ArrayList();
		hashtable = new Hashtable();
	}

	/**
	 * @return an Enumeration (elements are in the same order as they were inserted)
	 */
	public Enumeration enumeration() {
		return new Enumeration() {
			private Iterator it = list.iterator();
			public boolean hasMoreElements() {
				return it.hasNext();
			}
			public Object nextElement() {
				return it.next();
			}};
	}

	/**
	 * @return an Enumeration (elements are in the same order as they were inserted)
	 */
	public Iterator iterator() {
		return list.iterator();
	}

	/**
	 * @param key
	 * @return
	 */
	public Object get(Object key) {
		return hashtable.get(key);
	}

	/**
	 * @param key
	 * @param value
	 * @return
	 */
	public Object put(Object key, Object value) {
		final Object res = hashtable.put(key, value);
		if (res != null) {
			list.remove(res);
		}
		list.add(value);
		return res;
	}

	/**
	 * @param value
	 * This value will not be present in the hash due the lack of a key
	 */
	public void add(Object value) {
		list.add(value);
	}
}
