/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.actions;

import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.source.projection.ProjectionViewer;

public class ToggleFoldingAction extends AbstractDispatchAction {

    protected AbstractDispatchAction createAction() {
        return new ToggleFoldingAction();
    }

    public void run(ITextSelection textSelection) {
        ProjectionViewer viewer= (ProjectionViewer) editor.getViewer();
        viewer.doOperation(ProjectionViewer.TOGGLE);
        if (viewer.isProjectionMode()) {
            editor.updateFolding();
        }
    }
}
