/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.actions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import net.sourceforge.asneditor.model.ASNModule;
import net.sourceforge.asneditor.model.manager.ASNModelUtils;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;


public class SplitModules extends AbstractDispatchAction {

  /* (non-Javadoc)
   * @see org.eclipse.jface.action.Action#run()
   */
  public void run() {
  	SplitModules action = new SplitModules();
  	action.setEditor(editor);
  	action.run((ITextSelection) null);
  }

  public void run(ITextSelection selection) {
    final boolean create = MessageDialog.openQuestion(null, "Split modules",
        "Each module in this ASN.1 file will be written to a file.\n" +
        "If the file exists it will be overwritten!\n\nAre you sure ?");
    if (create) {
      splitModulesIntoFiles();
    }
	}

  public void splitModulesIntoFiles() {
    final IResource res = editor.getEditorInputResource();
    final IContainer parent = res.getParent();

    final IDocument doc = editor.getViewer().getDocument();
    Collection<ASNModule> modules = ASNModelUtils.getModulesForResource(res);

    if (modules.isEmpty()) {
        MessageDialog.openError(null, "Split modules", "No modules found!");
    } else if (modules.size() == 1) {
        MessageDialog.openError(null, "Split modules", "Only one module found!");
    } else {
      for (ASNModule module : modules) {
        final String name = module.getName();
        try {
          final int startPos = doc.getLineOffset(module.getStartModuleLine()) + module.getStartModuleColumn();
          final int endPos = doc.getLineOffset(module.getEndLine()) + module.getEndColumn();
          final String moduleText = doc.get(startPos, endPos-startPos);
          createModule(parent, name, moduleText);
        } catch (BadLocationException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
  }

  private void createModule(IContainer parent, final String moduleName, final String text) {
    final IFile file = parent.getFile(new Path(moduleName+".asn"));
    try {
      final StringBuffer contents = new StringBuffer();
      contents.append(moduleName);
      contents.append(System.getProperty("line.separator")); //$NON-NLS-1$
      contents.append(text);
      InputStream stream =  new ByteArrayInputStream(contents.toString().getBytes());
      if (file.exists()) {
        file.setContents(stream, true, true, null);
      } else {
        file.create(stream, true, null);
      }
      stream.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /* (non-Javadoc)
   * @see net.sourceforge.asneditor.actions.AbstractDispatchAction#createAction()
   */
  protected AbstractDispatchAction createAction() {
    return new SplitModules();
  }
}
