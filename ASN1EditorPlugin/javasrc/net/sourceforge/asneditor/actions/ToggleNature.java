/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.actions;

import java.util.Iterator;

import net.sourceforge.asneditor.ASNNature;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.IStructuredSelection;

public class ToggleNature extends AbstractDispatchAction {

  /* (non-Javadoc)
   * @see net.sourceforge.asneditor.actions.AbstractDispatchAction#run(org.eclipse.jface.text.ITextSelection)
   */
  public void run(ITextSelection selection) { /* empty */ }

  public void run(IAction action) {
  	if (! (selection instanceof IStructuredSelection))
      return;
    IStructuredSelection structured = (IStructuredSelection) selection;
    Iterator it = structured.iterator();
    while (it.hasNext()) {
      final Object item = it.next();
      if (item instanceof IAdaptable) {
	      IAdaptable adaptable = (IAdaptable) item;
	      IProject project = (IProject) adaptable.getAdapter(IProject.class);
	      if (project != null) {
  	    	ASNNature.toggleAsnNature(project);
	    	}
      }
    }
  }

/* (non-Javadoc)
   * @see net.sourceforge.asneditor.actions.AbstractDispatchAction#createAction()
   */
  protected AbstractDispatchAction createAction() {
		return new ToggleNature();
	}
}
