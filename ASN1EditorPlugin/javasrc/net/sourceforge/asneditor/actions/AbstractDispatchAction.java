/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.actions;

import net.sourceforge.asneditor.editors.ASNEditor;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

public abstract class AbstractDispatchAction extends Action
  implements IWorkbenchWindowActionDelegate, IEditorActionDelegate, IObjectActionDelegate {

  protected ASNEditor editor = null;
  protected AbstractDispatchAction action = null;
  protected IWorkbenchSite site = null;
  protected IWorkbenchWindow window = null;
  protected ISelection selection = null;

  // abstract methods

  /**
   * @param site
   * @param editor
   * @return
   */
  protected abstract AbstractDispatchAction createAction();

  public abstract void run(ITextSelection selection);
  public void run() {
    if (selection instanceof ITextSelection) {
      run((ITextSelection)selection);
    } else {
      run((ITextSelection)null);
    }
  }

  // for interface IWorkbenchWindowActionDelegate

  /* (non-Javadoc)
   * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#dispose()
   */
  public void dispose() {
    window = null;
  }
  /* (non-Javadoc)
   * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
   */
  public void init(IWorkbenchWindow window) {
    this.window = window;
  }

  public void run(IAction action2) {
    ASNEditor editor = null;
    IWorkbenchSite site = null;
    if (window == null) {
      startMyAction(this.editor);
      return;
    }
    IWorkbenchPage page = window.getActivePage();
    if (page == null) {
      startMyAction(this.editor);
      return;
    }
    IEditorPart part = page.getActiveEditor();
    if (part instanceof ASNEditor) {
      editor = (ASNEditor) part;
      site = editor.getSite();
    }
    else
      editor = null;
    if (editor == null) { 
      startMyAction(this.editor);
      return;
    }
    if(site != null){
      startMyAction(editor);
    }
  }

  // from interface IEditorActionDelegate

  private void startMyAction(ASNEditor editor) {
//  if (editor == null) {
//  return;
//  }
    if(action == null) {
      action = createAction();
    }
    action.editor = editor;
    action.selection = this.selection;
    action.run();
  }

  /* (non-Javadoc)
   * @see org.eclipse.ui.IEditorActionDelegate#setActiveEditor(org.eclipse.jface.action.IAction, org.eclipse.ui.IEditorPart)
   */
  public void setActiveEditor(IAction action, IEditorPart targetEditor){
    if(targetEditor instanceof ASNEditor){
      editor = (ASNEditor)targetEditor;
      site = editor.getSite();
    }
  }

  /* (non-Javadoc)
   * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction, org.eclipse.ui.IWorkbenchPart)
   */
  public void setActivePart(IAction action, IWorkbenchPart targetPart) {
    if (targetPart != null)
      site = targetPart.getSite();
  }

  /**
   * Activate action  (if we are getting text)
   */
  public void selectionChanged(IAction action, ISelection selection) {
    this.selection = selection;
//  if (selection instanceof TextSelection) {
//  action.setEnabled(true);
//  return;
//  }
//  action.setEnabled(editor != null);
  }

  /**
   * @param editor
   */
  public void setEditor(ASNEditor editor) {
    this.editor = editor;
  }
}
