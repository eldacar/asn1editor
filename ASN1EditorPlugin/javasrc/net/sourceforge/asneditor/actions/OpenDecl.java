/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.actions;

import java.util.Collection;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.editors.ASNEditor;
import net.sourceforge.asneditor.editors.HyperlinkMouseListener;
import net.sourceforge.asneditor.model.IASN;
import net.sourceforge.asneditor.model.manager.ASNModelUtils;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.AbstractTextEditor;


public class OpenDecl extends AbstractDispatchAction {

  public void run(ITextSelection sele) {
    final IRegion currentTextRegion = HyperlinkMouseListener.getCurrentCaretTextRegion(editor.getViewer(), editor);
    // model does not contains declaration (selection)
    if (currentTextRegion == null)
      return;
    ITextSelection openSel = new TextSelection(currentTextRegion.getOffset(), currentTextRegion.getLength());
    openDeclaration(openSel);
	}


  /**
   * Open declaration of given selection
   * @param selection
   */
  public void openDeclaration(ITextSelection selection){
    if (selection == null) {
      return;
    }
    try {
      IDocument doc = editor.getViewer().getDocument();
      final String str = doc.get(selection.getOffset(), selection.getLength());
      IResource resource = ASNEditorPlugin.findResource(editor.getEditorInput());
      // find location
      final Collection<IASN> asnDecls = ASNModelUtils.findDeclarationsInFile(resource, str);
      if (asnDecls.isEmpty())
        return;

      IASN asnDecl = asnDecls.iterator().next();
      IResource sourceFile = ASNModelUtils.getSourceFile(asnDecl);
      if (sourceFile == null) {
        ASNEditorPlugin.log("no source for "+asnDecl, new Exception());
      }

      IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
      IEditorPart _editor = IDE.openEditor(page, (IFile) sourceFile, true);
      if (!(_editor instanceof AbstractTextEditor)) {
        return;
      }
      AbstractTextEditor textEditor = (AbstractTextEditor) _editor;

      SourceViewer viewer = null;
      if (_editor instanceof ASNEditor) {
        viewer = ((ASNEditor)_editor).getViewer();
        textEditor.selectAndReveal(asnDecl.getOffset(viewer), 0);
      }
    } catch (BadLocationException e) {
      // TODO: handle exception
    } catch (PartInitException e) {
    } catch (Throwable e) {
      // TODO: handle exception
    }
  }

  /* (non-Javadoc)
   * @see net.sourceforge.asneditor.actions.AbstractDispatchAction#createAction()
   */
  protected AbstractDispatchAction createAction() {
    return new OpenDecl();
  }
}
