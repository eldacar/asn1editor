/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.actions;

import net.sourceforge.asneditor.actions.helpers.CommentHelper;
import net.sourceforge.asneditor.actions.helpers.Selection;

import org.eclipse.jface.text.ITextSelection;

/**
 * Uncomments all selected lines
 */
public class Uncomment extends AbstractDispatchAction {

  /* (non-Javadoc)
   * @see net.sourceforge.asneditor.actions.AbstractDispatchAction#run(org.eclipse.jface.text.ITextSelection)
   */
  public void run(ITextSelection selection) {
    try {
      // Select from text editor
      Selection ps = new Selection(editor, false);
      // Perform the action
      CommentHelper.uncomment(ps);
    } catch (Exception e) {
      //beep ( e );
    }
  }

  /* (non-Javadoc)
   * @see net.sourceforge.asneditor.actions.AbstractDispatchAction#createAction()
   */
  protected AbstractDispatchAction createAction() {
    return new Uncomment();
  }
}
