/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 * Inspired from
 * org.python.pydev
 * Original License: Common Public License v1.0
 */
package net.sourceforge.asneditor.actions.helpers;

import net.sourceforge.asneditor.editors.IASNConstants;

public class CommentHelper {

  /**
   * Performs the action with a given Selection
   * 
   * @param ps Given Selection
   * @return boolean The success or failure of the action
   */
  public static boolean comment(Selection ps) {
    // What we'll be replacing the selected text with
    StringBuffer strbuf = new StringBuffer();
    
    // If they selected a partial line, count it as a full one
    ps.selectCompleteLines();
    
    int i;
    try {
      // For each line, comment them out
      for (i = ps.startLineIndex; i < ps.endLineIndex; i++) {
        strbuf.append(IASNConstants.COMMENT_TOKEN + ps.getLine(i) + ps.endLineDelim);
      }
      // Last line shouldn't add the delimiter
      strbuf.append(IASNConstants.COMMENT_TOKEN + ps.getLine(i));
      
      // Replace the text with the modified information
      ps.doc.replace(ps.startLine.getOffset(), ps.selLength, strbuf.toString());
      return true;
    } catch (Exception e) {
      //beep ( e );
    }
    
    // In event of problems, return false
    return false;
  }

  /**
   * Performs the action with a given Selection
   * 
   * @param ps
   *            Given Selection
   * @return boolean The success or failure of the action
   */
  public static boolean uncomment(Selection ps) {
      // What we'll be replacing the selected text with
      StringBuffer strbuf = new StringBuffer();
  
      // If they selected a partial line, count it as a full one
      ps.selectCompleteLines();
  
      int i;
      try {
          // For each line, comment them out
          for (i = ps.startLineIndex; i <= ps.endLineIndex; i++) {
              String l = ps.getLine(i);
              if (l.trim().startsWith(IASNConstants.COMMENT_TOKEN)){ //we may want to remove comment that are not really in the beggining...
                  strbuf.append(l.replaceFirst(IASNConstants.COMMENT_TOKEN, "") + (i < ps.endLineIndex ? ps.endLineDelim : ""));
              }else{
                  strbuf.append(l + (i < ps.endLineIndex ? ps.endLineDelim : "") ) ;
              }
          }
  
          // Replace the text with the modified information
          ps.doc.replace(ps.startLine.getOffset(), ps.selLength, strbuf.toString());
          return true;
      } catch (Exception e) {
          //beep(e);
      }
  
      // In event of problems, return false
      return false;
  }

  public static boolean toggleComment(Selection ps) {
    // If they selected a partial line, count it as a full one
    ps.selectCompleteLines();

    try {
      // For each line, comment them out
      boolean commentedSection = true;

      for (int i = ps.startLineIndex; i <= ps.endLineIndex; i++) {
        final String line = ps.getLine(i);
        if ( !line.trim().startsWith(IASNConstants.COMMENT_TOKEN) ) {
          commentedSection = false;
          break;
        }
      }

      if (commentedSection)
        uncomment(ps);
      else
        comment(ps);

      return true;
    } catch (Exception e) {
      //beep ( e );
    }

    // In event of problems, return false
    return false;
  }

  /**
   * Same as comment, but remove the first char.
   */
  protected static String replaceStr(String str, String endLineDelim) {
      str = str.replaceAll(endLineDelim + IASNConstants.COMMENT_TOKEN, endLineDelim);
      if (str.startsWith(IASNConstants.COMMENT_TOKEN)) {
          str = str.substring(IASNConstants.COMMENT_TOKEN.length());
      }
      return str;
  }

}
