/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

/**
 * @author bst
 * TODO comment this
 */
public class ASNNature implements IProjectNature {
	public static final String ASN_NATURE_ID = "net.sourceforge.asneditor.asn.nature";

  private IProject project;

  /* (non-Javadoc)
   * @see org.eclipse.core.resources.IProjectNature#configure()
   */
  public void configure() throws CoreException {
  }

  /* (non-Javadoc)
   * @see org.eclipse.core.resources.IProjectNature#deconfigure()
   */
  public void deconfigure() throws CoreException {
  }

  /* (non-Javadoc)
   * @see org.eclipse.core.resources.IProjectNature#getProject()
   */
  public IProject getProject() {
    return project;
  }

  /* (non-Javadoc)
   * @see org.eclipse.core.resources.IProjectNature#setProject(org.eclipse.core.resources.IProject)
   */
  public void setProject(IProject project) {
    this.project = project;
  }

  /**
   * @param project
   */
  public static synchronized void toggleAsnNature(final IProject project) {
  	if (project == null)
  		return;

  	try {
  		final boolean hasAsnNature = ASNNature.hasAsnNature(project);
  		if (hasAsnNature) {
  			ASNNature.removeAsnNature(project);
  		} else {
  			ASNNature.addAsnNature(project);
  		}
  	} catch (CoreException e) {
  		ASNEditorPlugin.log(e);
  	}
  }

  public static synchronized boolean hasAsnNature(IProject project) throws CoreException {
  		return project.hasNature(ASNNature.ASN_NATURE_ID);
  }

  public static synchronized void addAsnNature(IProject project) throws CoreException {
  	if ((project == null) || hasAsnNature(project))
  		return;

  	IProjectDescription description = project.getDescription();
  	String[] natures = description.getNatureIds();
  	String[] newNatures = new String[natures.length + 1];
  	System.arraycopy(natures, 0, newNatures, 0, natures.length);
  	newNatures[natures.length] = ASNNature.ASN_NATURE_ID;

    // check the status and decide what to do
  	IStatus status = project.getWorkspace().validateNatureSet(newNatures);
    if (status.getCode() == IStatus.OK) {
    	description.setNatureIds(newNatures);
    	project.setDescription(description, null);
    } else {
    	// raise a user error
    	ASNEditorPlugin.log(status);
    }
  }

  public static synchronized void removeAsnNature(IProject project) throws CoreException {
  	if ((project == null) || !hasAsnNature(project))
  		return;

  	IProjectDescription description = project.getDescription();
  	String[] natures = description.getNatureIds();
  	String[] newNatures = new String[natures.length - 1];
  	for (int i = 0; i < natures.length; i++) {
  		if (ASNNature.ASN_NATURE_ID.equals(natures[i])) {
  			System.arraycopy(natures, 0, newNatures, 0, i);
  			System.arraycopy(natures, i + 1, newNatures, i, natures.length - i - 1);

  	    // check the status and decide what to do
  	  	IStatus status = project.getWorkspace().validateNatureSet(newNatures);
  	    if (status.getCode() == IStatus.OK) {
  	    	description.setNatureIds(newNatures);
  	    	project.setDescription(description, null);
  	    } else {
  	    	// raise a user error
  	    	ASNEditorPlugin.log(status);
  	    }
  			break;
  		}
  	}
  }

}
