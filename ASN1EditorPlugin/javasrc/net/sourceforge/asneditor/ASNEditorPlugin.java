/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import net.sourceforge.asneditor.editors.IASNConstants;

import org.eclipse.core.internal.resources.Workspace;
import org.eclipse.core.internal.resources.WorkspaceRoot;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPluginDescriptor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.FormColors;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import antlr.RecognitionException;

/**
 * The main plugin class to be used in the desktop.
 */
public class ASNEditorPlugin extends AbstractUIPlugin {
  public static final String PLUGIN_ID = "net.sourceforge.asneditor";

  //The shared instance.
  private static ASNEditorPlugin plugin;
  //Resource bundle.
  private ResourceBundle resourceBundle;
  private FormToolkit fDialogsFormToolkit;

  // TODO added due compatibility issues with eclipse 3.0.1
  public ASNEditorPlugin(IPluginDescriptor descriptor) {
    super(descriptor);
    init();
  }

  public ASNEditorPlugin() {
    super();
    init();
  }

  /**
   *
   */
  private void init() {
    plugin = this;
    try {
      resourceBundle = ResourceBundle.getBundle("net.sourceforge.asneditor.messages"); //$NON-NLS-1$
    } catch (MissingResourceException x) {
      resourceBundle = null;
    }
  }
  /**
   * This method is called upon plug-in activation
   */
  public void start(BundleContext context) throws Exception {
    super.start(context);
  }

  /**
   * This method is called when the plug-in is stopped
   */
  public void stop(BundleContext context) throws Exception {
    super.stop(context);
  }

	public FormToolkit getDialogsFormToolkit() {
    if (fDialogsFormToolkit == null) {
      FormColors colors= new FormColors(Display.getCurrent());
      colors.setBackground(null);
      colors.setForeground(null);
      fDialogsFormToolkit= new FormToolkit(colors);
    }
    return fDialogsFormToolkit;
  }

  /**
   * Returns the shared instance.
   */
  public static ASNEditorPlugin getDefault() {
    return plugin;
  }

  /**
   * @return the current workspace
   */
  public static IWorkspace getWorkspace() {
    return ResourcesPlugin.getWorkspace();
  }

  /**
   * Returns the string from the plugin's resource bundle,
   * or 'key' if not found.
   */
  public static String getResourceString(String key) {
    ResourceBundle bundle = ASNEditorPlugin.getDefault().getResourceBundle();
    try {
      return (bundle != null) ? bundle.getString(key) : key;
    } catch (MissingResourceException e) {
      return key;
    }
  }

  /**
   * Returns the plugin's resource bundle,
   */
  public ResourceBundle getResourceBundle() {
    return resourceBundle;
  }
  public static void log(IStatus status) {
    getDefault().getLog().log(status);
  }

  public static void log(CoreException ex) {
    final IStatus status = ex.getStatus();
    log(new Status(status.getSeverity(), PLUGIN_ID, status.getCode(), status.getMessage(), ex));
  }

  public static void log(String message) {
    log(message, 1, null);
  }

  public static void log(String message, Throwable e) {
    log(message, 1, e);
  }

  public static void log(String message, int code, Throwable e) {
    log(new Status(IStatus.ERROR, ASNEditorPlugin.class.getName(), code, message, e));
  }

  public static void reportError(RecognitionException e,
      IEditorInput input,
      ISourceViewer viewer) {
    if (e == null)
      return;
    error(e.getMessage(), e, e.getLine(), e.getColumn(), input, viewer);
  }

  /**
   * Error at a point location.
   *
   * @param line Line number (starts from 1).
   * @param column Column number (starts from 1).
   */
  public static void error(
      String message,
      Throwable e,
      int line,
      int column,
      IEditorInput input,
      ISourceViewer viewer) {
    //
    IResource resource = findResource(input);
    message = addLinColumnToMessage(message, line, column);
    int offset = findOffset(line, column, viewer);
    errorMarker(
        message,
        e,
        resource,
        IASNConstants.ProblemMarker,
        offset,
        offset,
        line);
  }

  /**
   * @param message
   * @param line
   * @param column
   * @return the message suffixed by the (@ line, column) if specified
   */
  public static String addLinColumnToMessage(String message, int line, int column) {
    if (line > 0) {
      message += " (@" + line; //$NON-NLS-1$
      if (column >= 0) {
        message += ", " + column; //$NON-NLS-1$
      }
      message += ")"; //$NON-NLS-1$
    }
    return message;
  }

  /**
   * Warning at a point location.
   *
   * @param line Line number (starts from 1).
   * @param column Column number (starts from 1).
   */
  public static void warn(
      String message,
      Throwable e,
      int line,
      int column,
      IEditorInput input,
      ISourceViewer viewer) {
    //
    IResource resource = findResource(input);
    message = addLinColumnToMessage(message, line, column);
    int offset = findOffset(line, column, viewer);
    warnMarker(
        message,
        e,
        resource,
        IASNConstants.ProblemMarker,
        offset,
        offset,
        line);
  }

  public static int findOffset(int line, int column, final ISourceViewer viewer) {
    IDocument doc = viewer.getDocument();
    final int[] tabs = new int[]{2};
    runSynchronous(new Runnable(){
      public void run() {
        StyledText textWidget = viewer.getTextWidget();
        if (textWidget != null) {
          tabs[0] = textWidget.getTabs();
        }
      }});
    int tabwidth = tabs[0];

    return findOffset(doc, line, column, tabwidth);
  }

  public static int findOffset(IDocument doc, int line, int column) {
    int tabwidth = 2;
    return findOffset(doc, line, column, tabwidth);
  }
  public static int findOffset(IDocument doc, int line, int column, int tabwidth) {
    int offset = 0;
    if (line > 0) {
      try {
        offset = doc.getLineOffset(line - 1);
        if (column > 0) {
          char c;
          for (int i = 0; i < column - 1;) {
            c = doc.getChar(offset);
            ++offset;
            if (c == '\t')
              i = (i - (i % tabwidth)) + tabwidth;
            else
              ++i;
          }
        }
      } catch (BadLocationException e) {
        ASNEditorPlugin.log(
            ".findOffset(): Bad location: line="
            + line
            + ", column="
            + column, e);
      }
    }
    return offset;
  }
  public static void runSynchronous(Runnable runnable) {
    runInGuiThread(runnable, true);
  }

  public static void runAsynchronous(Runnable runnable) {
    runInGuiThread(runnable, false);
  }

  private static void runInGuiThread(Runnable runnable, final boolean sync) {
    if (runnable == null) {
      return;
    }
    Display display = PlatformUI.getWorkbench().getDisplay();

    if (display != null && !display.isDisposed()) {
      try {
        if (sync) {
          display.syncExec(runnable);
        } else {
          display.asyncExec(runnable);
        }
      }
      catch (final SWTException e) {
        log("error ", e);
      }
    }
  }

  /**
   * @param message the message to be displayed.
   * @param kind one of ({@link Status#ERROR} or {@link Status#OK})
   */
  public static void showStatusLineMessage(final String message, final int kind) {
    runAsynchronous(new Runnable() {

      public void run() {
        // Retrieves status line manager instance and sets the message into the instance.
        IWorkbenchWindow activeWindow = getDefault().getWorkbench().getActiveWorkbenchWindow();
        if (activeWindow != null) {
          IWorkbenchPage activePage = activeWindow.getActivePage();
          if (activePage != null) {
            showStatusMessage(activePage);
          }
        }
      }

      /**
       * Show the status message.
       *
       * @param activePage The active workbench.
       */
      private void showStatusMessage(IWorkbenchPage activePage) {
        IViewReference[] viewReferences = activePage.getViewReferences();
        for (int i = 0; i < viewReferences.length; i++) {
          IViewPart viewPart = viewReferences[i].getView(true);
          if (viewPart != null) {
            IActionBars viewActionbars = viewPart.getViewSite().getActionBars();
            IStatusLineManager viewStatusManager = viewActionbars.getStatusLineManager();
            // Set status line associated with view part such as Package Explore, and etc.
            switch (kind) {
            case Status.ERROR:
              viewStatusManager.setMessage(null);
              viewStatusManager.setErrorMessage(message);
              break;
            default:
              viewStatusManager.setErrorMessage(null);
              viewStatusManager.setMessage(message);
              break;
            }
          }
        }
        IEditorPart editorPart = activePage.getActiveEditor();
        if (editorPart != null) {
          IActionBars partActionBars = editorPart.getEditorSite().getActionBars();
          IStatusLineManager partStatusManager = partActionBars.getStatusLineManager();
          // Set status line associated with editor part such as text editor.
          switch (kind) {
          case Status.ERROR:
            partStatusManager.setMessage(null);
            partStatusManager.setErrorMessage(message);
            break;
          default:
            partStatusManager.setErrorMessage(null);
            partStatusManager.setMessage(message);
            break;
          }
        }
      }
    });
  }
  /**
   * Create an error marker.
   *
   * @param resource Resource to attach the marker to.
   * @param marker_type Marker id.
   * @param start Start offset (0-based) of error range.
   * @param end End offset (0-based) of error range.
   * @param line Line (1-based) of error.
   */
  public static void errorMarker(
      String message,
      Throwable e,
      IResource resource,
      String marker_type,
      int start,
      int end,
      int line) {
    //
    if (e != null) {
      String s = e.getMessage();
      message += ": " + s;
		// TODO check this
//      EditorsPlugin.log(e);
    }
    if (resource == null || marker_type == null) {
      System.err.println("ERROR: " + message);
      return;
    }
    createMarker(resource, marker_type, IMarker.SEVERITY_ERROR, message, start, end, line);
  }

  /**
   * Create a warning marker.
   *
   * @param resource Resource to attach the marker to.
   * @param marker_type Marker id.
   * @param start Start offset (0-based) of error range.
   * @param end End offset (0-based) of error range.
   * @param line Line (1-based) of error.
   */
  public static void warnMarker(
      String message,
      Throwable e,
      IResource resource,
      String marker_type,
      int start,
      int end,
      int line) {
    //
    if (e != null) {
      String s = e.getMessage();
      if (s != null)
        message += ": " + s;
// TODO check this
//			else
//				EditorsPlugin.log(e);
    }
    if (resource == null || marker_type == null) {
      System.err.println("WARN: " + message);
      return;
    }
    createMarker(resource, marker_type, IMarker.SEVERITY_WARNING, message, start, end, line);
  }

  /**
   * Create an info marker.
   *
   * @param resource Resource to attach the marker to.
   * @param marker_type Marker id.
   * @param start Start offset (0-based) of error range.
   * @param end End offset (0-based) of error range.
   * @param line Line (1-based) of error.
   */
  public static void infoMarker(
      String message,
      Throwable e,
      IResource resource,
      String marker_type,
      int start,
      int end,
      int line) {

    if (e != null) {
      String s = e.getMessage();
      if (s != null)
        message += ": " + s;
		// TODO check this
//			else
//				EditorsPlugin.log(e);
    }
    if (resource == null || marker_type == null) {
      System.err.println("INFO: " + message);
      return;
    }
    createMarker(resource, marker_type, IMarker.SEVERITY_INFO, message, start, end, line);
  }

  ////////////////////////////////////////////////////////////////////////

  public static void createMarker(
      IResource resource,
      String kind,
      int severity,
      String message,
      int start,
      int end,
      int line) {

    IMarker marker;
    try {
      marker = resource.createMarker(kind);
      marker.setAttributes(
          new String[] { IMarker.SEVERITY, IMarker.MESSAGE, IMarker.LINE_NUMBER },
          new Object[] { new Integer(severity), message, new Integer(line) });
      if (start >= 0) {
        marker.setAttribute(IMarker.CHAR_START, start);
      }
      if (end >= 0) {
        marker.setAttribute(IMarker.CHAR_START, end);
      }
    } catch (CoreException ex) {
      log( ".createMarker(): Unable to create marker");
      return;
    }
  }

  public static IMarker[] getMarkers(IResource resource, String type) {
    if (resource != null) {
      try {
        return resource.findMarkers(type, true, IResource.DEPTH_INFINITE);
      } catch (CoreException e) {
      }
    }
    return new IMarker[0];
  }

  public static IResource findResource(IEditorInput input) {
    IFile file = (IFile) input.getAdapter(IFile.class);
    IResource ret = null;
    if (file != null) {
      Workspace workspace = (Workspace) ResourcesPlugin.getWorkspace();
      WorkspaceRoot root = (WorkspaceRoot) workspace.getRoot();
      ret = root.findMember(file.getFullPath());
    }
    return ret;
  }

  public static void deleteMarkers(IResource resource, String type) {
    if (resource == null)
      return;
    try {
      resource.deleteMarkers(type, true, IResource.DEPTH_INFINITE);
    } catch (CoreException e) {
      System.err.println(".deleteProblemMarkers(): resource.deleteMarkers() error: resource="
          + resource.getFullPath());
    }
  }

  /**
   * @param editorInput
   * @param formatActionProblemMarker
   */
  public static void deleteProblemMarkers(IEditorInput editorInput, String formatActionProblemMarker) {
    deleteMarkers(findResource(editorInput), formatActionProblemMarker);
  }
}
