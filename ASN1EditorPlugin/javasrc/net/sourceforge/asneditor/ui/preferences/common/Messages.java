/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.ui.preferences.common;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
  private static final String BUNDLE_NAME = "net.sourceforge.asneditor.ui.preferences.common.messages"; //$NON-NLS-1$
  public static String AbstractConfigurationBlock_empty_input;
  public static String AbstractConfigurationBlock_invalid_input_param;
  public static String OverlayPreferenceStore_error_reading_pref;
  public static String PropertyAndPreferencePage_useworkspacesettings_change;
  public static String PropertyAndPreferencePage_showprojectspecificsettings_label;
  public static String PropertyAndPreferencePage_useprojectsettings_label;

  static {
    // initialize resource bundle
    NLS.initializeMessages(BUNDLE_NAME, Messages.class);
  }

  private Messages() {
  }
}
