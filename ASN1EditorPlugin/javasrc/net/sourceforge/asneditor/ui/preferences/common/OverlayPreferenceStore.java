/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.asneditor.ui.preferences.common;

import java.util.Properties;
import java.util.Map.Entry;

import net.sourceforge.asneditor.ASNEditorPlugin;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.osgi.util.NLS;

/**
 * An overlaying preference store.
 */
public class OverlayPreferenceStore  implements IPreferenceStore {


	public static final class TypeDescriptor {
		private final String name;
    private TypeDescriptor(String name) {
      this.name = name;
		}
		public String toString() {
		  return name;
		}
	}

  public static final TypeDescriptor STRING= new TypeDescriptor("string"); //$NON-NLS-1$
	public static final TypeDescriptor BOOLEAN= new TypeDescriptor("boolean"); //$NON-NLS-1$
	public static final TypeDescriptor DOUBLE= new TypeDescriptor("double"); //$NON-NLS-1$
	public static final TypeDescriptor FLOAT= new TypeDescriptor("float"); //$NON-NLS-1$
	public static final TypeDescriptor INT= new TypeDescriptor("int"); //$NON-NLS-1$
	public static final TypeDescriptor LONG= new TypeDescriptor("long"); //$NON-NLS-1$

	public static class OverlayKey {

		TypeDescriptor fDescriptor;
		String fKey;
    Object fDefaultValue;

		public OverlayKey(TypeDescriptor descriptor, String key) {
			fDescriptor= descriptor;
			fKey= key;
		}

    public void setDefaultValue(Object defaultValue) {
      this.fDefaultValue = defaultValue;
    }
    @SuppressWarnings("nls")
    public String toString() {
      return fKey + " "+ fDescriptor + "(" + fDefaultValue + ")";
    }
	}

	private class PropertyListener implements IPropertyChangeListener {

		/*
		 * @see IPropertyChangeListener#propertyChange(PropertyChangeEvent)
		 */
		public void propertyChange(PropertyChangeEvent event) {
			OverlayKey key= findOverlayKey(event.getProperty());
			if (key != null)
				propagateProperty(fParent, key, fStore);
		}
	}


	private IPreferenceStore fParent;
	private IPreferenceStore fStore;
	private OverlayKey[] fOverlayKeys;

	private PropertyListener fPropertyListener;
	private boolean fLoaded;


	public OverlayPreferenceStore(IPreferenceStore parent, OverlayKey[] overlayKeys) {
		fParent= parent;
		fOverlayKeys= overlayKeys;
		fStore= new PreferenceStore();

	}

	private OverlayKey findOverlayKey(String key) {
		for (int i= 0; i < fOverlayKeys.length; i++) {
			if (fOverlayKeys[i].fKey.equals(key))
				return fOverlayKeys[i];
		}
		return null;
	}

	private boolean covers(String key) {
		return (findOverlayKey(key) != null);
	}

	private void propagateProperty(IPreferenceStore orgin, OverlayKey key, IPreferenceStore target) {
		TypeDescriptor d= key.fDescriptor;
		if (STRING == d) {

		  String originValue= orgin.getString(key.fKey);
		  String targetValue= target.getString(key.fKey);
		  if (targetValue != null && originValue != null && !targetValue.equals(originValue))
		    target.setValue(key.fKey, originValue);

		} else if (BOOLEAN == d) {

			boolean originValue= orgin.getBoolean(key.fKey);
			boolean targetValue= target.getBoolean(key.fKey);
			if (targetValue != originValue)
				target.setValue(key.fKey, originValue);

		} else if (DOUBLE == d) {

			double originValue= orgin.getDouble(key.fKey);
			double targetValue= target.getDouble(key.fKey);
			if (targetValue != originValue)
				target.setValue(key.fKey, originValue);

		} else if (FLOAT == d) {

			float originValue= orgin.getFloat(key.fKey);
			float targetValue= target.getFloat(key.fKey);
			if (targetValue != originValue)
				target.setValue(key.fKey, originValue);

		} else if (INT == d) {

			int originValue= orgin.getInt(key.fKey);
			int targetValue= target.getInt(key.fKey);
			if (targetValue != originValue)
				target.setValue(key.fKey, originValue);

		} else if (LONG == d) {

			long originValue= orgin.getLong(key.fKey);
			long targetValue= target.getLong(key.fKey);
			if (targetValue != originValue)
				target.setValue(key.fKey, originValue);

		}
	}

	public void propagate() {
		for (int i= 0; i < fOverlayKeys.length; i++)
			propagateProperty(fStore, fOverlayKeys[i], fParent);
	}

	private void loadProperty(IPreferenceStore orgin, OverlayKey key, IPreferenceStore target, boolean forceInitialization) {
		TypeDescriptor d= key.fDescriptor;
		if (BOOLEAN == d) {

			if (forceInitialization)
				target.setValue(key.fKey, true);
			target.setValue(key.fKey, orgin.getBoolean(key.fKey));
			target.setDefault(key.fKey, (key.fDefaultValue != null) ? (Boolean)key.fDefaultValue : orgin.getDefaultBoolean(key.fKey));

		} else if (DOUBLE == d) {

			if (forceInitialization)
				target.setValue(key.fKey, 1.0D);
			target.setValue(key.fKey, orgin.getDouble(key.fKey));
			target.setDefault(key.fKey, (key.fDefaultValue != null) ? (Double)key.fDefaultValue : orgin.getDefaultDouble(key.fKey));

		} else if (FLOAT == d) {

			if (forceInitialization)
				target.setValue(key.fKey, 1.0F);
			target.setValue(key.fKey, orgin.getFloat(key.fKey));
			target.setDefault(key.fKey, (key.fDefaultValue != null) ? (Float)key.fDefaultValue : orgin.getDefaultFloat(key.fKey));

		} else if (INT == d) {

			if (forceInitialization)
				target.setValue(key.fKey, 1);
			target.setValue(key.fKey, orgin.getInt(key.fKey));
			target.setDefault(key.fKey, (key.fDefaultValue != null) ? (Integer)key.fDefaultValue : orgin.getDefaultInt(key.fKey));

		} else if (LONG == d) {

			if (forceInitialization)
				target.setValue(key.fKey, 1L);
			target.setValue(key.fKey, orgin.getLong(key.fKey));
			target.setDefault(key.fKey, (key.fDefaultValue != null) ? (Long)key.fDefaultValue : orgin.getDefaultLong(key.fKey));

		} else if (STRING == d) {

			if (forceInitialization)
				target.setValue(key.fKey, "1"); //$NON-NLS-1$
			target.setValue(key.fKey, orgin.getString(key.fKey));
			target.setDefault(key.fKey, (key.fDefaultValue != null) ? (String)key.fDefaultValue : orgin.getDefaultString(key.fKey));

		}
	}

  public Properties getAsProperties() {
    Properties props = new Properties();

    for (OverlayKey key : fOverlayKeys) {
      String value = ""; //$NON-NLS-1$
      TypeDescriptor d= key.fDescriptor;
      if (BOOLEAN == d) {
        value = Boolean.toString(getBoolean(key.fKey));
      } else if (DOUBLE == d) {
        value = Double.toString(getDouble(key.fKey));
      } else if (FLOAT == d) {
        value = Float.toString(getFloat(key.fKey));
      } else if (INT == d) {
        value = Integer.toString(getInt(key.fKey));
      } else if (LONG == d) {
        value = Long.toString(getLong(key.fKey));
      } else if (STRING == d) {
        value = getString(key.fKey);
      }
      props.setProperty(key.fKey, value);
    }
    return props;
  }

  public void setFromProperties(Properties props) {
    loadDefaults();

    for (Entry<Object,Object> prop : props.entrySet()) {
      String key = (String)prop.getKey();
      OverlayKey overlayKey = findOverlayKey(key);
      if (overlayKey == null) {
        continue;
      }
      TypeDescriptor d= overlayKey.fDescriptor;
      if (BOOLEAN == d) {
        setValue(key, Boolean.parseBoolean((String) prop.getValue()));
      } else if (DOUBLE == d) {
        setValue(key, Double.parseDouble((String) prop.getValue()));
      } else if (FLOAT == d) {
        setValue(key, Float.parseFloat((String) prop.getValue()));
      } else if (INT == d) {
        setValue(key, Integer.parseInt((String) prop.getValue()));
      } else if (LONG == d) {
        setValue(key, Long.parseLong((String) prop.getValue()));
      } else if (STRING == d) {
        setValue(key, (String)prop.getValue());
      }
    }
  }

  public void load() {
		for (int i= 0; i < fOverlayKeys.length; i++) {
		  try {
		    loadProperty(fParent, fOverlayKeys[i], fStore, true);
		  } catch (Throwable e) {
		    ASNEditorPlugin.log(new Status(IStatus.WARNING, ASNEditorPlugin.class.getName(), 1, NLS.bind(Messages.OverlayPreferenceStore_error_reading_pref, fOverlayKeys[i].fKey), e));
      }
		}
		fLoaded= true;
	}

	public void loadDefaults() {
		for (int i= 0; i < fOverlayKeys.length; i++)
			setToDefault(fOverlayKeys[i].fKey);
	}

	public void start() {
		if (fPropertyListener == null) {
			fPropertyListener= new PropertyListener();
			fParent.addPropertyChangeListener(fPropertyListener);
		}
	}

	public void stop() {
		if (fPropertyListener != null)  {
			fParent.removePropertyChangeListener(fPropertyListener);
			fPropertyListener= null;
		}
	}

	/*
	 * @see IPreferenceStore#addPropertyChangeListener(IPropertyChangeListener)
	 */
	public void addPropertyChangeListener(IPropertyChangeListener listener) {
		fStore.addPropertyChangeListener(listener);
	}

	/*
	 * @see IPreferenceStore#removePropertyChangeListener(IPropertyChangeListener)
	 */
	public void removePropertyChangeListener(IPropertyChangeListener listener) {
		fStore.removePropertyChangeListener(listener);
	}

	/*
	 * @see IPreferenceStore#firePropertyChangeEvent(String, Object, Object)
	 */
	public void firePropertyChangeEvent(String name, Object oldValue, Object newValue) {
		fStore.firePropertyChangeEvent(name, oldValue, newValue);
	}

	/*
	 * @see IPreferenceStore#contains(String)
	 */
	public boolean contains(String name) {
		return fStore.contains(name);
	}

	/*
	 * @see IPreferenceStore#getBoolean(String)
	 */
	public boolean getBoolean(String name) {
		return fStore.getBoolean(name);
	}

	/*
	 * @see IPreferenceStore#getDefaultBoolean(String)
	 */
	public boolean getDefaultBoolean(String name) {
		return fStore.getDefaultBoolean(name);
	}

	/*
	 * @see IPreferenceStore#getDefaultDouble(String)
	 */
	public double getDefaultDouble(String name) {
		return fStore.getDefaultDouble(name);
	}

	/*
	 * @see IPreferenceStore#getDefaultFloat(String)
	 */
	public float getDefaultFloat(String name) {
		return fStore.getDefaultFloat(name);
	}

	/*
	 * @see IPreferenceStore#getDefaultInt(String)
	 */
	public int getDefaultInt(String name) {
		return fStore.getDefaultInt(name);
	}

	/*
	 * @see IPreferenceStore#getDefaultLong(String)
	 */
	public long getDefaultLong(String name) {
		return fStore.getDefaultLong(name);
	}

	/*
	 * @see IPreferenceStore#getDefaultString(String)
	 */
	public String getDefaultString(String name) {
		return fStore.getDefaultString(name);
	}

	/*
	 * @see IPreferenceStore#getDouble(String)
	 */
	public double getDouble(String name) {
		return fStore.getDouble(name);
	}

	/*
	 * @see IPreferenceStore#getFloat(String)
	 */
	public float getFloat(String name) {
		return fStore.getFloat(name);
	}

	/*
	 * @see IPreferenceStore#getInt(String)
	 */
	public int getInt(String name) {
		return fStore.getInt(name);
	}

	/*
	 * @see IPreferenceStore#getLong(String)
	 */
	public long getLong(String name) {
		return fStore.getLong(name);
	}

	/*
	 * @see IPreferenceStore#getString(String)
	 */
	public String getString(String name) {
		return fStore.getString(name);
	}

	/*
	 * @see IPreferenceStore#isDefault(String)
	 */
	public boolean isDefault(String name) {
		return fStore.isDefault(name);
	}

	/*
	 * @see IPreferenceStore#needsSaving()
	 */
	public boolean needsSaving() {
		return fStore.needsSaving();
	}

	/*
	 * @see IPreferenceStore#putValue(String, String)
	 */
	public void putValue(String name, String value) {
		if (covers(name))
			fStore.putValue(name, value);
	}

	/*
	 * @see IPreferenceStore#setDefault(String, double)
	 */
	public void setDefault(String name, double value) {
		if (covers(name))
			fStore.setDefault(name, value);
	}

	/*
	 * @see IPreferenceStore#setDefault(String, float)
	 */
	public void setDefault(String name, float value) {
		if (covers(name))
			fStore.setDefault(name, value);
	}

	/*
	 * @see IPreferenceStore#setDefault(String, int)
	 */
	public void setDefault(String name, int value) {
		if (covers(name))
			fStore.setDefault(name, value);
	}

	/*
	 * @see IPreferenceStore#setDefault(String, long)
	 */
	public void setDefault(String name, long value) {
		if (covers(name))
			fStore.setDefault(name, value);
	}

	/*
	 * @see IPreferenceStore#setDefault(String, String)
	 */
	public void setDefault(String name, String value) {
		if (covers(name))
			fStore.setDefault(name, value);
	}

	/*
	 * @see IPreferenceStore#setDefault(String, boolean)
	 */
	public void setDefault(String name, boolean value) {
		if (covers(name))
			fStore.setDefault(name, value);
	}

	/*
	 * @see IPreferenceStore#setToDefault(String)
	 */
	public void setToDefault(String name) {
		fStore.setToDefault(name);
	}

  public void setFromParent(String key) {
    OverlayKey overlayKey = findOverlayKey(key);
    if (overlayKey == null) {
      return;
    }

    TypeDescriptor d= overlayKey.fDescriptor;
    if (BOOLEAN == d) {
      setValue(key, fParent.getBoolean(key));
    } else if (DOUBLE == d) {
      setValue(key, fParent.getDouble(key));
    } else if (FLOAT == d) {
      setValue(key, fParent.getFloat(key));
    } else if (INT == d) {
      setValue(key, fParent.getInt(key));
    } else if (LONG == d) {
      setValue(key, fParent.getLong(key));
    } else if (STRING == d) {
      setValue(key, fParent.getString(key));
    }
  }

	/*
	 * @see IPreferenceStore#setValue(String, double)
	 */
	public void setValue(String name, double value) {
		if (covers(name))
			fStore.setValue(name, value);
	}

	/*
	 * @see IPreferenceStore#setValue(String, float)
	 */
	public void setValue(String name, float value) {
		if (covers(name))
			fStore.setValue(name, value);
	}

	/*
	 * @see IPreferenceStore#setValue(String, int)
	 */
	public void setValue(String name, int value) {
		if (covers(name))
			fStore.setValue(name, value);
	}

	/*
	 * @see IPreferenceStore#setValue(String, long)
	 */
	public void setValue(String name, long value) {
		if (covers(name))
			fStore.setValue(name, value);
	}

	/*
	 * @see IPreferenceStore#setValue(String, String)
	 */
	public void setValue(String name, String value) {
		if (covers(name))
			fStore.setValue(name, value);
	}

	/*
	 * @see IPreferenceStore#setValue(String, boolean)
	 */
	public void setValue(String name, boolean value) {
		if (covers(name))
			fStore.setValue(name, value);
	}

	/**
	 * The keys to add to the list of overlay keys.
	 * <p>
	 * Note: This method must be called before {@link #load()} is called.
	 * </p>
	 *
	 * @param keys
	 * @since 3.0
	 */
	public void addKeys(OverlayKey[] keys) {
		Assert.isTrue(!fLoaded);
		Assert.isNotNull(keys);

		int overlayKeysLength= fOverlayKeys.length;
		OverlayKey[] result= new OverlayKey[keys.length + overlayKeysLength];

		for (int i= 0, length= overlayKeysLength; i < length; i++)
			result[i]= fOverlayKeys[i];

		for (int i= 0, length= keys.length; i < length; i++)
			result[overlayKeysLength + i]= keys[i];

		fOverlayKeys= result;

		if (fLoaded)
			load();
	}

  public static OverlayKey createBoolean(String key) {
    return new OverlayKey(BOOLEAN, key);
  }

  public static OverlayKey createDouble(String key) {
    return new OverlayKey(DOUBLE, key);
  }

  public static OverlayKey createFloat(String key) {
    return new OverlayKey(FLOAT, key);
  }

  public static OverlayKey createInt(String key) {
    return new OverlayKey(INT, key);
  }

  public static OverlayKey createLong(String key) {
    return new OverlayKey(LONG, key);
  }

  public static OverlayKey createString(String key) {
    return new OverlayKey(STRING, key);
  }
}
