/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.asneditor.ui.preferences.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import net.sourceforge.asneditor.ui.preferences.common.OverlayPreferenceStore.OverlayKey;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;

/**
 * Configures preferences.
 */
public abstract class AbstractConfigurationBlock implements IPreferenceConfigurationBlock {
  protected static final String CHECK_KEY_SUFFIX = "-check"; //$NON-NLS-1$
  protected static final String SETTINGS_EXPANDED= "expanded"; //$NON-NLS-1$

  /**
   * Use as follows:
   *
   * <pre>
   * SectionManager manager= new SectionManager();
   * Composite composite= manager.createSectionComposite(parent);
   *
   * Composite xSection= manager.createSection("section X"));
   * xSection.setLayout(new FillLayout());
   * new Button(xSection, SWT.PUSH); // add controls to section..
   *
   * [...]
   *
   * return composite; // return main composite
   * </pre>
   */
  protected final class SectionManager {
    /** The preference setting for keeping no section open. */
    private static final String __NONE= "__none"; //$NON-NLS-1$
    private Collection<ExpandableComposite> fSections= new LinkedList<ExpandableComposite>();
    private boolean fIsBeingManaged= false;
    private ExpansionAdapter fListener= new ExpansionAdapter() {
      public void expansionStateChanged(ExpansionEvent e) {
        ExpandableComposite source= (ExpandableComposite) e.getSource();
        updateSectionStyle(source);
        if (fIsBeingManaged)
          return;
        if (e.getState()) {
          try {
            fIsBeingManaged= true;
            for (ExpandableComposite composite : fSections) {
              if (composite != source)
                composite.setExpanded(false);
            }
          } finally {
            fIsBeingManaged= false;
          }
          if (fLastOpenKey != null && fDialogSettingsStore != null)
            fDialogSettingsStore.setValue(fLastOpenKey, source.getText());
        } else {
          if (!fIsBeingManaged && fLastOpenKey != null && fDialogSettingsStore != null)
            fDialogSettingsStore.setValue(fLastOpenKey, __NONE);
        }
        ExpandableComposite exComp= getParentExpandableComposite(source);
        if (exComp != null)
          exComp.layout(true, true);
        ScrolledPageContent parentScrolledComposite= getParentScrolledComposite(source);
        if (parentScrolledComposite != null) {
          parentScrolledComposite.reflow(true);
        }
      }
    };
    private Composite fBody;
    private final String fLastOpenKey;
    private final IPreferenceStore fDialogSettingsStore;
    private ExpandableComposite fFirstChild= null;
    /**
     * Creates a new section manager.
     */
    public SectionManager() {
      this(null, null);
    }
    /**
     * Creates a new section manager.
     */
    public SectionManager(IPreferenceStore dialogSettingsStore, String lastOpenKey) {
      fDialogSettingsStore= dialogSettingsStore;
      fLastOpenKey= lastOpenKey;
    }
    private void manage(ExpandableComposite section) {
      if (section == null)
        throw new NullPointerException();
      if (fSections.add(section))
        section.addExpansionListener(fListener);
      makeScrollableCompositeAware(section);
    }

    /**
     * Creates a new composite that can contain a set of expandable
     * sections. A <code>ScrolledPageComposite</code> is created and a new
     * composite within that, to ensure that expanding the sections will
     * always have enough space, unless there already is a
     * <code>ScrolledComposite</code> along the parent chain of
     * <code>parent</code>, in which case a normal <code>Composite</code>
     * is created.
     * <p>
     * The receiver keeps a reference to the inner body composite, so that
     * new sections can be added via <code>createSection</code>.
     * </p>
     *
     * @param parent the parent composite
     * @return the newly created composite
     */
    public Composite createSectionComposite(Composite parent) {
      Assert.isTrue(fBody == null);
      boolean isNested= isNestedInScrolledComposite(parent);
      Composite composite;
      if (isNested) {
        composite= new Composite(parent, SWT.NONE);
        fBody= composite;
      } else {
        composite= new ScrolledPageContent(parent);
        fBody= ((ScrolledPageContent) composite).getBody();
      }

      fBody.setLayout(new GridLayout());

      return composite;
    }

    /**
     * Creates an expandable section within the parent created previously by
     * calling <code>createSectionComposite</code>. Controls can be added
     * directly to the returned composite, which has no layout initially.
     *
     * @param label the display name of the section
     * @return a composite within the expandable section
     */
    public Composite createSection(String label) {
      Assert.isNotNull(fBody);
      final ExpandableComposite excomposite= new ExpandableComposite(fBody, SWT.NONE, ExpandableComposite.TWISTIE | ExpandableComposite.CLIENT_INDENT | ExpandableComposite.COMPACT);
      if (fFirstChild == null)
        fFirstChild= excomposite;
      excomposite.setText(label);
      String last= null;
      if (fLastOpenKey != null && fDialogSettingsStore != null)
        last= fDialogSettingsStore.getString(fLastOpenKey);

      if (fFirstChild == excomposite && !__NONE.equals(last) || label.equals(last)) {
        excomposite.setExpanded(true);
        if (fFirstChild != excomposite)
          fFirstChild.setExpanded(false);
      } else {
        excomposite.setExpanded(false);
      }
      excomposite.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));

      updateSectionStyle(excomposite);
      manage(excomposite);

      Composite contents= new Composite(excomposite, SWT.NONE);
      excomposite.setClient(contents);

      return contents;
    }
  }

  protected static final int INDENT= 20;
  protected OverlayPreferenceStore fStore;

  protected final Collection<ExpandableComposite> fExpandedComposites = new LinkedList<ExpandableComposite>();

  protected Map<Button, String> fButtons= new Hashtable<Button, String>();
  protected Map<Button, String> fCheckBoxes= new Hashtable<Button, String>();
  protected SelectionListener fCheckBoxListener= new SelectionListener() {
    public void widgetDefaultSelected(SelectionEvent e) {
    }
    public void widgetSelected(SelectionEvent e) {
      Button button= (Button) e.widget;
      fStore.setValue((String) fCheckBoxes.get(button), button.getSelection());
    }
  };

  protected Map<Text, String> fTextFields= new Hashtable<Text, String>();
  protected ModifyListener fTextFieldListener= new ModifyListener() {
    public void modifyText(ModifyEvent e) {
      Text text= (Text) e.widget;
      fStore.setValue((String) fTextFields.get(text), text.getText());
    }
  };

  protected Collection<Text> fNumberFields= new LinkedList<Text>();
  protected ModifyListener fNumberFieldListener= new ModifyListener() {
    public void modifyText(ModifyEvent e) {
      numberFieldChanged((Text) e.widget);
    }
  };

  protected Map<Combo, String> fComboBoxes= new Hashtable<Combo, String>();
  protected SelectionListener fComboBoxListener= new SelectionListener() {
    public void widgetDefaultSelected(SelectionEvent e) {
    }
    public void widgetSelected(SelectionEvent e) {
      Combo combo= (Combo) e.widget;
      int selectionIndex = combo.getSelectionIndex();
      ControlData data = (ControlData) combo.getData();
      fStore.setValue((String) fComboBoxes.get(combo), data.getValue(selectionIndex));
    }
  };

  protected Map<String, Control> fControls= new Hashtable<String, Control>();


  /**
   * List of master/slave listeners when there's a dependency.
   *
   * @see #createDependency(Button, Control)
   * @since 3.0
   */
  protected Collection<SelectionListener> fMasterSlaveListeners= new LinkedList<SelectionListener>();

  protected StatusInfo fStatus;
  protected final PreferencePage fMainPage;
  protected PixelConverter pixelConverter;

  protected static class ControlData {
    private String fKey;
    private String[] fValues;

    public ControlData(String key, String[] values) {
      this.fKey= key;
      this.fValues= values;
    }

    public String getKey() {
      return fKey;
    }

    public String getValue(boolean selection) {
      int index= selection ? 0 : 1;
      return getValue(index);
    }

    public String getValue(int index) {
      return fValues[index];
    }

    public int getSelection(String value) {
      return getSelection(value, fValues);
    }

    public int getSelection(String value, String[] values) {
      if (value != null) {
        for (int i= 0; i < values.length; i++) {
          if (value.equals(values[i])) {
            return i;
          }
        }
      }
      return values.length -1; // assume the last option is the least severe
    }
  }

  public AbstractConfigurationBlock(OverlayPreferenceStore store) {
    this(store, null, false);
    Assert.isNotNull(store);
  }

  public AbstractConfigurationBlock(OverlayPreferenceStore store, PreferencePage mainPreferencePage) {
    this(store, mainPreferencePage, false);
    Assert.isNotNull(store);
    Assert.isNotNull(mainPreferencePage);
  }

  protected AbstractConfigurationBlock(OverlayPreferenceStore store, PreferencePage mainPreferencePage, boolean check) {
    fStore= store;
    fMainPage= mainPreferencePage;
    fStore.addKeys(createOverlayStoreKeys());
    pixelConverter = null;
  }

  protected abstract OverlayKey[] createOverlayStoreKeys();

  public Control createControl(Composite parent) {
    pixelConverter= new PixelConverter(parent);
    return parent;
  }

  protected final ScrolledPageContent getParentScrolledComposite(Control control) {
    Control parent= control.getParent();
    while (!(parent instanceof ScrolledPageContent) && parent != null) {
      parent= parent.getParent();
    }
    if (parent instanceof ScrolledPageContent) {
      return (ScrolledPageContent) parent;
    }
    return null;
  }

  private final ExpandableComposite getParentExpandableComposite(Control control) {
    Control parent= control.getParent();
    while (!(parent instanceof ExpandableComposite) && parent != null) {
      parent= parent.getParent();
    }
    if (parent instanceof ExpandableComposite) {
      return (ExpandableComposite) parent;
    }
    return null;
  }

  protected void updateSectionStyle(ExpandableComposite excomposite) {
    excomposite.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DIALOG_FONT));
  }

  private void makeScrollableCompositeAware(Control control) {
    ScrolledPageContent parentScrolledComposite= getParentScrolledComposite(control);
    if (parentScrolledComposite != null) {
      parentScrolledComposite.adaptChild(control);
    }
  }

  private boolean isNestedInScrolledComposite(Composite parent) {
    return getParentScrolledComposite(parent) != null;
  }

  protected Combo newComboControl(Composite composite, String key, String[] values, String[] valueLabels) {
    ControlData data= new ControlData(key, values);

    Combo comboBox= new Combo(composite, SWT.READ_ONLY);
    comboBox.setItems(valueLabels);
    comboBox.setData(data);
    comboBox.addSelectionListener(fComboBoxListener);
    comboBox.setFont(JFaceResources.getDialogFont());

    makeScrollableCompositeAware(comboBox);

    String currValue = fStore.getString(key);
    comboBox.select(data.getSelection(currValue));

    fComboBoxes.put(comboBox, key);
    fControls.put(key, comboBox);
    return comboBox;
  }

  protected Combo addComboBox(Composite parent, String label, String key, String[] values, String[] valueLabels, int indent) {
    GridData gd= new GridData(GridData.FILL_HORIZONTAL);
    gd.horizontalIndent= indent;

    Label labelControl= new Label(parent, SWT.LEFT);
    labelControl.setFont(JFaceResources.getDialogFont());
    if (label != null)
      labelControl.setText(label);
    labelControl.setLayoutData(gd);

    Combo comboBox= newComboControl(parent, key, values, valueLabels);
    comboBox.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));

    return comboBox;
  }

  protected Button addRadioButton(Composite parent, String label, String key, int indentation) {
    return addRadioButton(parent, label, key, indentation, 1);
  }

  protected Button addRadioButton(Composite parent, String label, String key, int indentation, int colspan) {
    return addButton(parent, label, key, indentation, SWT.RADIO, colspan);
  }

  protected Button addCheckBox(Composite parent, String label, String key, int indentation) {
    return addCheckBox(parent, label, key, indentation, 1);
  }

  protected Button addCheckBox(Composite parent, String label, String key, int indentation, int colspan) {
    return addButton(parent, label, key, indentation, SWT.CHECK, colspan);
  }

  protected Button addButton(Composite parent, String label, String key, int indentation, int style, int colspan) {
    Button button= new Button(parent, style);
    button.setText(label);

    GridData gd= new GridData(GridData.BEGINNING);
    if (indentation > -1) {
      gd.horizontalIndent= indentation;
    }
    gd.horizontalSpan= colspan;
    button.setLayoutData(gd);
    button.addSelectionListener(fCheckBoxListener);
    makeScrollableCompositeAware(button);

    fCheckBoxes.put(button, key);
    fControls.put(key, button);

    return button;
  }

  /**
   * Returns an array of size 2:
   *  - first element is of type <code>Label</code>
   *  - second element is of type <code>Text</code>
   * Use <code>getLabelControl</code> and <code>getTextControl</code> to get the 2 controls.
   *
   * @param composite   the parent composite
   * @param label     the text field's label
   * @param key     the preference key
   * @param textLimit   the text limit
   * @param indentation the field's indentation
   * @param isNumber    <code>true</code> if this text field is used to e4dit a number
   * @return the controls added
   */
  protected Control[] addLabelledTextField(Composite composite, String label, String key, int textLimit, int indentation, boolean isNumber) {
    return addLabelledTextField(composite, label, key, textLimit, true, indentation, isNumber);
  }

  /**
   * Returns an array of size 2:
   *  - first element is of type <code>Label</code>
   *  - second element is of type <code>Text</code>
   * Use <code>getLabelControl</code> and <code>getTextControl</code> to get the 2 controls.
   *
   * @param composite 	the parent composite
   * @param label			the text field's label
   * @param key			the preference key
   * @param textLimit		the text limit
   * @param hardLimit if <code>true</code> then no more than textLimit chars are allowed,
   *  else the textLimit chars describe the width hint of the field
   * @param indentation	the field's indentation
   * @param isNumber		<code>true</code> if this text field is used to e4dit a number
   * @return the controls added
   */
  protected Control[] addLabelledTextField(Composite composite, String label, String key, int textLimit, boolean hardLimit, int indentation, boolean isNumber) {
    Label labelControl = null;
    if (label != null) {
      labelControl = addLabel(composite, label, indentation);
    }

    Text textControl= new Text(composite, SWT.BORDER | SWT.SINGLE);
    GridData gd= new GridData(GridData.FILL_HORIZONTAL);
    if (pixelConverter != null && textLimit > 0) {
      gd.widthHint= pixelConverter.convertWidthInCharsToPixels(textLimit + 1);
    }
    textControl.setLayoutData(gd);
    if (hardLimit && textLimit > 0) {
      textControl.setTextLimit(textLimit);
    }
    fTextFields.put(textControl, key);
    fControls.put(key, textControl);

    if (isNumber) {
      fNumberFields.add(textControl);
      textControl.addModifyListener(fNumberFieldListener);
    } else {
      textControl.addModifyListener(fTextFieldListener);
    }

    return new Control[]{labelControl, textControl};
  }

  public Control getControlForPreferenceKey(String key) {
    return fControls.get(key);
  }

  public String getPreferenceKeyForControl(Control control) {
    if (control instanceof Combo) {
      return fComboBoxes.get(control);
    } else if (control instanceof Button) {
      String key = fCheckBoxes.get(control);
      if (key == null) {
        key = fButtons.get(control);
      }
      return key;
    } else if (control instanceof Text) {
      return fTextFields.get(control);
    }
    return null;
  }

  public Label addLabel(Composite composite, String label, int indentation) {
    Label labelControl;
    labelControl = new Label(composite, SWT.NONE);
    labelControl.setText(label);
    GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
    gd.horizontalIndent= indentation;
    labelControl.setLayoutData(gd);
    return labelControl;
  }
  protected Control[] addLabelledTextFieldWithButton(Composite composite, String label, String buttonLabel, String key,
      final ITextButtonListener listener, int textLimit, boolean hardLimit, int indentation) {
    return addLabelledTextFieldWithButton(composite, label, buttonLabel, key, listener, textLimit, hardLimit, indentation, false);
  }

  protected Control[] addLabelledTextFieldWithButton(Composite composite, String label, String buttonLabel, String key,
      final ITextButtonListener listener, int textLimit, boolean hardLimit, int indentation, boolean addEnableButton) {

    Button enableCheckBox = null;
    if (addEnableButton) {
      enableCheckBox = addCheckBox(composite, label, getEnableCheckboxKeyFor(key), -1);
      enableCheckBox.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING));
    }
    Control[] labelledTextFields = addLabelledTextField(composite, addEnableButton ? null : label, key, textLimit, false, indentation, false);
    final Text text = (Text) labelledTextFields[1];

    Button buttonControl = new Button(composite, SWT.PUSH);
    buttonControl.setText(buttonLabel);
    GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_END);
    buttonControl.setLayoutData(gd);
    buttonControl.addSelectionListener(new SelectionAdapter(){
      public void widgetSelected(SelectionEvent e) {
        listener.setText(text);
        listener.processPush();
      }});

    fButtons.put(buttonControl, key);

    ArrayList<Control> l = new ArrayList<Control>(Arrays.asList(labelledTextFields));
    l.add(buttonControl);

    if (addEnableButton) {
      createDependency(enableCheckBox, text);
      createDependency(enableCheckBox, buttonControl);
      l.add(enableCheckBox);
    }
    labelledTextFields = l.toArray(new Control[0]);
    return labelledTextFields;
  }

  public static final String getEnableCheckboxKeyFor(String key) {
    return key+CHECK_KEY_SUFFIX;
  }

  protected void createDependency(final Button master, final Control slave) {
    createDependency(master, new Control[] {slave});
  }

  protected void createInverseDependency(final Button master, final Control slave) {
    createInverseDependency(master, new Control[] {slave});
  }

  protected void createDependency(final Button master, final Control[] slaves) {
    createDependency(master, slaves, false);
  }

  protected void createInverseDependency(final Button master, final Control[] slaves) {
    createDependency(master, slaves, true);
  }

  /**
   * @param master
   * @param slaves
   * @param inverse if <b>true</b> the slaves are enabled if master is disabled
   */
  protected void createDependency(final Button master, final Control[] slaves, final boolean inverse) {
    Assert.isTrue(slaves.length > 0);
    indent(slaves[0]);
    SelectionListener listener= new SelectionListener() {
      public void widgetSelected(SelectionEvent e) {
        boolean state= master.getSelection();
        if (inverse) {
          state = !state;
        }
        for (Control control : slaves) {
          enableControl(control, state);
        }
      }

      public void widgetDefaultSelected(SelectionEvent e) {}
    };
    master.addSelectionListener(listener);
    fMasterSlaveListeners.add(listener);
  }

  protected void enableControl(Control control, boolean enabled) {
    if (control != null) {
      control.setEnabled(enabled);
    }
  }

  protected static void indent(Control control) {
    if (control == null)
      return;
    Object layoutData = control.getLayoutData();
    if (layoutData != null && layoutData instanceof GridData)
      ((GridData) layoutData).horizontalIndent+= INDENT;
  }

  public void initialize() {
    initializeFields();
  }

  private void initializeFields() {

    for (Entry<Button,String> entry : fCheckBoxes.entrySet()) {
      Button button = entry.getKey();
      String key = entry.getValue();
      button.setSelection(fStore.getBoolean(key));
    }

    for (Entry<Text,String> entry : fTextFields.entrySet()) {
      Text t = entry.getKey();
      String key = entry.getValue();
      t.setText(fStore.getString(key));
    }

    for (Entry<Combo,String> entry : fComboBoxes.entrySet()) {
      Combo comboBox = entry.getKey();
      String key = entry.getValue();
      String currValue = fStore.getString(key);
      ControlData data = (ControlData) comboBox.getData();
      comboBox.select(data.getSelection(currValue));
    }

    updateSlaves();
  }

  public void updateSlaves() {
    // Update slaves
    for (SelectionListener listener : fMasterSlaveListeners) {
      listener.widgetSelected(null);
    }

    updateStatus(new StatusInfo());
  }

  public void performOk() {
  }

  public void performDefaults() {
    initializeFields();
  }

  IStatus getStatus() {
    if (fStatus == null)
      fStatus= new StatusInfo();
    return fStatus;
  }

  /*
   * @see org.eclipse.jdt.internal.ui.preferences.IPreferenceConfigurationBlock#dispose()
   * @since 3.0
   */
  public void dispose() {
  }

  private void numberFieldChanged(Text textControl) {
    String number= textControl.getText();
    IStatus status= validatePositiveNumber(number);
    if (!status.matches(IStatus.ERROR))
      fStore.setValue((String) fTextFields.get(textControl), number);
    updateStatus(status);
  }

  private IStatus validatePositiveNumber(String number) {
    StatusInfo status= new StatusInfo();
    if (number.length() == 0) {
      status.setError(Messages.AbstractConfigurationBlock_empty_input);
    } else {
      try {
        int value= Integer.parseInt(number);
        if (value < 0)
          status.setError(NLS.bind(Messages.AbstractConfigurationBlock_invalid_input_param, number));
      } catch (NumberFormatException e) {
        status.setError(NLS.bind(Messages.AbstractConfigurationBlock_invalid_input_param, number));
      }
    }
    return status;
  }

  protected void updateStatus(IStatus status) {
    if (fMainPage == null)
      return;
    fMainPage.setValid(status.isOK());
    applyToStatusLine(fMainPage, status);
  }

  protected final OverlayPreferenceStore getPreferenceStore() {
    return fStore;
  }

  protected Composite createSubsection(Composite parent, SectionManager manager, String label) {
    if (manager != null) {
      return manager.createSection(label);
    } else {
      Group group= new Group(parent, SWT.SHADOW_NONE);
      group.setText(label);
      GridData data= new GridData(SWT.FILL, SWT.CENTER, true, false);
      group.setLayoutData(data);
      return group;
    }
  }

  protected ExpandableComposite createStyleSection(Composite parent, String label, int nColumns) {
    ExpandableComposite excomposite= new ExpandableComposite(parent, SWT.NONE, ExpandableComposite.TWISTIE | ExpandableComposite.CLIENT_INDENT);
    excomposite.setText(label);
    excomposite.setExpanded(false);
    excomposite.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DIALOG_FONT));
    excomposite.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, nColumns, 1));
    excomposite.addExpansionListener(new ExpansionAdapter() {
      public void expansionStateChanged(ExpansionEvent e) {
        expandedStateChanged((ExpandableComposite) e.getSource());
      }
    });
    fExpandedComposites.add(excomposite);
    makeScrollableCompositeAware(excomposite);
    return excomposite;
  }

  protected final void expandedStateChanged(ExpandableComposite expandable) {
    ScrolledPageContent parentScrolledComposite= getParentScrolledComposite(expandable);
    if (parentScrolledComposite != null) {
      parentScrolledComposite.reflow(true);
    }
  }

  protected void restoreSectionExpansionStates(IDialogSettings settings) {
    int i= 0;
    for (ExpandableComposite excomposite : fExpandedComposites) {
      if (settings == null) {
        excomposite.setExpanded(i == 0); // only expand the first node by default
      } else {
        excomposite.setExpanded(settings.getBoolean(SETTINGS_EXPANDED + String.valueOf(i)));
      }
      i++;
    }
  }

  protected void storeSectionExpansionStates(IDialogSettings settings) {
    int i= 0;
    for (ExpandableComposite excomposite : fExpandedComposites) {
      settings.put(SETTINGS_EXPANDED + String.valueOf(i), excomposite.isExpanded());
      i++;
    }
  }

  /**
   * Applies the status to the status line of a dialog page.
   */
  public static void applyToStatusLine(DialogPage page, IStatus status) {
    String message= status.getMessage();
    switch (status.getSeverity()) {
      case IStatus.OK:
        page.setMessage(message, IMessageProvider.NONE);
        page.setErrorMessage(null);
        break;
      case IStatus.WARNING:
        page.setMessage(message, IMessageProvider.WARNING);
        page.setErrorMessage(null);
        break;
      case IStatus.INFO:
        page.setMessage(message, IMessageProvider.INFORMATION);
        page.setErrorMessage(null);
        break;
      default:
        if (message.length() == 0) {
          message= null;
        }
        page.setMessage(null);
        page.setErrorMessage(message);
        break;
    }
  }

  public static Composite createComposite(Composite composite, int columns) {
    return createComposite(composite, columns, 1);
  }

  public static Composite createComposite(Composite composite, int columns, int colspan) {
    return createComposite(composite, columns, false, colspan);
  }

  public static Composite createComposite(Composite composite, int columns, boolean makeColumnsEqualWidth, int colspan) {
    Composite local = new Composite(composite, SWT.NONE);
    applyGridLayout(columns, local, makeColumnsEqualWidth);
    GridData data = new GridData(GridData.FILL, GridData.BEGINNING, true, false);
    data.horizontalSpan = colspan;
    local.setLayoutData(data);
    return local;
  }

  public static Group createGroup(Composite composite, int columns, String text) {
    return createGroup(composite, columns, false, text, 1);
  }

  public static Group createGroup(Composite composite, int columns, boolean makeColumnsEqualWidth, String text, int colspan) {
    Group local = new Group(composite, SWT.NONE);
    if (text != null)
      local.setText(text);
    applyGridLayout(columns, local, makeColumnsEqualWidth);
    GridData data = new GridData(GridData.FILL, GridData.BEGINNING, true, false);
    data.horizontalSpan = colspan;
    local.setLayoutData(data);
    return local;
  }

  public static void applyGridLayout(int columns, Composite control) {
    applyGridLayout(columns, control, false);
  }
  public static void applyGridLayout(int columns, Composite control, boolean makeColumnsEqualWidth) {
    GridLayout layout = new GridLayout(columns, makeColumnsEqualWidth);
//    layout.marginWidth=0;
//    layout.marginHeight=0;
//    layout.marginHeight= pixelConverter.convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
//    layout.marginWidth= pixelConverter.convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
//    layout.verticalSpacing= pixelConverter.convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
//    layout.horizontalSpacing= pixelConverter.convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
    control.setLayout(layout);
  }

  public static Composite createTabItem(TabFolder tabFolder, String title) {
    TabItem tabItem = new TabItem(tabFolder, SWT.SINGLE);
    Composite composite = createComposite(tabFolder, 1);
    tabItem.setText(title);
    tabItem.setControl(composite);
    return composite;
  }
}
