package net.sourceforge.asneditor.ui.preferences.common;

import org.eclipse.swt.widgets.Text;

public interface ITextButtonListener {

  void setText(Text text);

  void processPush();

}
