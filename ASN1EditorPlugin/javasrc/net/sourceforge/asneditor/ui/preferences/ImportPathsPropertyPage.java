/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.ui.preferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.ASNNature;
import net.sourceforge.asneditor.ui.wizards.TypedViewerFilter;
import net.sourceforge.asneditor.util.ProjectUtils;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;
import org.eclipse.ui.dialogs.PropertyPage;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * Property page for the ASN.1 imports search path. 
 */
public class ImportPathsPropertyPage extends PropertyPage {
  private final String FILE_SEPARATOR = System.getProperty("file.separator", "/");
  /**
   * This is the property that has the ASN.1 path - associated with the project.
   */
  public static QualifiedName ASN_PATH_PROP = new QualifiedName(ASNEditorPlugin.class.getName(), "SOURCE_PATH");
  /**
   * This is the project we are editing
   */
  private IProject project;

  /**
   * This is the table to edit the ASN.1 path
   */
  private Table table;

  private IWorkspaceRoot workspaceRoot;

  protected LinkedList importPath;

  /**
   * Creates contents given its parent.
   */
  protected Control createContents(Composite p) {
    importPath = new LinkedList();

    workspaceRoot = ASNEditorPlugin.getWorkspace().getRoot();
    project = (IProject) getElement().getAdapter(IProject.class);

    Composite topComp = new Composite(p, SWT.NONE);
    GridLayout innerLayout = new GridLayout();
    innerLayout.numColumns = 2;
    innerLayout.marginHeight = 0;
    innerLayout.marginWidth = 0;
    topComp.setLayout(innerLayout);
    GridData gd = new GridData(GridData.FILL_BOTH);
    gd.horizontalSpan = 2;
    topComp.setLayoutData(gd);

    GridData data = new GridData();

    if (project != null) {

      //code to do a table...
      table = new Table(topComp, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION
          | SWT.HIDE_SELECTION | SWT.BORDER | SWT.MULTI);
      table.setHeaderVisible(true);
      table.setLinesVisible(false);

      TableColumn column = new TableColumn(table, SWT.NONE);
      column.setText("Import path");
      column.setWidth(300);
      column.setAlignment(SWT.LEFT);

      List paths = ProjectUtils.readImportPath(project, true, true);

      fillTableWithIResources(paths);

      data = new GridData(GridData.FILL_BOTH);
      data.widthHint = convertWidthInCharsToPixels(3);
      data.heightHint = convertHeightInCharsToPixels(10);
      table.setLayoutData(data);

      TableLayout tableLayout = new TableLayout();
      table.setLayout(tableLayout);

      // ----------------------- buttons
      Composite buttons = new Composite(topComp, SWT.NONE);
      buttons.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
      GridLayout layout = new GridLayout();
      layout.marginHeight = 0;
      layout.marginWidth = 0;
      buttons.setLayout(layout);

      // TODO implement or remove
//      Button buttonRestore = new Button(buttons, SWT.PUSH);
//      customizeRestoreDefaultPathButton(buttonRestore);

      Button buttonSearchPrj = new Button(buttons, SWT.PUSH);
      customizeSearchInProjectButton(buttonSearchPrj);

      Button buttonAdd = new Button(buttons, SWT.PUSH);
      customizeAddButton(buttonAdd);

      Button buttonRem = new Button(buttons, SWT.PUSH);
      customizeRemButton(buttonRem);

      Button buttonRemAll = new Button(buttons, SWT.PUSH);
      customizeRemoveAllButton(buttonRemAll);
    }
      
    return topComp;
  }

//  /**
//   * Creates button that when clicked gets the ASN.1 path from the server shell.
//   * 
//   * @param button
//   */
//  private void customizeRestoreDefaultPathButton(Button button) {
//    button.setText("Default");
//    button.setToolTipText("Add ASN1_PATH to the ASN.1 import folder");
//    button.addSelectionListener(new SelectionListener() {
//
//      public void widgetSelected(SelectionEvent e) {
//        try {
//          Set p = new HashSet();
//
//          // TODO implement this
//          List asnPath = Arrays.asList(new String[] {});
//          for (Iterator iter = asnPath.iterator(); iter.hasNext();) {
//            String element = ((String) iter.next()).trim();
//            if (element.length() > 0) {
//              File file = new File(element);
//              if (file.exists() && file.isDirectory()) {
//                p.add(element);
//              }
//            }
//          }
//
//          table.removeAll();
//
//          fillTableWithStrings(new ArrayList(p));
//
//        } catch (Exception e1) {
//          ASNEditorPlugin.log(e1.getMessage(), e1);
//        }
//      }
//
//      public void widgetDefaultSelected(SelectionEvent e) {
//      }
//
//    });
//    GridData data = new GridData();
//    data.verticalAlignment = GridData.END;
//    data.grabExcessHorizontalSpace = true;
//    data.horizontalAlignment = GridData.FILL;
//    button.setLayoutData(data);
//  }

  /**
   * Set actions for button and layout.
   * @param buttonSearchPrj
   */
  private void customizeSearchInProjectButton(Button buttonSearchPrj) {
    GridData data;
    buttonSearchPrj.setText("Search");
    buttonSearchPrj.setToolTipText("Search project for ASN.1 sources");
    buttonSearchPrj.addSelectionListener(new SelectionListener() {

      public void widgetSelected(SelectionEvent e) {
      	// get a list of all ASN.1 files from the project
      	List asnFiles = ProjectUtils.getAsnFiles(project);

      	// append their parents only once
      	HashSet containersHash = new HashSet();
      	Iterator it = asnFiles.iterator();
      	List containers = new LinkedList();
      	while (it.hasNext()) {
      	  IResource resource = (IResource) it.next();
      	  final boolean newValue = containersHash.add(resource.getParent());
      	  if (newValue)
      	  	containers.add(resource.getParent());
		}

      	// fill the list
      	fillTableWithIResources(containers);
      }

      public void widgetDefaultSelected(SelectionEvent e) {
      }

    });
    data = new GridData();
    data.horizontalAlignment = GridData.FILL;
    data.grabExcessHorizontalSpace = true;
    buttonSearchPrj.setLayoutData(data);
  }

  /**
   * Set actions for button and layout.
   * 
   * @param buttonAdd
   */
  private void customizeAddButton(final Button buttonAdd) {
    GridData data;
    buttonAdd.setText("Select");
    buttonAdd.setToolTipText("Select ASN.1 import folders");
    buttonAdd.addSelectionListener(new SelectionListener() {

      public void widgetSelected(SelectionEvent e) {
        final ArrayList newImportPath = openSourceContainerDialog(null);
        if (newImportPath != null) {
          fillTableWithIResources(newImportPath);
        }
      }

      public void widgetDefaultSelected(SelectionEvent e) {
      }

    });
    data = new GridData();
    data.grabExcessHorizontalSpace = true;
    data.horizontalAlignment = GridData.FILL;
    buttonAdd.setLayoutData(data);
  }

  /**
   * Set actions for button and layout.
   * @param buttonRem
   */
  private void customizeRemButton(Button buttonRem) {
    GridData data;
    buttonRem.setText("Remove");
    buttonRem.setToolTipText("Remove selected ASN.1 import folder");
    buttonRem.addSelectionListener(new SelectionListener() {

      public void widgetSelected(SelectionEvent e) {
        int[] indices = table.getSelectionIndices();
        Arrays.sort(indices); //just make sure it is sorted correctly
        for (int i = indices.length - 1; i >= 0; i--) {
          // remove from table
          final String itemText = table.getItem(indices[i]).getText();
          table.remove(indices[i]);
          // remove from list
          final IResource resource = getResourceFromString(itemText);
          importPath.remove(resource);
        }
      }

      public void widgetDefaultSelected(SelectionEvent e) {
      }

    });
    data = new GridData();
    data.horizontalAlignment = GridData.FILL;
    data.grabExcessHorizontalSpace = true;
    buttonRem.setLayoutData(data);
  }

  /**
   * Creates button to clear the path.
   * 
   * @param button
   */
  private void customizeRemoveAllButton(Button button) {
    button.setText("Clear");
    button.setToolTipText("Clear the ASN.1 import list");
    button.addSelectionListener(new SelectionListener() {

      public void widgetSelected(SelectionEvent e) {
        try {
          table.removeAll();
        } catch (Exception e1) {
          ASNEditorPlugin.log(e1.getMessage(), e1);
        }
      }

      public void widgetDefaultSelected(SelectionEvent e) {
      }

    });
    GridData data = new GridData();
    data.verticalAlignment = GridData.END;
    data.grabExcessHorizontalSpace = true;
    data.horizontalAlignment = GridData.FILL;
    button.setLayoutData(data);
  }

//  /**
//   * Appends the paths given to the table
//   * 
//   * @param paths
//   */
//  private void fillTableWithStrings(List paths) {
//    for (Iterator iter = paths.iterator(); iter.hasNext();) {
//      final String element = (String) iter.next();
//      TableItem item = new TableItem(table, SWT.NONE);
//      item.setText(new String[]{element});
//
//      final IResource resource = getResourceFromString(element);
//    	importPath.add(resource);
//    }
//  }

  /**
   * @param index
   * @param element
   * @return
   */
  private IResource getResourceFromString(final String element) {
      final int index = project.getName().length() + 1;

      final IResource resource;
      if (element.length() <= index) {
          resource = project;
      } else {
          final String memberName = element.substring(index);
          resource = project.findMember(memberName);
      }
      return resource;
  }
  
  /**
   * Apply only saves the new value. does not do code completion update.
   * 
   * @see org.eclipse.jface.preference.PreferencePage#performApply()
   */
  protected void performApply() {
    doIt(false);
  }

  /**
   * Saves values into the project and updates the code completion. 
   */
  public boolean performOk() {
    return doIt(true);
  }

  /**
   * Save the ASN.1 path - only updates model if asked to.
   * @return
   */
  private boolean doIt(boolean updatePath) {
    if (project != null) {
      try {
        ProjectUtils.saveImportPath(project, importPath);
        IProjectNature nature = project.getNature(ASNNature.ASN_NATURE_ID);

        if (updatePath) {
          if (nature instanceof ASNNature) {
//            ((ASNNature) nature).rebuildPath(s);
          }
        }

      } catch (Exception e) {
        ASNEditorPlugin.log("Unexpected error setting project properties", IStatus.ERROR, e);
      }
    }
    return true;
  }

  private ArrayList openSourceContainerDialog(IResource existing) {
		Class[] acceptedClasses= new Class[] { IProject.class, IFolder.class };
		List existingContainers= getExistingContainers(null);
		
		IProject[] allProjects= workspaceRoot.getProjects();
		ArrayList rejectedElements= new ArrayList(allProjects.length);
		IProject currProject= project.getProject();
		for (int i= 0; i < allProjects.length; i++) {
			if (!allProjects[i].equals(currProject)) {
				rejectedElements.add(allProjects[i]);
			}
		}

		ViewerFilter filter= new TypedViewerFilter(acceptedClasses, rejectedElements.toArray());
		
		ILabelProvider lp= new WorkbenchLabelProvider();
		ITreeContentProvider cp= new BaseWorkbenchContentProvider();

		final String title= "Import path selection";
		final String message= "Choose folders to be added to the import path";

        CheckedTreeSelectionDialog dialog = new CheckedTreeSelectionDialog(getShell(), lp, cp);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setInput(project.getProject().getParent());
        dialog.addFilter(filter);
        final IContainer[] selectedContainers = (IContainer[]) existingContainers.toArray(new IContainer[existingContainers.size()]);
        dialog.setInitialSelections(selectedContainers);
        dialog.setExpandedElements(selectedContainers);

//        MultipleFolderSelectionDialog dialog= new MultipleFolderSelectionDialog(getShell(), lp, cp);
//		dialog.setInitialElementSelections(existingContainers);
//		dialog.setTitle(title);
//		dialog.setMessage(message);
//		dialog.addFilter(filter);
//		dialog.setInput(project.getProject().getParent());
//		dialog.setInitialFocus(project.getProject());
		if (dialog.open() == Window.OK) {
			Object[] elements = dialog.getResult();	
			ArrayList res = new ArrayList(Arrays.asList(elements));
			return res;
		}
		return null;
	}
	private List getExistingContainers(IResource existing) {
		List res= new ArrayList();
		Iterator it = importPath.iterator();
		while (it.hasNext()) {
			final IResource resource = (IResource) it.next();
			if (resource != existing) {
				if (resource instanceof IContainer) { // defensive code
					res.add(resource);
				}
			}
    }
		return res;
	}

	/**
	 * @param newImportPath
	 */
	private void fillTableWithIResources(final List newImportPath) {
		importPath.clear();
        if (newImportPath != null) {
          importPath.addAll(newImportPath);
        }
          try {
            table.removeAll();
          } catch (Exception e1) {
            ASNEditorPlugin.log(e1.getMessage(), e1);
          }
          Iterator it = importPath.iterator();
          while (it.hasNext()) {
            IResource resource = (IResource) it.next();
            TableItem item = new TableItem(table, SWT.NONE);
            final String tableItemText = getTableItemText(resource);
            item.setText(new String[]{tableItemText});
          }
	}

    /**
     * @param prjName
     * @param resource
     * @return
     */
    private String getTableItemText(IResource resource) {
        final String prjName = project.getName();
        return prjName + FILE_SEPARATOR + resource.getProjectRelativePath().toOSString();
    }
}
