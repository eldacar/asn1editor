/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 * Copyright (c) 2000, 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Scott Schlesier - Adapted for use in pydev
 *     Fabio Zadrozny 
 */
package net.sourceforge.asneditor.ui.preferences;

import java.util.HashMap;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.editors.ASNTextEnvironment;
import net.sourceforge.asneditor.editors.IASNSyntaxColorAndStyleConstants;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class ASNPreferencePage extends PreferencePage implements
		IWorkbenchPreferencePage, IASNPreferenceStoreConstants, IASNSyntaxColorAndStyleConstants {

	private List syntaxColorList;
	private ColorEditor colorEditor;
	private Button systemDefaultButton, boldButton, italicButton;
	private HashMap tmpPrefMap = new HashMap();

	public ASNPreferencePage() {
		setDescription("ASN.1 Editor syntax preferences");

		setPreferenceStore(ASNTextEnvironment.getInstance().getPreferenceStore());
	}

	/**
	 * Sets the default values of the preferences.
	 */
	private void initializeDefaults() {
		IPreferenceStore store = getPreferenceStore();
		loadDefaults(store);
	}

	/**
	 * @param store
	 */
	public static void loadDefaults(IPreferenceStore store) {
		PreferenceConverter.setDefault(store, EDITOR_COMMENT_COLOR, COMMENT_COLOR);
		store.setDefault(EDITOR_COMMENT_BOLD, COMMENT_BOLD);
		store.setDefault(EDITOR_COMMENT_ITALIC, COMMENT_ITALIC);
		
		PreferenceConverter.setDefault(store, EDITOR_KEYWORD_COLOR, KEYWORD_COLOR);
		store.setDefault(EDITOR_KEYWORD_BOLD, KEYWORD_BOLD);
		store.setDefault(EDITOR_KEYWORD_ITALIC, KEYWORD_ITALIC);
		
		PreferenceConverter.setDefault(store, EDITOR_TYPE_KEYWORD_COLOR, TYPE_KEYWORD_COLOR);
		store.setDefault(EDITOR_TYPE_KEYWORD_BOLD, TYPE_KEYWORD_BOLD);
		store.setDefault(EDITOR_TYPE_KEYWORD_ITALIC, TYPE_KEYWORD_ITALIC);
		
		PreferenceConverter.setDefault(store, EDITOR_CONSTANT_COLOR, CONSTANT_COLOR);
		store.setDefault(EDITOR_CONSTANT_BOLD, CONSTANT_BOLD);
		store.setDefault(EDITOR_CONSTANT_ITALIC, CONSTANT_ITALIC);

		PreferenceConverter.setDefault(store, EDITOR_STRING_COLOR, STRING_COLOR);
		store.setDefault(EDITOR_STRING_BOLD, STRING_BOLD);
		store.setDefault(EDITOR_STRING_ITALIC, STRING_ITALIC);
		
		PreferenceConverter.setDefault(store, EDITOR_DEFAULT_COLOR, DEFAULT_COLOR);
		store.setDefault(EDITOR_DEFAULT_BOLD, DEFAULT_BOLD);
		store.setDefault(EDITOR_DEFAULT_ITALIC, DEFAULT_ITALIC);
	}

	private void loadDefaults() {
		IPreferenceStore store = getPreferenceStore();

		store.setToDefault(EDITOR_COMMENT_COLOR);
		store.setToDefault(EDITOR_COMMENT_BOLD);
		store.setToDefault(EDITOR_COMMENT_ITALIC);
		
		store.setToDefault(EDITOR_KEYWORD_COLOR);
		store.setToDefault(EDITOR_KEYWORD_BOLD);
		store.setToDefault(EDITOR_KEYWORD_ITALIC);
		
		store.setToDefault(EDITOR_TYPE_KEYWORD_COLOR);
		store.setToDefault(EDITOR_TYPE_KEYWORD_BOLD);
		store.setToDefault(EDITOR_TYPE_KEYWORD_ITALIC);
		
		store.setToDefault(EDITOR_CONSTANT_COLOR);
		store.setToDefault(EDITOR_CONSTANT_BOLD);
		store.setToDefault(EDITOR_CONSTANT_ITALIC);

		store.setToDefault(EDITOR_STRING_COLOR);
		store.setToDefault(EDITOR_STRING_BOLD);
		store.setToDefault(EDITOR_STRING_ITALIC);
		
		store.setToDefault(EDITOR_DEFAULT_COLOR);
		store.setToDefault(EDITOR_DEFAULT_BOLD);
		store.setToDefault(EDITOR_DEFAULT_ITALIC);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
	/*
	 * @see PreferencePage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		super.createControl(parent);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
	 */
	protected Control createContents(Composite parent) {
		
//		initializeDefaultColors();
		
		//		store.load();
		//		store.start();
  
		Control control = createAppearancePage(parent);
		
		initialize();
		Dialog.applyDialogFont(control);
		return control;
	}

	
	private void initialize() {
//		initializeFields();

		loadTmpFromStore();

		for (int i= 0; i < editorHighlight.length; i++) {
			syntaxColorList.add(editorHighlight[i][0]);
		}

		syntaxColorList.getDisplay().asyncExec(new Runnable() {
			public void run() {
				if (syntaxColorList != null && !syntaxColorList.isDisposed()) {
					syntaxColorList.select(0);
					handleAppearanceColorListSelection();
				}
			}
		});
	}

	private void loadTmpFromStore() {
		IPreferenceStore store = getPreferenceStore();

		for (int i= 0; i < editorHighlight.length; i++) {
			// color
			RGB color = PreferenceConverter.getColor(store, editorHighlight[i][1]);
			tmpPrefMap.put(editorHighlight[i][1], color);
			// bold
			tmpPrefMap.put(editorHighlight[i][2], new Boolean(store.getBoolean(editorHighlight[i][2])));
			// italic
			tmpPrefMap.put(editorHighlight[i][3], new Boolean(store.getBoolean(editorHighlight[i][3])));
		}
	}

	private void saveTmpToStore() {
		IPreferenceStore store = getPreferenceStore();

		for (int i= 0; i < editorHighlight.length; i++) {
			// color
			RGB color = (RGB) tmpPrefMap.get(editorHighlight[i][1]);
			PreferenceConverter.setValue(store, editorHighlight[i][1], color);
			// bold
			boolean bold = ((Boolean)tmpPrefMap.get(editorHighlight[i][2])).booleanValue();
			store.setValue(editorHighlight[i][2], bold);
			// italic
			boolean italic = ((Boolean)tmpPrefMap.get(editorHighlight[i][3])).booleanValue();
			store.setValue(editorHighlight[i][3], italic);
		}
	}

	private Control createAppearancePage(Composite parent) {
		
		Composite appearanceComposite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		appearanceComposite.setLayout(layout);

    GridData data = new GridData(GridData.FILL, GridData.BEGINNING, true, false);
    appearanceComposite.setLayoutData(data);


//		addTextField(appearanceComposite, "Tab length:", TAB_WIDTH, 3, 0, true);
//		addCheckBox(appearanceComposite, "Highlight current line", AbstractDecoratedTextEditorPreferenceConstants.EDITOR_CURRENT_LINE, 0);

		Label l= new Label(appearanceComposite, SWT.LEFT );
		GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gd.horizontalSpan= 2;
		gd.heightHint= convertHeightInCharsToPixels(1) / 2;
		l.setLayoutData(gd);
		
		l= new Label(appearanceComposite, SWT.LEFT);
		l.setText("Syntax appearance options:"); 
		gd= new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gd.horizontalSpan= 2;
		l.setLayoutData(gd);
		
		Composite editorComposite= new Composite(appearanceComposite, SWT.NONE);
		layout= new GridLayout();
		layout.numColumns= 2;
		layout.marginHeight= 0;
		layout.marginWidth= 0;
		editorComposite.setLayout(layout);
		gd= new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.FILL_VERTICAL);
		gd.horizontalSpan= 2;
		editorComposite.setLayoutData(gd);		
		
		syntaxColorList= new List(editorComposite, SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER);
		gd= new GridData(GridData.VERTICAL_ALIGN_BEGINNING | GridData.FILL_BOTH);
		gd.heightHint= convertHeightInCharsToPixels(5);
		syntaxColorList.setLayoutData(gd);
		
		Composite stylesComposite= new Composite(editorComposite, SWT.NONE);
		layout= new GridLayout();
		layout.marginHeight= 0;
		layout.marginWidth= 0;
		layout.numColumns= 2;
		stylesComposite.setLayout(layout);
		stylesComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		l= new Label(stylesComposite, SWT.LEFT);
		l.setText("Color:"); 
		gd= new GridData();
		gd.horizontalAlignment= GridData.BEGINNING;
		l.setLayoutData(gd);
		
		colorEditor= new ColorEditor(stylesComposite);
		Button foregroundColorButton= colorEditor.getButton();
		gd= new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment= GridData.BEGINNING;
		foregroundColorButton.setLayoutData(gd);
		
		SelectionListener buttonsSelectionListener= new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				boolean systemDefault= systemDefaultButton.getSelection();
				colorEditor.getButton().setEnabled(!systemDefault);
				
				int i= syntaxColorList.getSelectionIndex();

				tmpPrefMap.put(editorHighlight[i][2], new Boolean(boldButton.getSelection()));
				tmpPrefMap.put(editorHighlight[i][3], new Boolean(italicButton.getSelection()));
//				tmpPrefMap.put(editorHighlight[i][4], new Boolean(systemDefault));
			}
			public void widgetDefaultSelected(SelectionEvent e) {}
		};
		
		systemDefaultButton = new Button(stylesComposite, SWT.CHECK);
		systemDefaultButton.setText("System default");
		gd= new GridData(GridData.FILL_HORIZONTAL);
//		gd.horizontalAlignment= GridData.BEGINNING;
		gd.horizontalSpan= 2;
		systemDefaultButton.setLayoutData(gd);
		systemDefaultButton.setEnabled(false);
		systemDefaultButton.setVisible(false);
		systemDefaultButton.addSelectionListener(buttonsSelectionListener);
		
		italicButton = new Button(stylesComposite, SWT.CHECK);
		italicButton.setText("Italic");
		gd= new GridData(GridData.FILL_HORIZONTAL);
//		gd.horizontalAlignment= GridData.BEGINNING;
		gd.horizontalSpan= 2;
		italicButton.setLayoutData(gd);
		italicButton.setEnabled(false);
		italicButton.addSelectionListener(buttonsSelectionListener);
		
		boldButton = new Button(stylesComposite, SWT.CHECK);
		boldButton.setText("Bold");
		gd= new GridData(GridData.FILL_HORIZONTAL);
//		gd.horizontalAlignment= GridData.BEGINNING;
		gd.horizontalSpan= 2;
		boldButton.setLayoutData(gd);
		boldButton.setEnabled(false);
		boldButton.addSelectionListener(buttonsSelectionListener);
		
		syntaxColorList.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}
			public void widgetSelected(SelectionEvent e) {
				handleAppearanceColorListSelection();
			}
		});
		foregroundColorButton.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}
			public void widgetSelected(SelectionEvent e) {
				int i= syntaxColorList.getSelectionIndex();
				tmpPrefMap.put(editorHighlight[i][1], colorEditor.getColorValue());
				tmpPrefMap.put(editorHighlight[i][2], new Boolean(boldButton.getSelection()));
				tmpPrefMap.put(editorHighlight[i][3], new Boolean(italicButton.getSelection()));
			}
		});
		
		return appearanceComposite;
	}
	private void handleAppearanceColorListSelection() {	
		int i= syntaxColorList.getSelectionIndex();

		RGB rgb= (RGB) tmpPrefMap.get(editorHighlight[i][1]);
		colorEditor.setColorValue(rgb);

		boldButton.setEnabled(true);
		italicButton.setEnabled(true);

		boolean bold   = ((Boolean)tmpPrefMap.get(editorHighlight[i][2])).booleanValue();
		boolean italic = ((Boolean)tmpPrefMap.get(editorHighlight[i][3])).booleanValue();

		boldButton.setSelection(bold);
		italicButton.setSelection(italic);
//		colorEditor.getButton().setEnabled(!systemDefault);
	}
	
	/*
	 * @see PreferencePage#performOk()
	 */
	public boolean performOk() {
		saveTmpToStore();
		ASNEditorPlugin.getDefault().savePluginPreferences();
		return true;
	}
	
	/*
	 * @see PreferencePage#performDefaults()
	 */
	protected void performDefaults() {
		loadDefaults();
		loadTmpFromStore();
		handleAppearanceColorListSelection();
		super.performDefaults();
	}

	/*
	 * @see DialogPage#dispose()
	 */
	public void dispose() {
		if (tmpPrefMap != null) {
			tmpPrefMap.clear();
			tmpPrefMap= null;
		}

		super.dispose();
	}
	
//	private Button addCheckBox(Composite parent, String label, String key, int indentation) {		
//		Button checkBox= new Button(parent, SWT.CHECK);
//		checkBox.setText(label);
//		
//		GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
//		gd.horizontalIndent= indentation;
//		gd.horizontalSpan= 2;
//		checkBox.setLayoutData(gd);
////		checkBox.addSelectionListener(fCheckBoxListener);
////		
////		fCheckBoxes.put(checkBox, key);
//		
//		return checkBox;
//	}
//	
//	private Control addTextField(Composite composite, String label, String key, int textLimit, int indentation, boolean isNumber) {
//		
//		Label labelControl= new Label(composite, SWT.NONE);
//		labelControl.setText(label);
//		GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
//		gd.horizontalIndent= indentation;
//		labelControl.setLayoutData(gd);
//		
//		Text textControl= new Text(composite, SWT.BORDER | SWT.SINGLE);		
//		gd= new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
//		gd.widthHint= convertWidthInCharsToPixels(textLimit + 1);
//		textControl.setLayoutData(gd);
//		textControl.setTextLimit(textLimit);
////		fTextFields.put(textControl, key);
////		if (isNumber) {
////			fNumberFields.add(textControl);
////			textControl.addModifyListener(fNumberFieldListener);
////		} else {
////			textControl.addModifyListener(fTextFieldListener);
////		}
//			
//		return textControl;
//	}
}
