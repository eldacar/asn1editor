/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.ui.preferences;


public interface IASNPreferenceStoreConstants {

	public static final String EDITOR_BROWSER_LIKE_LINKS = "__asn1_browser_like_links";

	public static final String EDITOR_COMMENT_COLOR = "__asn1_comment_color";
	public static final String EDITOR_COMMENT_BOLD  = "__asn1_comment_bold";
	public static final String EDITOR_COMMENT_ITALIC  = "__asn1_comment_italic";

	public static final String EDITOR_STRING_COLOR = "__asn1_string_color";
	public static final String EDITOR_STRING_BOLD = "__asn1_string_bold";
	public static final String EDITOR_STRING_ITALIC = "__asn1_string_italic";

	public static final String EDITOR_CONSTANT_COLOR = "__asn1_constant_color";
	public static final String EDITOR_CONSTANT_BOLD = "__asn1_constant_bold";
	public static final String EDITOR_CONSTANT_ITALIC = "__asn1_constant_italic";

	public static final String EDITOR_KEYWORD_COLOR = "__asn1_keyword_color";
	public static final String EDITOR_KEYWORD_BOLD = "__asn1_keyword_bold";
	public static final String EDITOR_KEYWORD_ITALIC = "__asn1_keyword_italic";

	public static final String EDITOR_TYPE_KEYWORD_COLOR = "__asn1_type_keyword_color";
	public static final String EDITOR_TYPE_KEYWORD_BOLD = "__asn1_type_keyword_bold";
	public static final String EDITOR_TYPE_KEYWORD_ITALIC = "__asn1_type_keyword_italic";

	public static final String EDITOR_DEFAULT_COLOR = "__asn1_other_color";
	public static final String EDITOR_DEFAULT_BOLD = "__asn1_other_bold";
	public static final String EDITOR_DEFAULT_ITALIC = "__asn1_other_italic";

	 // keep in same order as ColorToken.getTokens()
	public static final String[] preferences = new String[] {
			EDITOR_COMMENT_COLOR,      EDITOR_COMMENT_BOLD,      EDITOR_COMMENT_ITALIC,
			EDITOR_KEYWORD_COLOR,      EDITOR_KEYWORD_BOLD,      EDITOR_KEYWORD_ITALIC,
			EDITOR_TYPE_KEYWORD_COLOR, EDITOR_TYPE_KEYWORD_BOLD, EDITOR_TYPE_KEYWORD_ITALIC,
			EDITOR_CONSTANT_COLOR,     EDITOR_CONSTANT_BOLD,     EDITOR_CONSTANT_ITALIC,
			EDITOR_STRING_COLOR,       EDITOR_STRING_BOLD,       EDITOR_STRING_ITALIC,
			EDITOR_DEFAULT_COLOR,      EDITOR_DEFAULT_BOLD,      EDITOR_DEFAULT_ITALIC
	};
// TODO someday use them too
//	{"Code", CODE_COLOR, null},
//	{"Keywords", KEYWORD_COLOR, null},
//	{"Strings", STRING_COLOR, null},
//	{"Comments", COMMENT_COLOR, null},
//	{"Background", AbstractTextEditor.PREFERENCE_COLOR_BACKGROUND, AbstractTextEditor.PREFERENCE_COLOR_BACKGROUND_SYSTEM_DEFAULT},
//	{"Current line highlight", AbstractDecoratedTextEditorPreferenceConstants.EDITOR_CURRENT_LINE_COLOR, null},
//	{"Line numbers", AbstractDecoratedTextEditorPreferenceConstants.EDITOR_LINE_NUMBER_RULER_COLOR, null},		 
//	{"Print margin", AbstractDecoratedTextEditorPreferenceConstants.EDITOR_PRINT_MARGIN_COLOR, null}, 
//	{"Selection foreground", AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SELECTION_FOREGROUND_COLOR, AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SELECTION_FOREGROUND_DEFAULT_COLOR}, 
//	{"Selection background", AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SELECTION_BACKGROUND_COLOR, AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SELECTION_BACKGROUND_DEFAULT_COLOR}, 

	public static final String editorHighlight[][] = {
			{"Comment", EDITOR_COMMENT_COLOR, EDITOR_COMMENT_BOLD, EDITOR_COMMENT_ITALIC },
			{"Keyword", EDITOR_KEYWORD_COLOR, EDITOR_KEYWORD_BOLD, EDITOR_KEYWORD_ITALIC },
			{"Builtin types", EDITOR_TYPE_KEYWORD_COLOR, EDITOR_TYPE_KEYWORD_BOLD, EDITOR_TYPE_KEYWORD_ITALIC },
			{"Constants", EDITOR_CONSTANT_COLOR, EDITOR_CONSTANT_BOLD, EDITOR_CONSTANT_ITALIC },
			{"Strings", EDITOR_STRING_COLOR, EDITOR_STRING_BOLD, EDITOR_STRING_ITALIC },
			{"Default", EDITOR_DEFAULT_COLOR, EDITOR_DEFAULT_BOLD, EDITOR_DEFAULT_ITALIC } 
	};

}
