/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.ui.dialogfields;

/**
 * Change listener used by <code>ListDialogField</code> and <code>CheckedListDialogField</code>
 */
public interface IListAdapter {

	/**
	 * A button from the button bar has been pressed.
	 */
	void customButtonPressed(ListDialogField field, int index);

	/**
	 * The selection of the list has changed.
	 */
	void selectionChanged(ListDialogField field);

	/**
	 * En entry in the list has been double clicked
	 */
	void doubleClicked(ListDialogField field);

}
