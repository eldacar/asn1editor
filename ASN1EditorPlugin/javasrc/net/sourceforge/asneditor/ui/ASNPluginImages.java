/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.ui;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import net.sourceforge.asneditor.ASNEditorPlugin;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.osgi.framework.Bundle;

/**
 * Class containing and handling all used image descriptors.
 */
@SuppressWarnings("nls")
public class ASNPluginImages {

    public static final String ICONS_FOLDER = "icons";
    public static final String TOOL_FOLDER = "full/elcl16/";
    public static final String OBJ_FOLDER = "full/obj16/";

    public static final String TOOL_REFRESH = TOOL_FOLDER+"refresh.gif";
    public static final String TOOL_COLLAPSE_ALL = TOOL_FOLDER+"collapseall.gif";
    public static final String TOOL_EXPAND_ALL = TOOL_FOLDER+"expandall.gif";
    public static final String TOOL_SORT = TOOL_FOLDER+"sort.gif";

    public static final String TOOL_LOAD = TOOL_FOLDER+"msg.gif";
    public static final String TOOL_CONFIG = TOOL_FOLDER+"config.gif";

    public static final String OBJS_MODULE = OBJ_FOLDER+"module_obj.gif";
    public static final String OBJS_IMPDECL = OBJ_FOLDER+"imp_obj.gif";
    public static final String OBJS_IMPCONT = OBJ_FOLDER+"impc_obj.gif";
    public static final String OBJS_EXPDECL = OBJ_FOLDER+"exp_obj.gif";
    public static final String OBJS_EXPCONT = OBJ_FOLDER+"expc_obj.gif";
    public static final String OBJS_MODULE_DECL = OBJ_FOLDER+"module_decl_obj.gif";
    public static final String OBJS_TYPE = OBJ_FOLDER+"type_obj.gif";
    public static final String OBJS_VALUE = OBJ_FOLDER+"value_obj.gif";

    public static final ImageDescriptor DESC_TOOL_REFRESH = getImageDescriptor(TOOL_REFRESH);
    public static final ImageDescriptor DESC_TOOL_COLLAPSE_ALL = getImageDescriptor(TOOL_COLLAPSE_ALL);
    public static final ImageDescriptor DESC_TOOL_EXPAND_ALL = getImageDescriptor(TOOL_EXPAND_ALL);

    public static final ImageDescriptor DESC_TOOL_LOAD = getImageDescriptor(TOOL_LOAD);
    public static final ImageDescriptor DESC_TOOL_CONFIG = getImageDescriptor(TOOL_CONFIG);

    public static final ImageDescriptor DESC_OBJS_MODULE = getImageDescriptor(OBJS_MODULE);
    public static final ImageDescriptor DESC_OBJS_IMPDECL = getImageDescriptor(OBJS_IMPDECL);
    public static final ImageDescriptor DESC_OBJS_IMPCONT = getImageDescriptor(OBJS_IMPCONT);
    public static final ImageDescriptor DESC_OBJS_EXPDECL = getImageDescriptor(OBJS_EXPDECL);
    public static final ImageDescriptor DESC_OBJS_EXPCONT = getImageDescriptor(OBJS_EXPCONT);
    public static final ImageDescriptor DESC_OBJS_MODULE_DECL = getImageDescriptor(OBJS_MODULE_DECL);
    public static final ImageDescriptor DESC_OBJS_TYPE = getImageDescriptor(OBJS_TYPE);
    public static final ImageDescriptor DESC_OBJS_VALUE = getImageDescriptor(OBJS_VALUE);

    private static File getRelativePath(IPath relative) throws CoreException {
        Bundle bundle = ASNEditorPlugin.getDefault().getBundle();

        URL bundleURL = Platform.find(bundle, relative);
        URL fileURL;
        try {
            fileURL = Platform.asLocalURL(bundleURL);
            File f = new File(fileURL.getPath());

            return f;
        } catch (IOException e) {
            throw new CoreException(
                    new Status(IStatus.ERROR, ASNEditorPlugin.class.toString(), IStatus.ERROR,
                            "Can't find relative path"+(relative == null ? "null" : relative.toString()), null));
        }
    }

    public static File getIconPath(String icon) throws CoreException {
        IPath relative = new Path(ICONS_FOLDER).addTrailingSeparator().append(icon);
        return getRelativePath(relative);
    }

    public static void setImageDescriptors(IAction action, String iconRelativePath) {
    	ImageDescriptor id = getImageDescriptor(iconRelativePath);
    	if (id != null) {
    	    action.setDisabledImageDescriptor(id);
    	    action.setHoverImageDescriptor(id);
    	    action.setImageDescriptor(id);
    	}
    }
    /**
     * @param iconRelativePath
     * @return
     */
    public static ImageDescriptor getImageDescriptor(String iconRelativePath) {
        ImageDescriptor id;
        try {
			final File iconFile = getIconPath(iconRelativePath);
			id = ImageDescriptor.createFromURL(iconFile.toURL());
		} catch (Exception e) {
			id = ImageDescriptor.getMissingImageDescriptor();
		}
        return id;
    }
}
