/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import org.eclipse.jface.text.*;
import org.eclipse.jface.text.rules.*;
import org.eclipse.swt.graphics.Color;

public class ASNStringScanner extends RuleBasedScanner {

	public ASNStringScanner(ColorManager manager) {
		final Color stringColor = manager.getColor(IASNSyntaxColorAndStyleConstants.STRING_COLOR);
    IToken string = new Token(new TextAttribute(stringColor));

		IRule[] rules = new IRule[2];

		// Add rule for double quotes
		rules[0] = new SingleLineRule("\"", "\"", string, '\\');
		// Add generic whitespace rule.
		rules[1] = new WhitespaceRule(new ASNWhitespaceDetector());

		setRules(rules);
	}
}
