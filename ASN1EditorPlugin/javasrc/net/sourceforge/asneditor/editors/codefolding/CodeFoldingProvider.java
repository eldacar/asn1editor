/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors.codefolding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.editors.ASNEditor;
import net.sourceforge.asneditor.model.ASNModule;
import net.sourceforge.asneditor.model.IASN;
import net.sourceforge.asneditor.model.manager.ASNModelUtils;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotationModel;
import org.eclipse.jface.text.source.projection.ProjectionViewer;
import org.eclipse.ui.IPropertyListener;

public class CodeFoldingProvider implements IPropertyListener {
  	private Annotation[] oldAnnotations; // old ProjectionAnnotations
  	private HashMap oldAnnotationHash; // used only in createPartControl()
    private ASNEditor editor;

    /**
     * @param editor
     */
    public CodeFoldingProvider(ASNEditor editor) {
        this.editor = editor;
    		oldAnnotationHash = new HashMap();
    }

    public void updateFoldingStructure(IDocument doc, String catagory) throws BadPositionCategoryException {
      final IResource resource = ASNEditorPlugin.findResource(editor.getEditorInput());
      final Collection<ASNModule> models = ASNModelUtils.getModulesForResource(resource);

  		ArrayList annotations = new ArrayList();

  		ProjectionViewer viewer =(ProjectionViewer)editor.getViewer();
  		ProjectionAnnotationModel annotationModel = viewer.getProjectionAnnotationModel();

  		//this will hold the new annotations along
  		//with their corresponding positions
  		oldAnnotationHash = new HashMap();

  		for (ASNModule module : models) {

  		  // TODO needed folding the whole module ?
//		  annotations.add(createFolding(doc, module));
  		  annotations.add(createFolding(doc, module.getImportsItem()));
  		  annotations.add(createFolding(doc, module.getExportsItem()));

  		  Iterator asnTypesIterator = module.asnTypesIterator();
  		  while (asnTypesIterator.hasNext()) {
  		    final IASN type = (IASN) asnTypesIterator.next();
  		    annotations.add(createFolding(doc, type));
  		  }

  		  Iterator asnValesIterator = module.asnValuesIterator();
  		  while (asnValesIterator.hasNext()) {
  		    final IASN value = (IASN) asnValesIterator.next();
  		    annotations.add(createFolding(doc, value));
  		  }
        
      }

  		if (annotationModel != null)
  			annotationModel.modifyAnnotations(oldAnnotations, oldAnnotationHash, null);
  		oldAnnotations = (Annotation[]) annotations.toArray(new Annotation[annotations.size()]);
  	}

  	/**
     * @param element
     */
    private ProjectionAnnotation createFolding(IDocument doc, IASN element) {
	    final Position newPosition = computeFoldingPosition(doc, element);
      if (newPosition == null)
          return null;
			final ProjectionAnnotation annotation = new ProjectionAnnotation();
			// TODO remove ? or keep ?
//			annotation.setRangeIndication(true);
			oldAnnotationHash.put(annotation, newPosition);
			return annotation;
    }

    /**
     * @param position
     * @param doc
     * @return
     */
    private Position computeFoldingPosition(IDocument doc, IASN asnItem) {
	    if (asnItem == null)
	        return null;
			final Position position = asnItem.getPosition();
	    if (position == null)
	        return null;

	    try {
        final int startLine = doc.getLineOfOffset(position.offset);
        final int endOffset = ASNEditorPlugin.findOffset(asnItem.getEndLine(), asnItem.getEndColumn(), editor.getViewer());
        int endLine = doc.getLineOfOffset(endOffset);

//		    if (startLine >= endLine)
//		        return null;
//	
//		    String line = null;
//		    boolean emptyLine = true;
//		    do {
//		        line = doc.get(doc.getLineOffset(endLine), doc.getLineLength(endLine));
//		        if (line != null) {
//		            line = line.trim();
//		            emptyLine = ((line.length() == 0) || line.startsWith(IASNConstants.COMMENT_TOKEN));
//		            emptyLine |= ((i+1 == positions.length) && (line.startsWith("END") || line.endsWith("END")));
//		        } else {
//		            emptyLine = true;
//		        }
//		        endLine--;
//		    } while (emptyLine);
//		    endLine++;
        
        if (startLine >= endLine)
            return null;
        
        final Position newPosition = new Position(position.offset, doc.getLineOffset(endLine) + doc.getLineLength(endLine) - position.offset);
        return newPosition;
	    } catch (BadLocationException e) {
        // TODO: handle exception
	    }
	    return null;
    }

    /**
  	 * @return
  	 */
  	public HashMap getAnnotationHash() {
  	    return oldAnnotationHash;
  	}

  	/* (non-Javadoc)
     * @see org.eclipse.ui.IPropertyListener#propertyChanged(java.lang.Object, int)
     */
    public void propertyChanged(Object source, int propId) {
        // TODO do this
//        if(propId == IASNPreferenceStoreConstants.EDITOR_FOLDING_CHANGED) {
//            editor.createModel();
//        }
    }
}
