/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * Copyright (c) 2003, 2004 Chris Leung. All rights reserved. 
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors.outline;

import org.eclipse.jface.viewers.StructuredSelection;

/**
 * Interface to allow incremental find action on the implementation object.
 * 
 * @author chrisl
 */
public interface IIncrementalFindTreeTarget {

	public boolean canPerformFind();
	public boolean isEditable();
	public StructuredSelection getSelection();
	/** Get text representation of the currently selected item. */
	public String getSelectionText();
	public void setSelection(Object object);
	/** Find item with the given string and select it. */
	public int findAndSelect(int start, String findString, boolean searchForward, boolean caseSensitive);
	/** Begin an incremental find session. This should be called before any increment find action can proceed. */
	public void beginSession(boolean forward,String defaultString);
	/** End an incremental find session. This should be called at the end of every incremental find session.*/
	public void endSession();
}
