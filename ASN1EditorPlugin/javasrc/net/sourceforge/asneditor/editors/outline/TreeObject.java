/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * Copyright (c) 2003 Chris Leung. All rights reserved. 
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors.outline;

import net.sourceforge.asneditor.ui.ASNPluginImages;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class TreeObject implements IAdaptable {

	public static final int TEXT = 0;
	public static final int FOLDER = 1;
	public static final int MODULE = 2;
	public static final int MODULE_DECLARATION = 3;
	public static final int IMPORT_DECLARATION = 4;
	public static final int IMPORT_CONTAINER = 5;
	public static final int EXPORT_DECLARATION = 6;
	public static final int EXPORT_CONTAINER = 7;
	public static final int TYPE = 8;
	public static final int VALUE = 9;

	@SuppressWarnings("nls")
	public static final String[] KIND_NAMES = new String[]{
	"Text", "Folder", "Module", "ModuleDeclaration", "ImportDeclaration",
	"ImportContainer", "ExportDeclaration", "ExportContainer", "Type", "Value"
	};

	/** Status code. */
	protected static final int ERROR = 1;

	protected static final Image[] ICONS;
	protected static Image ICON_ERROR;
	protected static Image ICON_OPEN_FOLDER;
	static {
		ICONS = new Image[] {
			PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FILE),
			PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER),
			ASNPluginImages.DESC_OBJS_MODULE.createImage(),
			ASNPluginImages.DESC_OBJS_MODULE_DECL.createImage(),
			ASNPluginImages.DESC_OBJS_IMPDECL.createImage(), // IMPORT_DECLARATION
			ASNPluginImages.DESC_OBJS_IMPCONT.createImage(), //IMPORT_CONTAINER
			ASNPluginImages.DESC_OBJS_EXPDECL.createImage(), // EXPORT_DECLARATION
			ASNPluginImages.DESC_OBJS_EXPCONT.createImage(), // EXPORT_CONTAINER
			ASNPluginImages.DESC_OBJS_TYPE.createImage(),
			ASNPluginImages.DESC_OBJS_VALUE.createImage()
			};
		ICON_ERROR = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_ERROR_TSK);
		ICON_OPEN_FOLDER = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER);
	}

	protected String fText;
	protected Object fElement;
	protected TreeFolder parent;
	protected int fStatus;
	protected int fKind;

	////////////////////////////////////////////////////////////////////////

	public TreeObject(int kind, String name, Object element) {
		fText = name;
		fElement = element;
		fKind = kind;
	}
	public TreeObject(String name, Object element) {
		this(TEXT, name, element);
		if (fElement != null && fElement instanceof NamedPosition)
			fKind = ((NamedPosition) fElement).getKind();
	}

	public Object getElement() {
		return fElement;
	}

	public void setParent(TreeFolder parent) {
		this.parent = parent;
	}

	public TreeFolder getParent() {
		return parent;
	}

	public String toString() {
		return (fText == null) ? "" : fText; //$NON-NLS-1$
	}

	public Object getAdapter(Class key) {
		return null;
	}

	public Image getImage() {
		if ((fStatus & ERROR) != 0) {
			return ICON_ERROR;
		}
		return ICONS[fKind];
	}
}
