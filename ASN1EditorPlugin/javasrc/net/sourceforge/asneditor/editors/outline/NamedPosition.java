/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * Copyright (c) 2003 Chris Leung. All rights reserved. 
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors.outline;

import java.util.Comparator;

import org.eclipse.jface.text.Position;

public class NamedPosition extends Position {

	/**
	 * Constructor for NamedPosition.
	 * @param offset
	 */
	public NamedPosition(int offset) {
		super(offset);
	}

	/**
	 * Constructor for NamedPosition.
	 * @param offset
	 * @param length
	 */
	public NamedPosition(int offset, int length) {
		super(offset, length);
	}

	public NamedPosition(String name, int offset, int length) {
		super(offset, length);
		fName = name;
	}

	public NamedPosition(int kind, String name, int offset, int length) {
		super(offset, length);
		fKind = kind;
		fName = name;
	}

	////////////////////////////////////////////////////////////////////////                                

	private static DescendingComparator fDescendingComparator;
	private static AscendingComparator fAscendingComparator;

	protected int fKind;
	protected String fName;
	protected Object fData;

	////////////////////////////////////////////////////////////////////////                                

	public static DescendingComparator getDescendingComparator() {
		if (fDescendingComparator == null)
			fDescendingComparator = new DescendingComparator();
		return fDescendingComparator;
	}

	public static AscendingComparator getAscendingComparator() {
		if (fAscendingComparator == null)
			fAscendingComparator = new AscendingComparator();
		return fAscendingComparator;
	}

	////////////////////////////////////////////////////////////////////////                                

	public void init(int kind, String name) {
		fKind = kind;
		fName = name;
	}

	public int getKind() {
		return fKind;
	}

	public void setKind(int kind) {
		fKind = kind;
	}

	public void setName(String name) {
		fName = name;
	}

	public String getName() {
		return fName;
	}

	public void setData(Object a) {
		fData = a;
	}

	public Object getData() {
		return fData;
	}

	public String toString() {
		return fName;
	}

	public boolean equals(Object a) {
		if (a == null || !(a instanceof NamedPosition))
			return false;
		NamedPosition aa = (NamedPosition) a;
		return (
			fKind == aa.fKind
				&& offset == aa.offset
				&& length == aa.length
				&& (fName == null && aa.fName == null || fName.equals(aa.fName))
				&& (fData == null && aa.fData == null || fData.equals(aa.fData)));
	}

	////////////////////////////////////////////////////////////////////////                                

	/**
	 * Sorted positions in descending offset order.
	 * @author chrisl
	 */
	public static class DescendingComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			Position a = (Position) o1;
			Position b = (Position) o2;
			if (a.offset > b.offset)
				return -1;
			if (a.offset < b.offset)
				return 1;
			return 0;
		}
	}

	/**
	 * Sorted positions in descending offset order.
	 * @author chrisl
	 */
	public static class AscendingComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			Position a = (Position) o1;
			Position b = (Position) o2;
			if (a.offset > b.offset)
				return 1;
			if (a.offset < b.offset)
				return -1;
			return 0;
		}
	}

	////////////////////////////////////////////////////////////////////////                                

}
