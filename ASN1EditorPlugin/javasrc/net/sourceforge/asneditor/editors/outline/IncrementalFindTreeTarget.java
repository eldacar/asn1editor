/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * Copyright (c) 2003, 2004 Chris Leung. All rights reserved. 
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 */ 
package net.sourceforge.asneditor.editors.outline;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import net.sourceforge.asneditor.ASNEditorPlugin;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Control;


/**
 * An incremental find target for StructuredViewer with an ILabelProvider. Replace is always disabled.
 */
public class IncrementalFindTreeTarget implements IIncrementalFindTreeTarget, KeyListener, MouseListener, FocusListener
//, ITextListener 
{

	////////////////////////////////////////////////////////////////////////

	private static final String NAME = "StructuredIncrementalFindTarget";
	/** The string representing rendered tab */
	// TODO
	private final static String TAB = "  ";//UtilPlugin.getString("FindIncremental.render.tab"); //$NON-NLS-1$
	private final static String PATTERN_NOTFOUND = "nomatch";//UtilPlugin.getString("FindIncremental.pattern.not_found"); //$NON-NLS-1$
	private final static String PATTERN_FOUND = "match";//UtilPlugin.getString("FindIncremental.pattern.found"); //$NON-NLS-1$
	private final static String PROMPT = "prompt";//UtilPlugin.getString("FindIncremental.prompt"); //$NON-NLS-1$
	/** A constant representing a find-next operation */
	private static final int NEXT = 1;
	/** A constant representing a find-previous operation */
	private static final int PREVIOUS = 2;
	/** A constant representing adding a character to the find pattern */
	private static final int CHAR = 3;
	/** A constant representing a wrap operation */
	private static final Object WRAPPED = new Object();
	private static final boolean DEBUG = false;

	private static String fSavedString = "";

	////////////////////////////////////////////////////////////////////////

	/** The text viewer to operate on */
	private final AbstractTreeViewer fViewer;
	/** The status line manager for output */
	private final IStatusLineManager fStatusLine;
	/** The find replace target to delegate find requests */
	//private final IFindReplaceTarget fTarget;

	/** The current find string */
	private StringBuffer fFindString = new StringBuffer();
	/** Index of first upper case char. in the find string. -1 if no upper case.*/
	private int fCasePosition;
	/** The position to start the find from */
	private int fBasePosition;
	/** The position of the last successful find */
	private int fCurrentIndex;
	/** A flag indicating if last find was successful */
	private boolean fFound;
	private boolean fForward;
	/** A flag indicating listeners are installed. */
	private boolean fInstalled;
	private Object[] fElements;
	private ILabelProvider fLabelProvider;
	private ITreeContentProvider fContentProvider;
	private KeyListener fPrevKeyListener;
	/** Index stack to backtrace when taking back characters.*/
	private Stack fStack;

	////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an instance of an incremental find target.
	 * 
	 * @param viewer the text viewer to operate on
	 * @param manager the status line manager for output
	 */
	public IncrementalFindTreeTarget(AbstractTreeViewer viewer, IStatusLineManager manager, KeyListener prev) {
		fViewer = viewer;
		fStatusLine = manager;
		fPrevKeyListener = prev;
		fStack = new Stack();
	}

	// IIncrementalFindTarget interface ////////////////////////////////////
	//

	/*
	 * @see IFindReplaceTarget#canPerformFind()
	 */
	public boolean canPerformFind() {
		return true;
	}

	public boolean isEditable() {
		return false;
	}

	public StructuredSelection getSelection() {
		return (StructuredSelection) fViewer.getSelection();
	}

	/** Get text representation of the currently selected item.
	 */
	public String getSelectionText() {
		StructuredSelection selection = getSelection();
		if (selection == null)
			return null;
		if (selection.isEmpty())
			return null;
		return fLabelProvider.getText(selection.getFirstElement());
	}

	/*
	 */
	public void setSelection(Object object) {
		fViewer.setSelection(new StructuredSelection(object), true);
		//fViewer.refresh();
	}

	/** Find item with the given string and select it.
	 * 
	 * @return The index of the selected item or -1.
	 * @param start  The index to start search from inclusively.
	 * @param findString  The target string.
	 * @param forward
	 * @param caseSensitive
	 */
	public int findAndSelect(int start, String findString, boolean forward, boolean caseSensitive) {
		int index = -1;
		if (!caseSensitive)
			findString = findString.toLowerCase();
		int step = forward ? 1 : -1;
		if (!forward && start >= 0)
			--start;
		for (int i = start; i >= 0 && i < fElements.length; i += step) {
			String s = fLabelProvider.getText(fElements[i]);
			if (!caseSensitive)
				s = s.toLowerCase();
			if (s.indexOf(findString) != -1) {
				index = i;
				break;
			}
		}
		if (index != -1)
			setSelection(fElements[index]);
		return index;
	}

	/*
	 * @see IFindReplaceTargetExtension#beginSession()
	 */
	public void beginSession(boolean forward, String defaultString) {
		if (DEBUG)
			msgDebug(".beginSession()");
		IContentProvider cp = fViewer.getContentProvider();
		if (cp instanceof ITreeContentProvider)
			fContentProvider = (ITreeContentProvider) cp;
		else
			return;
		IBaseLabelProvider p = fViewer.getLabelProvider();
		if (p instanceof ILabelProvider)
			fLabelProvider = (ILabelProvider) p;
		else
			return;
		install();
		//
		fForward = forward;
		if (defaultString != null)
			fSavedString = defaultString;
		fElements =
			getTreeElements(new Object[] { fViewer.getInput()}, fViewer.getSorter(), new ArrayList())
				.toArray();
		//			getTreeElements(fContentProvider.getElements(null), fViewer.getSorter(), new ArrayList())
		//				.toArray();
		if (fElements.length == 0) {
			ASNEditorPlugin.log(NAME + ".beginSession(): no element found.");
			return;
		}
		fCasePosition = -1;
		if (DEBUG)
			msgDebug(".beginSession(): " + fElements.length + ", saved=" + fSavedString);
		fBasePosition = 0;
		StructuredSelection selection = (StructuredSelection) fViewer.getSelection();
		if (selection != null) {
			Object a = selection.getFirstElement();
			if (a != null) {
				for (int i = 0; i < fElements.length; ++i) {
					if (a == fElements[i]) {
						fBasePosition = i;
						break;
					}
				}
			}
		}
		fCurrentIndex = fBasePosition;
		fFound = true;
		statusMessage(PROMPT);
	}

	/*
	 * @see IFindReplaceTargetExtension#endSession()
	 */
	public void endSession() {
		if (DEBUG)
			msgDebug(".endSession()");
		if (fInstalled) {
			if (fFindString.length() > 0)
				fSavedString = fFindString.toString();
			uninstall();
		}
		fFindString.setLength(0);
		statusClear();
	}

	////////////////////////////////////////////////////////////////////////

	/**
	 * Installs this target. I.e. adds all required listeners.
	 */
	private void install() {
		if (fInstalled)
			return;
		Control control = fViewer.getControl();
		if (fPrevKeyListener != null)
			control.removeKeyListener(fPrevKeyListener);
		control.addKeyListener(this);
		control.addMouseListener(this);
		control.addFocusListener(this);
		fInstalled = true;
	}

	/**
	 * Uninstalls itself. I.e. removes all listeners installed in <code>install</code>.
	 */
	private void uninstall() {
		if (!fInstalled)
			return;
		Control control = fViewer.getControl();
		//control.removeFocusListener(this);
		control.removeMouseListener(this);
		control.removeKeyListener(this);
		if (fPrevKeyListener != null)
			control.addKeyListener(fPrevKeyListener);
		fInstalled = false;
	}

	////////////////////////////////////////////////////////////////////////

	/**
	 * Returns whether the find string can be found using the given options.
	 * 
	 * @param forward the search direction
	 * @param position the start offset
	 * @param wrapSearch    should the search wrap to start/end if end/start is reached
	 * @param takeBack is the find string shortend
	 * @return <code>true</code> if find string can be found using the options
	 */
	private boolean performFindNext(boolean forward, int start, boolean wrapSearch, boolean takeBack) {
		String string = fFindString.toString();
		if (string.length() == 0)
			string = fSavedString;
		if (string.length() == 0) {
			fCurrentIndex = start;
			return false;
		}
		// If current index is not valid, start from start.
		if (start < 0 || start >= fElements.length)
			start = 0;
		int index = findIndex(string, start, forward, fCasePosition != -1, wrapSearch, takeBack);
		if (index != -1) {
			fCurrentIndex = index;
			fFound = true;
		} else {
			fFound = false;
		}
		if (DEBUG)
			System.out.println(
				NAME
					+ ".performFindNext(): forward="
					+ forward
					+ ", string="
					+ string
					+ ", found="
					+ fFound
					+ ", match="
					+ getSelectionText());
		return fFound;
	}

	/**
	 * Retuns the offset at which the search string is found next using the given options
	 * 
	 * @param findString the string to search for
	 * @param startPosition the start offset
	 * @param forward the search direction
	 * @param caseSensitive is the search case sensitive
	 * @param wrapSearch    should the search wrap to start/end if end/start is reached
	 * @param takeBack is the find string shortend
	 * @return the offset of the next match or <code>-1</code>
	 */
	private int findIndex(
		String findString,
		int startPosition,
		boolean forward,
		boolean caseSensitive,
		boolean wrapSearch,
		boolean takeBack) {

		int index = findAndSelect(startPosition, findString, forward, caseSensitive);
		if (index != -1)
			return index;
		// Not found, beep once
		if (!takeBack)
			beep();
		if (!wrapSearch)
			return -1;
		// Before wrap search, stop once.
		if (fFound)
			return -1;
		// Wrap search
		startPosition = forward ? 0 : fElements.length - 1;
		index = findAndSelect(startPosition, findString, forward, caseSensitive);
		return index;
	}

	/**
	 * Updates the status line appropriate for the indicated success.
	 * @param found the success
	 */
	private void updateStatus(boolean found) {
		String string = fFindString.toString();
		String prefix = ""; //$NON-NLS-1$
		if (!found) {
			statusError(MessageFormat.format(PATTERN_NOTFOUND, new Object[] { prefix, string }));
		} else {
			statusMessage(MessageFormat.format(PATTERN_FOUND, new Object[] { prefix, string }));
		}
	}

	////////////////////////////////////////////////////////////////////////

	/**
	 * Perform increment find.  All non-printable characters need to be escaped.
	 * TODO: escape non-printable characters.
	 * 
	 * @see KeyListener#keyPressed(Event)
	 */
	public void keyPressed(KeyEvent event) {
		//NOTE:
		// event.character=='' and event.keycode==0 for most keys and key combinations
		// not named in SWT. eg. no keycode for keypad keys, window key ... etc!!!
		if (DEBUG)
			System.err.println(
				NAME
					+ ".keyPressed(): state="
					+ event.stateMask
					+ ", char="
					+ event.character
					+ "keycode="
					+ event.keyCode);
		if (event.stateMask == 0 && event.character == 0) {
			switch (event.keyCode) {
				case SWT.HOME :
					fCurrentIndex = 0;
					setSelection(fElements[0]);
					break;
				case SWT.END :
					fCurrentIndex = fElements.length - 1;
					setSelection(fElements[fCurrentIndex]);
					break;
				case SWT.ALT :
				case SWT.ARROW_LEFT :
				case SWT.ARROW_RIGHT :
				case SWT.PAGE_DOWN :
				case SWT.PAGE_UP :
				case SWT.ARROW_DOWN :
				case SWT.ARROW_UP :
				default :
					break;
			}
			updateStatus(fFound);
			return;
		}
		// C-r,C-s are trapped.  For now, use 'M-C-r' and 'M-C-s' instead.
		if ((event.stateMask & SWT.ALT) != 0) {
			// if (event.stateMask == (SWT.CTRL | SWT.ALT | SWT.SHIFT) && event.character == 0x73) {
			if (event.keyCode == SWT.ARROW_DOWN) {
				// 'C-s' forward
				if (fFindString.length() == 0)
					fFindString.append(fSavedString);
				fForward = true;
				performFindNext(true, fCurrentIndex + 1, true, false);
			} else if (event.keyCode == SWT.ARROW_UP) {
				fForward = false;
				if (fFindString.length() == 0)
					fFindString.append(fSavedString);
				performFindNext(false, fCurrentIndex, true, false);
			}
			updateStatus(fFound);
			return;
		}
		// Characters.
		switch (event.character) {
			case 0x1B :
			case 0x0D :
				// ESC, CR = quit
				endSession();
				break;
			case 0x08 :
			case 0x7F :
				// backspace and delete
				if (fFindString.length() > 0) {
					popChar();
				} else if (fFindString.length() == 0) {
					setSelection(fElements[fBasePosition]);
				} else
					beep();
				updateStatus(fFound);
				break;
			default :
				if (event.stateMask == 0 || event.stateMask == SWT.SHIFT) {
					pushChar(event.character);
					performFindNext(fForward, fCurrentIndex, false, false);
				}
				updateStatus(fFound);
		}
	}

	public void keyReleased(KeyEvent event) {
		//		if ((event.stateMask & SWT.CTRL) != 0) {
		//			// if (event.stateMask == (SWT.ALT | SWT.CTRL | SWT.SHIFT) && event.character == 0x72) {
		//			// 'C-r' and 'M-C-r' keyPressed() are trapped by eclipse.
		//			// For now, we use 'M-C-r' keyReleased() instead.
		//			if (event.keyCode == SWT.ARROW_UP) {
		//				fForward = false;
		//				if (fFindString.length() == 0)
		//					fFindString.append(fSavedString);
		//				performFindNext(false, fCurrentIndex, true, false);
		//				updateStatus(fFound);
		//				return;
		//			}
		//		}
	}

	/**
	 * Extends the incremental search by the given character.
	 * @param character the character to append to the searhc string
	 */
	private void pushChar(char character) {
		if (fCasePosition == -1
			&& Character.isUpperCase(character)
			&& Character.toLowerCase(character) != character)
			fCasePosition = fFindString.length();
		fFindString.append(character);
		fStack.push(new Integer(fCurrentIndex));
	}

	/**
	 * Delete a character from the find string.
	 * @return Old length of find string.
	 */
	private int popChar() {
		int len = fFindString.length();
		if (len > 0) {
			fFindString.deleteCharAt(--len);
			if (fCasePosition == len)
				fCasePosition = -1;
		}
		// When findString is empty, go back to base position regardless.
		if (len == 0) {
			fCurrentIndex = fBasePosition;
			fStack.clear();
		} else if (fStack.empty())
			return -1;
		else
			fCurrentIndex = ((Integer) fStack.pop()).intValue();
		setSelection(fElements[fCurrentIndex]);
		return fCurrentIndex;
	}

	private void beep() {
		Control control = fViewer.getControl();
		if (control != null && !control.isDisposed())
			control.getDisplay().beep();
	}

	////////////////////////////////////////////////////////////////////////

	/*
	 * @see ITextListener#textChanged(TextEvent)
	 */
	//	public void textChanged(TextEvent event) {
	//		endSession();
	//	}

	////////////////////////////////////////////////////////////////////////

	/*
	 * @see MouseListener#mouseDoubleClick(MouseEvent)
	 */
	public void mouseDoubleClick(MouseEvent e) {
		endSession();
	}

	/*
	 * @see MouseListener#mouseDown(MouseEvent)
	 */
	public void mouseDown(MouseEvent e) {
		endSession();
	}

	/*
	 * @see MouseListener#mouseUp(MouseEvent)
	 */
	public void mouseUp(MouseEvent e) {
	}

	////////////////////////////////////////////////////////////////////////

	/*
	 * @see FocusListener#focusGained(FocusEvent)
	 */
	public void focusGained(FocusEvent e) {
		if (false)
			msgDebug(".focusGrained()");
	}

	/*
	 * @see FocusListener#focusLost(FocusEvent)
	 */
	public void focusLost(FocusEvent e) {
		if (false)
			msgDebug(".focusLost()");
		//FIXME: 'M-C-r' caused advertise focus lost, so just ignore focus change for now.
		//endSession();
	}

	////////////////////////////////////////////////////////////////////////

	/** Get an ordered list of elements in a tree. 
	 */
	private List getTreeElements(Object[] parents, ViewerSorter sorter, List ret) {
		Object[] aa = parents;
		if (sorter != null)
			sorter.sort(fViewer, aa);
		for (int i = 0; i < aa.length; ++i) {
			ret.add(aa[i]);
			getTreeElements(fContentProvider.getChildren(aa[i]), sorter, ret);
		}
		return ret;
	}

	////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the given string as status message, clears the status error message.
	 * @param string the status message
	 */
	private void statusMessage(String string) {
		fStatusLine.setErrorMessage(""); //$NON-NLS-1$
		fStatusLine.setMessage(escapeTabs(string));
	}

	/**
	 * Sets the status error message, clears the status message.
	 * @param string the status error message
	 */
	private void statusError(String string) {
		fStatusLine.setErrorMessage(escapeTabs(string));
		fStatusLine.setMessage(""); //$NON-NLS-1$
	}

	/**
	 * Clears the status message and the status error message.
	 */
	private void statusClear() {
		fStatusLine.setErrorMessage(""); //$NON-NLS-1$
		fStatusLine.setMessage(" "); //$NON-NLS-1$
	}

	/**
	 * Translates all tab characters into a proper status line presentation.
	 * @param string the string in which to translate the tabs
	 * @return the given string with all tab characters replace with a proper status line presentation
	 */
	private String escapeTabs(String string) {
		StringBuffer buffer = new StringBuffer();

		int begin = 0;
		int end = string.indexOf('\t', begin);

		while (end >= 0) {
			buffer.append(string.substring(begin, end));
			buffer.append(TAB);
			begin = end + 1;
			end = string.indexOf('\t', begin);
		}
		buffer.append(string.substring(begin));

		return buffer.toString();
	}

	private void msgDebug(String msg) {
		System.err.println(NAME + msg);
	}
}
