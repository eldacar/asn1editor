/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * Copyright (c) 2003 Chris Leung. All rights reserved. 
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors.outline;

import java.util.ArrayList;

public class TreeFolder extends TreeObject {

	private ArrayList children;

	public TreeFolder(int kind, String name, Object a) {
		super(kind, name, a);
		if (fKind == TEXT)
			fKind = FOLDER;
	}

	public TreeFolder(String name, Object a) {
		super(name, a);
		if (fKind == TEXT)
			fKind = FOLDER;
	}

	public void addChild(TreeObject child) {
		if (children == null)
			children = new ArrayList(4);
		children.add(child);
		child.setParent(this);
	}

	public void removeChild(TreeObject child) {
		if (children == null)
			return;
		children.remove(child);
		child.setParent(null);
	}

	public void removeChildren() {
		if (children == null)
			return;
		Object a;
		for (int i = 0; i < children.size(); ++i) {
			a = children.get(i);
			if (a instanceof TreeObject) {
				((TreeObject) a).setParent(null);
			} else {
				((TreeFolder) a).removeChildren();
				((TreeFolder) a).setParent(null);
			}
		}
		children.clear();
	}

	public int getNumChildren() {
		if (children == null)
			return 0;
		return children.size();
	}

	public TreeObject getChild(int i) {
		return (TreeObject) children.get(i);
	}

	public TreeObject[] getChildren() {
		if (children == null)
			return new TreeObject[0];
		return (TreeObject[]) children.toArray(new TreeObject[children.size()]);
	}

	public boolean hasChildren() {
		return children!=null && children.size() > 0;
	}

}
