/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors.outline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.editors.ASNEditor;
import net.sourceforge.asneditor.model.ASNModule;
import net.sourceforge.asneditor.model.IASN;
import net.sourceforge.asneditor.model.SymbolsFromModule;
import net.sourceforge.asneditor.model.manager.ASNModelUtils;
import net.sourceforge.asneditor.model.manager.ProjectManager;
import net.sourceforge.asneditor.model.manager.WorkspaceManager;
import net.sourceforge.asneditor.ui.ASNPluginImages;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.DefaultPositionUpdater;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.editors.text.EditorsPlugin;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;


/**
 * ContentOutlinePage for JavaccEditor to use the Eclipse Outline view to present an outline of terminal and
 * non-terminals in the document.
 */
@SuppressWarnings("nls")
public class ASNOutlinePage extends Page
	implements
		IContentOutlinePage,
		ISelectionChangedListener,
		KeyListener,
		ISelectionListener,
		IPropertyChangeListener {

	////////////////////////////////////////////////////////////////////////

	private static final String CONTEXT_MENU_ID = "ASNOutlinePage";

	public static final String ID = ASNOutlinePage.class.toString();
	public static final int SHOW_IMPORTS_EXPORTS = 0x01;
	public static final int SHOW_VALUES = 0x02;
	public static final int SHOW_TYPES = 0x04;
	public static final int SHOW_ALL = 0x07;

	private static int fDefaultShowLevel = SHOW_ALL;

	////////////////////////////////////////////////////////////////////////

	private Clipboard fClipboard;
	private ASNEditor fEditor;
	private ISourceViewer fEditorViewer;
	private Collection<ASNModule> fInput;

	private TreeViewer fViewer;
	private ASNOutlineContentProvider fProvider;
	private IPartListener fPartListener;
	private List fInTextOrder;
	private ViewerSorter fSorter;
	private DefaultPositionUpdater fPositionUpdater;

	private Action fActionSort;
	private Action fActionShowImports;
	private Action fActionShowValues;
	private Action fActionShowTypes;
	private Action fActionRefresh;
	private Action fActionGoto;
	private Action fActionExpandAll;
	private Action fActionCollapseAll;
	private Action fActionExpandItem;
	private Action fActionCollapseItem;
	private List fSelectionListeners;

	private IncrementalFindTreeTarget findTarget;
	private int fShowLevel;
//	private PrevAnnotation fActionPreviousAnnotation;
//	private NextAnnotation fActionNextAnnotation;

	public ASNOutlinePage(ASNEditor editor) {
		super();
		fEditor = editor;
		//
		fEditorViewer = editor.getViewer();
		fShowLevel = fDefaultShowLevel;
		fSelectionListeners = new ArrayList();
		fInTextOrder = new ArrayList();
		fSorter = new NameSorter();
		fProvider = new ASNOutlineContentProvider();
	}

	/**
	 * @see org.eclipse.ui.part.IPageBookViewPage#init(IPageSite)
	 */
	public void init(IPageSite site) {
		super.init(site);
	}

	/**
	 * @see IWorkbenchPart#dispose()
	 */
	public void dispose() {
		EditorsPlugin.getDefault().getPreferenceStore().removePropertyChangeListener(this);
		disableSyncSelection();
		fEditor.outlinePageDisposed();
		super.dispose();
		fClipboard = null;
		fEditor = null;
		fEditorViewer = null;
		fInput = null;
		fViewer = null;
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	public void createControl(Composite parent) {
		fViewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		fClipboard = new Clipboard(parent.getDisplay());
		fViewer.setContentProvider(fProvider);
		fViewer.setLabelProvider(fProvider);
		fViewer.setInput(fProvider.getRoot());
		getSite().setSelectionProvider(fViewer);
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		// hookPartListener();
		hookKeyListener();
		contributeToActionBars();
		IPreferenceStore prefs = EditorsPlugin.getDefault().getPreferenceStore();
//		if (prefs.getBoolean(IConstants.PREF_JAVACC_SYNC_SELECTION)) {
			enableSyncSelection();
//		}
		prefs.addPropertyChangeListener(this);
		IResource editorInputResource = fEditor.getEditorInputResource();
		Collection<ASNModule> modulesForResource = ASNModelUtils.getModulesForResource(editorInputResource);
    setInput(modulesForResource);
	}

	//	public void setInput(ASNEditor editor) {
	//		if (DEBUG)
	//			System.err.println(
	//				NAME
	//					+ ".setInput(): editor="
	//					+ (editor == null ? "null" : editor.getEditorInput().getName()));
	//		if (editor == null) {
	//			fViewer.collapseAll();
	//			if (fEditorViewer != null) {
	//				removePositions(fEditor, fEditorViewer);
	//			}
	//			fEditor = null;
	//			fEditorViewer = null;
	//			fInput = null;
	//			refresh();
	//		} else if (editor != fEditor) {
	//			if (editor.getModel() == null)
	//				editor.createModel();
	//			setInput(editor, editor.getViewer(), editor.getModel());
	//		}
	//	}

	public void setInput(Collection<ASNModule> input) {
		if (fInput != null && input == fInput)
			return;
		if (!isValidInput(input)) {
			setInput(null);
			return;
		}
		//fViewer.collapseAll();
		fInput = input;
		refresh();
		//fViewer.reveal(fProvider.getTop());
	}

	public ITextEditor getEditor() {
		return fEditor;
	}

	private void refresh() {
	  ASNEditorPlugin.runSynchronous(new Runnable(){
      public void run() {

        IStructuredSelection selection = (IStructuredSelection) fViewer.getSelection();
        TreeObject a = null;
        if (selection != null && !selection.isEmpty()) {
          a = (TreeObject) selection.getFirstElement();
        }
        fViewer.setSelection(StructuredSelection.EMPTY);
        fInTextOrder.clear();
        if (fInput != null) {
          final TreeFolder folder = createOutlineModel();
          //
          fProvider.setInput(folder);
        } else {
          fProvider.setInput(new TreeFolder("Outline not available", null));
        }
        // Added folders are collapsed by default, expand interesting ones.
        // NOTE: Need to refresh() before expandTree(), otherwise, collapsing to object do not work.
        fViewer.setInput(fProvider.getRoot());
        fViewer.refresh();
        fProvider.expandTree(fViewer);
        if (fEditorViewer != null && a != null) {
          a = findElement(a);
          if (a != null)
            fViewer.setSelection(new StructuredSelection(a), true);
        }
      }});
	}

	public TreeViewer getViewer() {
		return fViewer;
	}

	/**
	 * @see org.eclipse.ui.part.IPage#getControl()
	 */
	public Control getControl() {
		return fViewer.getControl();
	}

	/**
	 * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#getTreeViewer()
	 */
	protected TreeViewer getTreeViewer() {
		return fViewer;
	}

	public TreeObject findElement(TreeObject a) {
		return fProvider.findElement(a);
	}

	public void setShowLevel(int level) {
		this.fShowLevel = level;
		fDefaultShowLevel = fShowLevel;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		fViewer.getControl().setFocus();
	}

	private void makeActions() {
		fActionSort = new Action() {
			public void run() {
				setToolTipText(isChecked()
					? "Sort in document location order"
					: "Sort in lexical order");
				sortAction(isChecked());
			}
		};
		fActionSort.setChecked(false);
		fActionSort.setText("Sort in lexical order");
		fActionSort.setToolTipText("Sort in lexical order");
    ASNPluginImages.setImageDescriptors(fActionSort, ASNPluginImages.TOOL_SORT);

		fActionShowImports = new Action() {
			public void run() {
				showImportsExportsAction(isChecked());
			}
		};
		fActionShowImports.setChecked((fShowLevel & SHOW_IMPORTS_EXPORTS) != 0);
		fActionShowImports.setText("Show Imports and Exports");
		fActionShowImports.setToolTipText("Show imports and exports");
		fActionShowImports.setImageDescriptor(ASNPluginImages.DESC_OBJS_IMPCONT);

		fActionShowTypes = new Action() {
			public void run() {
				showTypeAction(isChecked());
			}
		};
		fActionShowTypes.setChecked((fShowLevel & SHOW_TYPES) != 0);
		fActionShowTypes.setText("Show Types");
		fActionShowTypes.setToolTipText("Show types");
		fActionShowTypes.setImageDescriptor(ASNPluginImages.DESC_OBJS_TYPE);

		fActionShowValues = new Action() {
			public void run() {
				showValueAction(isChecked());
			}
		};
		fActionShowValues.setChecked((fShowLevel & SHOW_VALUES) != 0);
		fActionShowValues.setText("Show Values");
		fActionShowValues.setToolTipText("Show values");
		fActionShowValues.setImageDescriptor(ASNPluginImages.DESC_OBJS_VALUE);

		fActionGoto = new Action() {
			public void run() {
				setSelectionToEditor((IStructuredSelection) fViewer.getSelection());
			}
		};
		fActionGoto.setText("Goto");
		fActionGoto.setToolTipText("Goto item");

		fActionRefresh = new Action() {
			public void run() {
				refreshAction();
			}
		};
		fActionRefresh.setText("Refresh");
		fActionRefresh.setToolTipText("Refresh");
//		fActionRefresh.setAccelerator(Action.convertAccelerator("F5"));
		ASNPluginImages.setImageDescriptors(fActionRefresh, ASNPluginImages.TOOL_REFRESH);

		fActionExpandAll = new Action() {
			public void run() {
		    getViewer().expandAll();
			}
		};
		fActionExpandAll.setText("Expand All");
		fActionExpandAll.setToolTipText("Expand all");
		ASNPluginImages.setImageDescriptors(fActionExpandAll, ASNPluginImages.TOOL_EXPAND_ALL);

		fActionCollapseAll = new Action() {
			public void run() {
		    getViewer().collapseAll();
			}
		};
		fActionCollapseAll.setText("Collapse All");
		fActionCollapseAll.setToolTipText("Collapse all");
		ASNPluginImages.setImageDescriptors(fActionCollapseAll, ASNPluginImages.TOOL_COLLAPSE_ALL);

		fActionExpandItem = new Action() {
		    public void run() {
		        IStructuredSelection selection = (IStructuredSelection) getSelection();
		        if (selection != null)
		            for (Iterator it = selection.iterator(); it.hasNext();)
		                expandItemAllChildrenAction(it.next());
		    }
		};
		fActionExpandItem.setText("Expand Item");
		fActionExpandItem.setToolTipText("Expand items children");
//		ASNPluginImages.setImageDescriptors(fActionExpandItem, ASNPluginImages.TOOL_EXPAND_ALL);

		fActionCollapseItem = new Action() {
		    public void run() {
		        IStructuredSelection selection = (IStructuredSelection) getSelection();
		        if (selection != null)
		            for (Iterator it = selection.iterator(); it.hasNext();)
		                collapseItemAllChildrenAction(it.next());
		    }
		};
		fActionCollapseItem.setText("Collapse Item");
		fActionCollapseItem.setToolTipText("Collapse items children");
//		ASNPluginImages.setImageDescriptors(fActionCollapseItem, ASNPluginImages.TOOL_COLLAPSE_ALL);
	}

	private void contributeToActionBars() {
		IActionBars bars = getSite().getActionBars();
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(fActionShowImports);
		manager.add(fActionShowTypes);
		manager.add(fActionShowValues);
		manager.add(new Separator());
		manager.add(fActionRefresh);
		manager.add(fActionSort);
		manager.add(fActionCollapseAll);
		manager.add(fActionExpandAll);
		manager.add(new Separator("Additions"));
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager(CONTEXT_MENU_ID);
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ASNOutlinePage.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(fViewer.getControl());
		fViewer.getControl().setMenu(menu);
		// Register menu so that other plugin can contribute to it.
		getSite().registerContextMenu(CONTEXT_MENU_ID, menuMgr, fViewer);
	}

	void fillContextMenu(IMenuManager manager) {
		manager.add(fActionExpandAll);
		manager.add(fActionCollapseAll);
		manager.add(fActionExpandItem);
		manager.add(fActionCollapseItem);
		manager.add(new Separator());
		manager.add(fActionRefresh);
		manager.add(fActionSort);
		manager.add(new Separator());
		manager.add(fActionShowImports);
		manager.add(fActionShowTypes);
		manager.add(fActionShowValues);
		manager.add(new Separator("Additions"));
	}

	private void hookDoubleClickAction() {
		fViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				setSelectionToEditor((IStructuredSelection) fViewer.getSelection());
			}
		});
	}

	/** Temporary hack to do the incremental search. Should move to StructuredViewer later. */
	private void hookKeyListener() {
		fViewer.getControl().addKeyListener(this);
	}

	// Actions /////////////////////////////////////////////////////////////
	//

	void showValueAction(boolean on) {
		int level = fShowLevel | SHOW_VALUES;
		setShowLevel(on ? level : (level ^ SHOW_VALUES));
		refresh();
	}

	void showTypeAction(boolean on) {
		int level = fShowLevel | SHOW_TYPES;
		setShowLevel(on ? level : (level ^ SHOW_TYPES));
		refresh();
	}

	void showImportsExportsAction(boolean on) {
		int level = fShowLevel | SHOW_IMPORTS_EXPORTS;
		setShowLevel(on ? level : (level ^ SHOW_IMPORTS_EXPORTS));
		refresh();
	}

	void refreshAction() {
	  IResource editorInputResource = fEditor.getEditorInputResource();
    ProjectManager projectManager = WorkspaceManager.getProjectManager(editorInputResource.getProject());
	  projectManager.rebuild(editorInputResource);
	}

	void sortAction(boolean on) {
		if (fViewer == null)
			return;
		fViewer.setSorter(on ? fSorter : null);
	}

	void setSelectionToEditor(IStructuredSelection selection) {
//		IStructuredSelection selection = (IStructuredSelection) fViewer.getSelection();
		if (selection == null || selection.isEmpty())
			return;
		Object a = selection.getFirstElement();
		if (!(a instanceof TreeObject))
			return;
		Object element = ((TreeObject) a).getElement();
		if (element == null || !(element instanceof IASN))
			return;
		final Position position = ((IASN)element).getPosition();
		if (position == null)
			return;
		int len = position.length;
		if (a instanceof TreeFolder)
			len = 0;

		fEditorViewer.setSelectedRange(position.offset, len);
		fEditorViewer.revealRange(position.offset, len);
	}

	void expandItemAllChildrenAction(Object a) {
		fViewer.expandToLevel(a, TreeViewer.ALL_LEVELS);
	}

	void collapseItemAllChildrenAction(Object a) {
		fViewer.collapseToLevel(a, TreeViewer.ALL_LEVELS);
	}

	// ISelectionChangeListener /////////////////////////////////////////

	/**
	 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(SelectionChangedEvent)
	 */
	public void selectionChanged(SelectionChangedEvent event) {
		final ISelection selection = event.getSelection();
		setSelection(selection);
		setSelectionToEditor((IStructuredSelection) selection);
	}

	/**
	 * @see org.eclipse.jface.viewers.ISelectionProvider#addSelectionChangedListener(ISelectionChangedListener)
	 */
	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		fSelectionListeners.add(listener);
	}

	/**
	 * @see org.eclipse.jface.viewers.ISelectionProvider#removeSelectionChangedListener(ISelectionChangedListener)
	 */
	public void removeSelectionChangedListener(ISelectionChangedListener listener) {
		fSelectionListeners.remove(listener);
	}

	/**
	 * @see org.eclipse.jface.viewers.ISelectionProvider#setSelection(ISelection)
	 */
	public void setSelection(ISelection selection) {
		if (fViewer != null)
			fViewer.setSelection(selection);
	}

	public ISelection getSelection() {
		if (fViewer == null)
			return StructuredSelection.EMPTY;
		return (IStructuredSelection) fViewer.getSelection();
	}

	/**
	 * Fires a selection changed event.
	 * 
	 * @param selction the new selection
	 */
	protected void fireSelectionChanged(ISelection selection) {
		// create an event
		SelectionChangedEvent event = new SelectionChangedEvent(this, selection);
		// fire the event
		for (int i = 0; i < fSelectionListeners.size(); ++i) {
			((ISelectionChangedListener) fSelectionListeners.get(i)).selectionChanged(event);
		}
	}

	// ISelectionListener //////////////////////////////////////////////////

	/**
	 * @see ISelectionListener#selectionChanged(IWorkbenchPart, ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (part == fEditor) {
			setSelectionFromEditor(selection, true);
		} else if (selection instanceof IStructuredSelection) {
			setSelectionToEditor((IStructuredSelection) selection);
		}
	}

	// IPropertyChangeListener /////////////////////////////////////////////

	/**
	 * @see org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent event) {
		// TODO repair
//		if (IConstants.PREF_JAVACC_SYNC_SELECTION.equals(event.getProperty())) {
//			Boolean enable = (Boolean) event.getNewValue();
//			if (enable.booleanValue())
//				enableSyncSelection();
//			else
//				disableSyncSelection();
//		}
	}

	private void enableSyncSelection() {
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);
	}

	private void disableSyncSelection() {
		getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(this);
	}

	// KeyListener ///////////////////////////////////////////////////////

	public void keyPressed(KeyEvent event) {
		// if (event.stateMask == (SWT.CTRL | SWT.ALT | SWT.SHIFT) && event.character == 0x73) {
		if (event.stateMask == 0 && event.character == 0x73) {
			// 'C-s' is trapped by eclipse. For now, use 'M-C-s' instead.
			beginSession(true);
		} else if (event.stateMask == 0 && event.character == 0x72) {
			// 'C-r' keyPressed() for 'M-C-r' are trapped by eclipse. For now, use 'M-C-r'
			// keyReleased() instead.
			beginSession(false);
		}
	}
	public void keyReleased(KeyEvent event) {
		// if (event.stateMask == (SWT.CTRL | SWT.ALT | SWT.SHIFT) && event.character == 0x72) {
	}

	public void beginSession(boolean forward) {
		findTarget = new IncrementalFindTreeTarget(fViewer, getSite()
			.getActionBars()
			.getStatusLineManager(), this);
		String defaultString = null;
		//		IStructuredSelection selection = getSelection();
		//		if (selection != null && !selection.isEmpty()) {
		//			Object a = selection.getFirstElement();
		//			IJavaElement element = ((JavaElementItem) a).getElement();
		//			defaultString = element.getElementName();
		//		}
		findTarget.beginSession(forward, defaultString);
	}

	// Update View on editor activation ///////////////////////////

	public void setSelectionFromEditor(ISelection selection, boolean reveal) {
		if (selection == null)
			return;
		if (!(selection instanceof ITextSelection))
			return;
		if (((ITextSelection) selection).getLength() == 0)
			return;
		final int offset = ((ITextSelection) selection).getOffset();
		final TreeObject a = findElementAtOffset(offset);
//		System.err.println(NAME + ".setSelectionFromEditor(): offset=" + offset + ", a=" + a);
		if (a == null)
			fViewer.setSelection(StructuredSelection.EMPTY);
		else
			fViewer.setSelection(new StructuredSelection(a), reveal);
	}

	//	void setInputFromEditor(IWorkbenchPart part) {
	//		if (true)
	//			System.err.println(NAME + ".setInputFromEditor(): part=" + part);
	//		if (!(part instanceof ASNEditor)) {
	//			setInput(null);
	//			return;
	//		}
	//		ASNEditor editor = (ASNEditor) part;
	//		if (editor == fEditor)
	//			return;
	//		setInput(editor);
	//	}

	/**
	 * Answers if the given <code>element</code> is a valid element for this part. For MethodView only
	 * IType is valid.
	 * 
	 * @param element the object to test
	 * @return <true>if the given element is a valid element
	 */
	protected boolean isValidInput(Object element) {
		return (element == null || element instanceof Collection);
	}

	private TreeFolder createOutlineModel() {
    final TreeFolder modelNode = new TreeFolder("", null);

    if (fInput == null) {
      return modelNode;
    }

    for (ASNModule module : fInput) {
      final TreeFolder moduleNode = new TreeFolder(TreeObject.MODULE, module.getName(), module);
			fInTextOrder.add(moduleNode);
      modelNode.addChild(moduleNode);

			// imports
			if ((fShowLevel & SHOW_IMPORTS_EXPORTS) != 0) {
				Iterator imports = module.importSymbolFromModuleListIterator();
				final TreeFolder importRoot = new TreeFolder(TreeObject.IMPORT_CONTAINER, "imports", null);
				while (imports.hasNext()) {
					final Object next = imports.next();
					final SymbolsFromModule importedModule = (SymbolsFromModule) next;
					// MODULE
					final TreeFolder importNode = new TreeFolder(TreeObject.IMPORT_CONTAINER, importedModule.getName(), importedModule);
					fInTextOrder.add(importNode);
					importRoot.addChild(importNode);
					// SYMBOLS
					Iterator it = importedModule.symbols();
					while (it.hasNext()) {
				    String importedSymbol = (String) it.next();
						final TreeObject importedSymbolNode = new TreeObject(TreeObject.IMPORT_DECLARATION, importedSymbol, importedModule);
						fInTextOrder.add(importNode);
						importNode.addChild(importedSymbolNode);
					}
				}
				moduleNode.addChild(importRoot);

				// exports
				Iterator exports = module.exportSymbolListIterator();
				final TreeFolder exportRoot = new TreeFolder(TreeObject.EXPORT_CONTAINER, "exports", null);
				while (exports.hasNext()) {
					final String export = (String) exports.next();
					final TreeObject exportNode = new TreeObject(TreeObject.EXPORT_DECLARATION, export, module.getExportsItem());
					fInTextOrder.add(exportNode);
					exportRoot.addChild(exportNode);
				}
				moduleNode.addChild(exportRoot);
			}

			// types
			if ((fShowLevel & SHOW_TYPES) != 0) {
				Iterator types = module.asnTypesIterator();
				while (types.hasNext()) {
					final IASN type = (IASN) types.next();
					final TreeObject typeNode = new TreeObject(TreeObject.TYPE, type.getName(), type);
					fInTextOrder.add(typeNode);
					moduleNode.addChild(typeNode);
				}
			}

			// values
			if ((fShowLevel & SHOW_VALUES) != 0) {
				Iterator values = module.asnValuesIterator();
				while (values.hasNext()) {
					final IASN value = (IASN) values.next();
					final TreeObject valueNode = new TreeObject(TreeObject.VALUE, value.getName(), value);
					fInTextOrder.add(valueNode);
					moduleNode.addChild(valueNode);
				}
			}
		}

		Collections.sort(fInTextOrder, new Comparator() {
			public int compare(Object o1, Object o2) {
				final Object a = ((TreeObject) o1).getElement();
				final Object b = ((TreeObject) o2).getElement();
				if (a == null
					|| b == null
					|| !(a instanceof IASN)
					|| !(b instanceof IASN))
					return 0;
				
				final Position ap = (Position) ((IASN)a).getPosition();
				final Position bp = (Position) ((IASN)b).getPosition();
				if (ap == null)
					return -1;
				if (bp == null)
					return 1;
				if (ap.offset > bp.offset)
					return 1;
				if (ap.offset < bp.offset)
					return -1;
				return 0;
			}
		});
		// TODO 
//		for (int i = 0; i < fInTextOrder.size(); ++i) {
//			TreeFolder child = (TreeFolder) fInTextOrder.get(i);
//			folder.addChild(child);
////			fillReferences(child, doc);
//		}
		return modelNode;
	}

	/**
	 * @enter Tree element must be sorted in text order in source file.
	 */
	private TreeObject findElementAtOffset(int offset) {
		if (fInTextOrder == null || fInTextOrder.size() == 0)
			return null;
		TreeObject child;
		TreeObject ret = null;
		Position p = null;
		IASN asnItem = null;
		for (int i = 0; i < fInTextOrder.size(); ++i) {
			child = (TreeObject) fInTextOrder.get(i);
			asnItem = (IASN)child.getElement();
			if (asnItem == null)
				continue;
			p = (Position) asnItem.getPosition();
			if (p != null && p.offset <= offset) {
				if (p.offset + p.length > offset)
					ret = child;
			} else
				break;
		}
		return ret;
	}

	static class NameSorter extends ViewerSorter {
		public NameSorter() {
			super(null);
		}
		/**
		 * @see org.eclipse.ui.internal.dialogs.ViewSorter#compare(Viewer, Object, Object)
		 */
		public int compare(Viewer viewer, Object e1, Object e2) {
			TreeObject a = (TreeObject) e1;
			TreeObject b = (TreeObject) e2;
			int akind = a.fKind;
			int bkind = b.fKind;
			if (akind > bkind)
				return 1;
			if (bkind > akind)
				return -1;
			return a.toString().compareTo(b.toString());
		}
	}

}