/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors.outline;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;

public class ASNOutlineContentProvider extends LabelProvider implements ITreeContentProvider {

	private TreeFolder fInvisibleRoot;

	/**
	 * Constructor for MethodViewContentProvider.
	 */
	public ASNOutlineContentProvider() {
		super();
		fInvisibleRoot = new TreeFolder("", null);
	}

	public void setInput(TreeObject top) {
		fInvisibleRoot=(TreeFolder) top;
	}

	public Object getParent(Object child) {
		if (child instanceof TreeObject) {
			return ((TreeObject) child).getParent();
		}
		return null;
	}

	public Object[] getChildren(Object parent) {
		if (parent instanceof TreeFolder) {
			return ((TreeFolder) parent).getChildren();
		}
		return new Object[0];
	}

	public boolean hasChildren(Object parent) {
		if (parent instanceof TreeFolder)
			return ((TreeFolder) parent).hasChildren();
		return false;
	}

	public Object[] getElements(Object parent) {
		if (parent == null || parent == ResourcesPlugin.getWorkspace()) {
			return getChildren(fInvisibleRoot);
		}
		return getChildren(parent);
	}

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	public void dispose() {
	}

	public String getText(Object a) {
		return a.toString();
	}

	public Image getImage(Object a) {
		return ((TreeObject) a).getImage();
	}

	public TreeFolder getRoot() {
		return fInvisibleRoot;
	}

	public void expandTree(TreeViewer viewer) {
		viewer.expandToLevel(2);
	}

	public TreeObject findElement(TreeObject a) {
		final TreeObject resElem = findElement(a, fInvisibleRoot);
    return resElem;
	}

	/** 
	 * Find object 'a' in the given 'subtree'.
	 */
	private TreeObject findElement(TreeObject a, TreeObject subtree) {
		if (a.fKind == subtree.fKind
			&& a.fElement == subtree.getElement())
			return subtree;
		if (subtree instanceof TreeFolder) {
			TreeFolder folder = (TreeFolder) subtree;
			TreeObject item;
			for (int i = 0; i < folder.getNumChildren(); ++i) {
				item = findElement(a, folder.getChild(i));
				if (item != null)
					return item;
			}
		}
		return null;
	}
}
