/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import java.util.ArrayList;

import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;

public class ASNPartitionScanner extends RuleBasedPartitionScanner {
	public final static String ASN_DEFAULT = "__asn_default";
	public final static String COMMENT = "__asn_comment";
	public final static String STRING = "__asn_string";

	public final static String[] TYPES= new String[] {STRING, COMMENT};	

	public ASNPartitionScanner() {

		IToken comment = new Token(COMMENT);
		IToken string = new Token(STRING);

		ArrayList rules = new ArrayList();

		rules.add(new MultiLineRule("/*", "*/", comment));
		rules.add(new SingleLineRule("--", "--", comment));
		rules.add(new SingleLineRule("--", "", comment));
		rules.add(new SingleLineRule("\"", "\"", string, '\\'));

		IPredicateRule[] result= new IPredicateRule[rules.size()];
		rules.toArray(result);
		setPredicateRules(result);
	}
}
