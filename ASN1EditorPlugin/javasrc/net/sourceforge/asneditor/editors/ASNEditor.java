/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import java.util.Collection;
import java.util.Iterator;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.editors.codefolding.CodeFoldingProvider;
import net.sourceforge.asneditor.editors.outline.ASNOutlinePage;
import net.sourceforge.asneditor.editors.outline.NamedPosition;
import net.sourceforge.asneditor.editors.outline.TreeObject;
import net.sourceforge.asneditor.model.ASNModule;
import net.sourceforge.asneditor.model.IASN;
import net.sourceforge.asneditor.model.manager.ASNModelUtils;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.DefaultPositionUpdater;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.PaintManager;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.DefaultCharacterPairMatcher;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.jface.text.source.MatchingCharacterPainter;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.source.projection.ProjectionAnnotationModel;
import org.eclipse.jface.text.source.projection.ProjectionSupport;
import org.eclipse.jface.text.source.projection.ProjectionViewer;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.MarkerAnnotation;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

public class ASNEditor extends TextEditor {
	private static final String CONTEXT_ID = "#ASNEditorContext"; //$NON-NLS-1$
	public static final String ASN_SCOPE_ID = "net.sourceforge.asneditor.editors.ASNEditorScope"; //$NON-NLS-1$

	static final int tabWidth = 2;
	private static final RGB BRACKETS_COLOR = new RGB(160, 160, 160);
	protected final static char[] BRACKETS = {'{', '}', '(', ')', '[', ']'};

	ASNOutlinePage fOutlinePage;

	protected MatchingCharacterPainter fBracketPainter;
  private PaintManager fPaintManager;

  private static DefaultPositionUpdater fPositionUpdater;

	private IPropertyChangeListener storeListener;
	/** The mouse listener */
	private HyperlinkMouseListener fMouseListener;

	public static final String PREFIX = "Editor."; //$NON-NLS-1$
	private ProjectionSupport projectionSupport;

	private CodeFoldingProvider codeFoldingProvider;

	protected void initializeEditor() {
		super.initializeEditor();
		// initialize editor environment

		// set listener for this plugin store listener
		// TextEditor has another store listener and another store
		storeListener = new IPropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				handlePreferenceStoreChanged(evt);
			}
		};
		ASNTextEnvironment.getInstance().getPreferenceStore().addPropertyChangeListener(storeListener);

		// initialize editor
		setSourceViewerConfiguration(new ASNSourceViewerConfiguration(this));
//		setDocumentProvider(new ASNDocumentProvider());

		// Set the id under which the editor's context menu is registered for extensions
		setEditorContextMenuId(CONTEXT_ID);

		codeFoldingProvider = new CodeFoldingProvider(this);
	}

	public void dispose() {
		stopBracketHighlighting();
		super.dispose();
	}
	/**
	 * Initializes the key binding scopes of this editor.
	 *
	 * @since 2.1
	 */
	protected void initializeKeyBindingScopes() {
		setKeyBindingScopes(new String[] { ASN_SCOPE_ID });
	}

	/**
	 * method handles the changes in of the preferences tor the editor
	 */
	protected void handlePreferenceStoreChanged(PropertyChangeEvent event) {
		super.handlePreferenceStoreChanged(event);

		ISourceViewer sourceViewer = getSourceViewer();
		ASNTextEnvironment textEnv = ASNTextEnvironment.getInstance();

		if ((sourceViewer != null) && textEnv.affectsTextRepresentation(event)) {
			sourceViewer.invalidateTextPresentation();
		}
	}
	/**
	 * @see org.eclipse.ui.IWorkbenchPart#createPartControl(Composite)
	 */
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);

		ProjectionViewer viewer =(ProjectionViewer)getSourceViewer();

		projectionSupport = new ProjectionSupport(viewer, getAnnotationAccess(), getSharedColors());
		projectionSupport.install();

		//turn projection mode on
		viewer.doOperation(ProjectionViewer.TOGGLE);

		ProjectionAnnotationModel annotationModel = viewer.getProjectionAnnotationModel();
		if (annotationModel != null)
			// update to initialize the folding after the model was created
			annotationModel.modifyAnnotations((Annotation[])null, codeFoldingProvider.getAnnotationHash(), null);

		fPaintManager = new PaintManager(getSourceViewer());
		startBracketHighlighting();
		enableBrowserLikeLinks();
		updateModel();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createSourceViewer(org.eclipse.swt.widgets.Composite, org.eclipse.jface.text.source.IVerticalRuler, int)
	 */
	protected ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {
		ISourceViewer viewer = new ProjectionViewer(parent, ruler,
				getOverviewRuler(), isOverviewRulerVisible(), styles);

		// ensure decoration support has been created and configured.
		getSourceViewerDecorationSupport(viewer);

		return viewer;
	}

	public Object getAdapter(Class adapter) {
		if (IContentOutlinePage.class.equals(adapter)) {
			if (fOutlinePage == null)
				fOutlinePage = new ASNOutlinePage(this);
			return fOutlinePage;
		}
		return super.getAdapter(adapter);
	}

	// adds a new annotation to the text
	public boolean addAnnotation(String message, int severity, Position pos, int line) {
		IAnnotationModel annotationModel = getSourceViewer().getAnnotationModel();
		IResource file = getEditorInputResource();

		if ((annotationModel != null) && (file != null)) {
			try {
				IMarker marker = file.createMarker(IMarker.PROBLEM);
				marker.setAttribute(IMarker.SEVERITY, severity);
				marker.setAttribute(IMarker.MESSAGE, message);
				marker.setAttribute(IMarker.TEXT, message);
				marker.setAttribute(IMarker.LINE_NUMBER, line);
				MarkerAnnotation annotation = new MarkerAnnotation(IASNConstants.ProblemMarker, marker);
				annotationModel.addAnnotation(annotation, pos);

				return true;
			} catch (Exception exc) {
				System.err.println("exc");
				exc.printStackTrace(System.err);
			}
		}

		return false;
	}

	private void startBracketHighlighting() {
		if (fBracketPainter == null) {
			ISourceViewer sourceViewer = getSourceViewer();
			fBracketPainter = new MatchingCharacterPainter(sourceViewer, new DefaultCharacterPairMatcher(BRACKETS));
			fBracketPainter.setColor(ASNTextEnvironment.getInstance().getColorManager().getColor(BRACKETS_COLOR));
			fPaintManager.addPainter(fBracketPainter);
		}
	}

	private void stopBracketHighlighting() {
		if (fBracketPainter != null) {
			fPaintManager.removePainter(fBracketPainter);
			fBracketPainter.deactivate(true);
			fBracketPainter.dispose();
			fBracketPainter = null;
		}
	}

	public SourceViewer getViewer() {
		return (SourceViewer) getSourceViewer();
	}

	public static void removePositions(String category, SourceViewer viewer) {
		if (viewer == null)
			return;
		IDocument doc = viewer.getDocument();
		if (doc == null || !doc.containsPositionCategory(category))
			return;
		try {
			Position[] positions = doc.getPositions(category);
			for (int i = 0; i < positions.length; ++i) {
				doc.removePosition(category, positions[i]);
			}
		} catch (BadPositionCategoryException e) {
			ASNEditorPlugin.log("removePositions(): Bad position category: " + category, e);
		}
	}

	public static DefaultPositionUpdater getDefaultPositionUpdater() {
		if (fPositionUpdater == null)
			fPositionUpdater = new DefaultPositionUpdater(IASNConstants.POSITION_CATEGORY);
		return fPositionUpdater;
	}

	public void updateFolding() {
		SourceViewer viewer = getViewer();
		if (viewer == null)
			return;
		IDocument doc = viewer.getDocument();
		if (doc == null)
			return;
		updateFolding(doc);
	}

	/**
	 * @param doc
	 */
	private void updateFolding(IDocument doc) {
		try {
			codeFoldingProvider.updateFoldingStructure(doc, IASNConstants.POSITION_CATEGORY);
		} catch (BadPositionCategoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return
	 */
	public IResource getEditorInputResource() {
		return ASNEditorPlugin.findResource(getEditorInput());
	}

	/**
	 * 
	 */
	private void removeAnnotations(String token, SourceViewer viewer) {
	  IResource file = getEditorInputResource();
		// Update marker visuals.
	  try {
	    file.deleteMarkers(token, true, IFile.DEPTH_ONE);
	  } catch (CoreException e) {
	    // TODO Auto-generated catch block
	    //e.printStackTrace();
	  }

		IAnnotationModel annoModel = viewer.getAnnotationModel();
		if (annoModel == null)
			return;

		Iterator annotationIterator = annoModel.getAnnotationIterator();
		while (annotationIterator.hasNext()) {
			Annotation anno = (Annotation) annotationIterator.next();
			//System.err.println("scanning anno "+anno.getText()+" type "+anno.getType()+" del"+anno.isMarkedDeleted());
			if (!anno.isMarkedDeleted() && token.equals(anno.getType())) {
				//System.err.println("removing anno "+anno.getText());
				annoModel.removeAnnotation(anno);
			}
		}
	}

	/**
	 * @param viewer
	 * @param doc
	 */
	private void updateAnnotations(SourceViewer viewer, IDocument doc) {
		// Update marker visuals.
		IAnnotationModel annotate = viewer.getAnnotationModel();
		if (annotate != null) {
			annotate.disconnect(doc);
			annotate.connect(doc);
		}
	}

	private void addPositions(IDocument doc) {
		Collection<ASNModule> model = ASNModelUtils.getModulesForResource(getEditorInputResource());
    if (model == null) {
			ASNEditorPlugin.log("No modules defined");
			return;
		}

		NamedPosition root = new NamedPosition(TreeObject.FOLDER, "Root", 0, 1);
		try {
			doc.addPosition(IASNConstants.POSITION_CATEGORY, root);
		} catch (BadLocationException e) {
			ASNEditorPlugin.log(getClass().toString()+".addPositions(): Bad location", e);
		} catch (BadPositionCategoryException e) {
			ASNEditorPlugin.log(getClass().toString()+".addPositions(): Bad category", e);
		}

		for (ASNModule module : model) {
			addPosition(TreeObject.MODULE, module.getName(), doc, module);

			// imports
			final IASN importsItem = module.getImportsItem();
			if (importsItem != null)
				addPosition(TreeObject.IMPORT_CONTAINER, importsItem.getName(), doc, importsItem);

			Iterator imports = module.importSymbolFromModuleListIterator();
			while (imports.hasNext()) {
				final Object next = imports.next();
				final IASN importNode = (IASN) next;
				addPosition(TreeObject.IMPORT_DECLARATION, importNode.getName(), doc, importNode);
			}

			// exports
			final IASN exportsItem = module.getExportsItem();
			if (exportsItem != null)
				addPosition(TreeObject.EXPORT_CONTAINER, exportsItem.getName(), doc, exportsItem);

			Iterator exports = module.exportSymbolListIterator();
			while (exports.hasNext()) {
				final String exportNode = (String) exports.next();
				addPosition(TreeObject.EXPORT_DECLARATION, exportNode, doc, module.getExportsItem());
			}

			// types
			Iterator types = module.asnTypesIterator();
			while (types.hasNext()) {
				final IASN type = (IASN) types.next();
				addPosition(TreeObject.TYPE, type.getName(), doc, type);
			}

			// values
			Iterator values = module.asnValuesIterator();
			while (values.hasNext()) {
				final IASN value = (IASN) values.next();
				addPosition(TreeObject.VALUE, value.getName(), doc, value);
			}
		}
	}

	private Position addPosition(int kind, String name, IDocument doc, IASN asnItem) {
		NamedPosition position = null;
//		int start, end;
//		if (first != null) {
//		start = ASNEditorPlugin.findOffset(first.beginLine, first.beginColumn, getSourceViewer());
//		if (last != null)
//		end = ASNEditorPlugin.findOffset(last.endLine, last.endColumn, getSourceViewer());
//		else
//		end = start;
		if (asnItem == null) {
			return null;
		}
		final int offset = asnItem.getOffset(getViewer());
		final int len = asnItem.getLength();
		position = new NamedPosition(kind, name, offset, len);
		asnItem.setPosition(position);
		try {
			doc.addPosition(IASNConstants.POSITION_CATEGORY, position);
		} catch (BadLocationException e) {
			ASNEditorPlugin.log(getClass().toString()+ ".addPositions(): Bad location", e);
		} catch (BadPositionCategoryException e) {
			ASNEditorPlugin.log(getClass().toString()+ ".addPositions(): Bad category", e);
		}
//		}
		return position;
	}

	public void refreshHighlight() {
		// Update highlighting.
		IDocument doc = getViewer().getDocument();
		IDocumentPartitioner p = doc.getDocumentPartitioner();
//		doc.setDocumentPartitioner(null);
		doc.setDocumentPartitioner(p);
	}

	public void refreshOutlineView() {
		if (fOutlinePage != null) {
	    Collection<ASNModule> model = ASNModelUtils.getModulesForResource(getEditorInputResource());
			fOutlinePage.setInput(model);
		}
	}

	public void outlinePageDisposed() {
		fOutlinePage = null;
	}

	protected void editorSaved() {
		super.editorSaved();
		updateModel();
	}

	public void doRevertToSaved() {
		super.doRevertToSaved();
		updateModel();
	}

  @Override
  protected void doSetInput(IEditorInput input) throws CoreException {
    super.doSetInput(input);
    updateModel();
  }

	private void updateModel() {
    // For now, model is used only for outline page.
    SourceViewer viewer = getViewer();
    if (viewer == null)
      return;
    IDocument doc = viewer.getDocument();
    if (doc == null)
      return;
    if (!doc.containsPositionCategory(IASNConstants.POSITION_CATEGORY)) {
      doc.addPositionCategory(IASNConstants.POSITION_CATEGORY);
      doc.addPositionUpdater(getDefaultPositionUpdater());
    } else {
      removePositions(IASNConstants.POSITION_CATEGORY, viewer);
    }

    // new model is being created
		IResource resource = getEditorInputResource();
		Collection<ASNModule> modulesForResource = ASNModelUtils.getModulesForResource(resource);

		// update positions
		if (modulesForResource != null) {
      addPositions(doc);
      updateFolding(doc);
    }
//    refreshHighlight();
    updateAnnotations(viewer, doc);
    refreshOutlineView();
	}

	/**
	 * Return whether the browser like links should be enabled
	 * according to the preference store settings.
	 * @return <code>true</code> if the browser like links should be enabled
	 */
	private boolean isBrowserLikeLinks() {
		// TODO create boolean property
//		IPreferenceStore store= getPreferenceStore();
//		return store.getBoolean(IASNPreferenceStoreConstants.EDITOR_BROWSER_LIKE_LINKS);
		return true;
	}

	/**
	 * Enables browser like links.
	 */
	private void enableBrowserLikeLinks() {
		if (fMouseListener == null) {
			fMouseListener= new HyperlinkMouseListener(getViewer(), this);
			fMouseListener.install();
		}
	}

	/**
	 * Disables browser like links.
	 */
	private void disableBrowserLikeLinks() {
		if (fMouseListener != null) {
			fMouseListener.uninstall();
			fMouseListener= null;
		}
	}

	/**
	 * Jumps to the next enabled annotation according to the given direction.
	 * An annotation type is enabled if it is configured to be in the
	 * Next/Previous tool bar drop down menu and if it is checked.
	 * 
	 * @param forward <code>true</code> if search direction is forward, <code>false</code> if backward
	 */
	public Annotation gotoAnnotation(boolean forward) {
		ISelectionProvider selectionProvider = getSelectionProvider();
		if (selectionProvider == null)
			return null;
		ITextSelection selection= (ITextSelection) selectionProvider.getSelection();
		Position position= new Position(0, 0);
		Annotation annotation= getNextAnnotation(selection.getOffset(), selection.getLength(), forward, position);
		if (annotation != null) {
			selectAndReveal(position.getOffset(), position.getLength());
		}
		return annotation;
	}
	/**
	 * Returns the annotation closest to the given range respecting the given
	 * direction. If an annotation is found, the annotations current position
	 * is copied into the provided annotation position.
	 * 
	 * @param offset the region offset
	 * @param length the region length
	 * @param forward <code>true</code> for forwards, <code>false</code> for backward
	 * @param annotationPosition the position of the found annotation
	 * @return the found annotation
	 */
	private Annotation getNextAnnotation(final int offset, final int length, boolean forward, Position annotationPosition) {

		Annotation nextAnnotation= null;
		Position nextAnnotationPosition= null;
		Annotation containingAnnotation= null;
		Position containingAnnotationPosition= null;
		boolean currentAnnotation= false;

		IDocument document= getDocumentProvider().getDocument(getEditorInput());
		int endOfDocument= document.getLength(); 
		int distance= Integer.MAX_VALUE;

		IAnnotationModel model= getDocumentProvider().getAnnotationModel(getEditorInput());
		Iterator e= model.getAnnotationIterator();
		while (e.hasNext()) {
			Annotation a= (Annotation) e.next();
			Position p= model.getPosition(a);
			if (p == null)
				continue;

			if (forward && p.offset == offset || !forward && p.offset + p.getLength() == offset + length) {// || p.includes(offset)) {
				if (containingAnnotation == null || (forward && p.length >= containingAnnotationPosition.length || !forward && p.length >= containingAnnotationPosition.length)) { 
					containingAnnotation= a;
					containingAnnotationPosition= p;
					currentAnnotation= p.length == length;
				}
			} else {
				int currentDistance= 0;

				if (forward) {
					currentDistance= p.getOffset() - offset;
					if (currentDistance < 0)
						currentDistance= endOfDocument + currentDistance;

					if (currentDistance < distance || currentDistance == distance && p.length < nextAnnotationPosition.length) {
						distance= currentDistance;
						nextAnnotation= a;
						nextAnnotationPosition= p;
					}
				} else {
					currentDistance= offset + length - (p.getOffset() + p.length);
					if (currentDistance < 0)
						currentDistance= endOfDocument + currentDistance;

					if (currentDistance < distance || currentDistance == distance && p.length < nextAnnotationPosition.length) {
						distance= currentDistance;
						nextAnnotation= a;
						nextAnnotationPosition= p;
					}
				}
			}
		}
		if (containingAnnotationPosition != null && (!currentAnnotation || nextAnnotation == null)) {
			annotationPosition.setOffset(containingAnnotationPosition.getOffset());
			annotationPosition.setLength(containingAnnotationPosition.getLength());
			return containingAnnotation;
		}
		if (nextAnnotationPosition != null) {
			annotationPosition.setOffset(nextAnnotationPosition.getOffset());
			annotationPosition.setLength(nextAnnotationPosition.getLength());
		}

		return nextAnnotation;
	}
}
