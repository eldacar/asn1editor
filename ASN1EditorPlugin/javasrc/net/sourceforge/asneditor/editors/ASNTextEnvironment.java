/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.ui.preferences.ASNPreferencePage;
import net.sourceforge.asneditor.ui.preferences.IASNPreferenceStoreConstants;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;


/**
 * @author bst
 * TODO comment this
 */
public class ASNTextEnvironment implements IASNPreferenceStoreConstants {

	private ColorManager colorManager = null; 
	private ColorToken colorTokens = null;
	
	private ASNScanner codeScanner = null;
	private SingleTokenScanner commentScanner = null;	
	private SingleTokenScanner stringScanner = null;

	private ASNPartitionScanner partitionScanner= null;	

	class PropertyChangeListener implements IPropertyChangeListener {
		public void propertyChange(PropertyChangeEvent event) {
			handlePropertyChange(event);
		}
	}
	
	private PropertyChangeListener listener = null;
	private IPreferenceStore store = null;
	private static ASNTextEnvironment myInstance;
	
	public ColorToken getColorTokens() {
		return colorTokens;
	}
	public ColorManager getColorManager() {
		return colorManager;
	}
	public ASNScanner getCodeScanner() {
		return codeScanner;
	}
	public SingleTokenScanner getCommentScanner() {
		return commentScanner;
	}
	public SingleTokenScanner getStringScanner() {
		return stringScanner;
	}
	public IPreferenceStore getPreferenceStore() {
		return store;
	}
	public ASNPartitionScanner getPartitionScanner() {
		return partitionScanner;
	}
	
	private ASNTextEnvironment(IPreferenceStore store) {

		this.store = store;

		colorManager = new ColorManager();
		colorTokens = new ColorToken();
		ColorTokenModifier.init(this);

		codeScanner  = new ASNScanner(colorManager);
		commentScanner = new SingleTokenScanner(colorTokens.COMMENT);
		stringScanner = new SingleTokenScanner(colorTokens.STRING);

		partitionScanner = new ASNPartitionScanner();

		listener = new PropertyChangeListener();
		store.addPropertyChangeListener(listener);
	}	
	
	/**
	 * Must be called then the plugin is closed to free the resources 
	 */
	public void dispose() {

		colorManager.dispose();
		colorManager = null;
		codeScanner  = null;
		commentScanner = null;
		stringScanner = null;
		
		store.removePropertyChangeListener(listener);
		store = null;
	}

	/**
   * @return
   */
  public static ASNTextEnvironment getInstance() {
    if (myInstance == null) {
			IPreferenceStore store = ASNEditorPlugin.getDefault().getPreferenceStore();
			ASNPreferencePage.loadDefaults(store);
      myInstance = new ASNTextEnvironment(store);
    }
    return myInstance;
  }

	/**
	 * Determines if the given PropertyChangeEvent changes the text 
	 * behavior in the plugin text representation
	 * @return boolean
	 */
	public boolean affectsTextRepresentation(PropertyChangeEvent event) {
		String property = event.getProperty();
		for (int i = preferences.length; i-- > 0; ) {
			if (preferences[i].equals(property)) return true;
		}
		return false;
//		final String property = event.getProperty();
//		return indexOf(property) >= 0;
	}

	private void handlePropertyChange(PropertyChangeEvent event) {
		final String property = event.getProperty();
		final int index = indexOf(property);
		if (index < 0 ) return; 

		final Token[] tokens = colorTokens.getTokens();
		final int tokenType = index % 3;
		final int tokenIndex = (index - tokenType) / 3;

		System.out.println("property change "+tokenType+" index "+tokenIndex+" "+preferences[index]);
		switch (tokenType) {
		case 0:
			// color property
			ColorTokenModifier.changeColor(colorManager, tokens[tokenIndex], event.getNewValue());
			break;
		case 1:
			// bold style property
			ColorTokenModifier.changeStyle(tokens[tokenIndex], event.getNewValue(), true);
			break;
		case 2:
			// italic style property
			ColorTokenModifier.changeStyle(tokens[tokenIndex], event.getNewValue(), false);
			break;
		}
	}
	
	private int indexOf(String property) {
		for (int i = preferences.length; i-- > 0; ) {
			if (preferences[i].equals(property)) return i;
		}
		return -1;
	}

}
