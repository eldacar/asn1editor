/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import net.sourceforge.asneditor.editors.html.HTMLTextPresenter;

import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.DefaultTextHover;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;

public class ASNSourceViewerConfiguration extends SourceViewerConfiguration {
	private ASNDoubleClickStrategy doubleClickStrategy;
	private final ASNEditor editor;

	public ASNSourceViewerConfiguration(ASNEditor editor) {
		this.editor = editor;
	}

	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] {
			IDocument.DEFAULT_CONTENT_TYPE,
			ASNPartitionScanner.COMMENT,
			ASNPartitionScanner.STRING };
	}

	public ITextDoubleClickStrategy getDoubleClickStrategy(ISourceViewer sourceViewer, String contentType) {
		if (doubleClickStrategy == null)
			doubleClickStrategy = new ASNDoubleClickStrategy();
		return doubleClickStrategy;
	}

	public IAnnotationHover getAnnotationHover(ISourceViewer sourceViewer) {
		return new MarkerAnnotationHover();
	}

	public IInformationControlCreator getInformationControlCreator(ISourceViewer sourceViewer) {
		return new IInformationControlCreator() {
			public IInformationControl createInformationControl(Shell parent) {
				return new DefaultInformationControl(parent, SWT.NONE, new HTMLTextPresenter(true));
			}
		};
	}

	/**
   * Returns the presentation reconciler ready to be used with the given
   * source viewer.
   *
   * @return the presentation reconciler or <code>null</code> if presentation
   *         reconciling should not be supported
   */
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		PresentationReconciler reconciler = new PresentationReconciler();
		final ASNTextEnvironment env = ASNTextEnvironment.getInstance();

		DefaultDamagerRepairer dr;

		dr = new DefaultDamagerRepairer(env.getCommentScanner());
		reconciler.setDamager(dr, ASNPartitionScanner.COMMENT);
		reconciler.setRepairer(dr, ASNPartitionScanner.COMMENT);

		dr = new DefaultDamagerRepairer(env.getStringScanner());
		reconciler.setDamager(dr, ASNPartitionScanner.STRING);
		reconciler.setRepairer(dr, ASNPartitionScanner.STRING);

		dr = new DefaultDamagerRepairer(env.getCodeScanner());
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		return reconciler;
	}

  /*
   * @see SourceViewerConfiguration#getContentAssistant(ISourceViewer)
   */
  public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
      if (sourceViewer == null || editor == null) {
    	  return null;
      }
      ContentAssistant assistant = new ContentAssistant();

      IContentAssistProcessor processor = new ASNCompletionProcessor(editor);
      assistant.setContentAssistProcessor(processor, IDocument.DEFAULT_CONTENT_TYPE);

      // Register the same processor for strings and single line comments to get code completion at the start of those partitions.
      assistant.setContentAssistProcessor(processor, ASNPartitionScanner.ASN_DEFAULT);
      assistant.setContentAssistProcessor(processor, ASNPartitionScanner.STRING);
      assistant.setContentAssistProcessor(processor, ASNPartitionScanner.COMMENT);

//			ContentAssistPreference.configure(assistant, getPreferenceStore());
      assistant.setContextInformationPopupOrientation(ContentAssistant.CONTEXT_INFO_ABOVE);
      assistant.setInformationControlCreator(getInformationControlCreator(sourceViewer));

      assistant.enableAutoActivation(true);
      assistant.setAutoActivationDelay(500);

      return assistant;
  }

	public ITextHover getTextHover(ISourceViewer sourceViewer, String contentType) {
		if (IASNConstants.ProblemMarker.equals(contentType))
			return new DefaultTextHover(sourceViewer);
		return super.getTextHover(sourceViewer, contentType);
	}
	
}
