/**
 * Copyright (c) 2007 Bogdan Stanca. All rights reserved.
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;

/**
 * The document setup participant for a ASN.1 document.
 */
public class ASNDocumentSetupParticipant implements IDocumentSetupParticipant {

	/*
	 * @see org.eclipse.core.filebuffers.IDocumentSetupParticipant#setup(org.eclipse.jface.text.IDocument)
	 */
	public void setup(IDocument document) {
		setupDocument(document);
	}

	/**
	 * @see org.eclipse.core.filebuffers.IDocumentSetupParticipant#setup(org.eclipse.jface.text.IDocument)
	 */
	public static void setupDocument(IDocument document) {
		IDocumentPartitioner partitioner= ASNDocumentProvider.createASNPartitioner();
		// TODO note: not using shared documents
//		if (document instanceof IDocumentExtension3) {
//			IDocumentExtension3 extension3= (IDocumentExtension3) document;
//			extension3.setDocumentPartitioner(ASNPartitionScanner.ASN_DEFAULT, partitioner);
//		} else {
			document.setDocumentPartitioner(partitioner);
//		}
		partitioner.connect(document);
	}

}
