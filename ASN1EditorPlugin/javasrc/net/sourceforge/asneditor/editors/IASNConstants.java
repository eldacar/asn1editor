/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import net.sourceforge.asneditor.ASNEditorPlugin;

public interface IASNConstants {

	public static final String ProblemMarker = ASNEditorPlugin.class + ".format_action_problem";
	public static final String POSITION_CATEGORY = ASNEditorPlugin.class + ".Outline";

	public static final String[] TYPE_KEYWORDS = {
			"BIT", "BMPString", "BOOLEAN", 
			"CHARACTER", "EMBEDDED", "PDV",
			 "ENUMERATED", "EXTERNAL", "GeneralizedTime", 
			"GeneralString", "GraphicString", "IA5String", "INTEGER",
			"ISO646String",
			
			"NumericString", "ObjectDescriptor", 
			"OBJECT", "IDENTIFIER", "OCTET", "STRING", 
			"PrintableString", "T61String", "TeletexString", "REAL",
			"RELATIVE-OID", "OF",
			
			"UniversalString", "UTCTime", "UTF8String", "VideotexString", "VisibleString", "NULL",
			
			"CHOICE", "SET", "SEQUENCE", "ANY", "DEFINED",
// complex keywords
			"BIT STRING", "CHARACTER STRING",
			"OBJECT IDENTIFIER", "OCTET STRING", "EMBEDDED PDV",
			"SET OF", "SEQUENCE OF", "ANY DEFINED BY"
	};

	public static final String[] STRING_CONSTANTS = {
			"FALSE", "MINUS-INFINITY", "PLUS-INFINITY", "TRUE"
	};

	public static final String[] KEYWORDS = {
			"BEGIN", "DEFINITIONS", "END", "EXPORTS",
			"EXTENSIBILITY", "IMPLIED", "FROM", "IMPORTS", 
			
			"ABSTRACT-SYNTAX", "BY", "CLASS", "CONSTRAINED", 
			"INSTANCE", "TYPE-IDENTIFIER", "SYNTAX", "UNIQUE",
			
			"APPLICATION", "UNIVERSAL", "PRIVATE",
			
			"DEFAULT", "ALL", "PRESENT", "ABSENT", "OPTIONAL",
			"EXCEPT", "INCLUDES", "INTERSECTION", "MAX", "MIN", 
			"SIZE", "UNION", "WITH", "COMPONENT", "COMPONENTS",
			
			"AUTOMATIC", "EXPLICIT", "IMPLICIT", "TAGS",
			"HAS", "PROPERTY", "IDENTIFIED",
//			 complex keywords
			"EXTENSIBILITY IMPLIED", "CONSTRAINED BY", 
			"INSTANCE OF", "WITH SYNTAX",
			"COMPONENTS OF", "WITH COMPONENT", "WITH COMPONENTS",
			
			"AUTOMATIC TAGS", "EXPLICIT TAGS", "IMPLICIT TAGS",
			"HAS PROPERTY", "IDENTIFIED BY"
	};

	public static final String COMMENT_TOKEN = "--";

}
