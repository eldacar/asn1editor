/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.ui.editors.text.FileDocumentProvider;

public class ASNDocumentProvider extends FileDocumentProvider {

	/* (non-Javadoc)
	 * Method declared on AbstractDocumentProvider
	 */
	protected IDocument createDocument(Object element) throws CoreException {
		IDocument document= super.createDocument(element);
		if (document != null) {
			IDocumentPartitioner partitioner= createASNPartitioner();
			document.setDocumentPartitioner(partitioner);
			partitioner.connect(document);
		}
		return document;
	}

	/**
	 * Return a partitioner for .asn files.
	 */
	public static IDocumentPartitioner createASNPartitioner() {
		ASNPartitionScanner scanner = ASNTextEnvironment.getInstance().getPartitionScanner();
		return new FastPartitioner(scanner, ASNPartitionScanner.TYPES);
	}
}