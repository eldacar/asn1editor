/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;

public class ASNScanner extends RuleBasedScanner implements IASNConstants {
  public ASNScanner(ColorManager manager) {
		IToken keywordToken = new Token(new TextAttribute(manager.getColor(IASNSyntaxColorAndStyleConstants.KEYWORD_COLOR)));
		IToken typeKeywordToken = new Token(new TextAttribute(manager.getColor(IASNSyntaxColorAndStyleConstants.TYPE_KEYWORD_COLOR)));
		IToken constantToken = new Token(new TextAttribute(manager.getColor(IASNSyntaxColorAndStyleConstants.CONSTANT_COLOR)));
		IToken defaultToken = new Token(new TextAttribute(manager.getColor(IASNSyntaxColorAndStyleConstants.DEFAULT_COLOR)));

		List<IRule> rules= new LinkedList<IRule>();

		//Add rule for keywords
		WordRule wordRule = new WordRule(new WordDetector(), defaultToken);
		for (int i = 0; i < IASNConstants.KEYWORDS.length; i++) {
			wordRule.addWord(IASNConstants.KEYWORDS[i], keywordToken);
    }

		//Add rule for builtin types
		for (int i = 0; i < IASNConstants.TYPE_KEYWORDS.length; i++) {
			wordRule.addWord(IASNConstants.TYPE_KEYWORDS[i], typeKeywordToken);
    }

		//Add rule for builtin types
		for (int i = 0; i < IASNConstants.STRING_CONSTANTS.length; i++) {
			wordRule.addWord(IASNConstants.STRING_CONSTANTS[i], constantToken);
    }
		rules.add(wordRule);

    // Add generic whitespace rule.
		rules.add(new WhitespaceRule(new ASNWhitespaceDetector()));

		IRule[] result= new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
	}
}
