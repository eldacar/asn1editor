/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import org.eclipse.swt.graphics.RGB;

public interface IASNSyntaxColorAndStyleConstants {
	static final RGB KEYWORD_COLOR			= new RGB(127, 0, 85);
	static final RGB TYPE_KEYWORD_COLOR = new RGB(127, 0, 85);//new RGB(127, 159, 191);
	static final RGB CONSTANT_COLOR			= new RGB(127, 0, 85);//new RGB(0, 128, 128);

	static final RGB COMMENT_COLOR			= new RGB(63, 127, 95);
	static final RGB STRING_COLOR				= new RGB(42, 0, 255);

	static final RGB DEFAULT_COLOR			= new RGB(0, 0, 0);



	static final boolean KEYWORD_BOLD				= true;
	static final boolean TYPE_KEYWORD_BOLD	= true;
	static final boolean CONSTANT_BOLD			= true;

	static final boolean COMMENT_BOLD				= false;
	static final boolean STRING_BOLD				= false;

	static final boolean DEFAULT_BOLD				= false;



	static final boolean KEYWORD_ITALIC			= true;
	static final boolean TYPE_KEYWORD_ITALIC= true;
	static final boolean CONSTANT_ITALIC		= true;

	static final boolean COMMENT_ITALIC			= false;
	static final boolean STRING_ITALIC			= false;

	static final boolean DEFAULT_ITALIC			= false;
}
