/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import net.sourceforge.asneditor.model.ASNModule;
import net.sourceforge.asneditor.model.IASN;
import net.sourceforge.asneditor.model.manager.ASNModelUtils;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;


public class ASNCompletionProcessor implements IContentAssistProcessor {

	private static final ICompletionProposal[] EMPTY_PROPOSAL = new ICompletionProposal[0];
	private final ASNEditor editor;

	// activation characters
	private static final char[] PROPOSAL_ACTIVATION_CHARS = new char[] { ' ', '=', '\t', '\n', '&' };

	private static final IContextInformation[] EMPTY_CONTEXT = new IContextInformation[0];

	/**
	 * @param editor
	 */
	public ASNCompletionProcessor(ASNEditor editor) {
		this.editor = editor;
		
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeCompletionProposals(org.eclipse.jface.text.ITextViewer, int)
	 */
	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer, int offset) {
		try {
			final IDocument document = viewer.getDocument();
			final String prefix = getTypedWordPrefix(document, offset);
			if (prefix == null)
				return EMPTY_PROPOSAL;
//			final String key = getKeyword(document, offset - prefix.length() - 1);
			
			final ArrayList<String> result = getStringProposalsStartingWith(prefix);
			final ICompletionProposal[] proposals = new ICompletionProposal[result.size()];
			final int length = prefix.length();
			for (int i = 0; i < result.size(); i++) {
				final String propStr = (String) result.get(i);
				proposals[i] = new CompletionProposal(propStr, offset - length, length, propStr.length());
			}
			// sort proposals
			sortProps(proposals, 0, proposals.length - 1);
			return proposals;
		} catch (Exception e) {
			return EMPTY_PROPOSAL;
		}
	}
	/**
	 * Qick sort proposals according to display strings
	 * @param lo
	 * @param hi
	 */
	private void sortProps(ICompletionProposal[] proposals, int lo, int hi) {
		int i = lo, j = hi;
		String x = proposals[(lo + hi) / 2].getDisplayString();

		while (i <= j) {
			while (proposals[i].getDisplayString().compareTo(x) < 0)
				i++;
			while (proposals[j].getDisplayString().compareTo(x) > 0)
				j--;
			if (i <= j) {
				exchangeProps(proposals, i, j);
				i++;
				j--;
			}
		}

		// Recursion
		if (lo < j)
			sortProps(proposals, lo, j);
		if (i < hi)
			sortProps(proposals, i, hi);
	}

	/**
	 * Exchange proposal
	 * @param i
	 * @param j
	 */
	private void exchangeProps(ICompletionProposal[] proposals, int i, int j) {
		ICompletionProposal t = proposals[i];
		proposals[i] = proposals[j];
		proposals[j] = t;
	}

	/**
	 * @param prefix
	 * @return
	 */
	private ArrayList<String> getStringProposalsStartingWith(String prefix) {
		final ArrayList<String> result = new ArrayList<String>();

		prefix = prefix.toLowerCase();

		// KEYWORDS
		matchPrefix(prefix, result, IASNConstants.KEYWORDS);
		matchPrefix(prefix, result, IASNConstants.STRING_CONSTANTS);
		matchPrefix(prefix, result, IASNConstants.TYPE_KEYWORDS);

		IResource editorInputResource = editor.getEditorInputResource();
		if (editorInputResource == null)
			return result;

		Collection<ASNModule> modulesForProject = ASNModelUtils.getModulesForProject(editorInputResource.getProject());
		for (ASNModule module : modulesForProject) {

			// defined types
			Iterator asnTypesIterator = module.asnTypesIterator();
			while (asnTypesIterator.hasNext()) {
				final IASN asnType = (IASN) asnTypesIterator.next();
				final String typeName = asnType.getName();
				if (typeName.toLowerCase().startsWith(prefix)) {
					result.add(typeName);
				}
			}

			// defined values
			Iterator asnValuesIterator = module.asnValuesIterator();
			while (asnValuesIterator.hasNext()) {
				final IASN asnValue = (IASN) asnValuesIterator.next();
				final String valueName = asnValue.getName();
				if (valueName.toLowerCase().startsWith(prefix)) {
					result.add(valueName);
				}
			}
		}
		

		return result;
	}

	/**
	 * @param prefix
	 * @param result
	 * @param strings
	 */
	private void matchPrefix(String prefix, final ArrayList<String> result, final String[] strings) {
		boolean emptyPrefix = (prefix.length() == 0);
		prefix = prefix.toLowerCase();
		for (String str : strings) {
			if (emptyPrefix || str.toLowerCase().startsWith(prefix)) {
				result.add(str);
			}
		}
	}

	private String getTypedWordPrefix(IDocument doc, int offset) {
		int start = offset;
		int end = offset;
		char idChar;
		while (start >= 0) {
			// go to left to get begining of String
			try {
				idChar = doc.getChar(--start);
				if (!Character.isJavaIdentifierPart(idChar) && !(idChar == '.'))
					break;
			} catch (Exception exc) {
				break;
			}
		}
		start++;
		int length = end - start;
		String id = null;
		try {
			id = doc.get(start, length);
		} catch (Exception ex) {

		}
		return id;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeContextInformation(org.eclipse.jface.text.ITextViewer, int)
	 */
	public IContextInformation[] computeContextInformation(ITextViewer viewer, int offset) {
		return EMPTY_CONTEXT;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getCompletionProposalAutoActivationCharacters()
	 */
	public char[] getCompletionProposalAutoActivationCharacters() {
		return PROPOSAL_ACTIVATION_CHARS;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationAutoActivationCharacters()
	 */
	public char[] getContextInformationAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getErrorMessage()
	 */
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationValidator()
	 */
	public IContextInformationValidator getContextInformationValidator() {
		// TODO Auto-generated method stub
		return null;
	}

}
