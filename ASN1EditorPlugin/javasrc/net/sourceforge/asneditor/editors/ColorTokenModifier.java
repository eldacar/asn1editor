/**
 * Copyright (c) 2005 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.editors;

import net.sourceforge.asneditor.ui.preferences.IASNPreferenceStoreConstants;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;


/**
 * This class is used to initialize and modify the color tokens in ColorToken class
 */
public class ColorTokenModifier extends ColorToken {

	public static void init(ASNTextEnvironment textEnv) {

		ColorManager colorManager = textEnv.getColorManager();
		IPreferenceStore store = textEnv.getPreferenceStore();
		ColorToken colorTokens = textEnv.getColorTokens();

		colorTokens.COMMENT = new Token(new TextAttribute(
				colorManager.getColor(PreferenceConverter.getColor(store, IASNPreferenceStoreConstants.EDITOR_COMMENT_COLOR))
				,null,getStyle(store.getBoolean(IASNPreferenceStoreConstants.EDITOR_COMMENT_BOLD),
						store.getBoolean(IASNPreferenceStoreConstants.EDITOR_COMMENT_ITALIC))));

		colorTokens.TYPE_KEYWORD = new Token(new TextAttribute(
				colorManager.getColor(PreferenceConverter.getColor(store, IASNPreferenceStoreConstants.EDITOR_TYPE_KEYWORD_COLOR))
				,null,getStyle(store.getBoolean(IASNPreferenceStoreConstants.EDITOR_TYPE_KEYWORD_BOLD),
						store.getBoolean(IASNPreferenceStoreConstants.EDITOR_TYPE_KEYWORD_ITALIC))));

		colorTokens.KEYWORD = new Token(new TextAttribute(
				colorManager.getColor(PreferenceConverter.getColor(store, IASNPreferenceStoreConstants.EDITOR_KEYWORD_COLOR))
				,null,getStyle(store.getBoolean(IASNPreferenceStoreConstants.EDITOR_KEYWORD_BOLD),
						store.getBoolean(IASNPreferenceStoreConstants.EDITOR_KEYWORD_ITALIC))));

		colorTokens.CONSTANT = new Token(new TextAttribute(
				colorManager.getColor(PreferenceConverter.getColor(store, IASNPreferenceStoreConstants.EDITOR_CONSTANT_COLOR))
				,null,getStyle(store.getBoolean(IASNPreferenceStoreConstants.EDITOR_CONSTANT_BOLD),
						store.getBoolean(IASNPreferenceStoreConstants.EDITOR_CONSTANT_ITALIC))));

		colorTokens.STRING = new Token(new TextAttribute(
				colorManager.getColor(PreferenceConverter.getColor(store, IASNPreferenceStoreConstants.EDITOR_STRING_COLOR))
				,null,getStyle(store.getBoolean(IASNPreferenceStoreConstants.EDITOR_STRING_BOLD),
						store.getBoolean(IASNPreferenceStoreConstants.EDITOR_STRING_ITALIC))));

		colorTokens.OTHER = new Token(new TextAttribute(
				colorManager.getColor(PreferenceConverter.getColor(store, IASNPreferenceStoreConstants.EDITOR_DEFAULT_COLOR))
				,null,getStyle(store.getBoolean(IASNPreferenceStoreConstants.EDITOR_DEFAULT_BOLD),
						store.getBoolean(IASNPreferenceStoreConstants.EDITOR_DEFAULT_ITALIC))));
	}
	
	public static void changeColor(ColorManager colorManager, Token token, Object value) {
		if (value instanceof RGB) {
			final TextAttribute data = (TextAttribute)token.getData();
			token.setData(new TextAttribute(colorManager.getColor((RGB)value), data.getBackground(), data.getStyle()));
		}
	}

	public static void changeStyle(Token token, Object value, boolean boldProperty) {
		if (value instanceof Boolean) {
			final TextAttribute data = (TextAttribute)token.getData();
			final boolean booleanValue = ((Boolean)value).booleanValue();
			int style = data.getStyle();
			if (boldProperty) { // BOLD
				style = getStyle(booleanValue, (style & SWT.ITALIC) == SWT.ITALIC);
			} else { // ITALIC
				style = getStyle((style & SWT.BOLD) == SWT.BOLD, booleanValue);
			}
			token.setData(new TextAttribute(data.getForeground(), data.getBackground(), style));
		}
	}

	private static int getStyle(boolean bold, boolean italic) {
		int style = SWT.NORMAL;
		if (bold)
			style |= SWT.BOLD;
		if (italic)
			style |= SWT.ITALIC;
		return style;
	}
}
