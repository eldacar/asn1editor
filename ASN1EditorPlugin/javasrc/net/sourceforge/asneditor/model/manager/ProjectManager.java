/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.model.manager;

import java.io.InputStream;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

import net.sourceforge.asneditor.model.ASNLexer;
import net.sourceforge.asneditor.model.ASNModule;
import net.sourceforge.asneditor.model.ASNParser;
import net.sourceforge.asneditor.util.ProjectUtils;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IStorage;

public class ProjectManager {
  private final String projectName;

  // models that are built but invalid
  private Map<IResource, Long> invalidModels;

  // valid models for resources
  private Map<IResource, Collection<ASNModule>> resources;
  private Map<String, Collection<ASNModule>> modules;

  public ProjectManager(String projectName) {
    this.projectName = projectName;

    invalidModels = new Hashtable<IResource, Long>();

    resources = new Hashtable<IResource, Collection<ASNModule>>();
    modules = new Hashtable<String, Collection<ASNModule>>();
  }

  public String getProjectName() {
    return projectName;
  }

  public Collection<ASNModule> getModulesByResource(IResource resource) {
    Collection<ASNModule> models = resources.get(resource);
    if (models == null || !verifyUpToDate(models)) {
      models = createModelForResource(resource);
    }
    return createNewList(models);
  }

  /**
   * @param models
   * @return true if up2date
   */
  private boolean verifyUpToDate(Collection<ASNModule> models) {
    boolean result = true;
    for (ASNModule module : models) {
      if (!module.isUpToDate()) {
        clean(module.getResource());
        result = false;
      }
    }
    return result;
  }

  public Collection<ASNModule> getModulesByName(String moduleName) {
    Collection<ASNModule> models = modules.get(moduleName);
    return createNewList(models);
  }

  public Collection<ASNModule> getModules() {
    Collection<ASNModule> models = new LinkedList<ASNModule>();
    Collection<Collection<ASNModule>> values = modules.values();
    for (Collection<ASNModule> collection : values) {
      models.addAll(collection);
    }
    return models;
  }

  /**
   * Adds the current module to the list.
   * If another module with the same name exists,
   * it will be replaced by module.
   * @param module
   * @return false if the module or it's name are null
   */
  public boolean add(ASNModule module) {
    if ((module == null) || (module.getName() == null)
            || (module.getResource() == null))
      return false;
    addToMapList(modules, module.getName(), module);
    addToMapList(resources, module.getResource(), module);
    return true;
  }

  public void clean(IResource resource) {
    Collection<ASNModule> models = resources.get(resource);
    if (models == null || models.isEmpty()) {
      invalidModels.remove(resource);
    } else {
      resources.remove(resource);
      for (ASNModule module : models) {
        Collection<ASNModule> res = modules.get(module.getName());
        if (res != null)
          res.remove(module);
      }
    }
  }

  public void clean() {
    modules.clear();
    resources.clear();
    invalidModels.clear();
  }

  protected synchronized Collection<ASNModule> createModelForResource(IResource resource) {
    Long savedModificationStamp = invalidModels.get(resource);
    if (savedModificationStamp != null) {
      long actualModificationStamp = resource.getModificationStamp();
      if (actualModificationStamp == savedModificationStamp) {
        return null;
      }
    }

    // remove old models
    clean(resource);

    IModelHandler modelHandler = WorkspaceManager.createModelHandler();
    try {
      IStorage storage = ((IStorage)resource.getAdapter(IStorage.class));
      InputStream input = storage.getContents();
      final ASNLexer lexer = new ASNLexer(input);
      final ASNParser parser = new ASNParser(lexer, modelHandler, resource);
//      final long startTime = Calendar.getInstance().getTimeInMillis();
      parser.grammar();
//      final long stopTime = Calendar.getInstance().getTimeInMillis();
//      String name = getEditorInput().getName();
//      int lines = doc.getNumberOfLines();
//      System.err.println(" * "+name+": parse: "+lines+" lines in "+(stopTime - startTime)+" (sec)="+(lines / (stopTime - startTime))+" (lines/sec)");



      // TODO CHECK THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//      ASNEditorPlugin.runSynchronous(new Runnable(){
//        public void run() {
//          refreshHighlight();
//        }});
      // TODO CHECK THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    } catch (Exception e) {
      modelHandler.reportError(e.getMessage(), e, 0, 0);
//      reportError(e.getMessage(), e, 0, 0);
      //              ASNEditorPlugin.log("Parse error:", e);
      //              e.printStackTrace();
    }
    Collection<ASNModule> models = resources.get(resource);
    if (models == null) {
      invalidModels.put(resource, resource.getModificationStamp());
    }
    return createNewList(models);
  }

  protected <T> Collection<T> createNewList(Collection<T> models) {
    Collection<T> result = new LinkedList<T>();
    if (models != null) {
      result.addAll(models);
    }
    return result;
  }

  private <K, V> void addToMapList(Map<K, Collection<V>> map, K key, V value) {
    Collection<V> collection = map.get(key);
    if (collection == null) {
      collection = new LinkedList<V>();
      map.put(key, collection);
    }
    collection.add(value);
  }

  public void rebuild(IResource resource) {
    clean(resource);
    getModulesByResource(resource);
  }

  public void rebuild() {
    build(true);
  }

  public void build() {
    build(false);
  }

  /**
   * @param clean clean model if true
   */
  public void build(boolean clean) {
    Collection<IResource> asnFiles = getAsnResources();
    for (IResource resource : asnFiles) {
      if (clean)
        clean(resource);
      getModulesByResource(resource);
    }
  }

  public Collection<IResource> getAsnResources() {
    IProject project = ProjectUtils.getProject(getProjectName());
    if (project == null)
      return null;
    return ProjectUtils.getAsnFiles(project);
  }
}
