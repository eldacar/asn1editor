/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.model.manager;

import org.eclipse.core.resources.IResource;

import antlr.RecognitionException;

public interface IErrorHandler {
  public static final String EXT_ID = "net.sourceforge.asneditor.errorhandler"; //$NON-NLS-1$
  // extension point attributes
  public static final String EXT_ATTR_ID = "id"; //$NON-NLS-1$
  public static final String EXT_ATTR_NAME = "name"; //$NON-NLS-1$
  public static final String EXT_ATTR_CLASS = "class"; //$NON-NLS-1$
  public static final String EXT_ATTR_KIND = "kind"; //$NON-NLS-1$

  
  void processing(IResource moduleResource);

  void reportError(RecognitionException ex);

  void reportError(String message, Exception e, int line, int column);

  void reportWarning(String message);

}