/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.model.manager;

import java.util.Collection;
import java.util.LinkedList;

import net.sourceforge.asneditor.model.ASNModule;
import net.sourceforge.asneditor.model.IASN;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

public class ASNModelUtils {

  public static Collection<IASN> findDeclarationsInProject(IProject project, String declarationName) {
    Collection<IASN> result = new LinkedList<IASN>();
    ProjectManager projectManager = WorkspaceManager.getProjectManager(project);
    Collection<ASNModule> modules = projectManager.getModules();
    findDeclarationInModules(declarationName, result, modules);
    return result;
  }

  public static Collection<IASN> findDeclarationsInModule(IProject project, String moduleName, String declarationName) {
    Collection<IASN> result = new LinkedList<IASN>();
    ProjectManager projectManager = WorkspaceManager.getProjectManager(project);
    Collection<ASNModule> modules = projectManager.getModulesByName(moduleName);
    findDeclarationInModules(declarationName, result, modules);
    return result;
  }

  public static Collection<IASN> findDeclarationsInFile(IResource resource, String identifier) {
    Collection<IASN> result = new LinkedList<IASN>();
    Collection<ASNModule> modules = getModulesForResource(resource);
    findDeclarationInModules(identifier, result, modules);
    return result;
  }

  public static Collection<ASNModule> getModulesForResource(IResource resource) {
    if (resource == null) {
      return new LinkedList<ASNModule>();
    }
    ProjectManager projectManager = WorkspaceManager.getProjectManager(resource.getProject());
    Collection<ASNModule> modules = projectManager.getModulesByResource(resource);
    return modules;
  }

  public static Collection<ASNModule> getModulesForProject(IProject project) {
    ProjectManager projectManager = WorkspaceManager.getProjectManager(project);
    Collection<ASNModule> modules = projectManager.getModules();
    return modules;
  }

  public static Collection<ASNModule> getModules(IProject project, String moduleName) {
    if (project == null) {
      return new LinkedList<ASNModule>();
    }
    ProjectManager projectManager = WorkspaceManager.getProjectManager(project);
    Collection<ASNModule> modules = projectManager.getModulesByName(moduleName);
    return modules;
  }

  private static void findDeclarationInModules(String identifier,
      Collection<IASN> result, Collection<ASNModule> modules) {
    for (ASNModule module : modules) {
      final IASN asnDecl = module.findDeclaration(identifier);
      if (asnDecl != null) {
        result.add(asnDecl);
      }
    }
  }

  public static ASNModule getSurroundingModule(IResource res, int offset, IDocument document) {
    ASNModule result = null;

    Collection<ASNModule> modules = getModulesForResource(res);
    if (!modules.isEmpty()) {
      for (ASNModule module : modules) {
        try {
          final int startPos = document.getLineOffset(module.getStartModuleLine()) + module.getStartModuleColumn();
          final int endPos = document.getLineOffset(module.getEndLine()) + module.getEndColumn();
          if (startPos >= offset && offset <= endPos) {
            result = module;
            break;
          }
        } catch (BadLocationException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
    return result;
  }

  public static IResource getSourceFile(IASN asnDecl) {
    IASN parent = asnDecl;
    while (parent != null && !(parent instanceof ASNModule)) {
      parent = asnDecl.getParent();
    }
    if (parent == null) {
      return null;
    }
    return ((ASNModule)parent).getResource();
  }
}
