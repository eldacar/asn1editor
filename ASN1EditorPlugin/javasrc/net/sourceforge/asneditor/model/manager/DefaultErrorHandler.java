package net.sourceforge.asneditor.model.manager;

import net.sourceforge.asneditor.ASNEditorPlugin;
import net.sourceforge.asneditor.editors.IASNConstants;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;

import antlr.RecognitionException;

public class DefaultErrorHandler implements IErrorHandler {
  /** set this to a negative integer in order to show all errors,
   * zero for none, or limit error count to an positive integer */
  public static int MAX_ERRORS = 100;

  private int errorCount;
  private int warningCount;

  private IResource resource;

  public int getErrorCount() {
    return errorCount;
  }

  public int getWarningCount() {
    return warningCount;
  }

  public IResource getResource() {
    return resource;
  }

  public void processing(IResource resource) {
    this.resource = resource;
    errorCount = 0;
    warningCount = 0;
    if (MAX_ERRORS != 0) {
      ASNEditorPlugin.deleteMarkers(resource, IMarker.PROBLEM);
      ASNEditorPlugin.deleteMarkers(resource, IASNConstants.ProblemMarker);
    }
  }

  public void reportError(RecognitionException e) {
    if (e == null)
      return;
    reportError(e.getMessage(), e, e.getLine(), e.getColumn());
  }

  public void reportError(String message, Exception e, int line, int column) {
    reportMessage(message, line, column, IMarker.SEVERITY_ERROR);
  }

  public void reportWarning(String str) {
    reportMessage(str, 0, 0, IMarker.SEVERITY_WARNING);
  }

  public void reportMessage(String message, int line, int column, int messageKind) {
    if (messageKind == IMarker.SEVERITY_ERROR) {
      errorCount++;
    }
    if (messageKind == IMarker.SEVERITY_WARNING) {
      warningCount++;
    }
    if (MAX_ERRORS >= 0 && errorCount > MAX_ERRORS) {
      // ignore other errors if more than MAX_ERRORS, 
      // or a negative number for MAX_ERRORS is specified
      return;
    }
    // only column
    message = ASNEditorPlugin.addLinColumnToMessage(message, -1, column);
//    int offset = ASNEditorPlugin.findOffset(line, column, getViewer());
//    addAnnotation(message, messageKind, new Position(offset), line);
//    ASNEditorPlugin.createMarker(resource, IASNConstants.ProblemMarker, messageKind, message, column, column, line);
    ASNEditorPlugin.createMarker(resource, IMarker.PROBLEM, messageKind, message, -1, -1, line);
//    System.out.println(e.getMessage());
//    e.printStackTrace();
  }
}
