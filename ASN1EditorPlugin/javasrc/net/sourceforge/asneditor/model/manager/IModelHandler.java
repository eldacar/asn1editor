/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.model.manager;

import net.sourceforge.asneditor.model.ASNModule;

import org.eclipse.core.resources.IResource;

public interface IModelHandler extends IErrorHandler {

  void processing(IResource moduleResource);

  void finishedParsing();

  void addModule(ASNModule module);

  void setErrorHandler(IErrorHandler errorHandler);

  IErrorHandler getErrorHandler();
}
