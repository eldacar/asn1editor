/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.model.manager;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

public class WorkspaceManager {
  private static WorkspaceManager singleton;

  public static WorkspaceManager getInstance() {
    if (singleton == null) {
      singleton = new WorkspaceManager();
    }
    return singleton;
  }

  private Map<String, ProjectManager> projects = new Hashtable<String, ProjectManager>();

  public static ProjectManager getProjectManager(IProject prj) {
    String projectName = prj.getName();
    return getProjectManager(projectName);
  }

  public static ProjectManager getProjectManager(String projectName) {
    return getInstance().getInternalProjectManager(projectName);
  }

  protected ProjectManager getInternalProjectManager(String projectName) {
    ProjectManager modelManager = projects.get(projectName);
    if (modelManager == null) {
      modelManager = createManager(projectName);
    }
    return modelManager;
  }

  protected ProjectManager createManager(String projectName) {
    ProjectManager projectManager = new ProjectManager(projectName);
    projects.put(projectName, projectManager);
    projectManager.build();
    return projectManager;
  }

  public static IModelHandler createModelHandler() {
    IConfigurationElement internal = null;
    IConfigurationElement overwrite = null;
    Vector<IConfigurationElement> additive = new Vector<IConfigurationElement>();

    IExtensionRegistry registry = Platform.getExtensionRegistry();
    IExtensionPoint extensionPoint = registry.getExtensionPoint(IErrorHandler.EXT_ID);
    IExtension[] extensions = extensionPoint.getExtensions();
    // For each extension ...
    for (IExtension extension : extensions) {
      IConfigurationElement[] elements = extension.getConfigurationElements();
      // For each member of the extension ...
      for (IConfigurationElement element : elements) {

        ErrorHandlerKind kind = ErrorHandlerKind.decode(element.getAttribute(IErrorHandler.EXT_ATTR_KIND));
        switch (kind) {
        case internal:
          internal = element;
          break;
        case overwrite:
          overwrite = element;
          break;

        case additive:
        default:
          additive.add(element);
        break;
        }
//      System.err.println(extension.getSimpleIdentifier()+" "+element.getAttribute("class"));
      }
    }


    if (overwrite != null) {
      additive.clear();
    }

    IErrorHandler errorHandler = createHandler(overwrite);
    if (errorHandler == null) {
      errorHandler = createHandler(internal);
    }
    if (errorHandler == null) {
      // TODO process additive if no internal & and no overwrite found
//      return null;
      errorHandler = new DefaultErrorHandler();
    }
    if (errorHandler == null) {
      errorHandler = new DefaultErrorHandler();
    }
    DefaultModelHandler defaultModelHandler = new DefaultModelHandler();
    defaultModelHandler.setErrorHandler(errorHandler);
    return defaultModelHandler;
  }

  private static IErrorHandler createHandler(IConfigurationElement element) {
    if (element == null)
      return null;
    try {
      IErrorHandler errorHandler = (IErrorHandler) element.createExecutableExtension(IErrorHandler.EXT_ATTR_CLASS);
      return errorHandler;
    } catch (Exception e) {
      return null;
    }
  }
}
