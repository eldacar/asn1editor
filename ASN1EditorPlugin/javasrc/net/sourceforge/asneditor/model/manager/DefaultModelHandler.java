/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * 
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 * 
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 * 
 */
package net.sourceforge.asneditor.model.manager;

import net.sourceforge.asneditor.model.ASNModule;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;

import antlr.RecognitionException;

public class DefaultModelHandler implements IModelHandler {

  private IErrorHandler errorHandler;

  public IErrorHandler getErrorHandler() {
    return errorHandler;
  }

  public void setErrorHandler(IErrorHandler errorHandler) {
    this.errorHandler = errorHandler;
  }

  public void addModule(ASNModule module) {
    IProject project = module.getResource().getProject();
    ProjectManager projectManager = WorkspaceManager.getProjectManager(project);
    projectManager.add(module);
  }

  public void processing(IResource moduleResource) {
    getErrorHandler().processing(moduleResource);
  }

  public void finishedParsing() {
    // TODO Auto-generated method stub
  }

  public void reportError(RecognitionException ex) {
    getErrorHandler().reportError(ex);
  }

  public void reportError(String message, Exception e, int line, int column) {
    getErrorHandler().reportError(message, e, line, column);
  }

  public void reportWarning(String message) {
    getErrorHandler().reportWarning(message);
  }
}
