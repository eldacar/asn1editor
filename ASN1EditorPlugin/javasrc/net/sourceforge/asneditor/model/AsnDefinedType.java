package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnDefinedType extends AsnType {
	boolean	isModuleReference;
	String moduleReference;
	String typeReference;
	AsnConstraint constraint;
	
	// Default Constructor
	AsnDefinedType() {
		name="";
		moduleReference	="";
		typeReference =	"";			
		isModuleReference =	false;
	}
	// toString() Definition
	
	public String toString() {
		String ts =	"";
		if(isModuleReference){
			ts += (moduleReference+	"."	+ typeReference);
		} else {
			ts += (typeReference);
		}
		if(constraint!=null){
			ts += constraint;
		}
	
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return typeReference;
	}
}

//*********************************************
// Definition of MACRO OPERATION
//*********************************************
