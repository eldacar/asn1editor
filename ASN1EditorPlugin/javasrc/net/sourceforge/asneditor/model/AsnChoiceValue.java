package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnChoiceValue extends IASN {
	AsnValue value;
	
	// Default Constructor
	AsnChoiceValue() {
	}
	
	// toString Method
	public String toString(){
		String ts ="";
		ts += name;
		ts += "\t" + ":" + value;
	return ts;
	}
}
//*********************************************
// Definition of SignedNumber
//*********************************************
