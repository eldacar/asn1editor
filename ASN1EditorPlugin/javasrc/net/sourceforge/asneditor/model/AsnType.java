package net.sourceforge.asneditor.model;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public abstract class AsnType extends IASN {

	public abstract String getTypeName(); 
}
