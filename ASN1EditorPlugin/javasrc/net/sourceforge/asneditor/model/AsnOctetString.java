package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnOctetString extends AsnType {
		
	AsnConstraint constraint;
	final String BUILTINTYPE = "OCTET STRING";

	// Default Constructor
	AsnOctetString() {
		name="";
	}
	
	//toString Definition
	public String toString() {
		String ts =	"";
		ts += name + "\t::=" + BUILTINTYPE+	"\t";
		if(constraint!=null){
			ts += constraint ;
		}
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}
//*********************************************
// Definition of Real
//*********************************************	
