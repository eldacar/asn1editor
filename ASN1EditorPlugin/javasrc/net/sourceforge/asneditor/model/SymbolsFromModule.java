package	net.sourceforge.asneditor.model;

import java.util.Iterator;

import net.sourceforge.asneditor.util.HashableList;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class SymbolsFromModule extends IASN {
	HashableList symbolList ;
	String modref;
	boolean isOidValue;
	boolean isDefinedValue;
	AsnOidComponentList cmplist;
	AsnDefinedValue	defval;
	
	// Default Constructor
	SymbolsFromModule(){
		symbolList = new HashableList();
	}
	
	// toString Method
	public String toString(){
		String ts = "Following SYMBOLS ::\n" ;
		Iterator s = symbolList.iterator() ;
		if(s!=null){
			while(s.hasNext()){
				ts += s.next() + "\n";
			}
		}
		ts += "ARE IMPORTED FROM \n";
		ts += modref;
		if(isOidValue)
			ts += cmplist;
		if(isDefinedValue)
			ts += defval;
			
	return ts;
	}
	
	public String getName() {
		return modref;
	}

	/**
	 * @return an iterator over the list of imported symbols
	 */
	public Iterator symbols() {
    return symbolList.iterator();
	}
}
//*********************************************
// Definition of Basic Type
//*********************************************
