package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class ErrorMacro extends AsnMacro {
	String parameterName;
	boolean	isParameter;
	AsnType typeReference;
	boolean	isDefinedType;			// Type	is Defined or builtin type
	String typeName;				// Name	of the Defined Type
	
	// Default Constructor
	ErrorMacro() {
	}
		
	// toString	Method Definition
	
	public String toString() {
		String ts =	"";
		ts += (name	+ "\t::=\tERROR");				
		if(isParameter){
			if(parameterName!=""){
				ts += ("PARAMETER\t" + parameterName);
			} else {
				ts += ("PARAMETER\t" );
				if(isDefinedType)
					ts += (typeName);
				else
					ts += (typeReference.getClass().getName());
			}
		}
	return ts;
	}
}

//*********************************************
// Definition of OBJECT-TYPE Macro
//*********************************************
