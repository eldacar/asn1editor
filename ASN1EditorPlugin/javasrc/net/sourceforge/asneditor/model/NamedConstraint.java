package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class NamedConstraint extends IASN {
	boolean isConstraint;
	boolean isPresentKw;
	boolean isAbsentKw;		
	boolean isOptionalKw;
	AsnConstraint constraint;
	
	NamedConstraint(){
	}
	
	public String toString() {
		String ts = "";
		ts += name ;
		if(isConstraint){
			ts += constraint;
		}
		if(isPresentKw)
			ts += "\t" + "PRESENT";
		if(isAbsentKw)
			ts += "\t" + "ABSENT";
		if(isOptionalKw)
			ts += "\t"+ "OPTIONAL";
	return ts ;			
	}
}
//*********************************************
// Definition of Value
//*********************************************
