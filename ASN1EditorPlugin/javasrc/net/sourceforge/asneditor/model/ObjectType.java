package	net.sourceforge.asneditor.model;

import java.util.ArrayList;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class ObjectType extends AsnType {

	String accessPart;	String statusPart;
	ArrayList elements ;
	AsnValue value;
	AsnType type;
	final String BUILTINTYPE = "OBJECT-TYPE";
	
	// Default Constructor
	ObjectType(){
		elements = new ArrayList();
	}
	
	public String toString() {
		String ts =	"";
		ts += ("AccessPart is "	+ accessPart);
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}

//*********************************************
// Definition of Element Type List
//*********************************************
