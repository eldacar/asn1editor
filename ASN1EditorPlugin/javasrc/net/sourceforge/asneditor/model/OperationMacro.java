package	net.sourceforge.asneditor.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class OperationMacro extends AsnMacro  {
	String argumentTypeIdentifier;	// Argument NamedType identifier
	AsnType argumentType;			// Holds the argument Type
	public String argumentName;		// Argument	Type Name 
	String resultTypeIdentifier;		// Result NamedType identifier 
	AsnType resultType;				// Holds the resultType
	String resultName;				// Result Type Name
	boolean	isArgumentName;			
	boolean	isResult;
	boolean	isLinkedOperation;		
	boolean	isResultName;
	boolean	isErrors;			
	ArrayList errorList 	;	
	ArrayList linkedOpList	;		
	
	// Default Constructors
	OperationMacro(){
		errorList 	= new ArrayList();	
		linkedOpList	= new ArrayList();		
	}
	
	// toString() definition
	
	public String toString() {
		String ts =	"";
		ts += (name	+ "\t::=\t OPERATION");
		if(isArgumentName)
				ts += ("ARGUMENT\t\t" +	argumentName);
		if(isResult){
				ts += ("RESULT\t\t"	+ resultName);
		}
		if(isLinkedOperation){
			ts += ("LINKED OPERATION\t"	);
			Iterator e = linkedOpList.iterator();
			if(e.hasNext()){
				while(e.hasNext())
						ts += (e.next());
			}
			ts += ("}");
		}
		if(isErrors){
			ts += ("ERRORS\t{");			  
			Iterator e = errorList.iterator();
			if(e.hasNext()){
				while(e.hasNext())
					ts += e.next();
			}
			ts += ("}");
		}
	return ts ;	   
	}
	
	// Get the first linked operationName
	public String get_firstLinkedOpName(){
		Object obj = linkedOpList.get(0);
		if((AsnValue.class).isInstance(obj)){
			return "isValue";
		}
		else if((AsnDefinedType.class).isInstance(obj)){
			return ((AsnDefinedType)obj).typeReference;
		}else {
			String nameoftype = null;
			try {
				Field nameField;
				Class c = obj.getClass();
				nameField = c.getField("name");
				nameoftype = (String)nameField.get(obj);
			}catch(Exception e){
				e.printStackTrace(System.err);		
			}
		return nameoftype;
		}	
	}
} 
//*********************************************
// Definition of Macro ERROR
//*********************************************	
