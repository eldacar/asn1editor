package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnCharacterStringValue extends IASN {
		
	boolean isTuple;	boolean isQuadruple;
	boolean isCharDefList;	String cStr;
	ArrayList charDefsList ;	
	ArrayList tupleQuad ;
	
	//Default Constructor
	AsnCharacterStringValue()  {
		charDefsList = new ArrayList();	
		tupleQuad = new ArrayList();
	}
		
	public String toString() {
		String ts = "";
		if (isTuple || isQuadruple){
			Iterator i = tupleQuad.iterator();
			while(i.hasNext()){
				ts+=i.next() + "\n" ;
			}
		}
		else if(isCharDefList){
			Iterator i = charDefsList.iterator();
			while(i.hasNext()){
				ts += i.next();
			}
		}
	return ts;
	}
}
// 