package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnValue extends IASN {
		
	String typeName;			boolean	isTrueKW;
	boolean	isFalseKW;			boolean	isNullKW;
	boolean	isPlusInfinity;		boolean	isMinusInfinity;
	boolean	isCString;			boolean	isSignedNumber;
	boolean	isDefinedValue;		boolean isBStrOrOstrValue;
	boolean isCStrValue;		boolean isSequenceValue;
	boolean isSequenceOfValue;	boolean isEnumIntValue;
	boolean isAsnOIDValue;		boolean isChoiceValue;
	String bStr, cStr, hStr, enumIntVal;
	AsnBitOrOctetStringValue bStrValue;
	AsnCharacterStringValue cStrValue;
	AsnSequenceValue seqval;	
	AsnSequenceOfValue seqOfVal;
	AsnChoiceValue chval;
	AsnDefinedValue	definedValue;
	AsnSignedNumber	signedNumber;
	AsnOidComponentList oidval;
	
	//Default Constructor	
	AsnValue() {
	}
		
	// toString() Method Definition
	
	public String toString() {
		String ts =	"";
		if(name != null && typeName!= null){
			ts += (name	+"\t" +	typeName);
		}
		if(isTrueKW){
			ts += ("\tTRUE");
		}
		else if(isFalseKW){
			ts += ("\tFALSE");
		}
		else if(isNullKW){
			ts += ("\tNULL");
		}
		else if(isPlusInfinity){
			ts += ("\tplusInfinity");
		}
		else if(isMinusInfinity){
			ts += ("\tminusInfinity");
		}
		else if(isCString){
			ts += ("\t"	+ cStr);
		}
		else if(isBStrOrOstrValue){
			ts += ("\t"	+ bStrValue);
		}
		else if(isCStrValue){
			ts+= ("\t" + cStrValue);
		}
		else if(isSequenceValue){
			ts +=("\t" + seqval);
		}
		else if(isChoiceValue){
			ts +=("\t" + chval);
		}
		else if(isEnumIntValue){
			ts +=("\t" + enumIntVal);
		}
		else if(isSignedNumber){
			ts += signedNumber;
		}
		else if(isAsnOIDValue){
			ts += oidval;
		}
		else if(isSequenceOfValue){
			ts += seqOfVal;
		}
		else if(isDefinedValue){
			ts += (definedValue);
		}
		else{
			ts += ("Unknown	Value");
		}
	return ts;
	}
} 
//*********************************************
// Definition of AsnBitStringOrOctetStringValue
//*********************************************
