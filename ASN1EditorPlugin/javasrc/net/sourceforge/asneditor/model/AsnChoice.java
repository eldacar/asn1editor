package	net.sourceforge.asneditor.model;

import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnChoice extends AsnType	{
	AsnElementTypeList elementTypeList;
	final String BUILTINTYPE = "CHOICE";
	
	// Default Constructor
	AsnChoice()	{
		name="";
	}
	public String toString() {
		String ts =	"";
		ts += name + "\t::=\t" + BUILTINTYPE + "\t {";
		if(elementTypeList!=null){
			Iterator e = elementTypeList.elementsIterator();
			while(e.hasNext()){
				ts += e.next();
			}
		}
		ts += "}" ;
	return ts ;
	}		

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
} 
//*********************************************
// Definition of ENUMERATED
//*********************************************	
