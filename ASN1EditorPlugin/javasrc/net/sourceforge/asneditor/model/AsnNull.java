package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnNull extends AsnType {
		
	final String BUILTINTYPE = "NULL";
	
	// Default Constructor
	AsnNull() {
		name="";
	}
	
	//toString() definition
	public String toString() {
		String ts =	"";
		ts += name + "\t::=\t" + BUILTINTYPE ;
	return ts ;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}
				
//*********************************************
// Definition of ObjectIdentifier
//*********************************************	
