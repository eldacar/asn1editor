package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnNamedValue extends IASN	{
	AsnValue value;
	
	// Default Constructor
	AsnNamedValue() {
	}
	
	public String toString() {
		String ts =	"";
		ts += name;
		ts += ("\t"	+ value);
	return ts;
	}
	
	public String toString(boolean name){
		String ts ="";
		if(name){
			ts += name;
		}
		else {
			ts += value;
		}
	return ts;
	}
}
//*********************************************
// Definition of Defined Value
//*********************************************
