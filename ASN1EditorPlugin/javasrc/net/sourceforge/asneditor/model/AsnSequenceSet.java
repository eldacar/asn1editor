package	net.sourceforge.asneditor.model;

import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnSequenceSet extends AsnType {
		
	// name = Name of Sequence
	AsnElementTypeList elementTypeList;
	boolean	isSequence;
	final String BUILTINTYPE = "SEQUENCE";
	final String BUILTINTYPE1 =	"SET";
	
	// Default Constructor
	AsnSequenceSet() {
		name = "";
		isSequence = false;
	}

	// toString	definition
	
	public String toString() {
		String ts =	"";
		ts += (name);
		if(isSequence)
			ts += "\t::=" +	BUILTINTYPE	+ "\t";
		else
			ts += "\t::=" +	BUILTINTYPE1 + "\t";
		
		ts +="{" ;
		
		if(elementTypeList!=null){
			Iterator e = elementTypeList.elementsIterator();
			while(e.hasNext()){
				ts += e.next();
			}
		}
		ts += "}";
	return ts;
	}		

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
} // End Class

//*********************************************
// Definition of Sequence  & Set OF	Elements
//*********************************************
