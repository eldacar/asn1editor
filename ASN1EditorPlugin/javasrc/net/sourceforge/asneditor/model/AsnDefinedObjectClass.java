package net.sourceforge.asneditor.model;


public class AsnDefinedObjectClass extends AsnType {
  String typeReference;
  boolean isModuleReference;
  String moduleReference;

  public AsnDefinedObjectClass() {
    typeReference = "";
  }

  public String toString() {
    String ts = "";
    if(isModuleReference){
      ts += (moduleReference+ "." + typeReference);
    } else {
      ts += (typeReference);
    }
  
  return ts;
  }

  /* (non-Javadoc)
   * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
   */
  public String getTypeName() {
    return typeReference;
  }
}
