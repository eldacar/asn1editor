package	net.sourceforge.asneditor.model;

import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnEnum extends AsnType {
	public String snaccName;	// specifically added for snacc code generation
	AsnNamedNumberList namedNumberList;
	final String BUILTINTYPE = "ENUMERATED";

	// Default Constructor
	AsnEnum() {
		name="";
		snaccName = "";
	}
	public String toString() {
		String ts =	"";
		ts += name + "\t::=" + BUILTINTYPE + "\t{" ;
		if(namedNumberList!=null){		
				Iterator nl	= namedNumberList.namedNumbersIterator();
				while(nl.hasNext()){
						ts += nl.next();
				}
		}
	ts +="}";
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}
//*********************************************
// Definition of EMBEDDED PDV TYPE
//*********************************************	
