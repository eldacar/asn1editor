package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

import antlr.RecognitionException;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnNamedNumberList extends IASN {
	private ArrayList namedNumbers;
	
	// Default Constructor
	AsnNamedNumberList() {
		namedNumbers = new ArrayList();
	}
	
	// Return the total number of elements in the list
	public int count(){
		return namedNumbers.size();
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.grammar.IASN#toString()
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		Iterator iterator = namedNumbers.iterator();
		while (iterator.hasNext()) {
			Object element = (Object) iterator.next();
			buf.append(element);
		}
		return buf.toString();
	}

	/**
	 * @param nnum
	 * @throws RecognitionException
	 */
	public void addNamedNumber(AsnNamedNumber nnum, String filename) throws RecognitionException {
		final String fieldName = nnum.name;

		if (!nnum.isDotDotDot) {
			Iterator iterator = namedNumbers.iterator();
			while (iterator.hasNext()) {
				final AsnNamedNumber element = (AsnNamedNumber) iterator.next();
				if (!nnum.isDotDotDot && element.name.equals(fieldName)) {
					throw new RecognitionException("Identifier "+element.name+
							" allready defined at line "+element.getLine()+" col "+element.getColumn()+" ",
							filename, nnum.getLine(), nnum.getColumn());
				}
			}
		}

		namedNumbers.add(nnum);
	}

	/**
	 * @return
	 */
	public Iterator namedNumbersIterator() {
		return namedNumbers.iterator();
	}
}
//*********************************************
// Definition of NamedNumber
//*********************************************
