package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnObjectIdentifier extends AsnType {
		
	final String BUILTINTYPE = "OBJECT IDENTIFIER";
	
	// Default Constructor
	AsnObjectIdentifier() {
		name="";
	}

	//toString() definition
	public String toString() {
		String ts =	"";
		ts += name + "\t::=\t" + BUILTINTYPE;
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}
//*********************************************
// Definition of Octet String
//*********************************************	
