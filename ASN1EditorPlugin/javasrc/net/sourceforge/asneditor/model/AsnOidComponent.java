package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnOidComponent extends IASN {
		
	boolean	nameForm;
	boolean	numberForm;
	boolean	nameAndNumberForm;
	boolean isDefinedValue;
	AsnDefinedValue defval;
	Integer	num;

	// Default Constructor
	AsnOidComponent() {
		name ="";
		num = null;
	}

	// toString	Implementation
	public String toString() {
		String ts =	"";
		if(numberForm){
			ts += num +	"\t"; 
		}
		else if(nameForm){
			ts	+= name;
			if(nameAndNumberForm){
				ts += "(" +	num	+ ")\t";	
			}
		}
		else if(isDefinedValue){
			ts += defval;
		}
	return ts;
	}
}
//*********************************************
// Definition of Symbols From Module List
//*********************************************
