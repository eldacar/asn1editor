package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnExternal extends AsnType {
	final String BUILTINTYPE = "EXTERNAL";
	// Default Constructor
	AsnExternal(){
	}

	public String toString() {
		String ts =	"";
		ts += BUILTINTYPE;
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
} 
//*********************************************
// Definition of INTEGER
//*********************************************	
