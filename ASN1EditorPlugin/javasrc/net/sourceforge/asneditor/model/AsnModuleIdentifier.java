package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
//*********************************************
//Definition of ASN ModuleIdentifier
//*********************************************
public class AsnModuleIdentifier extends IASN	{
	AsnOidComponentList	componentList;
	// Default Constructor
	AsnModuleIdentifier	() {
		componentList =	new	AsnOidComponentList();
	}
	// toString	Implementation
	public String toString() {
		String ts =	"" ;
		ts = name +	"  ";
		if(componentList!=null)
			ts += componentList;
	return ts;
	}
}
//*********************************************
// Definition of OID_Component_LIST
//*********************************************
