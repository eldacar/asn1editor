package	net.sourceforge.asneditor.model;

import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnBitString extends AsnType {
	AsnNamedNumberList namedNumberList;
	AsnConstraint constraint;
	final String BUILTINTYPE = "BIT	STRING";

	// Default Constructor	
	AsnBitString() {
		name="";
	}
	
	// toString	definition
	public String toString(){
		String ts =	"";
		ts += (name+ "\t::=\t" + BUILTINTYPE + "\n {");		  
		if(namedNumberList!=null){		
				Iterator nl	= namedNumberList.namedNumbersIterator();
				while(nl.hasNext()){
						ts += nl.next();
				}
		}
		ts += "}";
		if(constraint!=null){
			ts += constraint ;
		}
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}
//*********************************************
// Definition of Character String
//*********************************************	

