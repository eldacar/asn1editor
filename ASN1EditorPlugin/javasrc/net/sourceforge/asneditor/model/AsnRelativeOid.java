package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnRelativeOid extends AsnType {
	final String BUILTINTYPE = "RELATIVE-OID";
	
	// Default Constructor
	AsnRelativeOid() {
	}
	
	public String toString() {
		String ts =	"";
		ts += BUILTINTYPE;
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
} 
//*********************************************
// Definition of Sequence  & Set 
//*********************************************
