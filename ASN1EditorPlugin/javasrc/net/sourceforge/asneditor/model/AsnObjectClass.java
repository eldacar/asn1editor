package net.sourceforge.asneditor.model;

import java.util.Iterator;

/** This class defines the class holding for ASN.1 modules and basic Types    */
public class AsnObjectClass extends AsnType { // name = Name of Sequence
  AsnElementTypeList classElement_list;
  AsnElementTypeList syntaxElement_list;
  final String BUILTINTYPE = "CLASS";
  
  // Default Constructor
  AsnObjectClass() {
    name = "";
  }

  // toString definition
  
  public String toString() {
    String ts = "";
    ts += (name);
    ts += "\t::=" + BUILTINTYPE + "\t";
    ts +="{" ;
    
    if(classElement_list!=null){
      Iterator e = classElement_list.elementsIterator();
      while(e.hasNext()){
        ts += e.next();
      }
    }
    ts += "}";
  return ts;
  }   

  /* (non-Javadoc)
   * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
   */
  public String getTypeName() {
    return BUILTINTYPE;
  }
}
//*********************************************
// Definition of OBJECT CLASS
//********************************************* 
