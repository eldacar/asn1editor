package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnCharacterString extends AsnType {
		
	boolean isUCSType;	// Is Unrestricted Character String Type
	String stringtype ;
	AsnConstraint constraint;
	final String BUILTINTYPE = "CHARACTER STRING";
	
	// Default Constructor
	AsnCharacterString() {
			name="";
			stringtype = "";
	}
	public String toString() {
		String ts =	"";
		ts += name+	"\t" + stringtype ;
		if(constraint!=null){
			ts += constraint ;	
		}
	return ts ;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}

//*********************************************
// Definition of Boolean
//*********************************************	
