package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnSequenceOf extends AsnType	{
		
	// name;	  // Refers	to assignment name
	AsnConstraint constraint;
	Object typeReference; // Refers	to typeReference after OF KW
	boolean	isSequenceOf; // Differntiates between SEQUENCE	OF and SET OF types
	boolean	isDefinedType;
	boolean isSizeConstraint;
	String typeName;	// Name	of the defined type
	final String BUILTINTYPE = "SEQUENCE OF";
	final String BUILTINTYPE1 =	"SET OF";
	
	// Default Constructor
	AsnSequenceOf()	{
		name = "";
		constraint = null;
		typeReference = null;
		isSequenceOf = false;
		isDefinedType = false;
		isSizeConstraint = false;
		typeName = "";
		
	}
	
	// toString	definition
	public String toString() {
		String ts =	"";			
		ts += name + "\t::=\t";
		if(isSequenceOf){
			ts += ("SEQUENCE\t");
			if(constraint!=null)
					ts += (constraint);			  
			ts += ("\tOF\t");
		}else {
			ts += ("SET\t");
			if(constraint!=null)
					ts += (constraint);			  
			ts += ("\tOF\t");
		}
		if(isDefinedType){
			ts += (typeName);
		} else {
			ts += (typeReference.getClass().getName());	  // Print builtinType Class Name 
		}
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
} 
//*********************************************
// Definition of Tagged	Type
//*********************************************
