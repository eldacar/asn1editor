package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnTag extends IASN {
	String clazz;
	AsnClassNumber classNumber;
	
	// Default Constructor
	AsnTag() {
		clazz =	"";
	}

	//toString() definition
	public String toString() {
		String ts =	"";
		ts += ("[");
		if(clazz!= ""){
				ts += (clazz);
		}
		if(classNumber!=null){
				ts += (classNumber);
		}
		ts += ("]");
	return ts;
	}
	
	public String getName() {
		return toString();
	}
}
//*********************************************
// Definition of Class_Number
//*********************************************
