package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class Intersection extends IASN {
	boolean isInterSection;
	boolean isExcept;
	ArrayList cnsElemList ;
	ArrayList exceptCnsElem ;	
	
	// Default Constructor
	Intersection(){
		cnsElemList = new ArrayList();
		exceptCnsElem = new ArrayList();	
	}
		
	// toString Method
	public String toString(){
		String ts = "";
		Iterator e = cnsElemList.iterator();
		Iterator i = exceptCnsElem.iterator();
		while(e.hasNext()){	
			ts += "\t" + e.next();
		}
		if(isExcept){
			ts += "EXCEPT" ;
			while(i.hasNext()){
				ts += "\t" + i.next();
			}
		}
	return ts;		
	}
}
//*********************************************
// Definition of Constraint_Elements
//*********************************************
