package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnBitOrOctetStringValue extends IASN {
	boolean isHString;
	boolean isBString;
	String bhStr;
	ArrayList idlist ;	
	
	//Default Constructor
	AsnBitOrOctetStringValue()  {
		idlist = new ArrayList();	
	}
	
	public String toString() {
		String ts = "";
		if(isHString ||isBString)
			ts += bhStr;
		else{
			if(idlist!=null){
				Iterator e = idlist.iterator();
				while(e.hasNext()){
					ts += e.next();
				}
			}
		}
	return ts;
	}
	
	public String getName() {
		return toString();
	}
}
//*********************************************
// Definition of AsnCharacterStringValue
//*********************************************
