package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnNamedNumber extends IASN {
	boolean	isSignedNumber;
	AsnSignedNumber	signedNumber ;
	AsnDefinedValue	definedValue ;
	boolean isDotDotDot = false;
	
	// Default Constructor
	AsnNamedNumber() {
		name ="";
	}
	
	// toString() Method Definition
	
	public String toString() {
		String ts =	"";
		ts += (name	+ "\t(");
		
		if(isDotDotDot) {
			ts += "...";
		} else if(isSignedNumber){
			ts += (signedNumber);
		} else{
			ts += (definedValue);
		}
		ts += (")");
	return ts;		
	}
} 
// New Definition of Constraints
//*********************************************
// Definition of AsnConstraint
//*********************************************
