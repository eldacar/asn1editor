package	net.sourceforge.asneditor.model;

import java.math.BigInteger;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnSignedNumber extends IASN {
	boolean	positive;
	BigInteger num;
	
	// Default Constructor
	AsnSignedNumber() {
		positive = true;
	}
	
	// toString() Method Definition
	public String toString() {
		String ts =	"";
		if(num!=null){		
			ts += (num);
		}
		else{
			ts += ("Signed Number is Null");
		}
	return ts;
	}		
	
	public String getName() {
		return (num != null) ? num.toString() : "NaN";
	}
} 
//*********************************************
// Definition of NamedValue
//*********************************************
