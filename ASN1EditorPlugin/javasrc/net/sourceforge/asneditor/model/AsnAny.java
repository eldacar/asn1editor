package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnAny extends AsnType {
		
	boolean	isDefinedBy;
	String definedByType;
	final String BUILTINTYPE = "ANY";
	
	// Default Constructor
	AsnAny() {
		name = "";
		isDefinedBy	= false;
		definedByType =	"";
	}
	
	public String toString() {
		return name;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return BUILTINTYPE;
	}
}
//*********************************************
// Definition of BIT STRING	Type
//*********************************************
