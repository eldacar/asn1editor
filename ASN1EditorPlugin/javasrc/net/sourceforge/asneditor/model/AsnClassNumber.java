package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnClassNumber extends IASN	{
	Integer	num;
	
	// Default Constructor
	AsnClassNumber() {
	}
	
	//toString definition
	public String toString() {
		String ts =	"";
		if(num!=null){
			ts += (num);
		}else {
			ts += (name);
		}
	return ts;
	}
}		

//*********************************************
// Definition of Defined Type
//*********************************************
