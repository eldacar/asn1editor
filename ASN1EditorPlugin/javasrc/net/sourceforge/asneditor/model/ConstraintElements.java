package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class ConstraintElements extends IASN {
	
	boolean isValue;			boolean isValueRange;
	boolean isSizeConstraint;	boolean isElementSetSpec; 	
	boolean isIncludeType;		boolean isPatternValue; 	
	boolean isWithComponent;	boolean isWithComponents;	
	boolean isMinKw;			boolean isMaxKw;
	boolean isLEndLess;			boolean isUEndLess;
	boolean isTypeConstraint;	boolean isAlphabetConstraint;
	
	AsnConstraint constraint;
	AsnValue value,lEndValue,uEndValue;
	ElementSetSpec elespec;
	Object type;
	ArrayList typeConstraintList ;
		
	ConstraintElements(){
		typeConstraintList = new ArrayList();
	}	
	
	//toString definition
	public String toString(){
		String ts = "";
		if(isValue){
			ts += value;
		}
		else if(isValueRange){
			ts += lEndValue + ".." + uEndValue;
		}
		else if(isSizeConstraint){
			ts += constraint ;
		}
		else if(isElementSetSpec){
			ts += elespec;
		}
		else if(isPatternValue){
			ts += "PATTERN" + value;
		}		
		else if(isWithComponent){
			ts += "WITH COMPONENT " + constraint;
		}
		else if(isWithComponents){
			Iterator e = typeConstraintList.iterator();
			ts += "WITH COMPONENTS" + "\t";
			while(e.hasNext()){
				ts += e.next();
			}
		}
		else if(isAlphabetConstraint){
			ts +="FROM" + constraint;
		}
	return ts ;			
	}
//	public String getName(){
//		if(isValue){
//			return value.getName();
//		}
//		else if(isValueRange){
//			return lEndValue.getName() + ".." + uEndValue.getName();
//		}
//		else if(isSizeConstraint){
//			return constraint.getName() ;
//		}
//		else if(isElementSetSpec){
//			return elespec.getName();
//		}
//		else if(isPatternValue){
//			return value.getName();
//		}		
//		else if(isWithComponent){
//			return constraint.getName();
//		}
//		else if(isWithComponents){
//			Iterator e = typeConstraintList.iterator();
//			ts += "WITH COMPONENTS" + "\t";
//			while(e.hasNext()){
//				ts += e.next();
//			}
//		}
//		else if(isAlphabetConstraint){
//			return constraint.getName();
//		}
//	return ts ;			
//	}
}
