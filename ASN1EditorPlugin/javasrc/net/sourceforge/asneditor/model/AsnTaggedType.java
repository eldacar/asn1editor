package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnTaggedType extends AsnType	{
		
	AsnTag tag;
	String tagDefault ;
	AsnType typeReference;  // Type Reference
	boolean	isDefinedType; // Distinguish between builtin and defined types
	String typeName;	   // Name of defined type
	
	// Default Constructor
	AsnTaggedType()	{
		name="";
		tagDefault = "";
		typeReference =	null;
		isDefinedType =	false;
		typeName = "";
	}
	
	//toString() definition
	
	public String toString() {
		String ts =	"";
		ts += name;
		ts += ("\t"	+ tag +	"\t" + tagDefault+"\t" );
		if(isDefinedType){
			ts += (typeName);
		}
		else{		
			ts += (typeReference.getClass().getName());
		}
	return ts;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.asneditor.model.AsnType#getTypeId()
	 */
	public String getTypeName() {
		return typeName;
	}
}

//*********************************************
// Definition of Selection Type
//*********************************************
