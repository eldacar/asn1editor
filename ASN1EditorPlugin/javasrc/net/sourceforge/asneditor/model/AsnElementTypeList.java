package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import antlr.RecognitionException;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnElementTypeList extends IASN {
	private ArrayList elements;
	private Hashtable names;
	
	// Default Constructor
	AsnElementTypeList(){
		elements = new ArrayList();
		names = new Hashtable();
	}

	public void addElementType(AsnElementType elemType, String filename) throws RecognitionException {
		if (!elemType.isDotDotDot) {
			if (elemType.name == null) {
				throw new RecognitionException("Undefined identifier name: line"+
						elemType.getLine()+" col "+elemType.getColumn()+" ",
						filename, elemType.getLine(), elemType.getColumn());
			}
			final AsnElementType element = (AsnElementType) names.get(elemType.name);
			if (element != null) {
				throw new RecognitionException("Identifier "+element.name+
						" allready defined at line "+element.getLine()+" col "+element.getColumn()+" ",
						filename, elemType.getLine(), elemType.getColumn());
			}
		}
		elements.add(elemType);
		if (elemType.name != null)
			names.put(elemType.name, elemType);
	}

	public String toString() {
		String ts =	"";
	
	return ts;
	}

	/**
	 * @return
	 */
	public Iterator elementsIterator() {
		return elements.iterator();
	}
}
//*********************************************
// Definition of Element Type
//*********************************************
