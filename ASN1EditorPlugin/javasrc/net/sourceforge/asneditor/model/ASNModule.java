package	net.sourceforge.asneditor.model;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import net.sourceforge.asneditor.model.manager.ASNModelUtils;
import net.sourceforge.asneditor.util.HashableList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;

import antlr.RecognitionException;
import antlr.Token;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class ASNModule extends IASN	{

  AsnModuleIdentifier	moduleIdentifier; // Name of Module
  HashableList exportSymbolList;
  HashableList importSymbolList;
  private HashableList importSymbolFromModuleList;

  private HashableList asnTypes;
  private HashableList asnValues;

  boolean	tag;
  boolean	extensible;
  String tagDefault;
  boolean	exported;
  boolean	imported;
  private IASN exportsItem, importsItem;
  private final IResource resource;
  private final long creationTimeStamp;

  // Default Constructor
  public ASNModule(IResource resource){
    this.resource = resource;
    creationTimeStamp = resource.getModificationStamp();

    exportsItem = null;
    exportSymbolList = new HashableList();
    importSymbolList = new HashableList();
    importSymbolFromModuleList = new HashableList();
    asnTypes =	new	HashableList();
    asnValues = new HashableList();
    tagDefault ="";
  }

  // To String Method
  public String toString(){
    String ts = "";
    Enumeration ii;
    ts += "MODULE NAME ::= \n" ;
    ts += moduleIdentifier + "\n";
    ts += "IMPORT SYMBOL LIST"+ "\n";
    Iterator it = importSymbolList.iterator();
    while(it.hasNext()){
      ts += it.next() + "\n";
    }
    ts += "IMPORT SYMBOLS FROM MODULE \n";
    it = importSymbolFromModuleList.iterator();
    while(it.hasNext()){
      ts += it.next() + "\n";
    }
    ts += "EXPORT SYMBOL LIST \n";
    it = exportSymbolList.iterator();
    while(it.hasNext()){
      ts += it.next() + "\n";
    }
    ts += "ASN TYPES LIST \n";
    ts += asnTypes + "\n";

    ts += "ASN VALUES LIST \n";
    ii = asnValues.enumeration();
    while(ii.hasMoreElements()){
      ts += ii.nextElement() + "\n";
    }
    return ts ;
  }

  public Iterator importSymbolListIterator() {
    return importSymbolList.iterator();
  }

  public Iterator importSymbolFromModuleListIterator() {
    return importSymbolFromModuleList.iterator();
  }

  public Iterator exportSymbolListIterator() {
    return exportSymbolList.iterator();
  }

  public Iterator asnTypesIterator() {
    return asnTypes.iterator();
  }

  public Iterator asnValuesIterator() {
    return asnValues.iterator();
  }

  public String getName() {
    return (moduleIdentifier != null) ? moduleIdentifier.getName() : "<unknown>";
  }

  /**
   * @return the resource from which the module was read.
   */
  public IResource getResource() {
    return resource;
  }

  /**
   * @return the time stamp of the resource when this model was build
   */
  public long getCreationTimeStamp() {
    return creationTimeStamp;
  }

  /**
   * @return true if the creation time stamp is the same as the time stamp of the resource
   */
  public boolean isUpToDate() {
    return resource.getModificationStamp() == creationTimeStamp;
  }

  /**
   * @param declarationName
   * @return
   */
  public IASN findDeclaration(String declarationName) {
    if (declarationName == null)
      return null;
    // values
    try {
      final IASN value = (IASN) asnValues.get(declarationName);
      if (value != null)
        return value;
    } catch (Exception e) {
      // TODO: handle exception
      System.err.println(asnValues.get(declarationName).toString());
      e.printStackTrace();
    }
    // types
    final AsnType type = (AsnType) asnTypes.get(declarationName);
    if (type != null)
      return type;

    // imported declarations
    return findImportedDeclaration(declarationName);
  }

  private IASN findImportedDeclaration(String declarationName) {
    Iterator imports = importSymbolFromModuleListIterator();
    while (imports.hasNext()) {
      final Object next = imports.next();
      final SymbolsFromModule importedModule = (SymbolsFromModule) next;

      // MODULE
      if (declarationName.equals(importedModule.getName())) {
        Collection<ASNModule> modules = ASNModelUtils.getModules(getProject(), importedModule.getName());
        if (!modules.isEmpty()) {
          return modules.iterator().next();
        }
      }
      // SYMBOLS
      Iterator it = importedModule.symbols();
      while (it.hasNext()) {
        String importedSymbol = (String) it.next();
        if (!declarationName.equals(importedSymbol)) {
          continue;
        }
        Collection<IASN> findDeclarationsInModule = ASNModelUtils.findDeclarationsInModule(getProject(), importedModule.getName(), importedSymbol);
        if (!findDeclarationsInModule.isEmpty()) {
          return findDeclarationsInModule.iterator().next();
        }
      }
    }
    return null;
  }

  public IProject getProject() {
    return resource.getProject();
  }

  /**
   * @param declarationName
   * @return
   */
  public boolean containsDeclaration(String declarationName) {
    return findDeclaration(declarationName) != null;
  }

  public void setExportsToken(antlr.Token token) {
    if (token == null)
      return;
    if (exportsItem == null) {
      exportsItem = new IASN() {
        public String toString() {
          return getName();
        }};
        exportsItem.name = "EXPORTS";
    }
    exportsItem.setToken(token);
  }

  /**
   * @param token
   */
  public void setExportsEndToken(Token token) {
    if (token == null)
      return;
    if (exportsItem == null) {
      exportsItem = new IASN() {
        public String toString() {
          return getName();
        }};
        exportsItem.name = "EXPORTS";
    }
    exportsItem.setEndToken(token);
  }

  public void setImportsToken(antlr.Token token) {
    if (token == null)
      return;
    if (importsItem == null) {
      importsItem = new IASN() {
        public String toString() {
          return getName();
        }};
        importsItem.name = "IMPORTS";
    }
    importsItem.setToken(token);
  }

  /**
   * @param token
   */
  public void setImportsEndToken(Token token) {
    if (token == null)
      return;
    if (importsItem == null) {
      importsItem = new IASN() {
        public String toString() {
          return getName();
        }};
        importsItem.name = "IMPORTS";
    }
    importsItem.setEndToken(token);
  }

  /**
   * @return
   */
  public IASN getExportsItem() {
    return exportsItem;
  }

  /**
   * @return
   */
  public IASN getImportsItem() {
    return importsItem;
  }

  /**
   * @param val
   * @throws RecognitionException
   */
  public void addValue(AsnValue val, String filename) throws RecognitionException {
    if (val == null)
      return;
    val.setParent(this);
    final AsnValue oldVal = (AsnValue) asnValues.put(val.name, val);
    if (oldVal != null) {
      throw new RecognitionException("Value allready defined at line "+
          oldVal.getLine()+" col "+oldVal.getColumn(), filename, val.getLine(), val.getColumn());
    }
  }

  /**
   * @param val
   * @throws RecognitionException
   */
  public void addType(AsnType val, String filename) throws RecognitionException {
    if (val == null)
      return;
    val.setParent(this);
    final IASN oldType = (IASN) asnTypes.put(val.name, val);
    if (oldType != null) {
      throw new RecognitionException("Type allready defined at line "+
          oldType.getLine()+" col "+oldType.getColumn(), filename, val.getLine(), val.getColumn());
    }
  }

  public int getStartModuleLine() {
    return moduleIdentifier.getLine();
  }

  public int getStartModuleColumn() {
    return moduleIdentifier.getColumn();
  }

  public void addImportSymbolFromModuleList(SymbolsFromModule sym) {
    sym.setParent(this);
    importSymbolFromModuleList.put(sym.modref, sym);
  }

///**
//* Returns the ASN.1 elements corresponding to the given selected text in this ASNModule.
//* The <code>offset</code> is the 0-based index of the first selected character.
//* The <code>length</code> is the number of selected characters.
//*
//* @param offset the given offset position
//* @param length the number of selected characters
//* @return the ASN.1 elements corresponding to the given selected text
//*
//*/
////* @exception JavaModelException if code resolve could not be performed. Reasons include:
////*  <li>This Java element does not exist (ELEMENT_DOES_NOT_EXIST)</li>
////*  <li> The range specified is not within this element's
////*      source range (INDEX_OUT_OF_BOUNDS)
////* </ul>
//public IASN[] codeSelect(int offset, int length) {
//return null;
//}
}
