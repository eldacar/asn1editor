package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class ElementSetSpec extends IASN {
	boolean isAllExcept;
	ConstraintElements allExceptCnselem;
	ArrayList intersectionList ;
	
	// Default Constructor
	ElementSetSpec(){
		intersectionList = new ArrayList();
	}
	
	// toString Method
	public String toString(){
		String ts ="";
		Iterator e = intersectionList.iterator();
		if(e!=null){
			while(e.hasNext()){
				ts += e.next() ;
				ts += "|" ;
			}
		}
		if(isAllExcept){
			ts += "ALL EXCEPT  " + allExceptCnselem;
		}
	return ts ;
	}
}

//-------------------------------------------------------
