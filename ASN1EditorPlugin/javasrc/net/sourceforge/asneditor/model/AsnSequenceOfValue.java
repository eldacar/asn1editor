package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnSequenceOfValue extends IASN {
	boolean isValPresent;
	ArrayList valueList ;
	
	// Default Constructor
	AsnSequenceOfValue(){
		valueList = new ArrayList();
	}
	
	// toString Method
	public String toString(){
		String ts ="";
		Iterator i = valueList.iterator();
		while(i.hasNext()){
			ts += i.next();
		}
	return ts;
	}
}	
//*********************************************
// Definition of ChoiceValue
//*********************************************
