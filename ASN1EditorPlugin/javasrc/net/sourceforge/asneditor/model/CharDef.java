package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
// Class CharDef 
class CharDef extends IASN {

	boolean isCString; 		boolean isTuple;
	boolean isQuadruple;	boolean isDefinedValue;
	AsnDefinedValue defval;	String cStr;
	ArrayList tupleQuad ;	
	
	// Default Constructor
	CharDef(){
		tupleQuad = new ArrayList();	
	}
	
	public String toString() {
		String ts = "";
		if(isCString){
			ts += ("\t"	+ cStr);
		}
		else if (isTuple || isQuadruple){
			Iterator i = tupleQuad.iterator();
			while(i.hasNext()){
				ts+=i.next() + "\n" ;
			}
		}
		else if (isDefinedValue){
			ts += ("\t"	+ defval);
		}
	return ts ;
	}
}
//*********************************************
// Definition of AsnSequenceValue
//*********************************************
