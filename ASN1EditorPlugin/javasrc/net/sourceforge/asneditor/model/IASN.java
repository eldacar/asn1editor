package net.sourceforge.asneditor.model;

import net.sourceforge.asneditor.ASNEditorPlugin;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.ISourceViewer;


/**
 * Base class for ASN grammar
 */
public abstract class IASN {
	public String name = null;
	private Position position = null;
	int line, column, length, endLine, endColumn;
	private IASN parent = null;

	abstract public String toString();

	public String getName() {
		return name;
	}
//
//	public Position getPosition(ISourceViewer viewer) throws BadLocationException {
//		// TODO only once computed ?
////		if (position == null) {
//			return new Position(ASNEditorPlugin.findOffset(line, column, viewer), length);
////		}
//	}

	/**
	 * @return Single line text line position in document
	 */
	public int getLine() {
		return line;
	}

	/**
	 * @return Single line text column position in document
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * @return Single line text length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @return End line of multiline text
	 */
	public int getEndLine() {
		return endLine;
	}

	/**
	 * @return End column of multiline text
	 */
	public int getEndColumn() {
		return endColumn;
	}

	/**
	 * @return position in text
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * @param position
	 */
	public void setPosition(Position position) {
		this.position = position;
	}

	/**
	 * @param sourceViewer
	 * @return
	 */
	public int getOffset(IDocument doc) {
		return ASNEditorPlugin.findOffset(doc, line, column);
	}

  /**
   * @param sourceViewer
   * @return
   */
  public int getOffset(ISourceViewer sourceViewer) {
    return ASNEditorPlugin.findOffset(line, column, sourceViewer);
  }

	/**
	 * Reads the start & end position from the given token
	 * @param token
	 */
	public void setToken(antlr.Token token) {
		if (token == null)
			return;
		line = token.getLine();
		column = token.getColumn();
		length = token.getText().length();
		endLine = line;
		endColumn = column;
	}

	/**
	 * Reads the end position from the given token
	 * @param token
	 */
	public void setEndToken(antlr.Token token) {
		if (token == null)
			return;
		endLine = token.getLine();
		endColumn = token.getColumn();
	}

  public IASN getParent() {
    return parent;
  }

  protected void setParent(IASN parent) {
    this.parent = parent;
  }
}
