package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnElementType extends IASN {

	boolean isDotDotDot;
	boolean	isOptional;			boolean	isDefault;
  boolean isUnique;
	boolean	isComponentsOf;		boolean	isTagDefault;
	boolean	isTag;				boolean	isDefinedType;			// Element type	is defined Type
	AsnTag tag;					AsnValue value;
	String typeTagDefault;	
	//name;			// type
	AsnType typeReference;		// type	Reference
	String typeName;			// If defined type then	typeName
	
	// Default Constructor
	AsnElementType() {
		isOptional = false;
		isUnique = false;
	}
	
	// toString() Method Definition
	
	public String toString() {
		String ts =	"";
		if(isComponentsOf){
				ts += ("\tCOMPONENTS	OF\t");
		} else {
			ts += (name);
			if(isTag){		
				ts += ("\t");
				ts += (tag);  
			}
			if(isTagDefault){		
				ts += ("\t");
				ts += (typeTagDefault);		  
			}
		}
		if(isDotDotDot){
			ts += "...";
		} else if(isDefinedType){				
			ts += ("\t");
			ts += (typeName);
		} else {
			ts += (typeReference.getClass().getName());
		}
    if(isOptional){
      ts += ("\tOPTIONAL");
    }
      if(isUnique){
        ts += ("\tUNIQUE");
    }
		if(isDefault){
				ts += ("\tDEFAULT\t");
				ts += (value);
		}
	return ts;
	}		
}

//*********************************************
// Definition of Named Number List
//*********************************************
