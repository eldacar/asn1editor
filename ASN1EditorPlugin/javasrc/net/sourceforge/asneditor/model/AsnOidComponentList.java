package	net.sourceforge.asneditor.model;

import java.util.ArrayList;
import java.util.Iterator;

/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnOidComponentList extends IASN {
	boolean isDefinitive;
	AsnDefinedValue defval;
	ArrayList components ;
	
	// Default Constructor
	AsnOidComponentList() {
		components = new ArrayList();
	}
	
	// toString Method
	public String toString(){
		String ts = "";
		if(isDefinitive)
			ts += defval;
		if(components!=null){
			Iterator i = components.iterator();
			while(i.hasNext()){
				ts += (i.next());
			}
		}
	return ts;
	}
}
//*********************************************
// Definition of OID_Component
//*********************************************
