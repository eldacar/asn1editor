package	net.sourceforge.asneditor.model;


/**	This class defines the class holding for ASN.1 modules and basic Types		*/
public class AsnDefinedValue extends IASN {
		
	boolean	isDotPresent;
	String moduleIdentifier;
	
	// Default Constructor
	AsnDefinedValue() {
        isDotPresent = false;
	}

	// toString() Method Definition
	public String toString() {
		String ts =	"";
		if(isDotPresent){
			ts += (moduleIdentifier	+ "." +	name) ;
		} else {
			ts += name;
		}
	return ts;
	}
}
