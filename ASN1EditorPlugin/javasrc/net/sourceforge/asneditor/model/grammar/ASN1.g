header {
/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * Based upon the grammar submitted by Vivek Gupta to antlr.org.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.model;

import antlr.*;

import java.math.*;
import java.util.*;

import org.eclipse.core.resources.IResource;

import net.sourceforge.asneditor.util.HashableList;
import net.sourceforge.asneditor.model.manager.IModelHandler;
}

//	Creation of ASN.1 grammar for ANTLR	V2.7.1
// ===================================================
//		  TOKENS FOR ASN.1 LEXER DEFINITIONS
// ===================================================

class ASNLexer extends Lexer;
options	{
	k =	2;
	exportVocab=ASN;
	charVocabulary = '\3'..'\377';
	caseSensitive=true;
	testLiterals = true;
	codeGenMakeSwitchThreshold = 2;  // Some optimizations
	codeGenBitsetTestThreshold = 3;
}

//	ASN1 Tokens

tokens {
	DOT;
  DOTDOT;

ABSENT_KW				=	"ABSENT"			;
ABSTRACT_SYNTAX_KW		=	"ABSTRACT-SYNTAX"	;
ALL_KW					=	"ALL"				;
ANY_KW					=	"ANY"				;
ARGUMENT_KW				=	"ARGUMENT"			;
APPLICATION_KW			=	"APPLICATION"		;
AUTOMATIC_KW			=	"AUTOMATIC"			;
BASED_NUM_KW			=	"BASEDNUM"			;
BEGIN_KW				=	"BEGIN"				;
BIT_KW					=	"BIT"				;
BMP_STR_KW				=	"BMPString"			;
BOOLEAN_KW				=	"BOOLEAN"			;
BY_KW					=	"BY"				;
CHARACTER_KW			=	"CHARACTER"			;
CHOICE_KW				=	"CHOICE"			;
CLASS_KW				=	"CLASS"				;
COMPONENTS_KW			=	"COMPONENTS"		;
COMPONENT_KW			=	"COMPONENT"			;
CONSTRAINED_KW			=	"CONSTRAINED"		;
DEFAULT_KW				=	"DEFAULT"			;
DEFINED_KW				=	"DEFINED"			;
DEFINITIONS_KW			=	"DEFINITIONS"		;
EMBEDDED_KW				=	"EMBEDDED"			;
END_KW					=	"END"				;
ENUMERATED_KW			=	"ENUMERATED"		;
ERROR_KW				=	"ERROR"				;
ERRORS_KW				=	"ERRORS"			;
EXCEPT_KW				=	"EXCEPT"			;
EXPLICIT_KW				=	"EXPLICIT"			;
EXPORTS_KW				=	"EXPORTS"			;
EXTENSIBILITY_KW		=	"EXTENSIBILITY"		;
EXTERNAL_KW				=	"EXTERNAL"			;
FALSE_KW				=	"FALSE"				;
FROM_KW					=	"FROM"				;
GENERALIZED_TIME_KW		=	"GeneralizedTime"	;
GENERAL_STR_KW			=	"GeneralString"		;
GRAPHIC_STR_KW			=	"GraphicString"		;
IA5_STR_KW				=	"IA5String"			;
IDENTIFIER_KW			=	"IDENTIFIER"		;
IMPLICIT_KW				=	"IMPLICIT"			;
IMPLIED_KW				=	"IMPLIED"			;
IMPORTS_KW				=	"IMPORTS"			;
INCLUDES_KW				=	"INCLUDES"			;
INSTANCE_KW				=	"INSTANCE"			;
INTEGER_KW				=	"INTEGER"			;
INTERSECTION_KW			=	"INTERSECTION"		;
ISO646_STR_KW			=	"ISO646String"		;
LINKED_KW				=	"LINKED"			;
MAX_KW					=	"MAX"				;
MINUS_INFINITY_KW		=	"MINUSINFINITY"		;
MIN_KW					=	"MIN"				;
NULL_KW					=	"NULL"				;
NUMERIC_STR_KW			=	"NumericString"		;
OBJECT_DESCRIPTOR_KW 	=	"ObjectDescriptor"	;
OBJECT_KW				=	"OBJECT"			;
OCTET_KW				=	"OCTET"				;
OPERATION_KW			=	"OPERATION"			;
OF_KW					=	"OF"				;
OID_KW					=	"OID"				;
OPTIONAL_KW				=	"OPTIONAL"			;
PARAMETER_KW			=	"PARAMETER"			;
PATTERN_KW				=	"PATTERN"			;
PDV_KW					=	"PDV"				;
PLUS_INFINITY_KW		=	"PLUSINFINITY"		;
PRESENT_KW				=	"PRESENT"			;
PRINTABLE_STR_KW		=	"PrintableString"	;
PRIVATE_KW				=	"PRIVATE"			;
REAL_KW					=	"REAL"				;
RELATIVE_KW				=	"RELATIVE"			;
RESULT_KW				=	"RESULT"			;
SEQUENCE_KW				=	"SEQUENCE"			;
SET_KW					=	"SET"				;
SIZE_KW					=	"SIZE"				;
STRING_KW				=	"STRING"			;
SYNTAX_KW				=   "SYNTAX"			;
TAGS_KW					=	"TAGS"				;
TELETEX_STR_KW			=	"TeletexString"		;
TRUE_KW					=	"TRUE"				;
TYPE_IDENTIFIER_KW		=	"TYPE-IDENTIFIER"	;
UNION_KW				=	"UNION"				;
UNIQUE_KW				=	"UNIQUE"			;
UNIVERSAL_KW			=	"UNIVERSAL"			;
UNIVERSAL_STR_KW		=	"UniversalString"	;
UTC_TIME_KW				=	"UTCTime"			;
UTF8_STR_KW				=	"UTF8String"		;
VIDEOTEX_STR_KW			=	"VideotexString"	;
VISIBLE_STR_KW			=	"VisibleString"		;
WITH_KW					=	"WITH"				;
}

// Operators

ASSIGN_OP			:	"::="	;
BAR					:	'|'		;
COLON				:	':'		;
COMMA				:	','		;
protected COMMENT				:	"--"	;
ELLIPSIS			:
	("...") => "..."
	|("..") => (".." {$setType(DOTDOT);})
	| ("." {$setType(DOT);})
	;
EXCLAMATION			:	'!'		;
INTERSECTION		:	'^'		;
LESS				:	'<'		;
L_BRACE				:	'{'		;
L_BRACKET			:	'['		;
L_PAREN				:	'('		;
MINUS				:	'-'		;
PLUS				:	'+'		;
R_BRACE				:	'}'		;
R_BRACKET			:	']'		;
R_PAREN				:	')'		;
SEMI				:	';'		;
SINGLE_QUOTE		:	"'"		;
//CHARB				:	"'B"	;
//CHARH				:	"'H"	;
QUOT				: '"'   ;


// Whitespace -- ignored

WS
	:	(	' ' | '\t' | '\f'	|	(	options {generateAmbigWarnings=false;}
	:	"\r\n"		{ newline(); }// DOS
	|	'\r'   		{ newline(); }// Macintosh
	|	'\n'		{ newline(); }// Unix
	))+
	{$setType(Token.SKIP); } ;

protected VALID_SL_COMMENT_CHARS : ~('-'|'\n'|'\r'|'\uffff');

// Single-line comments
SL_COMMENT
	: (options {warnWhenFollowAmbig=false;}
	: COMMENT
		(  { LA(2)!='-' }? '-' 	|	VALID_SL_COMMENT_CHARS)*
		( COMMENT | (("\r\n" | '\r' | '\n') { newline(); })|'\uffff' ) )
		{$setType(Token.SKIP);  }
	;

//	: COMMENT
//		( ((VALID_SL_COMMENT_CHARS) => (VALID_SL_COMMENT_CHARS))
//			| ((MINUS VALID_SL_COMMENT_CHARS) => (MINUS VALID_SL_COMMENT_CHARS)) )*
//		( ("\r\n" | '\r' | '\n') { newline(); } | COMMENT)

// @TODO original follows
//	: (options {warnWhenFollowAmbig=false;}
//	: COMMENT (  { LA(2)!='-' }? '-' 	|	~('-'|'\n'|'\r'))*	( (('\r')? '\n') { newline(); }| COMMENT) )
//		{$setType(Token.SKIP);  }
//	;

ML_COMMENT
		:	"/*"  ( options {greedy=false;}:.)* "*/" 			  {$setType(Token.SKIP);}
		|	'/' '\\' '\n' '*'  ( options {greedy=false;}:.)* "*/" {$setType(Token.SKIP);}
		;

NUMBER	:	('0'..'9')+ ;

UPPER
options {testLiterals = true;}
	:   ('A'..'Z')
		( (('-')* ALPHA_NUM) => IDENTIFIER_REST)* 	;

LOWER
options {testLiterals = true;}
	:	('a'..'z')
		((('-')* ALPHA_NUM) => IDENTIFIER_REST)*	;

protected IDENTIFIER_REST:
 (options {warnWhenFollowAmbig = false;}
	: ('-')+ ALPHA_NUM) => ('-')+ ALPHA_NUM | ALPHA_NUM;

protected ALPHA_NUM :
	( 'a'..'z' | 'A'..'Z' | '0'..'9' );

AMP_UPPER : '&' UPPER ;
AMP_LOWER : '&' LOWER ;
AT_LOWER : '@' LOWER ;

protected
BDIG		: ('0'|'1') ;
protected
HDIG		:	(options {warnWhenFollowAmbig = false;} :('0'..'9') )
			|	('A'..'F')
			|	('a'..'f')
			;

B_OR_H_STRING
	:	(options {warnWhenFollowAmbig = false;}
		:(B_STRING)=>B_STRING {$setType(B_STRING);}
		| H_STRING {$setType(H_STRING);})
	;

protected
B_STRING 	: 	SINGLE_QUOTE (BDIG)+ SINGLE_QUOTE 'B' 	;
protected
H_STRING 	: 	SINGLE_QUOTE (HDIG)+ SINGLE_QUOTE 'H'  ;


C_STRING 	: 	QUOT	((QUOT QUOT)=>(QUOT QUOT) | ~('"'|'\n'|'\r'))*  QUOT ;


//*************************************************************************
//**********		PARSER DEFINITIONS
//*************************************************************************


class ASNParser	extends	Parser;
options	{
	exportVocab=ASN;
	k=3;
}

{
	protected IModelHandler modelHandler = null;
	protected IResource moduleResource = null;

	public ASNParser(TokenStream lexer, IModelHandler modelHandler, IResource moduleResource) {
	  this(lexer,3);
	  this.modelHandler = modelHandler;
	  this.moduleResource = moduleResource;
	}

	/* dummy method in order to handle ANTLR above 2.7.5 */
	public void recover(RecognitionException ex, BitSet set) throws TokenStreamException {
		super.recover(ex, set);
	}

	public void reportError(RecognitionException ex) {
		modelHandler.reportError(ex);
        //super.reportError(ex);
	}
  /* (non-Javadoc)
   * @see antlr.Parser#reportError(java.lang.String)
   */
  public void reportError(String arg0) {
		modelHandler.reportError(arg0, null, 0, 0);
        //super.reportError(arg0);
  }

  /* (non-Javadoc)
   * @see antlr.Parser#reportWarning(java.lang.String)
   */
  public void reportWarning(String str) {
    modelHandler.reportWarning(str);
    //super.reportWarning(str);
  }

	public final void grammar() {
    	modelHandler.processing(moduleResource);
    	try {      // for error handling
    	  asn_file();
    	} catch (RecognitionException ex) {
    	  reportError(ex);
		} catch (TokenStreamRecognitionException e) {
          Throwable cause = e.recog;
          if (cause instanceof RecognitionException) {
            reportError((RecognitionException) cause);
          } else {
            modelHandler.reportError(e.getMessage(), e, -1, -1);
          }
		} catch (TokenStreamException e) {
		  modelHandler.reportError(e.getMessage(), e, -1, -1);
		}
	}
	private void checkDeclaration(Token token, ASNModule module) throws RecognitionException {
		final IASN prevDeclaration = module.findDeclaration(token.getText());
		if (prevDeclaration != null)
			throw new RecognitionException("Identifier "+token.getText()+
				" allready defined at line "+prevDeclaration.getLine()+" col "+prevDeclaration.getColumn()+" ",
				getFilename(), token.getLine(), token.getColumn());
	}
	private void checkExports(ASNModule module) throws RecognitionException {
		// exports
		Iterator exports = module.exportSymbolListIterator();
		while (exports.hasNext()) {
			final String export = (String) exports.next();
			final IASN exportDeclaration = module.findDeclaration(export);
			if (exportDeclaration == null)
				throw new RecognitionException("Identifier "+export+" was exported but not defined ",
					getFilename(), module.getExportsItem().getLine(), module.getExportsItem().getColumn());
		}
	}
}

// Grammar Definitions

// multiple modules allowed in the ASN.1 file
asn_file
{
	ASNModule module;
}
	:	({
//			long startTime = java.util.Calendar.getInstance().getTimeInMillis();
		}
		module = module_definition[moduleResource]
		{
//			long parseTime = java.util.Calendar.getInstance().getTimeInMillis();
			checkExports(module);
//			long endTime = java.util.Calendar.getInstance().getTimeInMillis();
//			System.out.println("Processed module "+module.getName()+" in "+((endTime - startTime) / 1000.0)+
//				" sec (type check "+((endTime - parseTime) / 1000.0)+" sec)");
			if (module.getName() == null)
				break;
			modelHandler.addModule(module);
		})+
	;

// module
module_definition[IResource moduleResource] returns [ASNModule module]
{
	module = new ASNModule(moduleResource);
	AsnModuleIdentifier mid;
	String s ;

}
	:	(mid = module_identifier
		{ module.moduleIdentifier = mid; 	})
		def:DEFINITIONS_KW  {module.setToken(def); }
		(( e:EXPLICIT_KW {module.tagDefault = e.getText();}
		  |i:IMPLICIT_KW {module.tagDefault = i.getText();}
		  |a:AUTOMATIC_KW {module.tagDefault = a.getText();}
		 ) TAGS_KW {module.tag = true;} |)
		(EXTENSIBILITY_KW IMPLIED_KW {module.extensible=true;} | )
		ASSIGN_OP
		BEGIN_KW
		module_body[module]
		end:END_KW {module.setEndToken(end); }
	;

module_identifier returns [ AsnModuleIdentifier mid ]
{mid = new AsnModuleIdentifier();
AsnOidComponentList cmplst; }
	:	(( md:UPPER { mid.name = md.getText(); mid.setToken(md); })
		 ((cmplst = obj_id_comp_lst { mid.componentList = cmplst; })|)
		)
	;

obj_id_comp_lst	returns [AsnOidComponentList oidcmplst]
{oidcmplst = new AsnOidComponentList();
AsnOidComponent oidcmp; AsnDefinedValue defval; }
	:	L_BRACE (
  		((defined_value)=>(defval = defined_value {oidcmplst.isDefinitive=true;oidcmplst.defval=defval;}))?
		  (oidcmp = obj_id_component {oidcmplst.components.add(oidcmp);})+
		) R_BRACE
	;

obj_id_component returns [AsnOidComponent oidcmp ]
{oidcmp = new AsnOidComponent(); AsnDefinedValue defval;
String s,n =""; }
	: 	((num:NUMBER {s=num.getText();oidcmp.num = new Integer(s); oidcmp.numberForm=true; oidcmp.setToken(num);})
	|	(LOWER (L_PAREN NUMBER R_PAREN)?)=>((lid:LOWER {oidcmp.name = lid.getText();oidcmp.nameForm=true;oidcmp.setToken(lid);})
		( L_PAREN
		 (num1:NUMBER {n=num1.getText(); oidcmp.num = new Integer(n);oidcmp.nameAndNumberForm=true;})
		R_PAREN ) ? )
	|	(defined_value)=>(defval = defined_value {oidcmp.isDefinedValue=true;oidcmp.defval=defval;}))
	;

tag_default returns [String s]
{ s = ""; }
	:	(tg:EXPLICIT_KW   {s = tg.getText();})
	|	(tg1:IMPLICIT_KW  {s = tg1.getText();})
	|	(tg2:AUTOMATIC_KW {s = tg2.getText();})
	;

module_body[ASNModule module]
//	:	(exports[module])? (imports[module])? (assignment[module])+
	:	(exports[module]|) (imports[module]|) ((assignment[module])+ |)
	;

exports[ASNModule module]
{String s; HashableList syml = new HashableList();}
//	:	(EXPORTS_KW {module.exported=true;})   (s = symbol { module.exportList.add(s) ; })
//		(COMMA (s = symbol {module.exportList.add(s) ;} ) )*  SEMI
//	;
	:	e:EXPORTS_KW {module.exported=true; module.setExportsToken(e);}
		((syml = symbol_list {module.exportSymbolList = syml;} |)
		|ALL_KW)
		semi:SEMI {module.setExportsEndToken(semi); }
	;

imports[ASNModule module]
	:	(imp:IMPORTS_KW (symbols_from_module[module])*  semi:SEMI)
		{module.imported=true; module.setImportsToken(imp); module.setImportsEndToken(semi);}
	;

symbols_from_module[ASNModule module]
{SymbolsFromModule sym = new SymbolsFromModule();
String s = "" ; AsnModuleIdentifier mid; AsnDefinedValue defval;
HashableList arl; AsnOidComponentList cmplist;}

	:	((arl= symbol_list {sym.symbolList = arl;}) FROM_KW
	    ((up:UPPER {sym.modref = up.getText(); sym.setToken(up);})
	     (cmplist = obj_id_comp_lst {sym.isOidValue=true;sym.cmplist = cmplist;}
	     |(defined_value)=>(defval = defined_value {sym.isDefinedValue=true;sym.defval=defval;})
	     |
	     )
	    )
	   )
	    {module.addImportSymbolFromModuleList(sym);}
	;

symbol_list returns[HashableList symlist]
{symlist = new HashableList(); String s=""; }
	:	((s = symbol {symlist.add(s); })
		(COMMA (s = symbol {symlist.add(s); }))*)
	;

symbol returns [String s]
{s="";}
	:	(
		up:UPPER { s = up.getText();}
	|	lid:LOWER { s = lid.getText();}
	|	s=macroName   	//To solve the matching of Macro Name with Keyword
		)  (L_BRACE R_BRACE)?
	;

macroName returns [String s]
{s="";}
	:	OPERATION_KW 						{ s = "OPERATION";}
	|	ERROR_KW							{ s = "ERROR";}
 	|	"BIND"								{ s = "BIND";}
 	|	"UNBIND"							{ s = "UNBIND";}
 	|	"APPLICATION-SERVICE-ELEMENT"		{ s = "APPLICATION-SERVICE-ELEMENT";}
 	|	"APPLICATION-CONTEXT"				{ s = "APPLICATION-CONTEXT";}
 	|	"EXTENSION"							{ s = "EXTENSION";}
 	|	"EXTENSIONS"						{ s = "EXTENSIONS";}
 	|	"EXTENSION-ATTRIBUTE"				{ s = "EXTENSION-ATTRIBUTE";}
 	|	"TOKEN"								{ s = "TOKEN";}
 	|	"TOKEN-DATA"						{ s = "TOKEN-DATA";}
 	|	"SECURITY-CATEGORY"					{ s = "SECURITY-CATEGORY";}
 	|	"OBJECT"							{ s = "OBJECT";}
 	|	"PORT"								{ s = "PORT";}
 	|	"REFINE"							{ s = "REFINE";}
 	|	"ABSTRACT-BIND"						{ s = "ABSTRACT-BIND";}
 	|	"ABSTRACT-UNBIND"					{ s = "ABSTRACT-UNBIND";}
 	|	"ABSTRACT-OPERATION"				{ s = "ABSTRACT-OPERATION";}
 	|	"ABSTRACT-ERROR"					{ s = "ABSTRACT-ERROR";}
 	|	"ALGORITHM"							{ s = "ALGORITHM";}
 	|	"ENCRYPTED"							{ s = "ENCRYPTED";}
 	|	"SIGNED"							{ s = "SIGNED";}
 	|	"SIGNATURE"							{ s = "SIGNATURE";}
 	|	"PROTECTED"							{ s = "PROTECTED";}
 	|	"OBJECT-TYPE"						{ s = "OBJECT-TYPE";}
 	;

assignment[ASNModule module]
{AsnType obj ; AsnValue val;	}

	:typeAssignment[module]
	|valueAssignment[module]
// Definition of Macro type. Consume the definitions . No Actions
	| (UPPER "MACRO" ASSIGN_OP BEGIN_KW (~(END_KW) )* END_KW) =>
		(up2:UPPER "MACRO" ASSIGN_OP BEGIN_KW (~(END_KW))* END_KW) {
		checkDeclaration(up2, module);
		}
// ***************************************************
// Define the following
// ***************************************************
//	|xmlValueAssignment
//	|valueSetTypeAssignment
    // TODO dummy to be removed
	|dummyObjectClassAssignment[module]
	|(UPPER UPPER ASSIGN_OP L_BRACE) => dummy_constrained_type[module]
	|(LOWER UPPER ASSIGN_OP L_BRACE) => dummy_constrained_value[module]
//	|objectClassAssignment[module]
//	|objectAssignment
//	|objectSetAssignment
//	|parameterizedAssignment
	;

typeAssignment[ASNModule module]
{AsnType obj;} :
// Type Assignment Definition
		(up:UPPER ASSIGN_OP	(obj=type)
{
		if (obj != null) {
			obj.setToken(up);
			obj.setEndToken(LT(0));
			if (obj instanceof AsnType) {
				((AsnType)obj).name = up.getText();
				module.addType((AsnType)obj, getFilename());
			} else {
				System.out.print("Unknown Type "+up.getText()+" ::= ");
				System.out.println((obj.getClass().getName()) );
			}
		} else {
			System.out.println("Type obj = null");
		}
}
		);

valueAssignment[ASNModule module]
{AsnType objv; AsnValue val;} :
// Value Assignment definition
	(lid:LOWER (objv = type ) ASSIGN_OP (val = value)
		{
		if (val != null) {
			val.setToken(lid);
			val.setEndToken(LT(0));
			val.name=lid.getText();
			val.typeName = objv.getTypeName();
			module.addValue(val, getFilename());
		}
		if (val.typeName == null) {
			System.out.println("Value not defined  " + (objv.getClass().getName()));
		}
		});

/*TYPES===============================================================*/

type returns [AsnType obj]
{obj = null;}
	:	(obj = built_in_type)		// Builtin Type
	|	(defined_type) => (obj = defined_type)		// Referenced Type
//	|	(obj = useful_type)			// Referenced Type OK contained in Character_Str type
	|	(obj = selection_type ) 		// Referenced Type	// Grammar ok Frames to be created
//	|	(obj = type_from_Object )		// Referenced Type
//	|	(obj = value_set_from_objects)	// Referenced Type
//	|   (obj = type_and_constraint )		// Constrained Type Causing Infinit Recursion. In case of
// Parsing errors this needs to be built in the assignment itself
//	|	(obj = type_with_constraint)	// Constrained Type OK in built in types
	|	(obj = macros_type)
	;

built_in_type returns [AsnType obj]
{obj = null;}
	:	(obj = any_type)
	|	(obj = bit_string_type)
	|	(obj = boolean_type )
	|	(character_str_type)=> (obj = character_str_type) // Not Correct
	|	(obj = choice_type)
	|	(obj = embedded_type) EMBEDDED_KW  PDV_KW
	|	(obj = enum_type)
	|	(obj = external_type)
//	|	INSTANCE_KW OF_KW
	|	(obj = integer_type )
	|	(obj = null_type )
//	|	ObjectClassFieldType OBJECT_DESCRIPTOR_KW
	|	(obj = object_identifier_type)
	|	(octetString_type) => (obj = octetString_type)
	|	(obj = real_type )
	|	(obj = relativeOid_type)
	|	(obj = sequence_type)
	|	(obj = sequenceof_type)
	|	(obj = set_type)
	|	(obj = setof_type)
	|	(obj = tagged_type)
	;

any_type returns [AsnType obj]
{obj = null;AsnAny an = new AsnAny();}
	: (	t:ANY_KW  ( DEFINED_KW BY_KW {an.isDefinedBy = true ;} lid:LOWER { an.definedByType = lid.getText();})? )
		{obj = an ;  an = null; obj.setToken(t); }
	;

bit_string_type	returns [AsnType obj]
{AsnBitString bstr = new AsnBitString();
AsnNamedNumberList nnlst ; AsnConstraint cnstrnt;obj = null;}
	:	(t:BIT_KW STRING_KW
			((namedNumber_list)=>(nnlst =namedNumber_list { bstr.namedNumberList = nnlst;}))?
			((constraint)=>(cnstrnt = constraint { bstr.constraint = cnstrnt;} )+|)
		)
		{obj=bstr; nnlst = null ; cnstrnt = null; obj.setToken(t);}
	;
// @TODO original code
//	:	(BIT_KW STRING_KW (nnlst =namedNumber_list { bstr.namedNumberList = nnlst;})?
//		(cnstrnt = constraint { bstr.constraint = cnstrnt;} )? )
//		{obj=bstr; nnlst = null ; cnstrnt = null;}
//	;

// Includes Useful types as well
character_str_type returns [AsnType obj]
{AsnCharacterString cstr = new AsnCharacterString();
String s ; AsnConstraint cnstrnt; obj = null;}
	:	((t:CHARACTER_KW STRING_KW {cstr.isUCSType = true;})
	(cnstrnt = constraint{cstr.constraint = cnstrnt;})*
	|	(character_set constraint) => (s = character_set {cstr.stringtype = s;}
		(cnstrnt = constraint{cstr.constraint = cnstrnt;})+ )
	|	(s = character_set {cstr.stringtype = s;})
		)
		{obj = cstr; cnstrnt = null; cstr = null; obj.setToken(t);}
	;

character_set returns [String s]
{s = "";}
	:	(s1:BMP_STR_KW 			{s = s1.getText();})
	|	(s2:GENERALIZED_TIME_KW	{s = s2.getText();})
	|	(s3:GENERAL_STR_KW		{s = s3.getText();})
	|	(s4:GRAPHIC_STR_KW		{s = s4.getText();})
	|	(s5:IA5_STR_KW			{s = s5.getText();})
	|	(s6:ISO646_STR_KW		{s = s6.getText();})
	|	(s7:NUMERIC_STR_KW		{s = s7.getText();})
	|	(s8:PRINTABLE_STR_KW	{s = s8.getText();})
	|	(s9:TELETEX_STR_KW		{s = s9.getText();})
	|	(s10:T61_STR_KW			{s = s10.getText();})
	|	(s11:UNIVERSAL_STR_KW	{s = s11.getText();})
	|	(s12:UTF8_STR_KW		{s = s12.getText();})
	|	(s13:UTC_TIME_KW		{s = s13.getText();})
	|	(s14:VIDEOTEX_STR_KW	{s = s14.getText();})
	|	(s15:VISIBLE_STR_KW		{s = s15.getText();})
	;

boolean_type returns [AsnType obj]
{obj = null;}
	: t:BOOLEAN_KW
	  {obj = new AsnBoolean(); obj.setToken(t);}
	;

choice_type	returns [AsnType obj]
{AsnChoice ch = new AsnChoice(); AsnElementTypeList eltplst ;
obj = null;}
	: (	t:CHOICE_KW L_BRACE (eltplst = elementType_list {ch.elementTypeList = eltplst ;}) R_BRACE )
		{obj = ch; eltplst = null; ch = null; obj.setToken(t);}
	;

embedded_type returns [AsnType obj]
{obj = null;}
	:	(t:EMBEDDED_KW  PDV_KW)
		{obj = new AsnEmbedded(); obj.setToken(t);}
	;
enum_type returns [AsnType obj]
{AsnEnum enumtyp = new AsnEnum() ;
AsnNamedNumberList nnlst; obj = null;}
	: ( t:ENUMERATED_KW (nnlst = namedNumber_list { enumtyp.namedNumberList = nnlst;}) )
	  {obj = enumtyp ; enumtyp=null; obj.setToken(t);}
	;

external_type returns [AsnType obj]
{obj = null; }
	: t:EXTERNAL_KW {obj = new AsnExternal(); obj.setToken(t);}
	;

integer_type returns [AsnType obj]
{AsnInteger intgr = new AsnInteger();
AsnNamedNumberList numlst; AsnConstraint cnstrnt; obj=null;}
	: (	t:INTEGER_KW
			((namedNumber_list)=>(numlst = namedNumber_list {intgr.namedNumberList = numlst;}))?
			((constraint)=>(cnstrnt = constraint { intgr.constraint = cnstrnt;} )+|)
		)
		{obj = intgr ; numlst = null ; cnstrnt = null; intgr = null;  obj.setToken(t);}
	;
// orig
//	: (	INTEGER_KW (numlst = namedNumber_list {intgr.namedNumberList = numlst;}
//		| cnstrnt = constraint {intgr.constraint = cnstrnt;})? )

null_type returns [AsnType obj]
{AsnNull nll = new AsnNull(); obj = null;}
	: t:NULL_KW
	  {obj = nll; nll = null ; obj.setToken(t); }
	;

object_identifier_type returns [AsnType obj]
{AsnObjectIdentifier objident = new AsnObjectIdentifier(); obj = null;}
	: t:OBJECT_KW IDENTIFIER_KW
	  {obj = objident; objident = null; obj.setToken(t);}
	;

octetString_type returns [AsnType obj]
{AsnOctetString oct = new AsnOctetString(); AsnConstraint cnstrnt ; obj = null;}
	: ((OCTET_KW STRING_KW constraint) =>
		(	t1:OCTET_KW STRING_KW (cnstrnt = constraint{oct.constraint = cnstrnt;  oct.setToken(t1);})+)
	|   t2:OCTET_KW STRING_KW {oct.setToken(t2);})
		{obj = oct ; cnstrnt = null;}
	;

real_type returns [AsnType obj]
{AsnReal rl = new AsnReal();obj = null;}
	: t:REAL_KW  {obj = rl ; rl = null; obj.setToken(t);}
	;

relativeOid_type returns [AsnType obj]
{obj = null; }
	: t:RELATIVE_KW MINUS OID_KW {obj = new AsnRelativeOid(); obj.setToken(t);}
	;

sequence_type returns [AsnType obj]
{AsnSequenceSet seq = new AsnSequenceSet();
AsnElementTypeList eltplist ; obj = null;}
	:  ( t:SEQUENCE_KW {seq.isSequence = true;}
	    L_BRACE
	    // ignore content to work with classes
	   (eltplist = elementType_list {seq.elementTypeList = eltplist;})?
	    R_BRACE
	    )
		{obj = seq ; eltplist = null; seq =null; obj.setToken(t); }
	;

sequenceof_type returns [AsnType obj]
{AsnSequenceOf seqof = new AsnSequenceOf();
AsnConstraint cns; obj = null; IASN obj1 ; String s ;}
	:  ( t:SEQUENCE_KW {seqof.isSequenceOf = true;}
	        ((L_PAREN (SIZE_KW {seqof.isSizeConstraint=true;})
	          (cns = constraint {seqof.constraint = cns ;}) R_PAREN)
          | ((SIZE_KW {seqof.isSizeConstraint=true;})
	          (cns = constraint {seqof.constraint = cns ;})))?
	        OF_KW
		( obj1 = type
		{	if((AsnDefinedType.class).isInstance(obj1)){
		  		seqof.isDefinedType=true;
				seqof.typeName = ((AsnDefinedType)obj1).typeReference ;
			}
			else{
				seqof.typeReference = obj1 ;
			}
		}) )
		{obj = seqof;  cns = null; seqof=null; obj.setToken(t);}
	;

set_type returns [AsnType obj]
{AsnSequenceSet set = new AsnSequenceSet();
AsnElementTypeList eltplist ;obj = null;}
	:  ( t:SET_KW L_BRACE (eltplist =  elementType_list {set.elementTypeList = eltplist ;})? R_BRACE )
		{obj = set ; eltplist = null; set = null; obj.setToken(t);}
	;

setof_type	returns [AsnType obj]
{AsnSequenceOf setof = new AsnSequenceOf();
AsnConstraint cns; obj = null;
IASN obj1 ; String s;}
	:	(t:SET_KW
	((SIZE_KW {setof.isSizeConstraint=true;}(cns = constraint {setof.constraint = cns ;}))
	| (L_PAREN SIZE_KW {setof.isSizeConstraint=true;}(cns = constraint {setof.constraint = cns ;}) R_PAREN))?
	OF_KW
		(obj1 = type
		{	if((AsnDefinedType.class).isInstance(obj1)){
		  		setof.isDefinedType=true;
				setof.typeName = ((AsnDefinedType)obj1).typeReference ;
			}
			else{
				setof.typeReference = obj1;
			}
		}) )
		{obj = setof; cns = null; obj1=null; setof=null; obj.setToken(t);}
	;

tagged_type returns [AsnType obj]
{AsnTaggedType tgtyp = new AsnTaggedType();
AsnTag tg; AsnType obj1 = null; String s; obj = null;}
	:	((tg = tag {tgtyp.tag = tg ;})
		(s = tag_default { tgtyp.tagDefault = s ;})?
		(obj1 = type
		{	if((AsnDefinedType.class).isInstance(obj1)){
		  		tgtyp.isDefinedType=true;
				tgtyp.typeName = ((AsnDefinedType)obj1).typeReference ;
			}
			else{
				tgtyp.typeReference = obj1;
			}
		}))
		{obj = tgtyp ; tg = null; obj1= null ;tgtyp = null; }
	;


tag	returns [AsnTag tg]
{tg = new AsnTag(); String s; AsnClassNumber cnum;}
	:	(t:L_BRACKET (s = clazz {tg.clazz = s ;})? (cnum = class_NUMBER { tg.classNumber = cnum ;}) R_BRACKET )
	{ tg.setToken(t);};

clazz returns [String s]
{s = ""; }
	:	(c1:UNIVERSAL_KW 	{s= c1.getText();})
	|	(c2:APPLICATION_KW	{s= c2.getText();})
	|	(c3:PRIVATE_KW		{s= c3.getText();})
	;

class_NUMBER returns [AsnClassNumber cnum]
{cnum = new AsnClassNumber() ; String s; }
	:	((num:NUMBER {s=num.getText(); cnum.num = new Integer(s); cnum.setToken(num);})
	|	(lid:LOWER  {s=lid.getText(); cnum.name = s ; cnum.setToken(lid);}) )

	;

// Useful types
defined_type returns [AsnType obj]
{AsnDefinedType deftype = new AsnDefinedType();
AsnConstraint cnstrnt; obj = null;}
	:	(
		(up:UPPER {deftype.isModuleReference = true ;deftype.moduleReference = up.getText();}
			DOT )?
		( (UPPER constraint) => (
			(up1:UPPER {deftype.typeReference = up1.getText();})
			(cnstrnt = constraint{deftype.constraint = cnstrnt;})+
														)
		| (up2:UPPER {deftype.typeReference = up2.getText();} )
		| (oup:OJECT_UPPER {deftype.typeReference = oup.getText();}
			(cnstrnt = constraint{deftype.constraint = cnstrnt;})+
		)
		| (olo:OJECT_LOWER {deftype.typeReference = olo.getText();}
			(cnstrnt = constraint{deftype.constraint = cnstrnt;})+
		)
		)
		)
		{obj = deftype; deftype=null ; cnstrnt = null; obj.setToken(up1);}
	;
// Referenced Type
selection_type returns [AsnType obj]
{AsnSelectionType seltype = new AsnSelectionType();
obj = null;AsnType obj1;}
	:	((lid:LOWER { seltype.selectionID = lid.getText();})
	 	LESS
	 	(obj1=type {seltype.type = obj1;}))
	 	{obj=seltype; seltype=null; obj.setToken(lid);}
	;

//Constrained Type Causes Infinite Recursion
// Resolved by parsing the constraints along with the types using them.
//type_and_constraint returns [IASN obj]
//{AsnTypeAndConstraint tpcns = new AsnTypeAndConstraint();
//obj=null;IASN obj1;AsnConstraint cnstrnt;}
//	:	((obj1 = type { tpcns.type = obj1;})
//		(cnstrnt = constraint {tpcns.constraint = cnstrnt;}))
//		{obj=tpcns; tpcns=null;}
//	;

// Macros Types
macros_type	returns [AsnType obj]
{obj = null;}
		:	(obj = operation_macro)
		|	(obj = error_macro)
		|	(obj = objecttype_macro)
		;

operation_macro	returns [AsnType obj]
{OperationMacro op = new OperationMacro();
String s ;obj = null; AsnType obj1; AsnType obj2;}
	: (	t:"OPERATION"
//orig	(ARGUMENT_KW (ld1:LOWER {op.argumentTypeIdentifier = ld1.getText();})?
		(ARGUMENT_KW ((LOWER)=>(ld1:LOWER {op.argumentTypeIdentifier = ld1.getText();}))?
		((obj2 = type)
			{op.argumentType = obj2; op.isArgumentName=true;
				if((AsnDefinedType.class).isInstance(obj2))
					op.argumentName = ((AsnDefinedType)obj2).typeReference;
				else
					op.argumentName = op.argumentTypeIdentifier;
			}
		))?
//orig	(RESULT_KW  {op.isResult=true;} ( (SEMI)=>SEMI|((LOWER)? type)=>(ld2:LOWER {op.resultTypeIdentifier = ld2.getText();})?

		((RESULT_KW)=>(RESULT_KW  {op.isResult=true;}
			(
				(SEMI)=>SEMI
				| ((LOWER)=>(ld2:LOWER {op.resultTypeIdentifier = ld2.getText();}))?
		(obj1=type
			{op.resultType=obj1;op.isResultName=true;
				if((AsnDefinedType.class).isInstance(obj1))
					op.resultName = ((AsnDefinedType)obj1).typeReference;
				else
					op.resultName = op.resultTypeIdentifier;
			}
		))))?
//orig	)|))?
		((ERRORS_KW L_BRACE)=>(ERRORS_KW L_BRACE (operation_errorlist[op]
				{op.isErrors=true;}|) R_BRACE ))?
		((LINKED_KW L_BRACE)=>(LINKED_KW L_BRACE (linkedOp_list[op]
			{op.isLinkedOperation = true ;})?	 R_BRACE ))? )
		{obj = op; obj.setToken(t);}
	;

operation_errorlist[OperationMacro oper]
{IASN obj;}
	:	obj = typeorvalue {oper.errorList.add(obj);}
		(COMMA (obj = typeorvalue {oper.errorList.add(obj);}))*
	;

linkedOp_list[OperationMacro oper]
{IASN obj;}
	:	obj = typeorvalue {oper.linkedOpList.add(obj);}
		(COMMA (obj = typeorvalue {oper.linkedOpList.add(obj);}))*
	;

error_macro	returns [AsnType obj]
{ErrorMacro merr = new ErrorMacro();obj = null;
AsnType obj1;}
	:  ( t:ERROR_KW  (PARAMETER_KW {merr.isParameter = true; }
		(((LOWER)=>(lw:LOWER { merr.parameterName = lw.getText(); }))?
		(obj1 = type
		{	if((AsnDefinedType.class).isInstance(obj1)){
				merr.isDefinedType=true;
				merr.typeName = ((AsnDefinedType)obj1).typeReference ;
			}
			else{
				merr.typeReference = obj1 ;
			}
		}) ) )? )
		{obj = merr ; merr = null; obj.setToken(t);}
	;

objecttype_macro returns [AsnType obj]
{ObjectType objtype = new ObjectType();
AsnValue val; obj = null; String s; AsnType typ;}
	: (t:"OBJECT-TYPE" SYNTAX_KW (typ = type {objtype.type=typ;} )
		("ACCESS" lid:LOWER {objtype.accessPart = lid.getText();})
	  ("STATUS" lid1:LOWER {objtype.statusPart = lid1.getText();})
	  ("DESCRIPTION" CHARACTER_KW STRING_KW)?
	  ("REFERENCE" CHARACTER_KW STRING_KW)?
	  ("INDEX" L_BRACE (typeorvaluelist[objtype]) R_BRACE)?
	  ("DEFVAL" L_BRACE ( val = value {objtype.value = val;}) R_BRACE )? )
	  {obj= objtype; objtype = null; obj.setToken(t);}
	;

typeorvaluelist[ObjectType objtype]
{IASN obj; }
	: ((obj = typeorvalue {objtype.elements.add(obj);})
	   (COMMA (obj=typeorvalue {objtype.elements.add(obj);})* ))
	;

typeorvalue returns [IASN obj]
{IASN obj1; obj=null;}
	: ((type)=>(obj1 = type) | obj1 = value)
	  {obj = obj1; obj1=null;}
	;

// field assignment list
elementType_list returns [AsnElementTypeList elelist]
{elelist = new AsnElementTypeList(); AsnElementType eletyp; }
	:	(eletyp = elementType {elelist.addElementType(eletyp, getFilename()); }
	    (COMMA (eletyp = elementType {elelist.addElementType(eletyp, getFilename());}))*)
	;

// field assignment
elementType	returns [AsnElementType eletyp]
{eletyp = new AsnElementType();AsnValue val;
AsnType obj = null; AsnTag tg; String s;}
	: (	(lid:LOWER {eletyp.name = lid.getText(); eletyp.setToken(lid); }
		((tag)=>(tg = tag {eletyp.isTag = true; eletyp.tag = tg ;}))?
		(s = tag_default {eletyp.isTagDefault = true; eletyp.typeTagDefault = s ;})?

		(((UPPER) =>  UPPER (DOT (AMP_LOWER | AMP_UPPER))+) | (obj = type))
		((L_BRACE) =>  ignored_brace_content)?
		((L_PAREN) =>  ignored_paren_content)?
		( (OPTIONAL_KW {eletyp.isOptional=true;})
		| (DEFAULT_KW { eletyp.isDefault = true;}
		 val = value {eletyp.value = val;} ))? )
		|	cmp:COMPONENTS_KW OF_KW {eletyp.isComponentsOf = true; eletyp.setToken(cmp); }
		(obj = type )
		)
		{
			if((AsnDefinedType.class).isInstance(obj)){
				eletyp.isDefinedType=true;
				eletyp.typeName = ((AsnDefinedType)obj).typeReference ;
			} else{
				eletyp.typeReference = obj;
			}
		}
		| (ELLIPSIS {eletyp.isDotDotDot = true;})
	;

namedNumber_list returns [AsnNamedNumberList nnlist]
{nnlist = new AsnNamedNumberList();AsnNamedNumber nnum ; }
	: (	L_BRACE (nnum= namedNumber {nnlist.addNamedNumber(nnum, getFilename()); })
	   (COMMA (nnum = namedNumber  {nnlist.addNamedNumber(nnum, getFilename()); }) )*  R_BRACE )
	;

namedNumber	returns [AsnNamedNumber nnum]
{nnum = new AsnNamedNumber() ;AsnSignedNumber i;
AsnDefinedValue s;	}
	:	(
		lid:LOWER {nnum.name = lid.getText(); nnum.setToken(lid);}
		(L_PAREN
			(
				i = signed_number {nnum.signedNumber = i;nnum.isSignedNumber=true;}
				| (s = defined_value {nnum.definedValue=s;})
			)
		R_PAREN)?
		)
	| (ELLIPSIS {nnum.isDotDotDot = true;})
	;

// Updated Grammar for ASN constraints

constraint returns [AsnConstraint cnstrnt]
{cnstrnt=new AsnConstraint();}
	: t:L_PAREN
		(element_set_specs[cnstrnt]{cnstrnt.isElementSetSpecs=true;})?
		(exception_spec[cnstrnt])?
	  R_PAREN
	{ cnstrnt.setToken(t);};

exception_spec[AsnConstraint cnstrnt]
{AsnSignedNumber signum; AsnDefinedValue defval;
IASN typ;AsnValue val;}
	: (t:EXCLAMATION
	  ( (signed_number)=>(signum=signed_number {cnstrnt.isSignedNumber=true;cnstrnt.signedNumber=signum;})
	   |(defined_value)=>(defval=defined_value {cnstrnt.isDefinedValue=true;cnstrnt.definedValue=defval;})
	   |typ=type COLON val=value {cnstrnt.isColonValue=true;cnstrnt.type=typ;cnstrnt.value=val;}))
	   {cnstrnt.isExceptionSpec=true; cnstrnt.setToken(t);}
	;

element_set_specs[AsnConstraint cnstrnt]
{ElementSetSpec elemspec;}
	:	(elemspec=element_set_spec {cnstrnt.elemSetSpec=elemspec;}
		(COMMA ELLIPSIS {cnstrnt.isCommaDotDot=true;})?
		(COMMA elemspec=element_set_spec {cnstrnt.addElemSetSpec=elemspec;cnstrnt.isAdditionalElementSpec=true;})?)
	;

element_set_spec returns [ElementSetSpec elemspec]
{elemspec = new ElementSetSpec(); Intersection intersect;ConstraintElements cnselem;}
	:	intersect=intersections {elemspec.intersectionList.add(intersect);}
		((BAR | UNION_KW) intersect=intersections {elemspec.intersectionList.add(intersect);})*
	| 	ALL EXCEPT cnselem=constraint_elements {elemspec.allExceptCnselem=cnselem;elemspec.isAllExcept=true;}
	;

// Coding is not proper for EXCEPT constraint elements.
// One EXCEPT constraint elements should be tied to one Constraint elements
//(an object of constraint and except list)
// and not in one single list
intersections returns [Intersection intersect]
{intersect = new Intersection();ConstraintElements cnselem;}

	:	cnselem=constraint_elements {intersect.cnsElemList.add(cnselem);}
	   (EXCEPT {intersect.isExcept=true;} cnselem=constraint_elements {intersect.exceptCnsElem.add(cnselem);})?
	   ((INTERSECTION | INTERSECTION_KW) {intersect.isInterSection=true;}
	   cnselem=constraint_elements {intersect.cnsElemList.add(cnselem);}
	   (EXCEPT cnselem=constraint_elements {intersect.exceptCnsElem.add(cnselem);})?)*
	;
constraint_elements	returns [ConstraintElements cnsElem]
{ cnsElem = null; }
	: (L_PAREN cnsElem=constraint_elements_intern R_PAREN) => (L_PAREN cnsElem=constraint_elements_intern R_PAREN)
	| (cnsElem=constraint_elements_intern)
	;

constraint_elements_intern	returns [ConstraintElements cnsElem]
{ cnsElem = new ConstraintElements(); AsnValue val; AsnDefinedValue defVal;
AsnConstraint cns; ElementSetSpec elespec;IASN typ; }
	:	(value)=>(val = value {cnsElem.isValue=true;cnsElem.value=val;})
	|	(defined_ObjRef)=>(defVal = defined_ObjRef
	{
		cnsElem.isValue=true;
		val = new AsnValue();
		val.isDefinedValue = true;
		val.definedValue = defVal;
		cnsElem.value=val;
	})
	|	(value_range[cnsElem])=>(value_range[cnsElem]	{cnsElem.isValueRange=true;})
	|	(SIZE_KW cns=constraint {cnsElem.isSizeConstraint=true;cnsElem.constraint=cns;})
	|	(FROM_KW cns=constraint {cnsElem.isAlphabetConstraint=true;cnsElem.constraint=cns;})
	|	(L_PAREN elespec=element_set_spec {cnsElem.isElementSetSpec=true;cnsElem.elespec=elespec;} R_PAREN)
	|	((INCLUDES {cnsElem.isIncludeType=true;})? typ=type {cnsElem.isTypeConstraint=true;cnsElem.type=typ;})
	|	(PATTERN_KW val=value {cnsElem.isPatternValue=true;cnsElem.value=val;})
	|	(WITH_KW
		((COMPONENT_KW cns=constraint {cnsElem.isWithComponent=true;cnsElem.constraint=cns;})
		|
		(COMPONENTS_KW {cnsElem.isWithComponents=true;}
		L_BRACE (ELLIPSIS COMMA)? type_constraint_list[cnsElem] R_BRACE )))
	;

value_range[ConstraintElements cnsElem]
{AsnValue val;}
	: (val=value {cnsElem.lEndValue=val;} | MIN_KW {cnsElem.isMinKw=true;}) (LESS {cnsElem.isLEndLess=true;})?  // lower end
	   DOTDOT
	  (LESS {cnsElem.isUEndLess=true;})? (val=value{cnsElem.uEndValue=val;} | MAX_KW {cnsElem.isMaxKw=true;}) // upper end
	;

type_constraint_list[ConstraintElements cnsElem]
{NamedConstraint namecns;}
	: namecns=named_constraint {cnsElem.typeConstraintList.add(namecns);}
	 (COMMA namecns=named_constraint {cnsElem.typeConstraintList.add(namecns);})*
	;

named_constraint returns [NamedConstraint namecns]
{namecns = new NamedConstraint(); AsnConstraint cns;}
	:	lid:LOWER {namecns.name=lid.getText(); namecns.setToken(lid);}
	    (cns=constraint {namecns.isConstraint=true;namecns.constraint=cns;})?
	    (PRESENT_KW {namecns.isPresentKw=true;}
	     |ABSENT_KW {namecns.isAbsentKw=true;}
	     | OPTIONAL_KW {namecns.isOptionalKw=true;})?
	;

/*-----------VALUES ---------------------------------------*/

value returns [AsnValue value]
{value = new AsnValue(); AsnSequenceValue seqval;
AsnDefinedValue defval;String aStr;AsnSignedNumber num;
AsnOidComponentList cmplst;}

	: 	(TRUE_KW)=>(t:TRUE_KW 				{value.isTrueKW = true; value.setToken(t);})
	|	(FALSE_KW)=>(t1:FALSE_KW				{value.isFalseKW = true; value.setToken(t1);})
	|	(NULL_KW)=>(t2:NULL_KW				{value.isNullKW = true; value.setToken(t2);})
	|	(C_STRING)=>(c:C_STRING				{value.isCString=true; value.cStr = c.getText(); value.setToken(c);})
	|	(defined_value)=>(defval = defined_value {value.isDefinedValue = true; value.definedValue = defval;})
	|	(signed_number)=>(num = signed_number	{value.isSignedNumber=true ; value.signedNumber = num;})
	|	(choice_value[value])=>(choice_value[value]	{value.isChoiceValue = true;})
	|	(sequence_value)=>(seqval=sequence_value	{value.isSequenceValue=true;value.seqval=seqval;})
	|	(sequenceof_value[value])=>(sequenceof_value[value] {value.isSequenceOfValue=true;})
	|	(cstr_value[value])=>(cstr_value[value]		{value.isCStrValue = true;})
	|	(obj_id_comp_lst)=>(cmplst=obj_id_comp_lst	{value.isAsnOIDValue=true;value.oidval=cmplst;})
	|	(PLUS_INFINITY_KW)=>(t3:PLUS_INFINITY_KW		{value.isPlusInfinity = true; value.setToken(t3);})
	|	(MINUS_INFINITY_KW)=>(t4:MINUS_INFINITY_KW		{value.isMinusInfinity = true; value.setToken(t4);})
	;

cstr_value[AsnValue value]
{AsnBitOrOctetStringValue bstrval = new AsnBitOrOctetStringValue();
AsnCharacterStringValue cstrval = new AsnCharacterStringValue();
AsnSequenceValue seqval;}
	:  ((H_STRING)=>(h:H_STRING 	{bstrval.isHString=true; bstrval.bhStr = h.getText(); value.setToken(h);})
	|	(B_STRING)=>(b:B_STRING		{bstrval.isBString=true; bstrval.bhStr = b.getText(); value.setToken(b);})
	|	(t:L_BRACE	((id_list[bstrval])=>(id_list[bstrval])
					|(char_defs_list[cstrval])=>(char_defs_list[cstrval])
					| tuple_or_quad[cstrval])    R_BRACE))
		{value.cStrValue=cstrval;value.bStrValue=bstrval; value.setToken(t);}
	;

id_list[AsnBitOrOctetStringValue bstrval]
{String s="";}
	: (ld:LOWER {s = ld.getText(); bstrval.idlist.add(s); bstrval.setToken(ld);})
	  (COMMA ld1:LOWER {s = ld1.getText();bstrval.idlist.add(s);})*
	;

char_defs_list[AsnCharacterStringValue cstrval]
{CharDef a ;}
	:a = char_defs {cstrval.isCharDefList = true;cstrval.charDefsList.add(a);}
	(COMMA (a = char_defs {cstrval.charDefsList.add(a);}))*
	;

tuple_or_quad[AsnCharacterStringValue cstrval]
{AsnSignedNumber n;}
	: (n = signed_number {cstrval.tupleQuad.add(n);})
	  COMMA
	  (n = signed_number {cstrval.tupleQuad.add(n);})
	  ((R_BRACE {cstrval.isTuple=true;})  |  (COMMA
	  (n = signed_number {cstrval.tupleQuad.add(n);})
	  COMMA (n = signed_number {cstrval.tupleQuad.add(n);})))
	;

char_defs  returns [CharDef chardef]
{chardef = new CharDef();
AsnSignedNumber n ; AsnDefinedValue defval;}
	:	(c:C_STRING {chardef.isCString = true;chardef.cStr=c.getText(); chardef.setToken(c);})
	|	(t:L_BRACE (n = signed_number {chardef.tupleQuad.add(n);}) COMMA (n = signed_number {chardef.tupleQuad.add(n);})
		((R_BRACE {chardef.isTuple=true; chardef.setToken(t);})
		|(COMMA (n = signed_number {chardef.tupleQuad.add(n);})
		COMMA (n = signed_number {chardef.tupleQuad.add(n);}) R_BRACE{chardef.isQuadruple=true;})))
	|	(defval = defined_value {chardef.defval=defval;})
	;

choice_value[AsnValue value]
{AsnChoiceValue chval = new AsnChoiceValue(); AsnValue val;}
	: ((lid:LOWER {chval.name = lid.getText();})
	   (COLON)?  (val=value {chval.value = val;}))
	  {value.chval = chval; value.setToken(lid);}
	;

sequence_value returns [AsnSequenceValue seqval]
{AsnNamedValue nameval = new AsnNamedValue();
seqval = new AsnSequenceValue();}
	:	t:L_BRACE  ((nameval=named_value {seqval.isValPresent=true;seqval.namedValueList.add(nameval);})?
		(COMMA nameval=named_value {seqval.namedValueList.add(nameval);})*)   R_BRACE
	{ seqval.setToken(t); };

sequenceof_value[AsnValue value]
{AsnValue val;value.seqOfVal = new AsnSequenceOfValue();}
	: t:L_BRACE ((val=value {value.seqOfVal.valueList.add(val);})?
       (COMMA val=value {value.seqOfVal.valueList.add(val);})*)
	  R_BRACE
	{ value.setToken(t); };

protected
defined_value returns [AsnDefinedValue defval]
{defval = new AsnDefinedValue(); }
	:	((up:UPPER {defval.moduleIdentifier = up.getText(); defval.setToken(up);}
			DOT {defval.isDotPresent=true;})?
		lid:LOWER { defval.name = lid.getText(); defval.setToken(lid);})
	;

signed_number returns [AsnSignedNumber i]
{i = new AsnSignedNumber() ; String s ; }
	:	((MINUS {i.positive=false;})?
		(n:NUMBER  {s = n.getText(); i.num= new BigInteger(s); i.setToken(n); i.setToken(n);}) )
	;

named_value returns [AsnNamedValue nameval]
{nameval = new AsnNamedValue(); AsnValue val;}
	:	(lid:LOWER	{nameval.name = lid.getText(); nameval.setToken(lid);}
		val=value	{nameval.value = val; nameval.setToken(lid); })
	;

/*------------ CLASS -------------------*/

dummyObjectClassAssignment[ASNModule module]
{AsnType obj;} :
	(up:UPPER ASSIGN_OP	CLASS_KW ignored_brace_content)
	(WITH_KW SYNTAX_KW ignored_brace_content)?
	;

protected
defined_ObjRef returns [AsnDefinedValue defval]
{defval = new AsnDefinedValue(); }
	:	((up:UPPER {defval.moduleIdentifier = up.getText(); defval.setToken(up);}
			DOT {defval.isDotPresent=true;})?
		lid:AT_LOWER { defval.name = lid.getText(); defval.setToken(lid);})
	;




ignored_brace_content
	:
	L_BRACE
	    // ignore content to work with classes
	    (options {greedy=false;}:
	    (L_BRACE)=>ignored_brace_content |
	     .)*
	R_BRACE
	;


ignored_paren_content
	:
	L_PAREN
	    // ignore content to work with classes
	    (options {greedy=false;}:
	    (L_PAREN)=>ignored_paren_content |
	     .)*
	R_PAREN
	;


dummy_constrained_type[ASNModule module]
{AsnSequenceSet obj = new AsnSequenceSet();}
	:
	(up:UPPER UPPER ASSIGN_OP ignored_brace_content) {
		obj.setToken(up);
		obj.setEndToken(LT(0));
		obj.name = up.getText();
		module.addType(obj, getFilename());
	}
	;

dummy_constrained_value[ASNModule module]
{AsnDefinedValue obj = new AsnDefinedValue();}
	:
	(lo:LOWER UPPER ASSIGN_OP ignored_brace_content) {
		AsnValue val = new AsnValue();
		val.isDefinedValue = true;
		val.definedValue = obj;
		obj.setToken(lo);
		obj.setEndToken(LT(0));
		obj.name = lo.getText();
		module.addValue(val, getFilename());
	}
	;