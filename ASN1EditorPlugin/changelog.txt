ASN.1 Editor for Eclipse
========================

Version 1.0.0 (released 24 February 2005)

    * Syntax highlight
    * Outline view shows for the current ASN.1 module: types, values, imports and exports
    * Eclipse like - navigate to declaration under cursor (CTRL + click)
    * Content assist for types and values (CTRL + SPACE)
    * Bracket matching
    * Problem markers for errors and warnings
    * Annotation hover for problem markers
    * Preferences page for editor's syntax highlight (colors and font styles)

Version 1.0.1 (released 1 March 2005)

    * Key bindings and menu actions fixed (Only one class per action needed: extend AbstractDispatchAction)
    * Major grammar issues fixed (removed k=11 from lexer)
    * Eclipse like - navigate to declaration under cursor - key bindings added (F3)
    * Comment, uncomment, toggle comment (ALT+8, ALT+9, CTRL+/)

Version 1.0.2 (released 5 March 2005)

    * Minor grammar issues fixed (BMPString and UTF8String bug fixed)
    * Highlighting fixed
    * Grammar: value, types and exports start offset fixed
    * Error report when undefined types are exported
    * Error report on already defined identifiers (type, value, macro, field, variant)
    * Goto next/previous annotation (CTRL+. and CTRL+,)
    * New PreferencePage
    * Outline refresh icon added
    * ASN.1 New Module wizard

Version 1.0.3 (released 6 May 2005)

    * Grammar improvements
    * Editor crash bug fixed (open from CVS resource history)
    * Code folding ( imports, exports, types, values )
    * New outline icons
    * Sort outline by name or item position
    * Imports grouped by modules
    * Improved performance on already defined identifiers (Hashing)
    * Bug 1184581 fixed (comma-terminated field list)
    * ASN.1 Nature
    * Added project specific ASN.1 import path

Version 1.0.4 (released 22 July 2005)

    * Grammar and model improvements
    * Outline now supports multiple modules in one file
    * Split modules into files (<moduleName>.asn - usefull for multiple modules per ASN.1 file)

Version 1.1.0 (released 24 October 2007)

    * Eclipse 3.3 compatibility
    * ASN.1 Grammar CLASS partially supported, fault-tolerant
    * Grammar and model improvements
    * Error handling improvements, limit errors by count
    * Bugs 1751372, 1309659, 1255688 fixed (error in GUI initialization)

Version 1.1.1 (released 16 February 2008)

    * Dump view for ASN.1 BER encoded messages
    * Compare view with ASN.1 highlighting
    * Grammar, model and GUI improvements

Version 1.1.2 (released 30 September 2008)

		* Overall stability improvements
		* Improved compatibility with older and newer Eclipse versions
		* Overall refactoring including model and error handling
		* All ASN.1 files from project are processed
		* Navigate to declaration under cursor (CTRL + click and also F3) works also for imported declarations

Future work
	* remove dependency to JDT
    * Grammar - parameterisable types ( via CLASS keyword )
    * Templates
