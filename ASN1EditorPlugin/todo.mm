<map version="0.7.1">
<node COLOR="#006699" TEXT="ASN.1 Editor">
<font NAME="SansSerif" BOLD="true" SIZE="12"/>
<node TEXT="todo" POSITION="right">
<font NAME="SansSerif" BOLD="true" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node TEXT="1.0.5">
<node TEXT="code">
<node TEXT="import module"/>
<node TEXT="autocomplition">
<node TEXT="after ::=">
<node TEXT="value"/>
<node TEXT="type"/>
</node>
<node TEXT="imports">
<node TEXT="module objid"/>
<node TEXT="module name"/>
</node>
<node TEXT="exports"/>
</node>
<node TEXT="add templates for constructed types"/>
</node>
<node TEXT="model bugs">
<node TEXT="class"/>
<node TEXT="System.outs">
<node TEXT="Value not defined  net.sourceforge.asneditor.model.OperationMacro"/>
<node TEXT="Value not defined  net.sourceforge.asneditor.model.ErrorMacro "/>
</node>
<node TEXT="versioning brackets [[ bla ]]"/>
</node>
<node TEXT="organize imports">
<node TEXT="remove unused types"/>
<node TEXT="elimina recursivitatea la imports"/>
<node TEXT="tine imports in Hashtable">
<node TEXT="module objid"/>
<node TEXT="module name"/>
</node>
</node>
<node TEXT="new module wizard + imports"/>
<node TEXT="resource popup">
<node TEXT="ASN file">
<node TEXT="import ASN.1 (unde)"/>
<node TEXT="add parent folder to search path"/>
</node>
<node TEXT="folder">
<node TEXT="add folder to search path"/>
</node>
</node>
<node TEXT="ASN.1 preferences">
<node TEXT="add / edit / del ASN.1 extensions"/>
<node TEXT="default (system) ASN include path"/>
</node>
<node TEXT="ASN.1 project properties">
<node TEXT="toggle nature"/>
<node TEXT="import search path">
<node TEXT="Add">
<node TEXT="extern folder"/>
<node TEXT="reference project"/>
</node>
<node TEXT="search in project for files with defined extensions">
<node TEXT="show current extensions"/>
<node TEXT="say no ASN.1 files found !"/>
<node TEXT="say you may add ext on AS.1 pref"/>
<node TEXT="ask delete prev. entries"/>
</node>
<node TEXT="move item up/down"/>
</node>
<node TEXT="asn file extensions">
<node TEXT="define"/>
<node TEXT="show current"/>
</node>
</node>
<node TEXT="System.outs">
<icon BUILTIN="flag"/>
<node TEXT="Value not defined  net.sourceforge.asneditor.model.OperationMacro"/>
<node TEXT="Value not defined  net.sourceforge.asneditor.model.ErrorMacro "/>
<node TEXT="parsing times"/>
</node>
<node TEXT="adjust preferences panel width to window">
<icon BUILTIN="flag"/>
</node>
</node>
</node>
<node TEXT="done" POSITION="left">
<font NAME="SansSerif" BOLD="true" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node TEXT="1.0.3" FOLDED="true">
<node TEXT="project">
<node TEXT="ASN.1 nature"/>
<node TEXT="toggle nature"/>
<node TEXT="import search path">
<node TEXT="Add">
<node TEXT="project/workspace folder"/>
</node>
<node TEXT="del"/>
<node TEXT="clear"/>
<node TEXT="search in project for files with defined extensions"/>
</node>
</node>
<node TEXT="code">
<node TEXT="folding">
<node TEXT="imports"/>
<node TEXT="exports"/>
<node TEXT="types"/>
<node TEXT="values"/>
</node>
</node>
<node TEXT="outline">
<node TEXT="add icons">
<node TEXT="collapse all"/>
<node TEXT="expand all"/>
</node>
<node TEXT="remove colapse/expand 2"/>
<node TEXT="added collapse/expand item"/>
<node TEXT="imports grouped by modules"/>
</node>
<node TEXT="icons">
<node TEXT="import item"/>
<node TEXT="export item"/>
</node>
</node>
<node TEXT="1.0.4">
<node TEXT="split modules into files"/>
<node TEXT="parse multiple modules from one file"/>
</node>
</node>
</node>
</map>
