// $ANTLR : "ASN1.g" -> "ASNParser.java"$

/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * Based upon the grammar submitted by Vivek Gupta to antlr.org.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.model;

import antlr.*;

import java.math.*;
import java.util.*;

import org.eclipse.core.resources.IResource;

import net.sourceforge.asneditor.util.HashableList;
import net.sourceforge.asneditor.model.manager.IModelHandler;

import antlr.TokenBuffer;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.ANTLRException;
import antlr.LLkParser;
import antlr.Token;
import antlr.TokenStream;
import antlr.RecognitionException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.ParserSharedInputState;
import antlr.collections.impl.BitSet;

public class ASNParser extends antlr.LLkParser       implements ASNTokenTypes
 {

	protected IModelHandler modelHandler = null;
	protected IResource moduleResource = null;

	public ASNParser(TokenStream lexer, IModelHandler modelHandler, IResource moduleResource) {
	  this(lexer,3);
	  this.modelHandler = modelHandler;
	  this.moduleResource = moduleResource;
	}

	/* dummy method in order to handle ANTLR above 2.7.5 */
	public void recover(RecognitionException ex, BitSet set) throws TokenStreamException {
		super.recover(ex, set);
	}

	public void reportError(RecognitionException ex) {
		modelHandler.reportError(ex);
        //super.reportError(ex);
	}
  /* (non-Javadoc)
   * @see antlr.Parser#reportError(java.lang.String)
   */
  public void reportError(String arg0) {
		modelHandler.reportError(arg0, null, 0, 0);
        //super.reportError(arg0);
  }

  /* (non-Javadoc)
   * @see antlr.Parser#reportWarning(java.lang.String)
   */
  public void reportWarning(String str) {
    modelHandler.reportWarning(str);
    //super.reportWarning(str);
  }

	public final void grammar() {
    	modelHandler.processing(moduleResource);
    	try {      // for error handling
    	  asn_file();
    	} catch (RecognitionException ex) {
    	  reportError(ex);
		} catch (TokenStreamRecognitionException e) {
          Throwable cause = e.recog;
          if (cause instanceof RecognitionException) {
            reportError((RecognitionException) cause);
          } else {
            modelHandler.reportError(e.getMessage(), e, -1, -1);
          }
		} catch (TokenStreamException e) {
		  modelHandler.reportError(e.getMessage(), e, -1, -1);
		}
	}
	private void checkDeclaration(Token token, ASNModule module) throws RecognitionException {
		final IASN prevDeclaration = module.findDeclaration(token.getText());
		if (prevDeclaration != null)
			throw new RecognitionException("Identifier "+token.getText()+
				" allready defined at line "+prevDeclaration.getLine()+" col "+prevDeclaration.getColumn()+" ",
				getFilename(), token.getLine(), token.getColumn());
	}
	private void checkExports(ASNModule module) throws RecognitionException {
		// exports
		Iterator exports = module.exportSymbolListIterator();
		while (exports.hasNext()) {
			final String export = (String) exports.next();
			final IASN exportDeclaration = module.findDeclaration(export);
			if (exportDeclaration == null)
				throw new RecognitionException("Identifier "+export+" was exported but not defined ",
					getFilename(), module.getExportsItem().getLine(), module.getExportsItem().getColumn());
		}
	}

protected ASNParser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
}

public ASNParser(TokenBuffer tokenBuf) {
  this(tokenBuf,3);
}

protected ASNParser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
}

public ASNParser(TokenStream lexer) {
  this(lexer,3);
}

public ASNParser(ParserSharedInputState state) {
  super(state,3);
  tokenNames = _tokenNames;
}

	public final void asn_file() throws RecognitionException, TokenStreamException {
		
		
			ASNModule module;
		
		
		try {      // for error handling
			{
			int _cnt3=0;
			_loop3:
			do {
				if ((LA(1)==UPPER)) {
					if ( inputState.guessing==0 ) {
						
						//			long startTime = java.util.Calendar.getInstance().getTimeInMillis();
								
					}
					module=module_definition(moduleResource);
					if ( inputState.guessing==0 ) {
						
						//			long parseTime = java.util.Calendar.getInstance().getTimeInMillis();
									checkExports(module);
						//			long endTime = java.util.Calendar.getInstance().getTimeInMillis();
						//			System.out.println("Processed module "+module.getName()+" in "+((endTime - startTime) / 1000.0)+
						//				" sec (type check "+((endTime - parseTime) / 1000.0)+" sec)");
									if (module.getName() == null)
										break;
									modelHandler.addModule(module);
								
					}
				}
				else {
					if ( _cnt3>=1 ) { break _loop3; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt3++;
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_0);
			} else {
			  throw ex;
			}
		}
	}
	
	public final ASNModule  module_definition(
		IResource moduleResource
	) throws RecognitionException, TokenStreamException {
		ASNModule module;
		
		Token  def = null;
		Token  e = null;
		Token  i = null;
		Token  a = null;
		Token  end = null;
		
			module = new ASNModule(moduleResource);
			AsnModuleIdentifier mid;
			String s ;
		
		
		
		try {      // for error handling
			{
			mid=module_identifier();
			if ( inputState.guessing==0 ) {
				module.moduleIdentifier = mid; 	
			}
			}
			def = LT(1);
			match(DEFINITIONS_KW);
			if ( inputState.guessing==0 ) {
				module.setToken(def);
			}
			{
			switch ( LA(1)) {
			case AUTOMATIC_KW:
			case EXPLICIT_KW:
			case IMPLICIT_KW:
			{
				{
				switch ( LA(1)) {
				case EXPLICIT_KW:
				{
					e = LT(1);
					match(EXPLICIT_KW);
					if ( inputState.guessing==0 ) {
						module.tagDefault = e.getText();
					}
					break;
				}
				case IMPLICIT_KW:
				{
					i = LT(1);
					match(IMPLICIT_KW);
					if ( inputState.guessing==0 ) {
						module.tagDefault = i.getText();
					}
					break;
				}
				case AUTOMATIC_KW:
				{
					a = LT(1);
					match(AUTOMATIC_KW);
					if ( inputState.guessing==0 ) {
						module.tagDefault = a.getText();
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				match(TAGS_KW);
				if ( inputState.guessing==0 ) {
					module.tag = true;
				}
				break;
			}
			case EXTENSIBILITY_KW:
			case ASSIGN_OP:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case EXTENSIBILITY_KW:
			{
				match(EXTENSIBILITY_KW);
				match(IMPLIED_KW);
				if ( inputState.guessing==0 ) {
					module.extensible=true;
				}
				break;
			}
			case ASSIGN_OP:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(ASSIGN_OP);
			match(BEGIN_KW);
			module_body(module);
			end = LT(1);
			match(END_KW);
			if ( inputState.guessing==0 ) {
				module.setEndToken(end);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_1);
			} else {
			  throw ex;
			}
		}
		return module;
	}
	
	public final  AsnModuleIdentifier  module_identifier() throws RecognitionException, TokenStreamException {
		 AsnModuleIdentifier mid ;
		
		Token  md = null;
		mid = new AsnModuleIdentifier();
		AsnOidComponentList cmplst;
		
		try {      // for error handling
			{
			{
			md = LT(1);
			match(UPPER);
			if ( inputState.guessing==0 ) {
				mid.name = md.getText(); mid.setToken(md);
			}
			}
			{
			switch ( LA(1)) {
			case L_BRACE:
			{
				{
				cmplst=obj_id_comp_lst();
				if ( inputState.guessing==0 ) {
					mid.componentList = cmplst;
				}
				}
				break;
			}
			case DEFINITIONS_KW:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		return mid ;
	}
	
	public final void module_body(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case EXPORTS_KW:
			{
				exports(module);
				break;
			}
			case END_KW:
			case IMPORTS_KW:
			case UPPER:
			case LOWER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case IMPORTS_KW:
			{
				imports(module);
				break;
			}
			case END_KW:
			case UPPER:
			case LOWER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case UPPER:
			case LOWER:
			{
				{
				int _cnt44=0;
				_loop44:
				do {
					if ((LA(1)==UPPER||LA(1)==LOWER)) {
						assignment(module);
					}
					else {
						if ( _cnt44>=1 ) { break _loop44; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt44++;
				} while (true);
				}
				break;
			}
			case END_KW:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_3);
			} else {
			  throw ex;
			}
		}
	}
	
	public final AsnOidComponentList  obj_id_comp_lst() throws RecognitionException, TokenStreamException {
		AsnOidComponentList oidcmplst;
		
		oidcmplst = new AsnOidComponentList();
		AsnOidComponent oidcmp; AsnDefinedValue defval;
		
		try {      // for error handling
			match(L_BRACE);
			{
			{
			boolean synPredMatched18 = false;
			if (((LA(1)==UPPER||LA(1)==LOWER) && (_tokenSet_4.member(LA(2))) && (_tokenSet_5.member(LA(3))))) {
				int _m18 = mark();
				synPredMatched18 = true;
				inputState.guessing++;
				try {
					{
					defined_value();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched18 = false;
				}
				rewind(_m18);
inputState.guessing--;
			}
			if ( synPredMatched18 ) {
				{
				defval=defined_value();
				if ( inputState.guessing==0 ) {
					oidcmplst.isDefinitive=true;oidcmplst.defval=defval;
				}
				}
			}
			else if (((LA(1) >= NUMBER && LA(1) <= LOWER)) && (_tokenSet_5.member(LA(2))) && (_tokenSet_6.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			int _cnt21=0;
			_loop21:
			do {
				if (((LA(1) >= NUMBER && LA(1) <= LOWER))) {
					oidcmp=obj_id_component();
					if ( inputState.guessing==0 ) {
						oidcmplst.components.add(oidcmp);
					}
				}
				else {
					if ( _cnt21>=1 ) { break _loop21; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt21++;
			} while (true);
			}
			}
			match(R_BRACE);
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_7);
			} else {
			  throw ex;
			}
		}
		return oidcmplst;
	}
	
	protected final AsnDefinedValue  defined_value() throws RecognitionException, TokenStreamException {
		AsnDefinedValue defval;
		
		Token  up = null;
		Token  lid = null;
		defval = new AsnDefinedValue();
		
		try {      // for error handling
			{
			{
			switch ( LA(1)) {
			case UPPER:
			{
				up = LT(1);
				match(UPPER);
				if ( inputState.guessing==0 ) {
					defval.moduleIdentifier = up.getText(); defval.setToken(up);
				}
				match(DOT);
				if ( inputState.guessing==0 ) {
					defval.isDotPresent=true;
				}
				break;
			}
			case LOWER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			lid = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				defval.name = lid.getText(); defval.setToken(lid);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_8);
			} else {
			  throw ex;
			}
		}
		return defval;
	}
	
	public final AsnOidComponent  obj_id_component() throws RecognitionException, TokenStreamException {
		AsnOidComponent oidcmp ;
		
		Token  num = null;
		Token  lid = null;
		Token  num1 = null;
		oidcmp = new AsnOidComponent(); AsnDefinedValue defval;
		String s,n ="";
		
		try {      // for error handling
			{
			if ((LA(1)==NUMBER)) {
				{
				num = LT(1);
				match(NUMBER);
				if ( inputState.guessing==0 ) {
					s=num.getText();oidcmp.num = new Integer(s); oidcmp.numberForm=true; oidcmp.setToken(num);
				}
				}
			}
			else {
				boolean synPredMatched27 = false;
				if (((LA(1)==LOWER) && (_tokenSet_9.member(LA(2))) && (_tokenSet_6.member(LA(3))))) {
					int _m27 = mark();
					synPredMatched27 = true;
					inputState.guessing++;
					try {
						{
						match(LOWER);
						{
						if ((LA(1)==L_PAREN)) {
							match(L_PAREN);
							match(NUMBER);
							match(R_PAREN);
						}
						else {
						}
						
						}
						}
					}
					catch (RecognitionException pe) {
						synPredMatched27 = false;
					}
					rewind(_m27);
inputState.guessing--;
				}
				if ( synPredMatched27 ) {
					{
					{
					lid = LT(1);
					match(LOWER);
					if ( inputState.guessing==0 ) {
						oidcmp.name = lid.getText();oidcmp.nameForm=true;oidcmp.setToken(lid);
					}
					}
					{
					switch ( LA(1)) {
					case L_PAREN:
					{
						match(L_PAREN);
						{
						num1 = LT(1);
						match(NUMBER);
						if ( inputState.guessing==0 ) {
							n=num1.getText(); oidcmp.num = new Integer(n);oidcmp.nameAndNumberForm=true;
						}
						}
						match(R_PAREN);
						break;
					}
					case R_BRACE:
					case NUMBER:
					case UPPER:
					case LOWER:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					}
				}
				else {
					boolean synPredMatched33 = false;
					if (((LA(1)==UPPER||LA(1)==LOWER) && (_tokenSet_10.member(LA(2))) && (_tokenSet_6.member(LA(3))))) {
						int _m33 = mark();
						synPredMatched33 = true;
						inputState.guessing++;
						try {
							{
							defined_value();
							}
						}
						catch (RecognitionException pe) {
							synPredMatched33 = false;
						}
						rewind(_m33);
inputState.guessing--;
					}
					if ( synPredMatched33 ) {
						{
						defval=defined_value();
						if ( inputState.guessing==0 ) {
							oidcmp.isDefinedValue=true;oidcmp.defval=defval;
						}
						}
					}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					}}
					}
				}
				catch (RecognitionException ex) {
					if (inputState.guessing==0) {
						reportError(ex);
						recover(ex,_tokenSet_11);
					} else {
					  throw ex;
					}
				}
				return oidcmp ;
			}
			
	public final String  tag_default() throws RecognitionException, TokenStreamException {
		String s;
		
		Token  tg = null;
		Token  tg1 = null;
		Token  tg2 = null;
		s = "";
		
		try {      // for error handling
			switch ( LA(1)) {
			case EXPLICIT_KW:
			{
				{
				tg = LT(1);
				match(EXPLICIT_KW);
				if ( inputState.guessing==0 ) {
					s = tg.getText();
				}
				}
				break;
			}
			case IMPLICIT_KW:
			{
				{
				tg1 = LT(1);
				match(IMPLICIT_KW);
				if ( inputState.guessing==0 ) {
					s = tg1.getText();
				}
				}
				break;
			}
			case AUTOMATIC_KW:
			{
				{
				tg2 = LT(1);
				match(AUTOMATIC_KW);
				if ( inputState.guessing==0 ) {
					s = tg2.getText();
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_12);
			} else {
			  throw ex;
			}
		}
		return s;
	}
	
	public final void exports(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  e = null;
		Token  semi = null;
		String s; HashableList syml = new HashableList();
		
		try {      // for error handling
			e = LT(1);
			match(EXPORTS_KW);
			if ( inputState.guessing==0 ) {
				module.exported=true; module.setExportsToken(e);
			}
			{
			switch ( LA(1)) {
			case ERROR_KW:
			case OBJECT_KW:
			case OPERATION_KW:
			case SEMI:
			case UPPER:
			case LOWER:
			case LITERAL_BIND:
			case LITERAL_UNBIND:
			case 134:
			case 135:
			case LITERAL_EXTENSION:
			case LITERAL_EXTENSIONS:
			case 138:
			case LITERAL_TOKEN:
			case 140:
			case 141:
			case LITERAL_PORT:
			case LITERAL_REFINE:
			case 144:
			case 145:
			case 146:
			case 147:
			case LITERAL_ALGORITHM:
			case LITERAL_ENCRYPTED:
			case LITERAL_SIGNED:
			case LITERAL_SIGNATURE:
			case LITERAL_PROTECTED:
			case 153:
			{
				{
				switch ( LA(1)) {
				case ERROR_KW:
				case OBJECT_KW:
				case OPERATION_KW:
				case UPPER:
				case LOWER:
				case LITERAL_BIND:
				case LITERAL_UNBIND:
				case 134:
				case 135:
				case LITERAL_EXTENSION:
				case LITERAL_EXTENSIONS:
				case 138:
				case LITERAL_TOKEN:
				case 140:
				case 141:
				case LITERAL_PORT:
				case LITERAL_REFINE:
				case 144:
				case 145:
				case 146:
				case 147:
				case LITERAL_ALGORITHM:
				case LITERAL_ENCRYPTED:
				case LITERAL_SIGNED:
				case LITERAL_SIGNATURE:
				case LITERAL_PROTECTED:
				case 153:
				{
					syml=symbol_list();
					if ( inputState.guessing==0 ) {
						module.exportSymbolList = syml;
					}
					break;
				}
				case SEMI:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				break;
			}
			case ALL_KW:
			{
				match(ALL_KW);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			semi = LT(1);
			match(SEMI);
			if ( inputState.guessing==0 ) {
				module.setExportsEndToken(semi);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_13);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void imports(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  imp = null;
		Token  semi = null;
		
		try {      // for error handling
			{
			imp = LT(1);
			match(IMPORTS_KW);
			{
			_loop51:
			do {
				if ((_tokenSet_14.member(LA(1)))) {
					symbols_from_module(module);
				}
				else {
					break _loop51;
				}
				
			} while (true);
			}
			semi = LT(1);
			match(SEMI);
			}
			if ( inputState.guessing==0 ) {
				module.imported=true; module.setImportsToken(imp); module.setImportsEndToken(semi);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_15);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void assignment(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  up2 = null;
		AsnType obj ; AsnValue val;	
		
		try {      // for error handling
			if ((LA(1)==UPPER) && (LA(2)==ASSIGN_OP) && (_tokenSet_12.member(LA(3)))) {
				typeAssignment(module);
			}
			else if ((LA(1)==LOWER) && (_tokenSet_12.member(LA(2))) && (_tokenSet_16.member(LA(3)))) {
				valueAssignment(module);
			}
			else {
				boolean synPredMatched76 = false;
				if (((LA(1)==UPPER) && (LA(2)==LITERAL_MACRO))) {
					int _m76 = mark();
					synPredMatched76 = true;
					inputState.guessing++;
					try {
						{
						match(UPPER);
						match(LITERAL_MACRO);
						match(ASSIGN_OP);
						match(BEGIN_KW);
						{
						_loop75:
						do {
							if ((_tokenSet_17.member(LA(1)))) {
								{
								match(_tokenSet_17);
								}
							}
							else {
								break _loop75;
							}
							
						} while (true);
						}
						match(END_KW);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched76 = false;
					}
					rewind(_m76);
inputState.guessing--;
				}
				if ( synPredMatched76 ) {
					{
					up2 = LT(1);
					match(UPPER);
					match(LITERAL_MACRO);
					match(ASSIGN_OP);
					match(BEGIN_KW);
					{
					_loop80:
					do {
						if ((_tokenSet_17.member(LA(1)))) {
							{
							match(_tokenSet_17);
							}
						}
						else {
							break _loop80;
						}
						
					} while (true);
					}
					match(END_KW);
					}
					if ( inputState.guessing==0 ) {
						
								checkDeclaration(up2, module);
								
					}
				}
				else if ((LA(1)==UPPER) && (LA(2)==ASSIGN_OP) && (LA(3)==CLASS_KW)) {
					dummyObjectClassAssignment(module);
				}
				else {
					boolean synPredMatched82 = false;
					if (((LA(1)==UPPER) && (LA(2)==UPPER))) {
						int _m82 = mark();
						synPredMatched82 = true;
						inputState.guessing++;
						try {
							{
							match(UPPER);
							match(UPPER);
							match(ASSIGN_OP);
							match(L_BRACE);
							}
						}
						catch (RecognitionException pe) {
							synPredMatched82 = false;
						}
						rewind(_m82);
inputState.guessing--;
					}
					if ( synPredMatched82 ) {
						dummy_constrained_type(module);
					}
					else {
						boolean synPredMatched84 = false;
						if (((LA(1)==LOWER) && (LA(2)==UPPER) && (LA(3)==ASSIGN_OP))) {
							int _m84 = mark();
							synPredMatched84 = true;
							inputState.guessing++;
							try {
								{
								match(LOWER);
								match(UPPER);
								match(ASSIGN_OP);
								match(L_BRACE);
								}
							}
							catch (RecognitionException pe) {
								synPredMatched84 = false;
							}
							rewind(_m84);
inputState.guessing--;
						}
						if ( synPredMatched84 ) {
							dummy_constrained_value(module);
						}
						else {
							throw new NoViableAltException(LT(1), getFilename());
						}
						}}}
					}
					catch (RecognitionException ex) {
						if (inputState.guessing==0) {
							reportError(ex);
							recover(ex,_tokenSet_15);
						} else {
						  throw ex;
						}
					}
				}
				
	public final HashableList  symbol_list() throws RecognitionException, TokenStreamException {
		HashableList symlist;
		
		symlist = new HashableList(); String s="";
		
		try {      // for error handling
			{
			{
			s=symbol();
			if ( inputState.guessing==0 ) {
				symlist.add(s);
			}
			}
			{
			_loop66:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					{
					s=symbol();
					if ( inputState.guessing==0 ) {
						symlist.add(s);
					}
					}
				}
				else {
					break _loop66;
				}
				
			} while (true);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_18);
			} else {
			  throw ex;
			}
		}
		return symlist;
	}
	
	public final void symbols_from_module(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  up = null;
		SymbolsFromModule sym = new SymbolsFromModule();
		String s = "" ; AsnModuleIdentifier mid; AsnDefinedValue defval;
		HashableList arl; AsnOidComponentList cmplist;
		
		try {      // for error handling
			{
			{
			arl=symbol_list();
			if ( inputState.guessing==0 ) {
				sym.symbolList = arl;
			}
			}
			match(FROM_KW);
			{
			{
			up = LT(1);
			match(UPPER);
			if ( inputState.guessing==0 ) {
				sym.modref = up.getText(); sym.setToken(up);
			}
			}
			{
			if ((LA(1)==L_BRACE)) {
				cmplist=obj_id_comp_lst();
				if ( inputState.guessing==0 ) {
					sym.isOidValue=true;sym.cmplist = cmplist;
				}
			}
			else {
				boolean synPredMatched59 = false;
				if (((LA(1)==UPPER||LA(1)==LOWER) && (_tokenSet_19.member(LA(2))) && (_tokenSet_20.member(LA(3))))) {
					int _m59 = mark();
					synPredMatched59 = true;
					inputState.guessing++;
					try {
						{
						defined_value();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched59 = false;
					}
					rewind(_m59);
inputState.guessing--;
				}
				if ( synPredMatched59 ) {
					{
					defval=defined_value();
					if ( inputState.guessing==0 ) {
						sym.isDefinedValue=true;sym.defval=defval;
					}
					}
				}
				else if ((_tokenSet_21.member(LA(1))) && (_tokenSet_20.member(LA(2))) && (_tokenSet_22.member(LA(3)))) {
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				}
				if ( inputState.guessing==0 ) {
					module.addImportSymbolFromModuleList(sym);
				}
			}
			catch (RecognitionException ex) {
				if (inputState.guessing==0) {
					reportError(ex);
					recover(ex,_tokenSet_21);
				} else {
				  throw ex;
				}
			}
		}
		
	public final String  symbol() throws RecognitionException, TokenStreamException {
		String s;
		
		Token  up = null;
		Token  lid = null;
		s="";
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case UPPER:
			{
				up = LT(1);
				match(UPPER);
				if ( inputState.guessing==0 ) {
					s = up.getText();
				}
				break;
			}
			case LOWER:
			{
				lid = LT(1);
				match(LOWER);
				if ( inputState.guessing==0 ) {
					s = lid.getText();
				}
				break;
			}
			case ERROR_KW:
			case OBJECT_KW:
			case OPERATION_KW:
			case LITERAL_BIND:
			case LITERAL_UNBIND:
			case 134:
			case 135:
			case LITERAL_EXTENSION:
			case LITERAL_EXTENSIONS:
			case 138:
			case LITERAL_TOKEN:
			case 140:
			case 141:
			case LITERAL_PORT:
			case LITERAL_REFINE:
			case 144:
			case 145:
			case 146:
			case 147:
			case LITERAL_ALGORITHM:
			case LITERAL_ENCRYPTED:
			case LITERAL_SIGNED:
			case LITERAL_SIGNATURE:
			case LITERAL_PROTECTED:
			case 153:
			{
				s=macroName();
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case L_BRACE:
			{
				match(L_BRACE);
				match(R_BRACE);
				break;
			}
			case FROM_KW:
			case COMMA:
			case SEMI:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_23);
			} else {
			  throw ex;
			}
		}
		return s;
	}
	
	public final String  macroName() throws RecognitionException, TokenStreamException {
		String s;
		
		s="";
		
		try {      // for error handling
			switch ( LA(1)) {
			case OPERATION_KW:
			{
				match(OPERATION_KW);
				if ( inputState.guessing==0 ) {
					s = "OPERATION";
				}
				break;
			}
			case ERROR_KW:
			{
				match(ERROR_KW);
				if ( inputState.guessing==0 ) {
					s = "ERROR";
				}
				break;
			}
			case LITERAL_BIND:
			{
				match(LITERAL_BIND);
				if ( inputState.guessing==0 ) {
					s = "BIND";
				}
				break;
			}
			case LITERAL_UNBIND:
			{
				match(LITERAL_UNBIND);
				if ( inputState.guessing==0 ) {
					s = "UNBIND";
				}
				break;
			}
			case 134:
			{
				match(134);
				if ( inputState.guessing==0 ) {
					s = "APPLICATION-SERVICE-ELEMENT";
				}
				break;
			}
			case 135:
			{
				match(135);
				if ( inputState.guessing==0 ) {
					s = "APPLICATION-CONTEXT";
				}
				break;
			}
			case LITERAL_EXTENSION:
			{
				match(LITERAL_EXTENSION);
				if ( inputState.guessing==0 ) {
					s = "EXTENSION";
				}
				break;
			}
			case LITERAL_EXTENSIONS:
			{
				match(LITERAL_EXTENSIONS);
				if ( inputState.guessing==0 ) {
					s = "EXTENSIONS";
				}
				break;
			}
			case 138:
			{
				match(138);
				if ( inputState.guessing==0 ) {
					s = "EXTENSION-ATTRIBUTE";
				}
				break;
			}
			case LITERAL_TOKEN:
			{
				match(LITERAL_TOKEN);
				if ( inputState.guessing==0 ) {
					s = "TOKEN";
				}
				break;
			}
			case 140:
			{
				match(140);
				if ( inputState.guessing==0 ) {
					s = "TOKEN-DATA";
				}
				break;
			}
			case 141:
			{
				match(141);
				if ( inputState.guessing==0 ) {
					s = "SECURITY-CATEGORY";
				}
				break;
			}
			case OBJECT_KW:
			{
				match(OBJECT_KW);
				if ( inputState.guessing==0 ) {
					s = "OBJECT";
				}
				break;
			}
			case LITERAL_PORT:
			{
				match(LITERAL_PORT);
				if ( inputState.guessing==0 ) {
					s = "PORT";
				}
				break;
			}
			case LITERAL_REFINE:
			{
				match(LITERAL_REFINE);
				if ( inputState.guessing==0 ) {
					s = "REFINE";
				}
				break;
			}
			case 144:
			{
				match(144);
				if ( inputState.guessing==0 ) {
					s = "ABSTRACT-BIND";
				}
				break;
			}
			case 145:
			{
				match(145);
				if ( inputState.guessing==0 ) {
					s = "ABSTRACT-UNBIND";
				}
				break;
			}
			case 146:
			{
				match(146);
				if ( inputState.guessing==0 ) {
					s = "ABSTRACT-OPERATION";
				}
				break;
			}
			case 147:
			{
				match(147);
				if ( inputState.guessing==0 ) {
					s = "ABSTRACT-ERROR";
				}
				break;
			}
			case LITERAL_ALGORITHM:
			{
				match(LITERAL_ALGORITHM);
				if ( inputState.guessing==0 ) {
					s = "ALGORITHM";
				}
				break;
			}
			case LITERAL_ENCRYPTED:
			{
				match(LITERAL_ENCRYPTED);
				if ( inputState.guessing==0 ) {
					s = "ENCRYPTED";
				}
				break;
			}
			case LITERAL_SIGNED:
			{
				match(LITERAL_SIGNED);
				if ( inputState.guessing==0 ) {
					s = "SIGNED";
				}
				break;
			}
			case LITERAL_SIGNATURE:
			{
				match(LITERAL_SIGNATURE);
				if ( inputState.guessing==0 ) {
					s = "SIGNATURE";
				}
				break;
			}
			case LITERAL_PROTECTED:
			{
				match(LITERAL_PROTECTED);
				if ( inputState.guessing==0 ) {
					s = "PROTECTED";
				}
				break;
			}
			case 153:
			{
				match(153);
				if ( inputState.guessing==0 ) {
					s = "OBJECT-TYPE";
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_24);
			} else {
			  throw ex;
			}
		}
		return s;
	}
	
	public final void typeAssignment(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  up = null;
		AsnType obj;
		
		try {      // for error handling
			{
			up = LT(1);
			match(UPPER);
			match(ASSIGN_OP);
			{
			obj=type();
			}
			if ( inputState.guessing==0 ) {
				
						if (obj != null) {
							obj.setToken(up);
							obj.setEndToken(LT(0));
							if (obj instanceof AsnType) {
								((AsnType)obj).name = up.getText();
								module.addType((AsnType)obj, getFilename());
							} else {
								System.out.print("Unknown Type "+up.getText()+" ::= ");
								System.out.println((obj.getClass().getName()) );
							}
						} else {
							System.out.println("Type obj = null");
						}
				
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_15);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void valueAssignment(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  lid = null;
		AsnType objv; AsnValue val;
		
		try {      // for error handling
			{
			lid = LT(1);
			match(LOWER);
			{
			objv=type();
			}
			match(ASSIGN_OP);
			{
			val=value();
			}
			if ( inputState.guessing==0 ) {
				
						if (val != null) {
							val.setToken(lid);
							val.setEndToken(LT(0));
							val.name=lid.getText();
							val.typeName = objv.getTypeName();
							module.addValue(val, getFilename());
						}
						if (val.typeName == null) {
							System.out.println("Value not defined  " + (objv.getClass().getName()));
						}
						
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_15);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void dummyObjectClassAssignment(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  up = null;
		AsnType obj;
		
		try {      // for error handling
			{
			up = LT(1);
			match(UPPER);
			match(ASSIGN_OP);
			match(CLASS_KW);
			ignored_brace_content();
			}
			{
			switch ( LA(1)) {
			case WITH_KW:
			{
				match(WITH_KW);
				match(SYNTAX_KW);
				ignored_brace_content();
				break;
			}
			case END_KW:
			case UPPER:
			case LOWER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_15);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void dummy_constrained_type(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  up = null;
		AsnSequenceSet obj = new AsnSequenceSet();
		
		try {      // for error handling
			{
			up = LT(1);
			match(UPPER);
			match(UPPER);
			match(ASSIGN_OP);
			ignored_brace_content();
			}
			if ( inputState.guessing==0 ) {
				
						obj.setToken(up);
						obj.setEndToken(LT(0));
						obj.name = up.getText();
						module.addType(obj, getFilename());
					
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_15);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void dummy_constrained_value(
		ASNModule module
	) throws RecognitionException, TokenStreamException {
		
		Token  lo = null;
		AsnDefinedValue obj = new AsnDefinedValue();
		
		try {      // for error handling
			{
			lo = LT(1);
			match(LOWER);
			match(UPPER);
			match(ASSIGN_OP);
			ignored_brace_content();
			}
			if ( inputState.guessing==0 ) {
				
						AsnValue val = new AsnValue();
						val.isDefinedValue = true;
						val.definedValue = obj;
						obj.setToken(lo);
						obj.setEndToken(LT(0));
						obj.name = lo.getText();
						module.addValue(val, getFilename());
					
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_15);
			} else {
			  throw ex;
			}
		}
	}
	
	public final AsnType  type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		obj = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case EMBEDDED_KW:
			case ENUMERATED_KW:
			case EXTERNAL_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case ISO646_STR_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case L_BRACKET:
			case T61_STR_KW:
			{
				{
				obj=built_in_type();
				}
				break;
			}
			case UPPER:
			case OJECT_UPPER:
			case OJECT_LOWER:
			{
				{
				obj=defined_type();
				}
				break;
			}
			case LOWER:
			{
				{
				obj=selection_type();
				}
				break;
			}
			case ERROR_KW:
			case OPERATION_KW:
			case 153:
			{
				{
				obj=macros_type();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnValue  value() throws RecognitionException, TokenStreamException {
		AsnValue value;
		
		Token  t = null;
		Token  t1 = null;
		Token  t2 = null;
		Token  c = null;
		Token  t3 = null;
		Token  t4 = null;
		value = new AsnValue(); AsnSequenceValue seqval;
		AsnDefinedValue defval;String aStr;AsnSignedNumber num;
		AsnOidComponentList cmplst;
		
		try {      // for error handling
			switch ( LA(1)) {
			case TRUE_KW:
			{
				{
				t = LT(1);
				match(TRUE_KW);
				if ( inputState.guessing==0 ) {
					value.isTrueKW = true; value.setToken(t);
				}
				}
				break;
			}
			case FALSE_KW:
			{
				{
				t1 = LT(1);
				match(FALSE_KW);
				if ( inputState.guessing==0 ) {
					value.isFalseKW = true; value.setToken(t1);
				}
				}
				break;
			}
			case NULL_KW:
			{
				{
				t2 = LT(1);
				match(NULL_KW);
				if ( inputState.guessing==0 ) {
					value.isNullKW = true; value.setToken(t2);
				}
				}
				break;
			}
			case C_STRING:
			{
				{
				c = LT(1);
				match(C_STRING);
				if ( inputState.guessing==0 ) {
					value.isCString=true; value.cStr = c.getText(); value.setToken(c);
				}
				}
				break;
			}
			case MINUS:
			case NUMBER:
			{
				{
				num=signed_number();
				if ( inputState.guessing==0 ) {
					value.isSignedNumber=true ; value.signedNumber = num;
				}
				}
				break;
			}
			case PLUS_INFINITY_KW:
			{
				{
				t3 = LT(1);
				match(PLUS_INFINITY_KW);
				if ( inputState.guessing==0 ) {
					value.isPlusInfinity = true; value.setToken(t3);
				}
				}
				break;
			}
			case MINUS_INFINITY_KW:
			{
				{
				t4 = LT(1);
				match(MINUS_INFINITY_KW);
				if ( inputState.guessing==0 ) {
					value.isMinusInfinity = true; value.setToken(t4);
				}
				}
				break;
			}
			default:
				boolean synPredMatched453 = false;
				if (((LA(1)==UPPER||LA(1)==LOWER) && (_tokenSet_26.member(LA(2))) && (_tokenSet_27.member(LA(3))))) {
					int _m453 = mark();
					synPredMatched453 = true;
					inputState.guessing++;
					try {
						{
						defined_value();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched453 = false;
					}
					rewind(_m453);
inputState.guessing--;
				}
				if ( synPredMatched453 ) {
					{
					defval=defined_value();
					if ( inputState.guessing==0 ) {
						value.isDefinedValue = true; value.definedValue = defval;
					}
					}
				}
				else {
					boolean synPredMatched459 = false;
					if (((LA(1)==LOWER) && (_tokenSet_28.member(LA(2))) && (_tokenSet_29.member(LA(3))))) {
						int _m459 = mark();
						synPredMatched459 = true;
						inputState.guessing++;
						try {
							{
							choice_value(value);
							}
						}
						catch (RecognitionException pe) {
							synPredMatched459 = false;
						}
						rewind(_m459);
inputState.guessing--;
					}
					if ( synPredMatched459 ) {
						{
						choice_value(value);
						if ( inputState.guessing==0 ) {
							value.isChoiceValue = true;
						}
						}
					}
					else {
						boolean synPredMatched462 = false;
						if (((LA(1)==L_BRACE) && (LA(2)==COMMA||LA(2)==R_BRACE||LA(2)==LOWER) && (_tokenSet_30.member(LA(3))))) {
							int _m462 = mark();
							synPredMatched462 = true;
							inputState.guessing++;
							try {
								{
								sequence_value();
								}
							}
							catch (RecognitionException pe) {
								synPredMatched462 = false;
							}
							rewind(_m462);
inputState.guessing--;
						}
						if ( synPredMatched462 ) {
							{
							seqval=sequence_value();
							if ( inputState.guessing==0 ) {
								value.isSequenceValue=true;value.seqval=seqval;
							}
							}
						}
						else {
							boolean synPredMatched465 = false;
							if (((LA(1)==L_BRACE) && (_tokenSet_31.member(LA(2))) && (_tokenSet_29.member(LA(3))))) {
								int _m465 = mark();
								synPredMatched465 = true;
								inputState.guessing++;
								try {
									{
									sequenceof_value(value);
									}
								}
								catch (RecognitionException pe) {
									synPredMatched465 = false;
								}
								rewind(_m465);
inputState.guessing--;
							}
							if ( synPredMatched465 ) {
								{
								sequenceof_value(value);
								if ( inputState.guessing==0 ) {
									value.isSequenceOfValue=true;
								}
								}
							}
							else {
								boolean synPredMatched468 = false;
								if (((LA(1)==L_BRACE||LA(1)==B_STRING||LA(1)==H_STRING) && (_tokenSet_30.member(LA(2))) && (_tokenSet_27.member(LA(3))))) {
									int _m468 = mark();
									synPredMatched468 = true;
									inputState.guessing++;
									try {
										{
										cstr_value(value);
										}
									}
									catch (RecognitionException pe) {
										synPredMatched468 = false;
									}
									rewind(_m468);
inputState.guessing--;
								}
								if ( synPredMatched468 ) {
									{
									cstr_value(value);
									if ( inputState.guessing==0 ) {
										value.isCStrValue = true;
									}
									}
								}
								else {
									boolean synPredMatched471 = false;
									if (((LA(1)==L_BRACE) && ((LA(2) >= NUMBER && LA(2) <= LOWER)) && (_tokenSet_5.member(LA(3))))) {
										int _m471 = mark();
										synPredMatched471 = true;
										inputState.guessing++;
										try {
											{
											obj_id_comp_lst();
											}
										}
										catch (RecognitionException pe) {
											synPredMatched471 = false;
										}
										rewind(_m471);
inputState.guessing--;
									}
									if ( synPredMatched471 ) {
										{
										cmplst=obj_id_comp_lst();
										if ( inputState.guessing==0 ) {
											value.isAsnOIDValue=true;value.oidval=cmplst;
										}
										}
									}
								else {
									throw new NoViableAltException(LT(1), getFilename());
								}
								}}}}}}
							}
							catch (RecognitionException ex) {
								if (inputState.guessing==0) {
									reportError(ex);
									recover(ex,_tokenSet_30);
								} else {
								  throw ex;
								}
							}
							return value;
						}
						
	public final AsnType  built_in_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		obj = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case ANY_KW:
			{
				{
				obj=any_type();
				}
				break;
			}
			case BIT_KW:
			{
				{
				obj=bit_string_type();
				}
				break;
			}
			case BOOLEAN_KW:
			{
				{
				obj=boolean_type();
				}
				break;
			}
			case BMP_STR_KW:
			case CHARACTER_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case ISO646_STR_KW:
			case NUMERIC_STR_KW:
			case PRINTABLE_STR_KW:
			case TELETEX_STR_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case T61_STR_KW:
			{
				{
				obj=character_str_type();
				}
				break;
			}
			case CHOICE_KW:
			{
				{
				obj=choice_type();
				}
				break;
			}
			case EMBEDDED_KW:
			{
				{
				obj=embedded_type();
				}
				match(EMBEDDED_KW);
				match(PDV_KW);
				break;
			}
			case ENUMERATED_KW:
			{
				{
				obj=enum_type();
				}
				break;
			}
			case EXTERNAL_KW:
			{
				{
				obj=external_type();
				}
				break;
			}
			case INTEGER_KW:
			{
				{
				obj=integer_type();
				}
				break;
			}
			case NULL_KW:
			{
				{
				obj=null_type();
				}
				break;
			}
			case OBJECT_KW:
			{
				{
				obj=object_identifier_type();
				}
				break;
			}
			case OCTET_KW:
			{
				{
				obj=octetString_type();
				}
				break;
			}
			case REAL_KW:
			{
				{
				obj=real_type();
				}
				break;
			}
			case RELATIVE_KW:
			{
				{
				obj=relativeOid_type();
				}
				break;
			}
			case L_BRACKET:
			{
				{
				obj=tagged_type();
				}
				break;
			}
			default:
				if ((LA(1)==SEQUENCE_KW) && (LA(2)==L_BRACE)) {
					{
					obj=sequence_type();
					}
				}
				else if ((LA(1)==SEQUENCE_KW) && (LA(2)==OF_KW||LA(2)==SIZE_KW||LA(2)==L_PAREN)) {
					{
					obj=sequenceof_type();
					}
				}
				else if ((LA(1)==SET_KW) && (LA(2)==L_BRACE)) {
					{
					obj=set_type();
					}
				}
				else if ((LA(1)==SET_KW) && (LA(2)==OF_KW||LA(2)==SIZE_KW||LA(2)==L_PAREN)) {
					{
					obj=setof_type();
					}
				}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  defined_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  up = null;
		Token  up1 = null;
		Token  up2 = null;
		Token  oup = null;
		Token  olo = null;
		AsnDefinedType deftype = new AsnDefinedType();
		AsnConstraint cnstrnt; obj = null;
		
		try {      // for error handling
			{
			{
			if ((LA(1)==UPPER) && (LA(2)==DOT)) {
				up = LT(1);
				match(UPPER);
				if ( inputState.guessing==0 ) {
					deftype.isModuleReference = true ;deftype.moduleReference = up.getText();
				}
				match(DOT);
			}
			else if ((LA(1)==UPPER||LA(1)==OJECT_UPPER||LA(1)==OJECT_LOWER) && (_tokenSet_25.member(LA(2)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			switch ( LA(1)) {
			case OJECT_UPPER:
			{
				{
				oup = LT(1);
				match(OJECT_UPPER);
				if ( inputState.guessing==0 ) {
					deftype.typeReference = oup.getText();
				}
				{
				int _cnt250=0;
				_loop250:
				do {
					if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
						cnstrnt=constraint();
						if ( inputState.guessing==0 ) {
							deftype.constraint = cnstrnt;
						}
					}
					else {
						if ( _cnt250>=1 ) { break _loop250; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt250++;
				} while (true);
				}
				}
				break;
			}
			case OJECT_LOWER:
			{
				{
				olo = LT(1);
				match(OJECT_LOWER);
				if ( inputState.guessing==0 ) {
					deftype.typeReference = olo.getText();
				}
				{
				int _cnt253=0;
				_loop253:
				do {
					if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
						cnstrnt=constraint();
						if ( inputState.guessing==0 ) {
							deftype.constraint = cnstrnt;
						}
					}
					else {
						if ( _cnt253>=1 ) { break _loop253; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt253++;
				} while (true);
				}
				}
				break;
			}
			default:
				boolean synPredMatched242 = false;
				if (((LA(1)==UPPER) && (LA(2)==L_PAREN) && (_tokenSet_32.member(LA(3))))) {
					int _m242 = mark();
					synPredMatched242 = true;
					inputState.guessing++;
					try {
						{
						match(UPPER);
						constraint();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched242 = false;
					}
					rewind(_m242);
inputState.guessing--;
				}
				if ( synPredMatched242 ) {
					{
					{
					up1 = LT(1);
					match(UPPER);
					if ( inputState.guessing==0 ) {
						deftype.typeReference = up1.getText();
					}
					}
					{
					int _cnt246=0;
					_loop246:
					do {
						if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
							cnstrnt=constraint();
							if ( inputState.guessing==0 ) {
								deftype.constraint = cnstrnt;
							}
						}
						else {
							if ( _cnt246>=1 ) { break _loop246; } else {throw new NoViableAltException(LT(1), getFilename());}
						}
						
						_cnt246++;
					} while (true);
					}
					}
				}
				else if ((LA(1)==UPPER) && (_tokenSet_25.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
					{
					up2 = LT(1);
					match(UPPER);
					if ( inputState.guessing==0 ) {
						deftype.typeReference = up2.getText();
					}
					}
				}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj = deftype; deftype=null ; cnstrnt = null; obj.setToken(up1);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  selection_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  lid = null;
		AsnSelectionType seltype = new AsnSelectionType();
		obj = null;AsnType obj1;
		
		try {      // for error handling
			{
			{
			lid = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				seltype.selectionID = lid.getText();
			}
			}
			match(LESS);
			{
			obj1=type();
			if ( inputState.guessing==0 ) {
				seltype.type = obj1;
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj=seltype; seltype=null; obj.setToken(lid);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  macros_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		obj = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case OPERATION_KW:
			{
				{
				obj=operation_macro();
				}
				break;
			}
			case ERROR_KW:
			{
				{
				obj=error_macro();
				}
				break;
			}
			case 153:
			{
				{
				obj=objecttype_macro();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  any_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		Token  lid = null;
		obj = null;AsnAny an = new AsnAny();
		
		try {      // for error handling
			{
			t = LT(1);
			match(ANY_KW);
			{
			switch ( LA(1)) {
			case DEFINED_KW:
			{
				match(DEFINED_KW);
				match(BY_KW);
				if ( inputState.guessing==0 ) {
					an.isDefinedBy = true ;
				}
				lid = LT(1);
				match(LOWER);
				if ( inputState.guessing==0 ) {
					an.definedByType = lid.getText();
				}
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case DEFAULT_KW:
			case EMBEDDED_KW:
			case END_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case ERRORS_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case INTERSECTION_KW:
			case ISO646_STR_KW:
			case LINKED_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case OPTIONAL_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case RESULT_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNION_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case ASSIGN_OP:
			case BAR:
			case COLON:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case R_BRACE:
			case R_PAREN:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case LITERAL_ACCESS:
			case EXCEPT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj = an ;  an = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  bit_string_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnBitString bstr = new AsnBitString();
		AsnNamedNumberList nnlst ; AsnConstraint cnstrnt;obj = null;
		
		try {      // for error handling
			{
			t = LT(1);
			match(BIT_KW);
			match(STRING_KW);
			{
			boolean synPredMatched130 = false;
			if (((LA(1)==L_BRACE) && (LA(2)==ELLIPSIS||LA(2)==LOWER) && (LA(3)==COMMA||LA(3)==L_PAREN||LA(3)==R_BRACE))) {
				int _m130 = mark();
				synPredMatched130 = true;
				inputState.guessing++;
				try {
					{
					namedNumber_list();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched130 = false;
				}
				rewind(_m130);
inputState.guessing--;
			}
			if ( synPredMatched130 ) {
				{
				nnlst=namedNumber_list();
				if ( inputState.guessing==0 ) {
					bstr.namedNumberList = nnlst;
				}
				}
			}
			else if ((_tokenSet_25.member(LA(1))) && (_tokenSet_34.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			boolean synPredMatched134 = false;
			if (((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3))))) {
				int _m134 = mark();
				synPredMatched134 = true;
				inputState.guessing++;
				try {
					{
					constraint();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched134 = false;
				}
				rewind(_m134);
inputState.guessing--;
			}
			if ( synPredMatched134 ) {
				{
				int _cnt136=0;
				_loop136:
				do {
					if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
						cnstrnt=constraint();
						if ( inputState.guessing==0 ) {
							bstr.constraint = cnstrnt;
						}
					}
					else {
						if ( _cnt136>=1 ) { break _loop136; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt136++;
				} while (true);
				}
			}
			else if ((_tokenSet_25.member(LA(1))) && (_tokenSet_34.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			}
			if ( inputState.guessing==0 ) {
				obj=bstr; nnlst = null ; cnstrnt = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  boolean_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		obj = null;
		
		try {      // for error handling
			t = LT(1);
			match(BOOLEAN_KW);
			if ( inputState.guessing==0 ) {
				obj = new AsnBoolean(); obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  character_str_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnCharacterString cstr = new AsnCharacterString();
		String s ; AsnConstraint cnstrnt; obj = null;
		
		try {      // for error handling
			{
			if ((LA(1)==CHARACTER_KW)) {
				{
				t = LT(1);
				match(CHARACTER_KW);
				match(STRING_KW);
				if ( inputState.guessing==0 ) {
					cstr.isUCSType = true;
				}
				}
				{
				_loop141:
				do {
					if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
						cnstrnt=constraint();
						if ( inputState.guessing==0 ) {
							cstr.constraint = cnstrnt;
						}
					}
					else {
						break _loop141;
					}
					
				} while (true);
				}
			}
			else {
				boolean synPredMatched143 = false;
				if (((_tokenSet_35.member(LA(1))) && (LA(2)==L_PAREN) && (_tokenSet_32.member(LA(3))))) {
					int _m143 = mark();
					synPredMatched143 = true;
					inputState.guessing++;
					try {
						{
						character_set();
						constraint();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched143 = false;
					}
					rewind(_m143);
inputState.guessing--;
				}
				if ( synPredMatched143 ) {
					{
					s=character_set();
					if ( inputState.guessing==0 ) {
						cstr.stringtype = s;
					}
					{
					int _cnt146=0;
					_loop146:
					do {
						if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
							cnstrnt=constraint();
							if ( inputState.guessing==0 ) {
								cstr.constraint = cnstrnt;
							}
						}
						else {
							if ( _cnt146>=1 ) { break _loop146; } else {throw new NoViableAltException(LT(1), getFilename());}
						}
						
						_cnt146++;
					} while (true);
					}
					}
				}
				else if ((_tokenSet_35.member(LA(1))) && (_tokenSet_25.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
					{
					s=character_set();
					if ( inputState.guessing==0 ) {
						cstr.stringtype = s;
					}
					}
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				if ( inputState.guessing==0 ) {
					obj = cstr; cnstrnt = null; cstr = null; obj.setToken(t);
				}
			}
			catch (RecognitionException ex) {
				if (inputState.guessing==0) {
					reportError(ex);
					recover(ex,_tokenSet_25);
				} else {
				  throw ex;
				}
			}
			return obj;
		}
		
	public final AsnType  choice_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnChoice ch = new AsnChoice(); AsnElementTypeList eltplst ;
		obj = null;
		
		try {      // for error handling
			{
			t = LT(1);
			match(CHOICE_KW);
			match(L_BRACE);
			{
			eltplst=elementType_list();
			if ( inputState.guessing==0 ) {
				ch.elementTypeList = eltplst ;
			}
			}
			match(R_BRACE);
			}
			if ( inputState.guessing==0 ) {
				obj = ch; eltplst = null; ch = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  embedded_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		obj = null;
		
		try {      // for error handling
			{
			t = LT(1);
			match(EMBEDDED_KW);
			match(PDV_KW);
			}
			if ( inputState.guessing==0 ) {
				obj = new AsnEmbedded(); obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_36);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  enum_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnEnum enumtyp = new AsnEnum() ;
		AsnNamedNumberList nnlst; obj = null;
		
		try {      // for error handling
			{
			t = LT(1);
			match(ENUMERATED_KW);
			{
			nnlst=namedNumber_list();
			if ( inputState.guessing==0 ) {
				enumtyp.namedNumberList = nnlst;
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj = enumtyp ; enumtyp=null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  external_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		obj = null;
		
		try {      // for error handling
			t = LT(1);
			match(EXTERNAL_KW);
			if ( inputState.guessing==0 ) {
				obj = new AsnExternal(); obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  integer_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnInteger intgr = new AsnInteger();
		AsnNamedNumberList numlst; AsnConstraint cnstrnt; obj=null;
		
		try {      // for error handling
			{
			t = LT(1);
			match(INTEGER_KW);
			{
			boolean synPredMatched178 = false;
			if (((LA(1)==L_BRACE) && (LA(2)==ELLIPSIS||LA(2)==LOWER) && (LA(3)==COMMA||LA(3)==L_PAREN||LA(3)==R_BRACE))) {
				int _m178 = mark();
				synPredMatched178 = true;
				inputState.guessing++;
				try {
					{
					namedNumber_list();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched178 = false;
				}
				rewind(_m178);
inputState.guessing--;
			}
			if ( synPredMatched178 ) {
				{
				numlst=namedNumber_list();
				if ( inputState.guessing==0 ) {
					intgr.namedNumberList = numlst;
				}
				}
			}
			else if ((_tokenSet_25.member(LA(1))) && (_tokenSet_34.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			boolean synPredMatched182 = false;
			if (((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3))))) {
				int _m182 = mark();
				synPredMatched182 = true;
				inputState.guessing++;
				try {
					{
					constraint();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched182 = false;
				}
				rewind(_m182);
inputState.guessing--;
			}
			if ( synPredMatched182 ) {
				{
				int _cnt184=0;
				_loop184:
				do {
					if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
						cnstrnt=constraint();
						if ( inputState.guessing==0 ) {
							intgr.constraint = cnstrnt;
						}
					}
					else {
						if ( _cnt184>=1 ) { break _loop184; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt184++;
				} while (true);
				}
			}
			else if ((_tokenSet_25.member(LA(1))) && (_tokenSet_34.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			}
			if ( inputState.guessing==0 ) {
				obj = intgr ; numlst = null ; cnstrnt = null; intgr = null;  obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  null_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnNull nll = new AsnNull(); obj = null;
		
		try {      // for error handling
			t = LT(1);
			match(NULL_KW);
			if ( inputState.guessing==0 ) {
				obj = nll; nll = null ; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  object_identifier_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnObjectIdentifier objident = new AsnObjectIdentifier(); obj = null;
		
		try {      // for error handling
			t = LT(1);
			match(OBJECT_KW);
			match(IDENTIFIER_KW);
			if ( inputState.guessing==0 ) {
				obj = objident; objident = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  octetString_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t1 = null;
		Token  t2 = null;
		AsnOctetString oct = new AsnOctetString(); AsnConstraint cnstrnt ; obj = null;
		
		try {      // for error handling
			{
			boolean synPredMatched190 = false;
			if (((LA(1)==OCTET_KW) && (LA(2)==STRING_KW) && (LA(3)==L_PAREN))) {
				int _m190 = mark();
				synPredMatched190 = true;
				inputState.guessing++;
				try {
					{
					match(OCTET_KW);
					match(STRING_KW);
					constraint();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched190 = false;
				}
				rewind(_m190);
inputState.guessing--;
			}
			if ( synPredMatched190 ) {
				{
				t1 = LT(1);
				match(OCTET_KW);
				match(STRING_KW);
				{
				int _cnt193=0;
				_loop193:
				do {
					if ((LA(1)==L_PAREN) && (_tokenSet_32.member(LA(2))) && (_tokenSet_33.member(LA(3)))) {
						cnstrnt=constraint();
						if ( inputState.guessing==0 ) {
							oct.constraint = cnstrnt;  oct.setToken(t1);
						}
					}
					else {
						if ( _cnt193>=1 ) { break _loop193; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt193++;
				} while (true);
				}
				}
			}
			else if ((LA(1)==OCTET_KW) && (LA(2)==STRING_KW) && (_tokenSet_25.member(LA(3)))) {
				t2 = LT(1);
				match(OCTET_KW);
				match(STRING_KW);
				if ( inputState.guessing==0 ) {
					oct.setToken(t2);
				}
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			if ( inputState.guessing==0 ) {
				obj = oct ; cnstrnt = null;
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  real_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnReal rl = new AsnReal();obj = null;
		
		try {      // for error handling
			t = LT(1);
			match(REAL_KW);
			if ( inputState.guessing==0 ) {
				obj = rl ; rl = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  relativeOid_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		obj = null;
		
		try {      // for error handling
			t = LT(1);
			match(RELATIVE_KW);
			match(MINUS);
			match(OID_KW);
			if ( inputState.guessing==0 ) {
				obj = new AsnRelativeOid(); obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  sequence_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnSequenceSet seq = new AsnSequenceSet();
		AsnElementTypeList eltplist ; obj = null;
		
		try {      // for error handling
			{
			t = LT(1);
			match(SEQUENCE_KW);
			if ( inputState.guessing==0 ) {
				seq.isSequence = true;
			}
			match(L_BRACE);
			{
			switch ( LA(1)) {
			case COMPONENTS_KW:
			case ELLIPSIS:
			case LOWER:
			{
				eltplist=elementType_list();
				if ( inputState.guessing==0 ) {
					seq.elementTypeList = eltplist;
				}
				break;
			}
			case R_BRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(R_BRACE);
			}
			if ( inputState.guessing==0 ) {
				obj = seq ; eltplist = null; seq =null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  sequenceof_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnSequenceOf seqof = new AsnSequenceOf();
		AsnConstraint cns; obj = null; IASN obj1 ; String s ;
		
		try {      // for error handling
			{
			t = LT(1);
			match(SEQUENCE_KW);
			if ( inputState.guessing==0 ) {
				seqof.isSequenceOf = true;
			}
			{
			switch ( LA(1)) {
			case L_PAREN:
			{
				{
				match(L_PAREN);
				{
				match(SIZE_KW);
				if ( inputState.guessing==0 ) {
					seqof.isSizeConstraint=true;
				}
				}
				{
				cns=constraint();
				if ( inputState.guessing==0 ) {
					seqof.constraint = cns ;
				}
				}
				match(R_PAREN);
				}
				break;
			}
			case SIZE_KW:
			{
				{
				{
				match(SIZE_KW);
				if ( inputState.guessing==0 ) {
					seqof.isSizeConstraint=true;
				}
				}
				{
				cns=constraint();
				if ( inputState.guessing==0 ) {
					seqof.constraint = cns ;
				}
				}
				}
				break;
			}
			case OF_KW:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(OF_KW);
			{
			obj1=type();
			if ( inputState.guessing==0 ) {
					if((AsnDefinedType.class).isInstance(obj1)){
						  		seqof.isDefinedType=true;
								seqof.typeName = ((AsnDefinedType)obj1).typeReference ;
							}
							else{
								seqof.typeReference = obj1 ;
							}
						
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj = seqof;  cns = null; seqof=null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  set_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnSequenceSet set = new AsnSequenceSet();
		AsnElementTypeList eltplist ;obj = null;
		
		try {      // for error handling
			{
			t = LT(1);
			match(SET_KW);
			match(L_BRACE);
			{
			switch ( LA(1)) {
			case COMPONENTS_KW:
			case ELLIPSIS:
			case LOWER:
			{
				eltplist=elementType_list();
				if ( inputState.guessing==0 ) {
					set.elementTypeList = eltplist ;
				}
				break;
			}
			case R_BRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(R_BRACE);
			}
			if ( inputState.guessing==0 ) {
				obj = set ; eltplist = null; set = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  setof_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		AsnSequenceOf setof = new AsnSequenceOf();
		AsnConstraint cns; obj = null;
		IASN obj1 ; String s;
		
		try {      // for error handling
			{
			t = LT(1);
			match(SET_KW);
			{
			switch ( LA(1)) {
			case SIZE_KW:
			{
				{
				match(SIZE_KW);
				if ( inputState.guessing==0 ) {
					setof.isSizeConstraint=true;
				}
				{
				cns=constraint();
				if ( inputState.guessing==0 ) {
					setof.constraint = cns ;
				}
				}
				}
				break;
			}
			case L_PAREN:
			{
				{
				match(L_PAREN);
				match(SIZE_KW);
				if ( inputState.guessing==0 ) {
					setof.isSizeConstraint=true;
				}
				{
				cns=constraint();
				if ( inputState.guessing==0 ) {
					setof.constraint = cns ;
				}
				}
				match(R_PAREN);
				}
				break;
			}
			case OF_KW:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(OF_KW);
			{
			obj1=type();
			if ( inputState.guessing==0 ) {
					if((AsnDefinedType.class).isInstance(obj1)){
						  		setof.isDefinedType=true;
								setof.typeName = ((AsnDefinedType)obj1).typeReference ;
							}
							else{
								setof.typeReference = obj1;
							}
						
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj = setof; cns = null; obj1=null; setof=null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  tagged_type() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		AsnTaggedType tgtyp = new AsnTaggedType();
		AsnTag tg; AsnType obj1 = null; String s; obj = null;
		
		try {      // for error handling
			{
			{
			tg=tag();
			if ( inputState.guessing==0 ) {
				tgtyp.tag = tg ;
			}
			}
			{
			switch ( LA(1)) {
			case AUTOMATIC_KW:
			case EXPLICIT_KW:
			case IMPLICIT_KW:
			{
				s=tag_default();
				if ( inputState.guessing==0 ) {
					tgtyp.tagDefault = s ;
				}
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case EMBEDDED_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case EXTERNAL_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case ISO646_STR_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case L_BRACKET:
			case UPPER:
			case LOWER:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			obj1=type();
			if ( inputState.guessing==0 ) {
					if((AsnDefinedType.class).isInstance(obj1)){
						  		tgtyp.isDefinedType=true;
								tgtyp.typeName = ((AsnDefinedType)obj1).typeReference ;
							}
							else{
								tgtyp.typeReference = obj1;
							}
						
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj = tgtyp ; tg = null; obj1= null ;tgtyp = null;
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnNamedNumberList  namedNumber_list() throws RecognitionException, TokenStreamException {
		AsnNamedNumberList nnlist;
		
		nnlist = new AsnNamedNumberList();AsnNamedNumber nnum ;
		
		try {      // for error handling
			{
			match(L_BRACE);
			{
			nnum=namedNumber();
			if ( inputState.guessing==0 ) {
				nnlist.addNamedNumber(nnum, getFilename());
			}
			}
			{
			_loop369:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					{
					nnum=namedNumber();
					if ( inputState.guessing==0 ) {
						nnlist.addNamedNumber(nnum, getFilename());
					}
					}
				}
				else {
					break _loop369;
				}
				
			} while (true);
			}
			match(R_BRACE);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return nnlist;
	}
	
	public final AsnConstraint  constraint() throws RecognitionException, TokenStreamException {
		AsnConstraint cnstrnt;
		
		Token  t = null;
		cnstrnt=new AsnConstraint();
		
		try {      // for error handling
			t = LT(1);
			match(L_PAREN);
			{
			switch ( LA(1)) {
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case EMBEDDED_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case FROM_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case ISO646_STR_KW:
			case MINUS_INFINITY_KW:
			case MIN_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case PATTERN_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case SIZE_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case WITH_KW:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case NUMBER:
			case UPPER:
			case LOWER:
			case AT_LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case ALL:
			case INCLUDES:
			{
				element_set_specs(cnstrnt);
				if ( inputState.guessing==0 ) {
					cnstrnt.isElementSetSpecs=true;
				}
				break;
			}
			case EXCLAMATION:
			case R_PAREN:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case EXCLAMATION:
			{
				exception_spec(cnstrnt);
				break;
			}
			case R_PAREN:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(R_PAREN);
			if ( inputState.guessing==0 ) {
				cnstrnt.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_37);
			} else {
			  throw ex;
			}
		}
		return cnstrnt;
	}
	
	public final String  character_set() throws RecognitionException, TokenStreamException {
		String s;
		
		Token  s1 = null;
		Token  s2 = null;
		Token  s3 = null;
		Token  s4 = null;
		Token  s5 = null;
		Token  s6 = null;
		Token  s7 = null;
		Token  s8 = null;
		Token  s9 = null;
		Token  s10 = null;
		Token  s11 = null;
		Token  s12 = null;
		Token  s13 = null;
		Token  s14 = null;
		Token  s15 = null;
		s = "";
		
		try {      // for error handling
			switch ( LA(1)) {
			case BMP_STR_KW:
			{
				{
				s1 = LT(1);
				match(BMP_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s1.getText();
				}
				}
				break;
			}
			case GENERALIZED_TIME_KW:
			{
				{
				s2 = LT(1);
				match(GENERALIZED_TIME_KW);
				if ( inputState.guessing==0 ) {
					s = s2.getText();
				}
				}
				break;
			}
			case GENERAL_STR_KW:
			{
				{
				s3 = LT(1);
				match(GENERAL_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s3.getText();
				}
				}
				break;
			}
			case GRAPHIC_STR_KW:
			{
				{
				s4 = LT(1);
				match(GRAPHIC_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s4.getText();
				}
				}
				break;
			}
			case IA5_STR_KW:
			{
				{
				s5 = LT(1);
				match(IA5_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s5.getText();
				}
				}
				break;
			}
			case ISO646_STR_KW:
			{
				{
				s6 = LT(1);
				match(ISO646_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s6.getText();
				}
				}
				break;
			}
			case NUMERIC_STR_KW:
			{
				{
				s7 = LT(1);
				match(NUMERIC_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s7.getText();
				}
				}
				break;
			}
			case PRINTABLE_STR_KW:
			{
				{
				s8 = LT(1);
				match(PRINTABLE_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s8.getText();
				}
				}
				break;
			}
			case TELETEX_STR_KW:
			{
				{
				s9 = LT(1);
				match(TELETEX_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s9.getText();
				}
				}
				break;
			}
			case T61_STR_KW:
			{
				{
				s10 = LT(1);
				match(T61_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s10.getText();
				}
				}
				break;
			}
			case UNIVERSAL_STR_KW:
			{
				{
				s11 = LT(1);
				match(UNIVERSAL_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s11.getText();
				}
				}
				break;
			}
			case UTF8_STR_KW:
			{
				{
				s12 = LT(1);
				match(UTF8_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s12.getText();
				}
				}
				break;
			}
			case UTC_TIME_KW:
			{
				{
				s13 = LT(1);
				match(UTC_TIME_KW);
				if ( inputState.guessing==0 ) {
					s = s13.getText();
				}
				}
				break;
			}
			case VIDEOTEX_STR_KW:
			{
				{
				s14 = LT(1);
				match(VIDEOTEX_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s14.getText();
				}
				}
				break;
			}
			case VISIBLE_STR_KW:
			{
				{
				s15 = LT(1);
				match(VISIBLE_STR_KW);
				if ( inputState.guessing==0 ) {
					s = s15.getText();
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return s;
	}
	
	public final AsnElementTypeList  elementType_list() throws RecognitionException, TokenStreamException {
		AsnElementTypeList elelist;
		
		elelist = new AsnElementTypeList(); AsnElementType eletyp;
		
		try {      // for error handling
			{
			eletyp=elementType();
			if ( inputState.guessing==0 ) {
				elelist.addElementType(eletyp, getFilename());
			}
			{
			_loop336:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					{
					eletyp=elementType();
					if ( inputState.guessing==0 ) {
						elelist.addElementType(eletyp, getFilename());
					}
					}
				}
				else {
					break _loop336;
				}
				
			} while (true);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
		return elelist;
	}
	
	public final AsnTag  tag() throws RecognitionException, TokenStreamException {
		AsnTag tg;
		
		Token  t = null;
		tg = new AsnTag(); String s; AsnClassNumber cnum;
		
		try {      // for error handling
			{
			t = LT(1);
			match(L_BRACKET);
			{
			switch ( LA(1)) {
			case APPLICATION_KW:
			case PRIVATE_KW:
			case UNIVERSAL_KW:
			{
				s=clazz();
				if ( inputState.guessing==0 ) {
					tg.clazz = s ;
				}
				break;
			}
			case NUMBER:
			case LOWER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			cnum=class_NUMBER();
			if ( inputState.guessing==0 ) {
				tg.classNumber = cnum ;
			}
			}
			match(R_BRACKET);
			}
			if ( inputState.guessing==0 ) {
				tg.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_39);
			} else {
			  throw ex;
			}
		}
		return tg;
	}
	
	public final String  clazz() throws RecognitionException, TokenStreamException {
		String s;
		
		Token  c1 = null;
		Token  c2 = null;
		Token  c3 = null;
		s = "";
		
		try {      // for error handling
			switch ( LA(1)) {
			case UNIVERSAL_KW:
			{
				{
				c1 = LT(1);
				match(UNIVERSAL_KW);
				if ( inputState.guessing==0 ) {
					s= c1.getText();
				}
				}
				break;
			}
			case APPLICATION_KW:
			{
				{
				c2 = LT(1);
				match(APPLICATION_KW);
				if ( inputState.guessing==0 ) {
					s= c2.getText();
				}
				}
				break;
			}
			case PRIVATE_KW:
			{
				{
				c3 = LT(1);
				match(PRIVATE_KW);
				if ( inputState.guessing==0 ) {
					s= c3.getText();
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_40);
			} else {
			  throw ex;
			}
		}
		return s;
	}
	
	public final AsnClassNumber  class_NUMBER() throws RecognitionException, TokenStreamException {
		AsnClassNumber cnum;
		
		Token  num = null;
		Token  lid = null;
		cnum = new AsnClassNumber() ; String s;
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case NUMBER:
			{
				{
				num = LT(1);
				match(NUMBER);
				if ( inputState.guessing==0 ) {
					s=num.getText(); cnum.num = new Integer(s); cnum.setToken(num);
				}
				}
				break;
			}
			case LOWER:
			{
				{
				lid = LT(1);
				match(LOWER);
				if ( inputState.guessing==0 ) {
					s=lid.getText(); cnum.name = s ; cnum.setToken(lid);
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_41);
			} else {
			  throw ex;
			}
		}
		return cnum;
	}
	
	public final AsnType  operation_macro() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		Token  ld1 = null;
		Token  ld2 = null;
		OperationMacro op = new OperationMacro();
		String s ;obj = null; AsnType obj1; AsnType obj2;
		
		try {      // for error handling
			{
			t = LT(1);
			match(OPERATION_KW);
			{
			switch ( LA(1)) {
			case ARGUMENT_KW:
			{
				match(ARGUMENT_KW);
				{
				boolean synPredMatched267 = false;
				if (((LA(1)==LOWER) && (_tokenSet_12.member(LA(2))) && (_tokenSet_42.member(LA(3))))) {
					int _m267 = mark();
					synPredMatched267 = true;
					inputState.guessing++;
					try {
						{
						match(LOWER);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched267 = false;
					}
					rewind(_m267);
inputState.guessing--;
				}
				if ( synPredMatched267 ) {
					{
					ld1 = LT(1);
					match(LOWER);
					if ( inputState.guessing==0 ) {
						op.argumentTypeIdentifier = ld1.getText();
					}
					}
				}
				else if ((_tokenSet_12.member(LA(1))) && (_tokenSet_42.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				{
				{
				obj2=type();
				}
				if ( inputState.guessing==0 ) {
					op.argumentType = obj2; op.isArgumentName=true;
									if((AsnDefinedType.class).isInstance(obj2))
										op.argumentName = ((AsnDefinedType)obj2).typeReference;
									else
										op.argumentName = op.argumentTypeIdentifier;
								
				}
				}
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case DEFAULT_KW:
			case EMBEDDED_KW:
			case END_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case ERRORS_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case INTERSECTION_KW:
			case ISO646_STR_KW:
			case LINKED_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case OPTIONAL_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case RESULT_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNION_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case ASSIGN_OP:
			case BAR:
			case COLON:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case R_BRACE:
			case R_PAREN:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case LITERAL_ACCESS:
			case EXCEPT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			boolean synPredMatched273 = false;
			if (((LA(1)==RESULT_KW) && (_tokenSet_43.member(LA(2))) && (_tokenSet_42.member(LA(3))))) {
				int _m273 = mark();
				synPredMatched273 = true;
				inputState.guessing++;
				try {
					{
					match(RESULT_KW);
					}
				}
				catch (RecognitionException pe) {
					synPredMatched273 = false;
				}
				rewind(_m273);
inputState.guessing--;
			}
			if ( synPredMatched273 ) {
				{
				match(RESULT_KW);
				if ( inputState.guessing==0 ) {
					op.isResult=true;
				}
				{
				switch ( LA(1)) {
				case SEMI:
				{
					match(SEMI);
					break;
				}
				case ANY_KW:
				case BIT_KW:
				case BMP_STR_KW:
				case BOOLEAN_KW:
				case CHARACTER_KW:
				case CHOICE_KW:
				case EMBEDDED_KW:
				case ENUMERATED_KW:
				case ERROR_KW:
				case EXTERNAL_KW:
				case GENERALIZED_TIME_KW:
				case GENERAL_STR_KW:
				case GRAPHIC_STR_KW:
				case IA5_STR_KW:
				case INTEGER_KW:
				case ISO646_STR_KW:
				case NULL_KW:
				case NUMERIC_STR_KW:
				case OBJECT_KW:
				case OCTET_KW:
				case OPERATION_KW:
				case PRINTABLE_STR_KW:
				case REAL_KW:
				case RELATIVE_KW:
				case SEQUENCE_KW:
				case SET_KW:
				case TELETEX_STR_KW:
				case UNIVERSAL_STR_KW:
				case UTC_TIME_KW:
				case UTF8_STR_KW:
				case VIDEOTEX_STR_KW:
				case VISIBLE_STR_KW:
				case L_BRACKET:
				case UPPER:
				case LOWER:
				case 153:
				case T61_STR_KW:
				case OJECT_UPPER:
				case OJECT_LOWER:
				{
					{
					boolean synPredMatched280 = false;
					if (((LA(1)==LOWER) && (_tokenSet_12.member(LA(2))) && (_tokenSet_42.member(LA(3))))) {
						int _m280 = mark();
						synPredMatched280 = true;
						inputState.guessing++;
						try {
							{
							match(LOWER);
							}
						}
						catch (RecognitionException pe) {
							synPredMatched280 = false;
						}
						rewind(_m280);
inputState.guessing--;
					}
					if ( synPredMatched280 ) {
						{
						ld2 = LT(1);
						match(LOWER);
						if ( inputState.guessing==0 ) {
							op.resultTypeIdentifier = ld2.getText();
						}
						}
					}
					else if ((_tokenSet_12.member(LA(1))) && (_tokenSet_42.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
					}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					
					}
					{
					obj1=type();
					if ( inputState.guessing==0 ) {
						op.resultType=obj1;op.isResultName=true;
										if((AsnDefinedType.class).isInstance(obj1))
											op.resultName = ((AsnDefinedType)obj1).typeReference;
										else
											op.resultName = op.resultTypeIdentifier;
									
					}
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
			}
			else if ((_tokenSet_25.member(LA(1))) && (_tokenSet_34.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			boolean synPredMatched285 = false;
			if (((LA(1)==ERRORS_KW) && (LA(2)==L_BRACE) && (_tokenSet_44.member(LA(3))))) {
				int _m285 = mark();
				synPredMatched285 = true;
				inputState.guessing++;
				try {
					{
					match(ERRORS_KW);
					match(L_BRACE);
					}
				}
				catch (RecognitionException pe) {
					synPredMatched285 = false;
				}
				rewind(_m285);
inputState.guessing--;
			}
			if ( synPredMatched285 ) {
				{
				match(ERRORS_KW);
				match(L_BRACE);
				{
				switch ( LA(1)) {
				case ANY_KW:
				case BIT_KW:
				case BMP_STR_KW:
				case BOOLEAN_KW:
				case CHARACTER_KW:
				case CHOICE_KW:
				case EMBEDDED_KW:
				case ENUMERATED_KW:
				case ERROR_KW:
				case EXTERNAL_KW:
				case FALSE_KW:
				case GENERALIZED_TIME_KW:
				case GENERAL_STR_KW:
				case GRAPHIC_STR_KW:
				case IA5_STR_KW:
				case INTEGER_KW:
				case ISO646_STR_KW:
				case MINUS_INFINITY_KW:
				case NULL_KW:
				case NUMERIC_STR_KW:
				case OBJECT_KW:
				case OCTET_KW:
				case OPERATION_KW:
				case PLUS_INFINITY_KW:
				case PRINTABLE_STR_KW:
				case REAL_KW:
				case RELATIVE_KW:
				case SEQUENCE_KW:
				case SET_KW:
				case TELETEX_STR_KW:
				case TRUE_KW:
				case UNIVERSAL_STR_KW:
				case UTC_TIME_KW:
				case UTF8_STR_KW:
				case VIDEOTEX_STR_KW:
				case VISIBLE_STR_KW:
				case L_BRACE:
				case L_BRACKET:
				case MINUS:
				case NUMBER:
				case UPPER:
				case LOWER:
				case B_STRING:
				case H_STRING:
				case C_STRING:
				case 153:
				case T61_STR_KW:
				case OJECT_UPPER:
				case OJECT_LOWER:
				{
					operation_errorlist(op);
					if ( inputState.guessing==0 ) {
						op.isErrors=true;
					}
					break;
				}
				case R_BRACE:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				match(R_BRACE);
				}
			}
			else if ((_tokenSet_25.member(LA(1))) && (_tokenSet_34.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			boolean synPredMatched290 = false;
			if (((LA(1)==LINKED_KW) && (LA(2)==L_BRACE) && (_tokenSet_44.member(LA(3))))) {
				int _m290 = mark();
				synPredMatched290 = true;
				inputState.guessing++;
				try {
					{
					match(LINKED_KW);
					match(L_BRACE);
					}
				}
				catch (RecognitionException pe) {
					synPredMatched290 = false;
				}
				rewind(_m290);
inputState.guessing--;
			}
			if ( synPredMatched290 ) {
				{
				match(LINKED_KW);
				match(L_BRACE);
				{
				switch ( LA(1)) {
				case ANY_KW:
				case BIT_KW:
				case BMP_STR_KW:
				case BOOLEAN_KW:
				case CHARACTER_KW:
				case CHOICE_KW:
				case EMBEDDED_KW:
				case ENUMERATED_KW:
				case ERROR_KW:
				case EXTERNAL_KW:
				case FALSE_KW:
				case GENERALIZED_TIME_KW:
				case GENERAL_STR_KW:
				case GRAPHIC_STR_KW:
				case IA5_STR_KW:
				case INTEGER_KW:
				case ISO646_STR_KW:
				case MINUS_INFINITY_KW:
				case NULL_KW:
				case NUMERIC_STR_KW:
				case OBJECT_KW:
				case OCTET_KW:
				case OPERATION_KW:
				case PLUS_INFINITY_KW:
				case PRINTABLE_STR_KW:
				case REAL_KW:
				case RELATIVE_KW:
				case SEQUENCE_KW:
				case SET_KW:
				case TELETEX_STR_KW:
				case TRUE_KW:
				case UNIVERSAL_STR_KW:
				case UTC_TIME_KW:
				case UTF8_STR_KW:
				case VIDEOTEX_STR_KW:
				case VISIBLE_STR_KW:
				case L_BRACE:
				case L_BRACKET:
				case MINUS:
				case NUMBER:
				case UPPER:
				case LOWER:
				case B_STRING:
				case H_STRING:
				case C_STRING:
				case 153:
				case T61_STR_KW:
				case OJECT_UPPER:
				case OJECT_LOWER:
				{
					linkedOp_list(op);
					if ( inputState.guessing==0 ) {
						op.isLinkedOperation = true ;
					}
					break;
				}
				case R_BRACE:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				match(R_BRACE);
				}
			}
			else if ((_tokenSet_25.member(LA(1))) && (_tokenSet_34.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			}
			if ( inputState.guessing==0 ) {
				obj = op; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  error_macro() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		Token  lw = null;
		ErrorMacro merr = new ErrorMacro();obj = null;
		AsnType obj1;
		
		try {      // for error handling
			{
			t = LT(1);
			match(ERROR_KW);
			{
			switch ( LA(1)) {
			case PARAMETER_KW:
			{
				match(PARAMETER_KW);
				if ( inputState.guessing==0 ) {
					merr.isParameter = true;
				}
				{
				{
				boolean synPredMatched307 = false;
				if (((LA(1)==LOWER) && (_tokenSet_12.member(LA(2))) && (_tokenSet_42.member(LA(3))))) {
					int _m307 = mark();
					synPredMatched307 = true;
					inputState.guessing++;
					try {
						{
						match(LOWER);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched307 = false;
					}
					rewind(_m307);
inputState.guessing--;
				}
				if ( synPredMatched307 ) {
					{
					lw = LT(1);
					match(LOWER);
					if ( inputState.guessing==0 ) {
						merr.parameterName = lw.getText();
					}
					}
				}
				else if ((_tokenSet_12.member(LA(1))) && (_tokenSet_42.member(LA(2))) && (_tokenSet_34.member(LA(3)))) {
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				{
				obj1=type();
				if ( inputState.guessing==0 ) {
						if((AsnDefinedType.class).isInstance(obj1)){
									merr.isDefinedType=true;
									merr.typeName = ((AsnDefinedType)obj1).typeReference ;
								}
								else{
									merr.typeReference = obj1 ;
								}
							
				}
				}
				}
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case DEFAULT_KW:
			case EMBEDDED_KW:
			case END_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case ERRORS_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case INTERSECTION_KW:
			case ISO646_STR_KW:
			case LINKED_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case OPTIONAL_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case RESULT_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNION_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case ASSIGN_OP:
			case BAR:
			case COLON:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case R_BRACE:
			case R_PAREN:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case LITERAL_ACCESS:
			case EXCEPT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj = merr ; merr = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final AsnType  objecttype_macro() throws RecognitionException, TokenStreamException {
		AsnType obj;
		
		Token  t = null;
		Token  lid = null;
		Token  lid1 = null;
		ObjectType objtype = new ObjectType();
		AsnValue val; obj = null; String s; AsnType typ;
		
		try {      // for error handling
			{
			t = LT(1);
			match(153);
			match(SYNTAX_KW);
			{
			typ=type();
			if ( inputState.guessing==0 ) {
				objtype.type=typ;
			}
			}
			{
			match(LITERAL_ACCESS);
			lid = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				objtype.accessPart = lid.getText();
			}
			}
			{
			match(LITERAL_STATUS);
			lid1 = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				objtype.statusPart = lid1.getText();
			}
			}
			{
			switch ( LA(1)) {
			case LITERAL_DESCRIPTION:
			{
				match(LITERAL_DESCRIPTION);
				match(CHARACTER_KW);
				match(STRING_KW);
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case DEFAULT_KW:
			case EMBEDDED_KW:
			case END_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case ERRORS_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case INTERSECTION_KW:
			case ISO646_STR_KW:
			case LINKED_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case OPTIONAL_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case RESULT_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNION_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case ASSIGN_OP:
			case BAR:
			case COLON:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case R_BRACE:
			case R_PAREN:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case LITERAL_ACCESS:
			case LITERAL_REFERENCE:
			case LITERAL_INDEX:
			case LITERAL_DEFVAL:
			case EXCEPT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case LITERAL_REFERENCE:
			{
				match(LITERAL_REFERENCE);
				match(CHARACTER_KW);
				match(STRING_KW);
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case DEFAULT_KW:
			case EMBEDDED_KW:
			case END_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case ERRORS_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case INTERSECTION_KW:
			case ISO646_STR_KW:
			case LINKED_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case OPTIONAL_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case RESULT_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNION_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case ASSIGN_OP:
			case BAR:
			case COLON:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case R_BRACE:
			case R_PAREN:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case LITERAL_ACCESS:
			case LITERAL_INDEX:
			case LITERAL_DEFVAL:
			case EXCEPT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case LITERAL_INDEX:
			{
				match(LITERAL_INDEX);
				match(L_BRACE);
				{
				typeorvaluelist(objtype);
				}
				match(R_BRACE);
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case DEFAULT_KW:
			case EMBEDDED_KW:
			case END_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case ERRORS_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case INTERSECTION_KW:
			case ISO646_STR_KW:
			case LINKED_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case OPTIONAL_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case RESULT_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNION_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case ASSIGN_OP:
			case BAR:
			case COLON:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case R_BRACE:
			case R_PAREN:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case LITERAL_ACCESS:
			case LITERAL_DEFVAL:
			case EXCEPT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case LITERAL_DEFVAL:
			{
				match(LITERAL_DEFVAL);
				match(L_BRACE);
				{
				val=value();
				if ( inputState.guessing==0 ) {
					objtype.value = val;
				}
				}
				match(R_BRACE);
				break;
			}
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case DEFAULT_KW:
			case EMBEDDED_KW:
			case END_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case ERRORS_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case INTERSECTION_KW:
			case ISO646_STR_KW:
			case LINKED_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case OPTIONAL_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case RESULT_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNION_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case ASSIGN_OP:
			case BAR:
			case COLON:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case R_BRACE:
			case R_PAREN:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case LITERAL_ACCESS:
			case EXCEPT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			}
			if ( inputState.guessing==0 ) {
				obj= objtype; objtype = null; obj.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_25);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final void operation_errorlist(
		OperationMacro oper
	) throws RecognitionException, TokenStreamException {
		
		IASN obj;
		
		try {      // for error handling
			obj=typeorvalue();
			if ( inputState.guessing==0 ) {
				oper.errorList.add(obj);
			}
			{
			_loop296:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					{
					obj=typeorvalue();
					if ( inputState.guessing==0 ) {
						oper.errorList.add(obj);
					}
					}
				}
				else {
					break _loop296;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void linkedOp_list(
		OperationMacro oper
	) throws RecognitionException, TokenStreamException {
		
		IASN obj;
		
		try {      // for error handling
			obj=typeorvalue();
			if ( inputState.guessing==0 ) {
				oper.linkedOpList.add(obj);
			}
			{
			_loop300:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					{
					obj=typeorvalue();
					if ( inputState.guessing==0 ) {
						oper.linkedOpList.add(obj);
					}
					}
				}
				else {
					break _loop300;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
	}
	
	public final IASN  typeorvalue() throws RecognitionException, TokenStreamException {
		IASN obj;
		
		IASN obj1; obj=null;
		
		try {      // for error handling
			{
			boolean synPredMatched330 = false;
			if (((_tokenSet_12.member(LA(1))) && (_tokenSet_45.member(LA(2))) && (_tokenSet_46.member(LA(3))))) {
				int _m330 = mark();
				synPredMatched330 = true;
				inputState.guessing++;
				try {
					{
					type();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched330 = false;
				}
				rewind(_m330);
inputState.guessing--;
			}
			if ( synPredMatched330 ) {
				{
				obj1=type();
				}
			}
			else if ((_tokenSet_47.member(LA(1))) && (_tokenSet_48.member(LA(2))) && (_tokenSet_49.member(LA(3)))) {
				obj1=value();
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			if ( inputState.guessing==0 ) {
				obj = obj1; obj1=null;
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_50);
			} else {
			  throw ex;
			}
		}
		return obj;
	}
	
	public final void typeorvaluelist(
		ObjectType objtype
	) throws RecognitionException, TokenStreamException {
		
		IASN obj;
		
		try {      // for error handling
			{
			{
			obj=typeorvalue();
			if ( inputState.guessing==0 ) {
				objtype.elements.add(obj);
			}
			}
			{
			match(COMMA);
			{
			_loop326:
			do {
				if ((_tokenSet_51.member(LA(1)))) {
					obj=typeorvalue();
					if ( inputState.guessing==0 ) {
						objtype.elements.add(obj);
					}
				}
				else {
					break _loop326;
				}
				
			} while (true);
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
	}
	
	public final AsnElementType  elementType() throws RecognitionException, TokenStreamException {
		AsnElementType eletyp;
		
		Token  lid = null;
		Token  cmp = null;
		eletyp = new AsnElementType();AsnValue val;
		AsnType obj = null; AsnTag tg; String s;
		
		try {      // for error handling
			switch ( LA(1)) {
			case COMPONENTS_KW:
			case LOWER:
			{
				{
				switch ( LA(1)) {
				case LOWER:
				{
					{
					lid = LT(1);
					match(LOWER);
					if ( inputState.guessing==0 ) {
						eletyp.name = lid.getText(); eletyp.setToken(lid);
					}
					{
					boolean synPredMatched342 = false;
					if (((LA(1)==L_BRACKET) && (_tokenSet_52.member(LA(2))) && (LA(3)==R_BRACKET||LA(3)==NUMBER||LA(3)==LOWER))) {
						int _m342 = mark();
						synPredMatched342 = true;
						inputState.guessing++;
						try {
							{
							tag();
							}
						}
						catch (RecognitionException pe) {
							synPredMatched342 = false;
						}
						rewind(_m342);
inputState.guessing--;
					}
					if ( synPredMatched342 ) {
						{
						tg=tag();
						if ( inputState.guessing==0 ) {
							eletyp.isTag = true; eletyp.tag = tg ;
						}
						}
					}
					else if ((_tokenSet_39.member(LA(1))) && (_tokenSet_53.member(LA(2))) && ((LA(3) >= DOT && LA(3) <= INCLUDES))) {
					}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					
					}
					{
					switch ( LA(1)) {
					case AUTOMATIC_KW:
					case EXPLICIT_KW:
					case IMPLICIT_KW:
					{
						s=tag_default();
						if ( inputState.guessing==0 ) {
							eletyp.isTagDefault = true; eletyp.typeTagDefault = s ;
						}
						break;
					}
					case ANY_KW:
					case BIT_KW:
					case BMP_STR_KW:
					case BOOLEAN_KW:
					case CHARACTER_KW:
					case CHOICE_KW:
					case EMBEDDED_KW:
					case ENUMERATED_KW:
					case ERROR_KW:
					case EXTERNAL_KW:
					case GENERALIZED_TIME_KW:
					case GENERAL_STR_KW:
					case GRAPHIC_STR_KW:
					case IA5_STR_KW:
					case INTEGER_KW:
					case ISO646_STR_KW:
					case NULL_KW:
					case NUMERIC_STR_KW:
					case OBJECT_KW:
					case OCTET_KW:
					case OPERATION_KW:
					case PRINTABLE_STR_KW:
					case REAL_KW:
					case RELATIVE_KW:
					case SEQUENCE_KW:
					case SET_KW:
					case TELETEX_STR_KW:
					case UNIVERSAL_STR_KW:
					case UTC_TIME_KW:
					case UTF8_STR_KW:
					case VIDEOTEX_STR_KW:
					case VISIBLE_STR_KW:
					case L_BRACKET:
					case UPPER:
					case LOWER:
					case 153:
					case T61_STR_KW:
					case OJECT_UPPER:
					case OJECT_LOWER:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					if ((LA(1)==UPPER) && (LA(2)==DOT) && (LA(3)==AMP_UPPER||LA(3)==AMP_LOWER)) {
						{
						match(UPPER);
						{
						int _cnt351=0;
						_loop351:
						do {
							if ((LA(1)==DOT)) {
								match(DOT);
								{
								switch ( LA(1)) {
								case AMP_LOWER:
								{
									match(AMP_LOWER);
									break;
								}
								case AMP_UPPER:
								{
									match(AMP_UPPER);
									break;
								}
								default:
								{
									throw new NoViableAltException(LT(1), getFilename());
								}
								}
								}
							}
							else {
								if ( _cnt351>=1 ) { break _loop351; } else {throw new NoViableAltException(LT(1), getFilename());}
							}
							
							_cnt351++;
						} while (true);
						}
						}
					}
					else if ((_tokenSet_12.member(LA(1))) && (_tokenSet_54.member(LA(2))) && ((LA(3) >= DOT && LA(3) <= INCLUDES))) {
						{
						obj=type();
						}
					}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					
					}
					{
					switch ( LA(1)) {
					case L_BRACE:
					{
						ignored_brace_content();
						break;
					}
					case DEFAULT_KW:
					case OPTIONAL_KW:
					case COMMA:
					case L_PAREN:
					case R_BRACE:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					switch ( LA(1)) {
					case L_PAREN:
					{
						ignored_paren_content();
						break;
					}
					case DEFAULT_KW:
					case OPTIONAL_KW:
					case COMMA:
					case R_BRACE:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					switch ( LA(1)) {
					case OPTIONAL_KW:
					{
						{
						match(OPTIONAL_KW);
						if ( inputState.guessing==0 ) {
							eletyp.isOptional=true;
						}
						}
						break;
					}
					case DEFAULT_KW:
					{
						{
						match(DEFAULT_KW);
						if ( inputState.guessing==0 ) {
							eletyp.isDefault = true;
						}
						val=value();
						if ( inputState.guessing==0 ) {
							eletyp.value = val;
						}
						}
						break;
					}
					case COMMA:
					case R_BRACE:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					}
					break;
				}
				case COMPONENTS_KW:
				{
					cmp = LT(1);
					match(COMPONENTS_KW);
					match(OF_KW);
					if ( inputState.guessing==0 ) {
						eletyp.isComponentsOf = true; eletyp.setToken(cmp);
					}
					{
					obj=type();
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				if ( inputState.guessing==0 ) {
					
								if((AsnDefinedType.class).isInstance(obj)){
									eletyp.isDefinedType=true;
									eletyp.typeName = ((AsnDefinedType)obj).typeReference ;
								} else{
									eletyp.typeReference = obj;
								}
							
				}
				break;
			}
			case ELLIPSIS:
			{
				{
				match(ELLIPSIS);
				if ( inputState.guessing==0 ) {
					eletyp.isDotDotDot = true;
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_55);
			} else {
			  throw ex;
			}
		}
		return eletyp;
	}
	
	public final void ignored_brace_content() throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			match(L_BRACE);
			{
			_loop556:
			do {
				// nongreedy exit test
				if ((LA(1)==R_BRACE) && ((LA(2) >= DOT && LA(2) <= INCLUDES)) && (_tokenSet_34.member(LA(3)))) break _loop556;
				boolean synPredMatched555 = false;
				if (((LA(1)==L_BRACE) && ((LA(2) >= DOT && LA(2) <= INCLUDES)) && ((LA(3) >= DOT && LA(3) <= INCLUDES)))) {
					int _m555 = mark();
					synPredMatched555 = true;
					inputState.guessing++;
					try {
						{
						match(L_BRACE);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched555 = false;
					}
					rewind(_m555);
inputState.guessing--;
				}
				if ( synPredMatched555 ) {
					ignored_brace_content();
				}
				else if (((LA(1) >= DOT && LA(1) <= INCLUDES)) && ((LA(2) >= DOT && LA(2) <= INCLUDES)) && ((LA(3) >= DOT && LA(3) <= INCLUDES))) {
					matchNot(EOF);
				}
				else {
					break _loop556;
				}
				
			} while (true);
			}
			match(R_BRACE);
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_56);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void ignored_paren_content() throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			match(L_PAREN);
			{
			_loop561:
			do {
				// nongreedy exit test
				if ((LA(1)==R_PAREN) && ((LA(2) >= DOT && LA(2) <= INCLUDES)) && ((LA(3) >= DOT && LA(3) <= INCLUDES))) break _loop561;
				boolean synPredMatched560 = false;
				if (((LA(1)==L_PAREN) && ((LA(2) >= DOT && LA(2) <= INCLUDES)) && ((LA(3) >= DOT && LA(3) <= INCLUDES)))) {
					int _m560 = mark();
					synPredMatched560 = true;
					inputState.guessing++;
					try {
						{
						match(L_PAREN);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched560 = false;
					}
					rewind(_m560);
inputState.guessing--;
				}
				if ( synPredMatched560 ) {
					ignored_paren_content();
				}
				else if (((LA(1) >= DOT && LA(1) <= INCLUDES)) && ((LA(2) >= DOT && LA(2) <= INCLUDES)) && ((LA(3) >= DOT && LA(3) <= INCLUDES))) {
					matchNot(EOF);
				}
				else {
					break _loop561;
				}
				
			} while (true);
			}
			match(R_PAREN);
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_56);
			} else {
			  throw ex;
			}
		}
	}
	
	public final AsnNamedNumber  namedNumber() throws RecognitionException, TokenStreamException {
		AsnNamedNumber nnum;
		
		Token  lid = null;
		nnum = new AsnNamedNumber() ;AsnSignedNumber i;
		AsnDefinedValue s;	
		
		try {      // for error handling
			switch ( LA(1)) {
			case LOWER:
			{
				{
				lid = LT(1);
				match(LOWER);
				if ( inputState.guessing==0 ) {
					nnum.name = lid.getText(); nnum.setToken(lid);
				}
				{
				switch ( LA(1)) {
				case L_PAREN:
				{
					match(L_PAREN);
					{
					switch ( LA(1)) {
					case MINUS:
					case NUMBER:
					{
						i=signed_number();
						if ( inputState.guessing==0 ) {
							nnum.signedNumber = i;nnum.isSignedNumber=true;
						}
						break;
					}
					case UPPER:
					case LOWER:
					{
						{
						s=defined_value();
						if ( inputState.guessing==0 ) {
							nnum.definedValue=s;
						}
						}
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					match(R_PAREN);
					break;
				}
				case COMMA:
				case R_BRACE:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				break;
			}
			case ELLIPSIS:
			{
				{
				match(ELLIPSIS);
				if ( inputState.guessing==0 ) {
					nnum.isDotDotDot = true;
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_55);
			} else {
			  throw ex;
			}
		}
		return nnum;
	}
	
	public final AsnSignedNumber  signed_number() throws RecognitionException, TokenStreamException {
		AsnSignedNumber i;
		
		Token  n = null;
		i = new AsnSignedNumber() ; String s ;
		
		try {      // for error handling
			{
			{
			switch ( LA(1)) {
			case MINUS:
			{
				match(MINUS);
				if ( inputState.guessing==0 ) {
					i.positive=false;
				}
				break;
			}
			case NUMBER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			n = LT(1);
			match(NUMBER);
			if ( inputState.guessing==0 ) {
				s = n.getText(); i.num= new BigInteger(s); i.setToken(n); i.setToken(n);
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_30);
			} else {
			  throw ex;
			}
		}
		return i;
	}
	
	public final void element_set_specs(
		AsnConstraint cnstrnt
	) throws RecognitionException, TokenStreamException {
		
		ElementSetSpec elemspec;
		
		try {      // for error handling
			{
			elemspec=element_set_spec();
			if ( inputState.guessing==0 ) {
				cnstrnt.elemSetSpec=elemspec;
			}
			{
			if ((LA(1)==COMMA) && (LA(2)==ELLIPSIS)) {
				match(COMMA);
				match(ELLIPSIS);
				if ( inputState.guessing==0 ) {
					cnstrnt.isCommaDotDot=true;
				}
			}
			else if ((LA(1)==COMMA||LA(1)==EXCLAMATION||LA(1)==R_PAREN) && (_tokenSet_57.member(LA(2)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			switch ( LA(1)) {
			case COMMA:
			{
				match(COMMA);
				elemspec=element_set_spec();
				if ( inputState.guessing==0 ) {
					cnstrnt.addElemSetSpec=elemspec;cnstrnt.isAdditionalElementSpec=true;
				}
				break;
			}
			case EXCLAMATION:
			case R_PAREN:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_58);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void exception_spec(
		AsnConstraint cnstrnt
	) throws RecognitionException, TokenStreamException {
		
		Token  t = null;
		AsnSignedNumber signum; AsnDefinedValue defval;
		IASN typ;AsnValue val;
		
		try {      // for error handling
			{
			t = LT(1);
			match(EXCLAMATION);
			{
			boolean synPredMatched383 = false;
			if (((LA(1)==MINUS||LA(1)==NUMBER))) {
				int _m383 = mark();
				synPredMatched383 = true;
				inputState.guessing++;
				try {
					{
					signed_number();
					}
				}
				catch (RecognitionException pe) {
					synPredMatched383 = false;
				}
				rewind(_m383);
inputState.guessing--;
			}
			if ( synPredMatched383 ) {
				{
				signum=signed_number();
				if ( inputState.guessing==0 ) {
					cnstrnt.isSignedNumber=true;cnstrnt.signedNumber=signum;
				}
				}
			}
			else {
				boolean synPredMatched386 = false;
				if (((LA(1)==UPPER||LA(1)==LOWER) && (LA(2)==DOT||LA(2)==R_PAREN) && (_tokenSet_37.member(LA(3))))) {
					int _m386 = mark();
					synPredMatched386 = true;
					inputState.guessing++;
					try {
						{
						defined_value();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched386 = false;
					}
					rewind(_m386);
inputState.guessing--;
				}
				if ( synPredMatched386 ) {
					{
					defval=defined_value();
					if ( inputState.guessing==0 ) {
						cnstrnt.isDefinedValue=true;cnstrnt.definedValue=defval;
					}
					}
				}
				else if ((_tokenSet_12.member(LA(1))) && (_tokenSet_59.member(LA(2))) && (_tokenSet_60.member(LA(3)))) {
					typ=type();
					match(COLON);
					val=value();
					if ( inputState.guessing==0 ) {
						cnstrnt.isColonValue=true;cnstrnt.type=typ;cnstrnt.value=val;
					}
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				if ( inputState.guessing==0 ) {
					cnstrnt.isExceptionSpec=true; cnstrnt.setToken(t);
				}
			}
			catch (RecognitionException ex) {
				if (inputState.guessing==0) {
					reportError(ex);
					recover(ex,_tokenSet_61);
				} else {
				  throw ex;
				}
			}
		}
		
	public final ElementSetSpec  element_set_spec() throws RecognitionException, TokenStreamException {
		ElementSetSpec elemspec;
		
		elemspec = new ElementSetSpec(); Intersection intersect;ConstraintElements cnselem;
		
		try {      // for error handling
			switch ( LA(1)) {
			case ANY_KW:
			case BIT_KW:
			case BMP_STR_KW:
			case BOOLEAN_KW:
			case CHARACTER_KW:
			case CHOICE_KW:
			case EMBEDDED_KW:
			case ENUMERATED_KW:
			case ERROR_KW:
			case EXTERNAL_KW:
			case FALSE_KW:
			case FROM_KW:
			case GENERALIZED_TIME_KW:
			case GENERAL_STR_KW:
			case GRAPHIC_STR_KW:
			case IA5_STR_KW:
			case INTEGER_KW:
			case ISO646_STR_KW:
			case MINUS_INFINITY_KW:
			case MIN_KW:
			case NULL_KW:
			case NUMERIC_STR_KW:
			case OBJECT_KW:
			case OCTET_KW:
			case OPERATION_KW:
			case PATTERN_KW:
			case PLUS_INFINITY_KW:
			case PRINTABLE_STR_KW:
			case REAL_KW:
			case RELATIVE_KW:
			case SEQUENCE_KW:
			case SET_KW:
			case SIZE_KW:
			case TELETEX_STR_KW:
			case TRUE_KW:
			case UNIVERSAL_STR_KW:
			case UTC_TIME_KW:
			case UTF8_STR_KW:
			case VIDEOTEX_STR_KW:
			case VISIBLE_STR_KW:
			case WITH_KW:
			case L_BRACE:
			case L_BRACKET:
			case L_PAREN:
			case MINUS:
			case NUMBER:
			case UPPER:
			case LOWER:
			case AT_LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			case 153:
			case T61_STR_KW:
			case OJECT_UPPER:
			case OJECT_LOWER:
			case INCLUDES:
			{
				intersect=intersections();
				if ( inputState.guessing==0 ) {
					elemspec.intersectionList.add(intersect);
				}
				{
				_loop395:
				do {
					if ((LA(1)==UNION_KW||LA(1)==BAR)) {
						{
						switch ( LA(1)) {
						case BAR:
						{
							match(BAR);
							break;
						}
						case UNION_KW:
						{
							match(UNION_KW);
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						intersect=intersections();
						if ( inputState.guessing==0 ) {
							elemspec.intersectionList.add(intersect);
						}
					}
					else {
						break _loop395;
					}
					
				} while (true);
				}
				break;
			}
			case ALL:
			{
				match(ALL);
				match(EXCEPT);
				cnselem=constraint_elements();
				if ( inputState.guessing==0 ) {
					elemspec.allExceptCnselem=cnselem;elemspec.isAllExcept=true;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_62);
			} else {
			  throw ex;
			}
		}
		return elemspec;
	}
	
	public final Intersection  intersections() throws RecognitionException, TokenStreamException {
		Intersection intersect;
		
		intersect = new Intersection();ConstraintElements cnselem;
		
		try {      // for error handling
			cnselem=constraint_elements();
			if ( inputState.guessing==0 ) {
				intersect.cnsElemList.add(cnselem);
			}
			{
			switch ( LA(1)) {
			case EXCEPT:
			{
				match(EXCEPT);
				if ( inputState.guessing==0 ) {
					intersect.isExcept=true;
				}
				cnselem=constraint_elements();
				if ( inputState.guessing==0 ) {
					intersect.exceptCnsElem.add(cnselem);
				}
				break;
			}
			case INTERSECTION_KW:
			case UNION_KW:
			case BAR:
			case COMMA:
			case EXCLAMATION:
			case INTERSECTION:
			case R_PAREN:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			_loop401:
			do {
				if ((LA(1)==INTERSECTION_KW||LA(1)==INTERSECTION)) {
					{
					switch ( LA(1)) {
					case INTERSECTION:
					{
						match(INTERSECTION);
						break;
					}
					case INTERSECTION_KW:
					{
						match(INTERSECTION_KW);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					if ( inputState.guessing==0 ) {
						intersect.isInterSection=true;
					}
					cnselem=constraint_elements();
					if ( inputState.guessing==0 ) {
						intersect.cnsElemList.add(cnselem);
					}
					{
					switch ( LA(1)) {
					case EXCEPT:
					{
						match(EXCEPT);
						cnselem=constraint_elements();
						if ( inputState.guessing==0 ) {
							intersect.exceptCnsElem.add(cnselem);
						}
						break;
					}
					case INTERSECTION_KW:
					case UNION_KW:
					case BAR:
					case COMMA:
					case EXCLAMATION:
					case INTERSECTION:
					case R_PAREN:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
				}
				else {
					break _loop401;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_63);
			} else {
			  throw ex;
			}
		}
		return intersect;
	}
	
	public final ConstraintElements  constraint_elements() throws RecognitionException, TokenStreamException {
		ConstraintElements cnsElem;
		
		cnsElem = null;
		
		try {      // for error handling
			boolean synPredMatched404 = false;
			if (((LA(1)==L_PAREN) && (_tokenSet_64.member(LA(2))) && (_tokenSet_65.member(LA(3))))) {
				int _m404 = mark();
				synPredMatched404 = true;
				inputState.guessing++;
				try {
					{
					match(L_PAREN);
					cnsElem=constraint_elements_intern();
					match(R_PAREN);
					}
				}
				catch (RecognitionException pe) {
					synPredMatched404 = false;
				}
				rewind(_m404);
inputState.guessing--;
			}
			if ( synPredMatched404 ) {
				{
				match(L_PAREN);
				cnsElem=constraint_elements_intern();
				match(R_PAREN);
				}
			}
			else if ((_tokenSet_64.member(LA(1))) && (_tokenSet_66.member(LA(2))) && (_tokenSet_67.member(LA(3)))) {
				{
				cnsElem=constraint_elements_intern();
				}
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		return cnsElem;
	}
	
	public final ConstraintElements  constraint_elements_intern() throws RecognitionException, TokenStreamException {
		ConstraintElements cnsElem;
		
		cnsElem = new ConstraintElements(); AsnValue val; AsnDefinedValue defVal;
		AsnConstraint cns; ElementSetSpec elespec;IASN typ;
		
		try {      // for error handling
			switch ( LA(1)) {
			case SIZE_KW:
			{
				{
				match(SIZE_KW);
				cns=constraint();
				if ( inputState.guessing==0 ) {
					cnsElem.isSizeConstraint=true;cnsElem.constraint=cns;
				}
				}
				break;
			}
			case FROM_KW:
			{
				{
				match(FROM_KW);
				cns=constraint();
				if ( inputState.guessing==0 ) {
					cnsElem.isAlphabetConstraint=true;cnsElem.constraint=cns;
				}
				}
				break;
			}
			case L_PAREN:
			{
				{
				match(L_PAREN);
				elespec=element_set_spec();
				if ( inputState.guessing==0 ) {
					cnsElem.isElementSetSpec=true;cnsElem.elespec=elespec;
				}
				match(R_PAREN);
				}
				break;
			}
			case PATTERN_KW:
			{
				{
				match(PATTERN_KW);
				val=value();
				if ( inputState.guessing==0 ) {
					cnsElem.isPatternValue=true;cnsElem.value=val;
				}
				}
				break;
			}
			case WITH_KW:
			{
				{
				match(WITH_KW);
				{
				switch ( LA(1)) {
				case COMPONENT_KW:
				{
					{
					match(COMPONENT_KW);
					cns=constraint();
					if ( inputState.guessing==0 ) {
						cnsElem.isWithComponent=true;cnsElem.constraint=cns;
					}
					}
					break;
				}
				case COMPONENTS_KW:
				{
					{
					match(COMPONENTS_KW);
					if ( inputState.guessing==0 ) {
						cnsElem.isWithComponents=true;
					}
					match(L_BRACE);
					{
					switch ( LA(1)) {
					case ELLIPSIS:
					{
						match(ELLIPSIS);
						match(COMMA);
						break;
					}
					case LOWER:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					type_constraint_list(cnsElem);
					match(R_BRACE);
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				break;
			}
			default:
				boolean synPredMatched409 = false;
				if (((_tokenSet_47.member(LA(1))) && (_tokenSet_69.member(LA(2))) && (_tokenSet_70.member(LA(3))))) {
					int _m409 = mark();
					synPredMatched409 = true;
					inputState.guessing++;
					try {
						{
						value();
						}
					}
					catch (RecognitionException pe) {
						synPredMatched409 = false;
					}
					rewind(_m409);
inputState.guessing--;
				}
				if ( synPredMatched409 ) {
					{
					val=value();
					if ( inputState.guessing==0 ) {
						cnsElem.isValue=true;cnsElem.value=val;
					}
					}
				}
				else {
					boolean synPredMatched412 = false;
					if (((LA(1)==UPPER||LA(1)==AT_LOWER) && (_tokenSet_71.member(LA(2))) && (_tokenSet_72.member(LA(3))))) {
						int _m412 = mark();
						synPredMatched412 = true;
						inputState.guessing++;
						try {
							{
							defined_ObjRef();
							}
						}
						catch (RecognitionException pe) {
							synPredMatched412 = false;
						}
						rewind(_m412);
inputState.guessing--;
					}
					if ( synPredMatched412 ) {
						{
						defVal=defined_ObjRef();
						if ( inputState.guessing==0 ) {
							
									cnsElem.isValue=true;
									val = new AsnValue();
									val.isDefinedValue = true;
									val.definedValue = defVal;
									cnsElem.value=val;
								
						}
						}
					}
					else {
						boolean synPredMatched415 = false;
						if (((_tokenSet_73.member(LA(1))) && (_tokenSet_74.member(LA(2))) && (_tokenSet_75.member(LA(3))))) {
							int _m415 = mark();
							synPredMatched415 = true;
							inputState.guessing++;
							try {
								{
								value_range(cnsElem);
								}
							}
							catch (RecognitionException pe) {
								synPredMatched415 = false;
							}
							rewind(_m415);
inputState.guessing--;
						}
						if ( synPredMatched415 ) {
							{
							value_range(cnsElem);
							if ( inputState.guessing==0 ) {
								cnsElem.isValueRange=true;
							}
							}
						}
						else if ((_tokenSet_76.member(LA(1))) && (_tokenSet_77.member(LA(2))) && (_tokenSet_78.member(LA(3)))) {
							{
							{
							switch ( LA(1)) {
							case INCLUDES:
							{
								match(INCLUDES);
								if ( inputState.guessing==0 ) {
									cnsElem.isIncludeType=true;
								}
								break;
							}
							case ANY_KW:
							case BIT_KW:
							case BMP_STR_KW:
							case BOOLEAN_KW:
							case CHARACTER_KW:
							case CHOICE_KW:
							case EMBEDDED_KW:
							case ENUMERATED_KW:
							case ERROR_KW:
							case EXTERNAL_KW:
							case GENERALIZED_TIME_KW:
							case GENERAL_STR_KW:
							case GRAPHIC_STR_KW:
							case IA5_STR_KW:
							case INTEGER_KW:
							case ISO646_STR_KW:
							case NULL_KW:
							case NUMERIC_STR_KW:
							case OBJECT_KW:
							case OCTET_KW:
							case OPERATION_KW:
							case PRINTABLE_STR_KW:
							case REAL_KW:
							case RELATIVE_KW:
							case SEQUENCE_KW:
							case SET_KW:
							case TELETEX_STR_KW:
							case UNIVERSAL_STR_KW:
							case UTC_TIME_KW:
							case UTF8_STR_KW:
							case VIDEOTEX_STR_KW:
							case VISIBLE_STR_KW:
							case L_BRACKET:
							case UPPER:
							case LOWER:
							case 153:
							case T61_STR_KW:
							case OJECT_UPPER:
							case OJECT_LOWER:
							{
								break;
							}
							default:
							{
								throw new NoViableAltException(LT(1), getFilename());
							}
							}
							}
							typ=type();
							if ( inputState.guessing==0 ) {
								cnsElem.isTypeConstraint=true;cnsElem.type=typ;
							}
							}
						}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					}}}
				}
				catch (RecognitionException ex) {
					if (inputState.guessing==0) {
						reportError(ex);
						recover(ex,_tokenSet_68);
					} else {
					  throw ex;
					}
				}
				return cnsElem;
			}
			
	protected final AsnDefinedValue  defined_ObjRef() throws RecognitionException, TokenStreamException {
		AsnDefinedValue defval;
		
		Token  up = null;
		Token  lid = null;
		defval = new AsnDefinedValue();
		
		try {      // for error handling
			{
			{
			switch ( LA(1)) {
			case UPPER:
			{
				up = LT(1);
				match(UPPER);
				if ( inputState.guessing==0 ) {
					defval.moduleIdentifier = up.getText(); defval.setToken(up);
				}
				match(DOT);
				if ( inputState.guessing==0 ) {
					defval.isDotPresent=true;
				}
				break;
			}
			case AT_LOWER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			lid = LT(1);
			match(AT_LOWER);
			if ( inputState.guessing==0 ) {
				defval.name = lid.getText(); defval.setToken(lid);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
		return defval;
	}
	
	public final void value_range(
		ConstraintElements cnsElem
	) throws RecognitionException, TokenStreamException {
		
		AsnValue val;
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case FALSE_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case PLUS_INFINITY_KW:
			case TRUE_KW:
			case L_BRACE:
			case MINUS:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			{
				val=value();
				if ( inputState.guessing==0 ) {
					cnsElem.lEndValue=val;
				}
				break;
			}
			case MIN_KW:
			{
				match(MIN_KW);
				if ( inputState.guessing==0 ) {
					cnsElem.isMinKw=true;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case LESS:
			{
				match(LESS);
				if ( inputState.guessing==0 ) {
					cnsElem.isLEndLess=true;
				}
				break;
			}
			case DOTDOT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(DOTDOT);
			{
			switch ( LA(1)) {
			case LESS:
			{
				match(LESS);
				if ( inputState.guessing==0 ) {
					cnsElem.isUEndLess=true;
				}
				break;
			}
			case FALSE_KW:
			case MAX_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case PLUS_INFINITY_KW:
			case TRUE_KW:
			case L_BRACE:
			case MINUS:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case FALSE_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case PLUS_INFINITY_KW:
			case TRUE_KW:
			case L_BRACE:
			case MINUS:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			{
				val=value();
				if ( inputState.guessing==0 ) {
					cnsElem.uEndValue=val;
				}
				break;
			}
			case MAX_KW:
			{
				match(MAX_KW);
				if ( inputState.guessing==0 ) {
					cnsElem.isMaxKw=true;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_68);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void type_constraint_list(
		ConstraintElements cnsElem
	) throws RecognitionException, TokenStreamException {
		
		NamedConstraint namecns;
		
		try {      // for error handling
			namecns=named_constraint();
			if ( inputState.guessing==0 ) {
				cnsElem.typeConstraintList.add(namecns);
			}
			{
			_loop435:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					namecns=named_constraint();
					if ( inputState.guessing==0 ) {
						cnsElem.typeConstraintList.add(namecns);
					}
				}
				else {
					break _loop435;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
	}
	
	public final NamedConstraint  named_constraint() throws RecognitionException, TokenStreamException {
		NamedConstraint namecns;
		
		Token  lid = null;
		namecns = new NamedConstraint(); AsnConstraint cns;
		
		try {      // for error handling
			lid = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				namecns.name=lid.getText(); namecns.setToken(lid);
			}
			{
			switch ( LA(1)) {
			case L_PAREN:
			{
				cns=constraint();
				if ( inputState.guessing==0 ) {
					namecns.isConstraint=true;namecns.constraint=cns;
				}
				break;
			}
			case ABSENT_KW:
			case OPTIONAL_KW:
			case PRESENT_KW:
			case COMMA:
			case R_BRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case PRESENT_KW:
			{
				match(PRESENT_KW);
				if ( inputState.guessing==0 ) {
					namecns.isPresentKw=true;
				}
				break;
			}
			case ABSENT_KW:
			{
				match(ABSENT_KW);
				if ( inputState.guessing==0 ) {
					namecns.isAbsentKw=true;
				}
				break;
			}
			case OPTIONAL_KW:
			{
				match(OPTIONAL_KW);
				if ( inputState.guessing==0 ) {
					namecns.isOptionalKw=true;
				}
				break;
			}
			case COMMA:
			case R_BRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_55);
			} else {
			  throw ex;
			}
		}
		return namecns;
	}
	
	public final void choice_value(
		AsnValue value
	) throws RecognitionException, TokenStreamException {
		
		Token  lid = null;
		AsnChoiceValue chval = new AsnChoiceValue(); AsnValue val;
		
		try {      // for error handling
			{
			{
			lid = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				chval.name = lid.getText();
			}
			}
			{
			switch ( LA(1)) {
			case COLON:
			{
				match(COLON);
				break;
			}
			case FALSE_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case PLUS_INFINITY_KW:
			case TRUE_KW:
			case L_BRACE:
			case MINUS:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			val=value();
			if ( inputState.guessing==0 ) {
				chval.value = val;
			}
			}
			}
			if ( inputState.guessing==0 ) {
				value.chval = chval; value.setToken(lid);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_30);
			} else {
			  throw ex;
			}
		}
	}
	
	public final AsnSequenceValue  sequence_value() throws RecognitionException, TokenStreamException {
		AsnSequenceValue seqval;
		
		Token  t = null;
		AsnNamedValue nameval = new AsnNamedValue();
		seqval = new AsnSequenceValue();
		
		try {      // for error handling
			t = LT(1);
			match(L_BRACE);
			{
			{
			switch ( LA(1)) {
			case LOWER:
			{
				nameval=named_value();
				if ( inputState.guessing==0 ) {
					seqval.isValPresent=true;seqval.namedValueList.add(nameval);
				}
				break;
			}
			case COMMA:
			case R_BRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			_loop531:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					nameval=named_value();
					if ( inputState.guessing==0 ) {
						seqval.namedValueList.add(nameval);
					}
				}
				else {
					break _loop531;
				}
				
			} while (true);
			}
			}
			match(R_BRACE);
			if ( inputState.guessing==0 ) {
				seqval.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_30);
			} else {
			  throw ex;
			}
		}
		return seqval;
	}
	
	public final void sequenceof_value(
		AsnValue value
	) throws RecognitionException, TokenStreamException {
		
		Token  t = null;
		AsnValue val;value.seqOfVal = new AsnSequenceOfValue();
		
		try {      // for error handling
			t = LT(1);
			match(L_BRACE);
			{
			{
			switch ( LA(1)) {
			case FALSE_KW:
			case MINUS_INFINITY_KW:
			case NULL_KW:
			case PLUS_INFINITY_KW:
			case TRUE_KW:
			case L_BRACE:
			case MINUS:
			case NUMBER:
			case UPPER:
			case LOWER:
			case B_STRING:
			case H_STRING:
			case C_STRING:
			{
				val=value();
				if ( inputState.guessing==0 ) {
					value.seqOfVal.valueList.add(val);
				}
				break;
			}
			case COMMA:
			case R_BRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			_loop536:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					val=value();
					if ( inputState.guessing==0 ) {
						value.seqOfVal.valueList.add(val);
					}
				}
				else {
					break _loop536;
				}
				
			} while (true);
			}
			}
			match(R_BRACE);
			if ( inputState.guessing==0 ) {
				value.setToken(t);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_30);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void cstr_value(
		AsnValue value
	) throws RecognitionException, TokenStreamException {
		
		Token  h = null;
		Token  b = null;
		Token  t = null;
		AsnBitOrOctetStringValue bstrval = new AsnBitOrOctetStringValue();
		AsnCharacterStringValue cstrval = new AsnCharacterStringValue();
		AsnSequenceValue seqval;
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case H_STRING:
			{
				{
				h = LT(1);
				match(H_STRING);
				if ( inputState.guessing==0 ) {
					bstrval.isHString=true; bstrval.bhStr = h.getText(); value.setToken(h);
				}
				}
				break;
			}
			case B_STRING:
			{
				{
				b = LT(1);
				match(B_STRING);
				if ( inputState.guessing==0 ) {
					bstrval.isBString=true; bstrval.bhStr = b.getText(); value.setToken(b);
				}
				}
				break;
			}
			case L_BRACE:
			{
				{
				t = LT(1);
				match(L_BRACE);
				{
				boolean synPredMatched490 = false;
				if (((LA(1)==LOWER) && (LA(2)==COMMA||LA(2)==R_BRACE) && (_tokenSet_30.member(LA(3))))) {
					int _m490 = mark();
					synPredMatched490 = true;
					inputState.guessing++;
					try {
						{
						id_list(bstrval);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched490 = false;
					}
					rewind(_m490);
inputState.guessing--;
				}
				if ( synPredMatched490 ) {
					{
					id_list(bstrval);
					}
				}
				else {
					boolean synPredMatched493 = false;
					if (((_tokenSet_79.member(LA(1))) && (_tokenSet_80.member(LA(2))) && (_tokenSet_30.member(LA(3))))) {
						int _m493 = mark();
						synPredMatched493 = true;
						inputState.guessing++;
						try {
							{
							char_defs_list(cstrval);
							}
						}
						catch (RecognitionException pe) {
							synPredMatched493 = false;
						}
						rewind(_m493);
inputState.guessing--;
					}
					if ( synPredMatched493 ) {
						{
						char_defs_list(cstrval);
						}
					}
					else if ((LA(1)==MINUS||LA(1)==NUMBER)) {
						tuple_or_quad(cstrval);
					}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					match(R_BRACE);
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				if ( inputState.guessing==0 ) {
					value.cStrValue=cstrval;value.bStrValue=bstrval; value.setToken(t);
				}
			}
			catch (RecognitionException ex) {
				if (inputState.guessing==0) {
					reportError(ex);
					recover(ex,_tokenSet_30);
				} else {
				  throw ex;
				}
			}
		}
		
	public final void id_list(
		AsnBitOrOctetStringValue bstrval
	) throws RecognitionException, TokenStreamException {
		
		Token  ld = null;
		Token  ld1 = null;
		String s="";
		
		try {      // for error handling
			{
			ld = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				s = ld.getText(); bstrval.idlist.add(s); bstrval.setToken(ld);
			}
			}
			{
			_loop498:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					ld1 = LT(1);
					match(LOWER);
					if ( inputState.guessing==0 ) {
						s = ld1.getText();bstrval.idlist.add(s);
					}
				}
				else {
					break _loop498;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void char_defs_list(
		AsnCharacterStringValue cstrval
	) throws RecognitionException, TokenStreamException {
		
		CharDef a ;
		
		try {      // for error handling
			a=char_defs();
			if ( inputState.guessing==0 ) {
				cstrval.isCharDefList = true;cstrval.charDefsList.add(a);
			}
			{
			_loop502:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					{
					a=char_defs();
					if ( inputState.guessing==0 ) {
						cstrval.charDefsList.add(a);
					}
					}
				}
				else {
					break _loop502;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
	}
	
	public final void tuple_or_quad(
		AsnCharacterStringValue cstrval
	) throws RecognitionException, TokenStreamException {
		
		AsnSignedNumber n;
		
		try {      // for error handling
			{
			n=signed_number();
			if ( inputState.guessing==0 ) {
				cstrval.tupleQuad.add(n);
			}
			}
			match(COMMA);
			{
			n=signed_number();
			if ( inputState.guessing==0 ) {
				cstrval.tupleQuad.add(n);
			}
			}
			{
			switch ( LA(1)) {
			case R_BRACE:
			{
				{
				match(R_BRACE);
				if ( inputState.guessing==0 ) {
					cstrval.isTuple=true;
				}
				}
				break;
			}
			case COMMA:
			{
				{
				match(COMMA);
				{
				n=signed_number();
				if ( inputState.guessing==0 ) {
					cstrval.tupleQuad.add(n);
				}
				}
				match(COMMA);
				{
				n=signed_number();
				if ( inputState.guessing==0 ) {
					cstrval.tupleQuad.add(n);
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_38);
			} else {
			  throw ex;
			}
		}
	}
	
	public final CharDef  char_defs() throws RecognitionException, TokenStreamException {
		CharDef chardef;
		
		Token  c = null;
		Token  t = null;
		chardef = new CharDef();
		AsnSignedNumber n ; AsnDefinedValue defval;
		
		try {      // for error handling
			switch ( LA(1)) {
			case C_STRING:
			{
				{
				c = LT(1);
				match(C_STRING);
				if ( inputState.guessing==0 ) {
					chardef.isCString = true;chardef.cStr=c.getText(); chardef.setToken(c);
				}
				}
				break;
			}
			case L_BRACE:
			{
				{
				t = LT(1);
				match(L_BRACE);
				{
				n=signed_number();
				if ( inputState.guessing==0 ) {
					chardef.tupleQuad.add(n);
				}
				}
				match(COMMA);
				{
				n=signed_number();
				if ( inputState.guessing==0 ) {
					chardef.tupleQuad.add(n);
				}
				}
				{
				switch ( LA(1)) {
				case R_BRACE:
				{
					{
					match(R_BRACE);
					if ( inputState.guessing==0 ) {
						chardef.isTuple=true; chardef.setToken(t);
					}
					}
					break;
				}
				case COMMA:
				{
					{
					match(COMMA);
					{
					n=signed_number();
					if ( inputState.guessing==0 ) {
						chardef.tupleQuad.add(n);
					}
					}
					match(COMMA);
					{
					n=signed_number();
					if ( inputState.guessing==0 ) {
						chardef.tupleQuad.add(n);
					}
					}
					match(R_BRACE);
					if ( inputState.guessing==0 ) {
						chardef.isQuadruple=true;
					}
					}
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				}
				break;
			}
			case UPPER:
			case LOWER:
			{
				{
				defval=defined_value();
				if ( inputState.guessing==0 ) {
					chardef.defval=defval;
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_55);
			} else {
			  throw ex;
			}
		}
		return chardef;
	}
	
	public final AsnNamedValue  named_value() throws RecognitionException, TokenStreamException {
		AsnNamedValue nameval;
		
		Token  lid = null;
		nameval = new AsnNamedValue(); AsnValue val;
		
		try {      // for error handling
			{
			lid = LT(1);
			match(LOWER);
			if ( inputState.guessing==0 ) {
				nameval.name = lid.getText(); nameval.setToken(lid);
			}
			val=value();
			if ( inputState.guessing==0 ) {
				nameval.value = val; nameval.setToken(lid);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				recover(ex,_tokenSet_55);
			} else {
			  throw ex;
			}
		}
		return nameval;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"DOT",
		"DOTDOT",
		"\"ABSENT\"",
		"\"ABSTRACT-SYNTAX\"",
		"\"ALL\"",
		"\"ANY\"",
		"\"ARGUMENT\"",
		"\"APPLICATION\"",
		"\"AUTOMATIC\"",
		"\"BASEDNUM\"",
		"\"BEGIN\"",
		"\"BIT\"",
		"\"BMPString\"",
		"\"BOOLEAN\"",
		"\"BY\"",
		"\"CHARACTER\"",
		"\"CHOICE\"",
		"\"CLASS\"",
		"\"COMPONENTS\"",
		"\"COMPONENT\"",
		"\"CONSTRAINED\"",
		"\"DEFAULT\"",
		"\"DEFINED\"",
		"\"DEFINITIONS\"",
		"\"EMBEDDED\"",
		"\"END\"",
		"\"ENUMERATED\"",
		"\"ERROR\"",
		"\"ERRORS\"",
		"\"EXCEPT\"",
		"\"EXPLICIT\"",
		"\"EXPORTS\"",
		"\"EXTENSIBILITY\"",
		"\"EXTERNAL\"",
		"\"FALSE\"",
		"\"FROM\"",
		"\"GeneralizedTime\"",
		"\"GeneralString\"",
		"\"GraphicString\"",
		"\"IA5String\"",
		"\"IDENTIFIER\"",
		"\"IMPLICIT\"",
		"\"IMPLIED\"",
		"\"IMPORTS\"",
		"\"INCLUDES\"",
		"\"INSTANCE\"",
		"\"INTEGER\"",
		"\"INTERSECTION\"",
		"\"ISO646String\"",
		"\"LINKED\"",
		"\"MAX\"",
		"\"MINUSINFINITY\"",
		"\"MIN\"",
		"\"NULL\"",
		"\"NumericString\"",
		"\"ObjectDescriptor\"",
		"\"OBJECT\"",
		"\"OCTET\"",
		"\"OPERATION\"",
		"\"OF\"",
		"\"OID\"",
		"\"OPTIONAL\"",
		"\"PARAMETER\"",
		"\"PATTERN\"",
		"\"PDV\"",
		"\"PLUSINFINITY\"",
		"\"PRESENT\"",
		"\"PrintableString\"",
		"\"PRIVATE\"",
		"\"REAL\"",
		"\"RELATIVE\"",
		"\"RESULT\"",
		"\"SEQUENCE\"",
		"\"SET\"",
		"\"SIZE\"",
		"\"STRING\"",
		"\"SYNTAX\"",
		"\"TAGS\"",
		"\"TeletexString\"",
		"\"TRUE\"",
		"\"TYPE-IDENTIFIER\"",
		"\"UNION\"",
		"\"UNIQUE\"",
		"\"UNIVERSAL\"",
		"\"UniversalString\"",
		"\"UTCTime\"",
		"\"UTF8String\"",
		"\"VideotexString\"",
		"\"VisibleString\"",
		"\"WITH\"",
		"ASSIGN_OP",
		"BAR",
		"COLON",
		"COMMA",
		"COMMENT",
		"ELLIPSIS",
		"EXCLAMATION",
		"INTERSECTION",
		"LESS",
		"L_BRACE",
		"L_BRACKET",
		"L_PAREN",
		"MINUS",
		"PLUS",
		"R_BRACE",
		"R_BRACKET",
		"R_PAREN",
		"SEMI",
		"SINGLE_QUOTE",
		"QUOT",
		"WS",
		"VALID_SL_COMMENT_CHARS",
		"SL_COMMENT",
		"ML_COMMENT",
		"NUMBER",
		"UPPER",
		"LOWER",
		"IDENTIFIER_REST",
		"ALPHA_NUM",
		"AMP_UPPER",
		"AMP_LOWER",
		"AT_LOWER",
		"BDIG",
		"HDIG",
		"B_OR_H_STRING",
		"B_STRING",
		"H_STRING",
		"C_STRING",
		"\"BIND\"",
		"\"UNBIND\"",
		"\"APPLICATION-SERVICE-ELEMENT\"",
		"\"APPLICATION-CONTEXT\"",
		"\"EXTENSION\"",
		"\"EXTENSIONS\"",
		"\"EXTENSION-ATTRIBUTE\"",
		"\"TOKEN\"",
		"\"TOKEN-DATA\"",
		"\"SECURITY-CATEGORY\"",
		"\"PORT\"",
		"\"REFINE\"",
		"\"ABSTRACT-BIND\"",
		"\"ABSTRACT-UNBIND\"",
		"\"ABSTRACT-OPERATION\"",
		"\"ABSTRACT-ERROR\"",
		"\"ALGORITHM\"",
		"\"ENCRYPTED\"",
		"\"SIGNED\"",
		"\"SIGNATURE\"",
		"\"PROTECTED\"",
		"\"OBJECT-TYPE\"",
		"\"MACRO\"",
		"T61_STR_KW",
		"OJECT_UPPER",
		"OJECT_LOWER",
		"\"ACCESS\"",
		"\"STATUS\"",
		"\"DESCRIPTION\"",
		"\"REFERENCE\"",
		"\"INDEX\"",
		"\"DEFVAL\"",
		"ALL",
		"EXCEPT",
		"INCLUDES"
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { 2L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 2L, 36028797018963968L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = { 134217728L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = { 536870912L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = { 16L, 126100789566373888L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = { 16L, 126120580775673856L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = { 8546723101996188208L, 126338226618906272L, 138445586430L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	private static final long[] mk_tokenSet_7() {
		long[] data = { 8546723101996188192L, 126336027595650720L, 138445586430L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_7 = new BitSet(mk_tokenSet_7());
	private static final long[] mk_tokenSet_8() {
		long[] data = { 8546723101861970464L, 126336027595650720L, 138445586430L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_8 = new BitSet(mk_tokenSet_8());
	private static final long[] mk_tokenSet_9() {
		long[] data = { 0L, 126120580775673856L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_9 = new BitSet(mk_tokenSet_9());
	private static final long[] mk_tokenSet_10() {
		long[] data = { 16L, 126118381752418304L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_10 = new BitSet(mk_tokenSet_10());
	private static final long[] mk_tokenSet_11() {
		long[] data = { 0L, 126118381752418304L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_11 = new BitSet(mk_tokenSet_11());
	private static final long[] mk_tokenSet_12() {
		long[] data = { 8508442229614543360L, 108087491088889472L, 973078528L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_12 = new BitSet(mk_tokenSet_12());
	private static final long[] mk_tokenSet_13() {
		long[] data = { 140738025226240L, 108086391056891904L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_13 = new BitSet(mk_tokenSet_13());
	private static final long[] mk_tokenSet_14() {
		long[] data = { 5764607525181718528L, 108086391056891904L, 67108848L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_14 = new BitSet(mk_tokenSet_14());
	private static final long[] mk_tokenSet_15() {
		long[] data = { 536870912L, 108086391056891904L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_15 = new BitSet(mk_tokenSet_15());
	private static final long[] mk_tokenSet_16() {
		long[] data = { -9214347241051911152L, 90079415333144852L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_16 = new BitSet(mk_tokenSet_16());
	private static final long[] mk_tokenSet_17() {
		long[] data = { -536870928L, -1L, 549755813887L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_17 = new BitSet(mk_tokenSet_17());
	private static final long[] mk_tokenSet_18() {
		long[] data = { 549755813888L, 140737488355328L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_18 = new BitSet(mk_tokenSet_18());
	private static final long[] mk_tokenSet_19() {
		long[] data = { 5764607525181718544L, 108227128545247232L, 67108848L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_19 = new BitSet(mk_tokenSet_19());
	private static final long[] mk_tokenSet_20() {
		long[] data = { 550292684800L, 108086949402640384L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_20 = new BitSet(mk_tokenSet_20());
	private static final long[] mk_tokenSet_21() {
		long[] data = { 5764607525181718528L, 108227128545247232L, 67108848L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_21 = new BitSet(mk_tokenSet_21());
	private static final long[] mk_tokenSet_22() {
		long[] data = { 8508442229614543362L, 108105084348675712L, 1073741808L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_22 = new BitSet(mk_tokenSet_22());
	private static final long[] mk_tokenSet_23() {
		long[] data = { 549755813888L, 140746078289920L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_23 = new BitSet(mk_tokenSet_23());
	private static final long[] mk_tokenSet_24() {
		long[] data = { 549755813888L, 141295834103808L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_24 = new BitSet(mk_tokenSet_24());
	private static final long[] mk_tokenSet_25() {
		long[] data = { 8555730305445233152L, 126197219621355170L, 139485773838L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_25 = new BitSet(mk_tokenSet_25());
	private static final long[] mk_tokenSet_26() {
		long[] data = { 8546723101861970480L, 126195290107295392L, 138412032014L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_26 = new BitSet(mk_tokenSet_26());
	private static final long[] mk_tokenSet_27() {
		long[] data = { -577551596848968078L, 2432040538618068990L, 517510004750L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_27 = new BitSet(mk_tokenSet_27());
	private static final long[] mk_tokenSet_28() {
		long[] data = { 180144259972726784L, 126105741664190496L, 14L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_28 = new BitSet(mk_tokenSet_28());
	private static final long[] mk_tokenSet_29() {
		long[] data = { 8546723101861970480L, 126195294402262688L, 138412032014L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_29 = new BitSet(mk_tokenSet_29());
	private static final long[] mk_tokenSet_30() {
		long[] data = { 8546723101861970464L, 126195290107295392L, 138412032014L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_30 = new BitSet(mk_tokenSet_30());
	private static final long[] mk_tokenSet_31() {
		long[] data = { 180144259972726784L, 126123338145202208L, 14L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_31 = new BitSet(mk_tokenSet_31());
	private static final long[] mk_tokenSet_32() {
		long[] data = { 8616529445305156096L, 2432022483638711976L, 344570462222L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_32 = new BitSet(mk_tokenSet_32());
	private static final long[] mk_tokenSet_33() {
		long[] data = { -595565995350061520L, 2432040504258330558L, 483083157518L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_33 = new BitSet(mk_tokenSet_33());
	private static final long[] mk_tokenSet_34() {
		long[] data = { -14L, -1L, 549755813887L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_34 = new BitSet(mk_tokenSet_34());
	private static final long[] mk_tokenSet_35() {
		long[] data = { 292750468453564416L, 520355968L, 134217728L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_35 = new BitSet(mk_tokenSet_35());
	private static final long[] mk_tokenSet_36() {
		long[] data = { 268435456L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_36 = new BitSet(mk_tokenSet_36());
	private static final long[] mk_tokenSet_37() {
		long[] data = { -667641731409542592L, 126197219621355234L, 139485773838L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_37 = new BitSet(mk_tokenSet_37());
	private static final long[] mk_tokenSet_38() {
		long[] data = { 0L, 17592186044416L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_38 = new BitSet(mk_tokenSet_38());
	private static final long[] mk_tokenSet_39() {
		long[] data = { 8508477431166505472L, 108087491088889472L, 973078528L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_39 = new BitSet(mk_tokenSet_39());
	private static final long[] mk_tokenSet_40() {
		long[] data = { 0L, 90071992547409920L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_40 = new BitSet(mk_tokenSet_40());
	private static final long[] mk_tokenSet_41() {
		long[] data = { 0L, 35184372088832L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_41 = new BitSet(mk_tokenSet_41());
	private static final long[] mk_tokenSet_42() {
		long[] data = { -667624139156386288L, 126197494507765686L, 139485773838L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_42 = new BitSet(mk_tokenSet_42());
	private static final long[] mk_tokenSet_43() {
		long[] data = { 8508442229614543360L, 108228228577244800L, 973078528L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_43 = new BitSet(mk_tokenSet_43());
	private static final long[] mk_tokenSet_44() {
		long[] data = { 8544471301511414272L, 126124429587265184L, 973078542L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_44 = new BitSet(mk_tokenSet_44());
	private static final long[] mk_tokenSet_45() {
		long[] data = { -669875939540496880L, 126126912086867892L, 973078542L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_45 = new BitSet(mk_tokenSet_45());
	private static final long[] mk_tokenSet_46() {
		long[] data = { -595565995358188016L, 2432216460478513087L, 517442895886L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_46 = new BitSet(mk_tokenSet_46());
	private static final long[] mk_tokenSet_47() {
		long[] data = { 180144259972726784L, 126105737369223200L, 14L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_47 = new BitSet(mk_tokenSet_47());
	private static final long[] mk_tokenSet_48() {
		long[] data = { 8544471301511414288L, 126124442472167072L, 973078542L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_48 = new BitSet(mk_tokenSet_48());
	private static final long[] mk_tokenSet_49() {
		long[] data = { -667624139156386288L, 126197494507765686L, 173845512206L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_49 = new BitSet(mk_tokenSet_49());
	private static final long[] mk_tokenSet_50() {
		long[] data = { 8544471301511414272L, 126124438177199776L, 973078542L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_50 = new BitSet(mk_tokenSet_50());
	private static final long[] mk_tokenSet_51() {
		long[] data = { 8544471301511414272L, 126106837401220768L, 973078542L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_51 = new BitSet(mk_tokenSet_51());
	private static final long[] mk_tokenSet_52() {
		long[] data = { 2048L, 90071992555798784L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_52 = new BitSet(mk_tokenSet_52());
	private static final long[] mk_tokenSet_53() {
		long[] data = { -705905011403813360L, 126126912086343574L, 973078528L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_53 = new BitSet(mk_tokenSet_53());
	private static final long[] mk_tokenSet_54() {
		long[] data = { -9214347241018356720L, 90097015035382038L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_54 = new BitSet(mk_tokenSet_54());
	private static final long[] mk_tokenSet_55() {
		long[] data = { 0L, 17600775979008L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_55 = new BitSet(mk_tokenSet_55());
	private static final long[] mk_tokenSet_56() {
		long[] data = { -16L, -1L, 549755813887L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_56 = new BitSet(mk_tokenSet_56());
	private static final long[] mk_tokenSet_57() {
		long[] data = { -595583587615800768L, 2432040229371936490L, 483083157518L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_57 = new BitSet(mk_tokenSet_57());
	private static final long[] mk_tokenSet_58() {
		long[] data = { 0L, 70437463654400L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_58 = new BitSet(mk_tokenSet_58());
	private static final long[] mk_tokenSet_59() {
		long[] data = { -9214347241051911152L, 90079418554370324L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_59 = new BitSet(mk_tokenSet_59());
	private static final long[] mk_tokenSet_60() {
		long[] data = { 8616529445309612544L, 2432216036339906217L, 344570462222L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_60 = new BitSet(mk_tokenSet_60());
	private static final long[] mk_tokenSet_61() {
		long[] data = { 0L, 70368744177664L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_61 = new BitSet(mk_tokenSet_61());
	private static final long[] mk_tokenSet_62() {
		long[] data = { 0L, 70446053588992L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_62 = new BitSet(mk_tokenSet_62());
	private static final long[] mk_tokenSet_63() {
		long[] data = { 0L, 70448203169792L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_63 = new BitSet(mk_tokenSet_63());
	private static final long[] mk_tokenSet_64() {
		long[] data = { 8616529445305156096L, 2431952046175057576L, 275850985486L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_64 = new BitSet(mk_tokenSet_64());
	private static final long[] mk_tokenSet_65() {
		long[] data = { -597817795734172112L, 2432040294876577724L, 344570462222L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_65 = new BitSet(mk_tokenSet_65());
	private static final long[] mk_tokenSet_66() {
		long[] data = { -595565995920486864L, 2432040503184588732L, 482009415694L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_66 = new BitSet(mk_tokenSet_66());
	private static final long[] mk_tokenSet_67() {
		long[] data = { -577551596840317328L, 2432216460478513151L, 483083157518L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_67 = new BitSet(mk_tokenSet_67());
	private static final long[] mk_tokenSet_68() {
		long[] data = { 2251799813685248L, 70585642123264L, 137438953472L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_68 = new BitSet(mk_tokenSet_68());
	private static final long[] mk_tokenSet_69() {
		long[] data = { 182396059786412048L, 126193919492358176L, 137438953486L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_69 = new BitSet(mk_tokenSet_69());
	private static final long[] mk_tokenSet_70() {
		long[] data = { -595583587615800752L, 2432040263731674858L, 483083157518L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_70 = new BitSet(mk_tokenSet_70());
	private static final long[] mk_tokenSet_71() {
		long[] data = { 2251799813685264L, 70585642123264L, 137438953472L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_71 = new BitSet(mk_tokenSet_71());
	private static final long[] mk_tokenSet_72() {
		long[] data = { -595583587615800768L, 2432040263731674858L, 483083157518L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_72 = new BitSet(mk_tokenSet_72());
	private static final long[] mk_tokenSet_73() {
		long[] data = { 252201854010654720L, 126105737369223200L, 14L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_73 = new BitSet(mk_tokenSet_73());
	private static final long[] mk_tokenSet_74() {
		long[] data = { 180144259972726832L, 126123617318076448L, 14L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_74 = new BitSet(mk_tokenSet_74());
	private static final long[] mk_tokenSet_75() {
		long[] data = { 198158658482208816L, 126125816341332000L, 14L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_75 = new BitSet(mk_tokenSet_75());
	private static final long[] mk_tokenSet_76() {
		long[] data = { 8508442229614543360L, 108087491088889472L, 275850985472L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_76 = new BitSet(mk_tokenSet_76());
	private static final long[] mk_tokenSet_77() {
		long[] data = { -703653211623682544L, 126179896952487828L, 138412032000L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_77 = new BitSet(mk_tokenSet_77());
	private static final long[] mk_tokenSet_78() {
		long[] data = { -595565995358187952L, 2432216460478513151L, 483083157518L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_78 = new BitSet(mk_tokenSet_78());
	private static final long[] mk_tokenSet_79() {
		long[] data = { 0L, 108086940812705792L, 8L, 0L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_79 = new BitSet(mk_tokenSet_79());
	private static final long[] mk_tokenSet_80() {
		long[] data = { 16L, 18036397331972096L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_80 = new BitSet(mk_tokenSet_80());
	
	}
