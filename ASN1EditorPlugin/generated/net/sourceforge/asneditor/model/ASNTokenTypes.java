// $ANTLR : "ASN1.g" -> "ASNParser.java"$

/**
 * Copyright (c) 2005-2008 Bogdan Stanca. All rights reserved.
 * Based upon the grammar submitted by Vivek Gupta to antlr.org.
 *
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * THIS SOFTWARE IS PROVIDED &quot;AS-IS,&quot;
 * WHITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * In no event shall the author or authors be held liable
 * for any damages arising from the use of the software.
 *
 */
package net.sourceforge.asneditor.model;

import antlr.*;

import java.math.*;
import java.util.*;

import org.eclipse.core.resources.IResource;

import net.sourceforge.asneditor.util.HashableList;
import net.sourceforge.asneditor.model.manager.IModelHandler;

public interface ASNTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int DOT = 4;
	int DOTDOT = 5;
	int ABSENT_KW = 6;
	int ABSTRACT_SYNTAX_KW = 7;
	int ALL_KW = 8;
	int ANY_KW = 9;
	int ARGUMENT_KW = 10;
	int APPLICATION_KW = 11;
	int AUTOMATIC_KW = 12;
	int BASED_NUM_KW = 13;
	int BEGIN_KW = 14;
	int BIT_KW = 15;
	int BMP_STR_KW = 16;
	int BOOLEAN_KW = 17;
	int BY_KW = 18;
	int CHARACTER_KW = 19;
	int CHOICE_KW = 20;
	int CLASS_KW = 21;
	int COMPONENTS_KW = 22;
	int COMPONENT_KW = 23;
	int CONSTRAINED_KW = 24;
	int DEFAULT_KW = 25;
	int DEFINED_KW = 26;
	int DEFINITIONS_KW = 27;
	int EMBEDDED_KW = 28;
	int END_KW = 29;
	int ENUMERATED_KW = 30;
	int ERROR_KW = 31;
	int ERRORS_KW = 32;
	int EXCEPT_KW = 33;
	int EXPLICIT_KW = 34;
	int EXPORTS_KW = 35;
	int EXTENSIBILITY_KW = 36;
	int EXTERNAL_KW = 37;
	int FALSE_KW = 38;
	int FROM_KW = 39;
	int GENERALIZED_TIME_KW = 40;
	int GENERAL_STR_KW = 41;
	int GRAPHIC_STR_KW = 42;
	int IA5_STR_KW = 43;
	int IDENTIFIER_KW = 44;
	int IMPLICIT_KW = 45;
	int IMPLIED_KW = 46;
	int IMPORTS_KW = 47;
	int INCLUDES_KW = 48;
	int INSTANCE_KW = 49;
	int INTEGER_KW = 50;
	int INTERSECTION_KW = 51;
	int ISO646_STR_KW = 52;
	int LINKED_KW = 53;
	int MAX_KW = 54;
	int MINUS_INFINITY_KW = 55;
	int MIN_KW = 56;
	int NULL_KW = 57;
	int NUMERIC_STR_KW = 58;
	int OBJECT_DESCRIPTOR_KW = 59;
	int OBJECT_KW = 60;
	int OCTET_KW = 61;
	int OPERATION_KW = 62;
	int OF_KW = 63;
	int OID_KW = 64;
	int OPTIONAL_KW = 65;
	int PARAMETER_KW = 66;
	int PATTERN_KW = 67;
	int PDV_KW = 68;
	int PLUS_INFINITY_KW = 69;
	int PRESENT_KW = 70;
	int PRINTABLE_STR_KW = 71;
	int PRIVATE_KW = 72;
	int REAL_KW = 73;
	int RELATIVE_KW = 74;
	int RESULT_KW = 75;
	int SEQUENCE_KW = 76;
	int SET_KW = 77;
	int SIZE_KW = 78;
	int STRING_KW = 79;
	int SYNTAX_KW = 80;
	int TAGS_KW = 81;
	int TELETEX_STR_KW = 82;
	int TRUE_KW = 83;
	int TYPE_IDENTIFIER_KW = 84;
	int UNION_KW = 85;
	int UNIQUE_KW = 86;
	int UNIVERSAL_KW = 87;
	int UNIVERSAL_STR_KW = 88;
	int UTC_TIME_KW = 89;
	int UTF8_STR_KW = 90;
	int VIDEOTEX_STR_KW = 91;
	int VISIBLE_STR_KW = 92;
	int WITH_KW = 93;
	int ASSIGN_OP = 94;
	int BAR = 95;
	int COLON = 96;
	int COMMA = 97;
	int COMMENT = 98;
	int ELLIPSIS = 99;
	int EXCLAMATION = 100;
	int INTERSECTION = 101;
	int LESS = 102;
	int L_BRACE = 103;
	int L_BRACKET = 104;
	int L_PAREN = 105;
	int MINUS = 106;
	int PLUS = 107;
	int R_BRACE = 108;
	int R_BRACKET = 109;
	int R_PAREN = 110;
	int SEMI = 111;
	int SINGLE_QUOTE = 112;
	int QUOT = 113;
	int WS = 114;
	int VALID_SL_COMMENT_CHARS = 115;
	int SL_COMMENT = 116;
	int ML_COMMENT = 117;
	int NUMBER = 118;
	int UPPER = 119;
	int LOWER = 120;
	int IDENTIFIER_REST = 121;
	int ALPHA_NUM = 122;
	int AMP_UPPER = 123;
	int AMP_LOWER = 124;
	int AT_LOWER = 125;
	int BDIG = 126;
	int HDIG = 127;
	int B_OR_H_STRING = 128;
	int B_STRING = 129;
	int H_STRING = 130;
	int C_STRING = 131;
	int LITERAL_BIND = 132;
	int LITERAL_UNBIND = 133;
	// "APPLICATION-SERVICE-ELEMENT" = 134
	// "APPLICATION-CONTEXT" = 135
	int LITERAL_EXTENSION = 136;
	int LITERAL_EXTENSIONS = 137;
	// "EXTENSION-ATTRIBUTE" = 138
	int LITERAL_TOKEN = 139;
	// "TOKEN-DATA" = 140
	// "SECURITY-CATEGORY" = 141
	int LITERAL_PORT = 142;
	int LITERAL_REFINE = 143;
	// "ABSTRACT-BIND" = 144
	// "ABSTRACT-UNBIND" = 145
	// "ABSTRACT-OPERATION" = 146
	// "ABSTRACT-ERROR" = 147
	int LITERAL_ALGORITHM = 148;
	int LITERAL_ENCRYPTED = 149;
	int LITERAL_SIGNED = 150;
	int LITERAL_SIGNATURE = 151;
	int LITERAL_PROTECTED = 152;
	// "OBJECT-TYPE" = 153
	int LITERAL_MACRO = 154;
	int T61_STR_KW = 155;
	int OJECT_UPPER = 156;
	int OJECT_LOWER = 157;
	int LITERAL_ACCESS = 158;
	int LITERAL_STATUS = 159;
	int LITERAL_DESCRIPTION = 160;
	int LITERAL_REFERENCE = 161;
	int LITERAL_INDEX = 162;
	int LITERAL_DEFVAL = 163;
	int ALL = 164;
	int EXCEPT = 165;
	int INCLUDES = 166;
}
